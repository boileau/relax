# Installation

> Note: A full documentation can be found
[here](https://boileau.pages.math.unistra.fr/relax).


## Requirements

### Python side

You need the following Python version and modules:

- python 3.6
- numpy
- scipy
- f2py
- h5py
- tabulate, progressbar, pandas, blessings, cycler
- sphinx and some extensions

### Fortran side

Only a Fortran compiler supporting openmp is required (gfortran is fine).

## Installation

### Python prerequisites

Main required packages are provided by the Anaconda package that can be installed with [miniconda](https://conda.io/docs/install/quick.html#miniconda-quick-install-requirements).

### Building

	python setup.py build

In particular, this will build a python module from Fortran files using f2py.

### Installing in developer mode

When the python files from this directory are edited, the modifications are available when importing the module from anywhere on the system or user space.

#### System installation

PIP complements automatically the missing modules. From the project root directory, run `pip install` in editable mode:

	pip install -e .

#### User installation

For a user installation (no need for admin rights but ):

	pip install --user -e .

In this case, `$HOME/.local/bin` must be in `PATH` environment variable. Add this line to your `~/.bashrc` or `~/.profile`:

	export PATH=$HOME/.local/bin:$PATH

### Installing permanently

	python setup.py install
	python setup.py install_lib


## Compilation of the Fortran/Python modules

To compile the Fortran sources into a Python module:

	cd Relax
	make 	

## Documentation

### Source files

Documentation source files are located in `./Doc`

> **Note:** the `Doc/installation.rst` file should not be modified because it is created from `README.md` by the `make html` and `make latexpdf` commands.
Only `README.md` should be edited.

### Prerequisites

- Install graphviz and pandoc using a package manager.

- Install sphinx and its extensions:

```
pip install sphinx
pip install sphinx_rtd_theme
pip install sphinxcontrib-bibtex
pip install sphinxcontrib-inheritance
```

### Building the documentation

From the project root directory:

```
make -C Doc html
```

The html output is in `Doc/_build/html/index.html`

More compilation options with:

	make -C Doc
