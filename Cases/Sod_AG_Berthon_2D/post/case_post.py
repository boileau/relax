# -*- coding: utf-8 -*-
"""
Compute L2 error between simulation and analytical solution.
Warning: exclude expansion zone.
"""

import numpy as np
import sys
from Relax import data_process as dproc


# Default tolerance
tol = {'p_11': 9.8148926596929886,
       'p_12': 0.46300203466339879,
       'p_22': 1.6499507060086405,
       'u': 6.6229326137800992,
       'rho': 2.7511216042372042,
       'v': 1.1904471544346729}

varnames = 'rho', 'u', 'v', 'p_11', 'p_12', 'p_22'


def load_ref(filename):
    """Return a dict of reference data"""
    ref = {}
    path = "../Ref/" + filename
    ref_data = np.loadtxt(path)

    ref['x'] = ref_data[:, 0]/10.
    for i in range(len(varnames)):
        var = varnames[i]
        ref[var] = ref_data[:, i+1]

    return ref


def get_index(x_ref, x_sim):
    """return index in ref at x_sim position"""
    if x_sim <= x_ref[0]:  # Far-left region
        j = 0
    else:
        j = 1
        while x_ref[j] < x_sim:
            j += 1
            if j == len(x_ref):  # Far-right region
                j -= 1
                break
    return j


def build_err_ref(ref, sim):
    """
    return a dict of reference solution at simulation coordinates position.
    (Exclude expansion zone)
    """
    ref_err = {}

    # Remove expansion zone
    x_ref = np.concatenate((ref['x'][:2], ref['x'][-10:]))
    x_expans_start = ref['x'][1]
    x_expans_end = ref['x'][-10]
    for var in list(ref.keys()):
        ref_err[var] = np.copy(sim[var])  # initialize ref_err
        if var != 'x':
            # Remove expansion zone
            filtered_ref = np.concatenate((ref[var][:2], ref[var][-10:]))
            for i in range(len(sim['x'])):
                x_sim = sim['x'][i]
                # Attribute reference value at local sim['x'][i] position
                if x_sim < x_expans_start or x_sim > x_expans_end:
                    ref_err[var][i] = filtered_ref[get_index(x_ref, x_sim)]

    return ref_err


def get_sol_dict(f, ite='last', color=None, direction='x'):
    """load solution and return a dictionary of variables"""

    def transpose(V):
        for i in range(7):
            V[i] = V[i].transpose()

        V_tmp = np.copy(V)
        # Change velocities
        V[1:3, :, :] = V_tmp[2:0:-1, :, :]
        # Change energies
        V[3:6, :, :] = V_tmp[5:2:-1, :, :]

    t, V = dproc.load_snapshot(f, ite=ite)
    N = dproc.get_param(f, 'N')
    gamma = dproc.get_param(f, 'gamma')
#    with h5py.File(FILE) as f:
    casetype = f['param']['casetype'][0].decode('utf8')
    if casetype == 'SOD-AG-2D-x':
        sim = dproc.build_vardict(V[:, :, 0], N, gamma, varlist=varnames)
    elif casetype == 'SOD-AG-2D-y':
        transpose(V)
        sim = dproc.build_vardict(V[:, :, 0], N, gamma, varlist=varnames)
    else:
        sys.exit("Wrong casetype: {}".format(casetype))
    sim['t'] = t
    return sim


def load_sim(sol_path='solution.h5'):
    """Return a sim dict from relax solution file"""
    sim = {}
    with dproc.open_h5(sol_path) as f:
        sim['x'] = dproc.get_grid(f)
        sim.update(get_sol_dict(f))
    return sim


def compute(sol_path='solution.h5', verbose=True):

    error = {}
    # Load and plot last solution
    ref = load_ref('analytical_solution.dat')
    sim = load_sim()
    sim['x'] -= 0.5  # Shift x sim data
    ref_err = build_err_ref(ref, sim)

    for var in varnames:
        error[var] = dproc.L2_err(sim[var], ref_err[var])

    if verbose:
        do_plot = False
        if do_plot:
            ref['t'] = sim['t']
            dproc.plot_ref_sim(sim, ref, varnames=varnames)
        for k, v in list(error.items()):
            print(("Error({}) = {}".format(k, v)))

    return error


def get_error_status(tol, tol_type, verbose=True):
    """Compute error and return status"""
    error = compute(verbose=verbose)
    return dproc.check(error, tol, 'lower', verbose=verbose)
