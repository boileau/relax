# -*- coding: utf-8 -*-
"""
Compute relative error
Reference code is in: Cases/AG_TSM/Code_JL
"""

import h5py
import numpy as np
import sys
from Relax import data_process as dproc


tol = {'mtot': 4.7900445861076982e-06,
       'qdmtot': 2.3200586920838553e-07,
       'Etot': 0.00046854128885927157}


def compute(verbose=False):

    ref = {}
    sim = {}
    error = {}
    # Load reference data
    filename = "Ref/solution_analytique.dat"
    ref_data = np.loadtxt(filename)

    # Load simulation data
    f = h5py.File('solution.h5', 'r')  # Open .h5 solution file
    prefix = "probes/probe_0/"
    path = prefix + "sum_data"
    sim_data = f[path][()]
    path = prefix + "time"
    time = f[path][()]

    # Use only common data
    commonlen = min(len(ref_data), len(sim_data))
    ref_data = ref_data[:commonlen]
    sim_data = sim_data[:commonlen]
    sim['time'] = time[:commonlen]

    # Store reference data in a dict
    ref['time'] = ref_data[:, 0]
    ref['mtot'] = ref_data[:, 1]
    ref['qdmtot'] = ref_data[:, 2]
    ref['Etot'] = ref_data[:, 3]

    # Store simulation data in a dict
    sim['mtot'] = sim_data[:, 0]
    sim['qdmtot'] = sim_data[:, 1]
    sim['Etot'] = sim_data[:, 3]

    # Exit if time error is too high
    time_error = max(abs(time - ref['time']))
    if time_error > 1e-9:
        msg = "Time error is: {}".format(time_error)
        sys.exit(msg)

    error['mtot'] = dproc.inf_err(sim['mtot'], ref['mtot'],
                                  norm=sim['mtot'][0])

    # Only for x direction
    error['qdmtot'] = dproc.inf_err(sim['qdmtot'], ref['qdmtot'],
                                    norm=sim['qdmtot'][0])

    # Only for x direction
    error['Etot'] = dproc.inf_err(sim['Etot'], ref['Etot'],
                                  norm=sim['Etot'][0])

    if verbose:
        for k, v in error.items():
            print("Error({}) = {}".format(k, v))
        print(error)

    return error


def get_error_status(tol, tol_type, verbose=False):

    """Compute error and return a status and warning message"""
    error = compute(verbose=verbose)
    status, msg = dproc.check(error, tol, 'lower', verbose=verbose)
    return status, msg
