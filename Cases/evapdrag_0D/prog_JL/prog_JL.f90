program test
use TSM_aff_evap_drag

implicit none

double precision :: rpar(4)

double precision :: eps
double precision :: aire0,aire1,aire2,aire3

integer, parameter :: Nsec=25, Nt=20, Nvit=1, Nene=1
integer :: k,i,Nmax
double precision :: t, dt, tmax
double precision :: rmax,smax,rhol
double precision :: dS,S(0:Nsec)
double precision :: ug(Nvit),nk(nsec),mk(nsec),uk(Nvit,nsec),sigmak(Nene,nsec),cst
double precision :: nkp(nsec),mkp(nsec),ukp(Nvit,nsec),sigmakp(Nene,nsec)

double precision :: Sinta(Nsec),Sintb(Nsec)
double precision :: Sm52, Sm72

double precision :: masse_an_tot(Nt+1), qdm_an_tot(Nt+1), energie_an_tot(Nt+1)
double precision :: masse_num_tot(Nt+1), qdm_num_tot(Nt+1), energie_num_tot(Nt+1)
double precision :: u_an_moy(Nt+1), sigma_an_moy(Nt+1)
double precision :: u_num_moy(Nt+1), sigma_num_moy(Nt+1)

double precision :: u_an(Nt+1,nsec), sigma_an(Nt+1,nsec)
double precision :: u_num(Nt+1,nsec), sigma_num(Nt+1,nsec)
double precision :: masse_an(Nt+1,nsec),qdm_an(Nt+1,nsec),energie_an(Nt+1,nsec)
double precision :: masse_num(Nt+1,nsec),qdm_num(Nt+1,nsec),energie_num(Nt+1,nsec)
double precision :: masse_initiale, qdm_initiale, energie_initiale


ug(1) = 4.d0                   ! vitesse max
rhol = 634.239d0              ! densite du liquide (kg/m**3)
alpha_drag = .9680385e-6      ! alpha (m^2/s)
Rs = 1.99e-7                  ! coefficient d'evaporation Rs (m**2/s)

smax = 11.310d3
S_lim = smax
ratio_mn = rhol/(6.d0*sqrt(pi))
tmax = smax/(Rs*1.d12)
dt = tmax/dble(Nt)
dS = smax/dble(Nsec)
do k = 0, Nsec
   S(k) = dble(k)*dS
end do

rpar(1) = 0.d0                  ! temps t
rpar(2) = Rs*1.d12              ! Rs - changement d'unite : m**2/s -> micron**2/s
rpar(3) = alpha_drag*1.d12      ! alpha_drag - changement d'unite : m**2/s -> micron**2/s
rpar(4) = ug(1)                 ! ug

eps = 1.d-6                     ! parametre de convergence pour IntRomberg

100 format (300(1x,1pe22.15)) 

!Initialisation de nk,mk,uk,sigmak
do k = 0,Nsec-1
   call IntRomberg(get_nk_JL,S(k),S(k+1),eps,aire0,rpar)
   call IntRomberg(get_mk_JL,S(k),S(k+1),eps,aire1,rpar)
   call IntRomberg(get_mkuk_JL,S(k),S(k+1),eps,aire2,rpar)
   call IntRomberg(get_energie_JL,S(k),S(k+1),eps,aire3,rpar)
   nk(k+1) = 1.d-18*aire0 ! Changement d'unite !!!
   mk(k+1) = (1.d-18*ratio_mn)*aire1   ! Changement d'unite de ratio_mn
   uk(1,k+1) = aire2/aire1
   sigmak(1,k+1) = aire3/aire1-(aire2/aire1)**2.d0

   masse_num(1,k+1) = mk(k+1)
   qdm_num(1,k+1) = mk(k+1)*uk(1,k+1)
   energie_num(1,k+1) = mk(k+1)*(sigmak(1,k+1)+uk(1,k+1)**2.d0)
   u_num(1,k+1) = uk(1,k+1)
   sigma_num(1,k+1) = sigmak(1,k+1)
enddo

!Premier terme des grandeurs numeriques
masse_num_tot(1) = sum(masse_num(1,:))
qdm_num_tot(1) = sum(qdm_num(1,:))
energie_num_tot(1) = sum(energie_num(1,:))
u_num_moy(1) = qdm_num_tot(1)/masse_num_tot(1)
sigma_num_moy(1) = energie_num_tot(1)/masse_num_tot(1)-u_num_moy(1)**2.d0

!Ecriture de la solution analytique
open(70,file='Result/solution_analytique.dat',form='formatted')

do i=1,Nt+1                     ! Boucle temporelle

   write(*,*) 'pas de temps analytique', i, '/', Nt+1

   Nmax = nbre_sections_non_vides(smax,Nsec,rpar)

   do k = 0,Nmax-2

      call IntRomberg(get_mk_JL,S(k),S(k+1),eps,aire1,rpar)
      call IntRomberg(get_mkuk_JL,S(k),S(k+1),eps,aire2,rpar)
      call IntRomberg(get_energie_JL,S(k),S(k+1),eps,aire3,rpar)

      masse_an(i,k+1) = (1.d-18*ratio_mn)*aire1
      qdm_an(i,k+1) = (1.d-18*ratio_mn)*aire2
      energie_an(i,k+1) = (1.d-18*ratio_mn)*aire3

   enddo

   !Derniere section non vide : Nmax-1
   k = Nmax-1

   if (Nmax.ge.1) then
      if (abs(S(Nsec)-rpar(1)*rpar(2)-S(k)).lt.S(1)/1.d10) then
         aire1 = 0.d0
         aire2 = 0.d0
         aire3 = 0.d0
      else
         call IntRomberg(get_mk_JL,S(k),S(Nsec)-rpar(1)*rpar(2),eps,aire1,rpar)
         call IntRomberg(get_mkuk_JL,S(k),S(Nsec)-rpar(1)*rpar(2),eps,aire2,rpar)
         call IntRomberg(get_energie_JL,S(k),S(Nsec)-rpar(1)*rpar(2),eps,aire3,rpar)
      endif

      masse_an(i,k+1) = (1.d-18*ratio_mn)*aire1
      qdm_an(i,k+1) = (1.d-18*ratio_mn)*aire2
      energie_an(i,k+1) = (1.d-18*ratio_mn)*aire3

      do k=Nmax,Nsec-1
         masse_an(i,k+1) = 0.d0
         qdm_an(i,k+1) = 0.d0
         energie_an(i,k+1) = 0.d0
      enddo
   elseif (Nmax.eq.0) then
      masse_an(i,:) = 0.d0
      qdm_an(i,:) = 0.d0
      energie_an(i,:) = 0.d0
   else
      write(*,*) 'probleme grave'
   endif   

   !masse, qdm et energie totales au pas de temps i
   masse_an_tot(i) = sum(masse_an(i,:))
   qdm_an_tot(i) = sum(qdm_an(i,:))
   energie_an_tot(i) = sum(energie_an(i,:))

   !recuperation de u et sigma par section et moyens
   do k=1,Nsec
      if (masse_an(i,k).gt.0.d0) then
         u_an(i,k) = qdm_an(i,k)/masse_an(i,k)
         sigma_an(i,k) = energie_an(i,k)/masse_an(i,k)-u_an(i,k)**2.d0
      else
         u_an(i,k) = 0.d0
         sigma_an(i,k) = 0.d0
      endif

      if (masse_an_tot(i).gt.0.d0) then
         u_an_moy(i) = qdm_an_tot(i)/masse_an_tot(i)
         sigma_an_moy(i) = energie_an_tot(i)/masse_an_tot(i)-u_an_moy(i)**2.d0
      else
         u_an_moy(i) = 0.d0
         sigma_an_moy(i) = 0.d0
      endif
   enddo

   !Ecriture dans le fichier contenant la solution analytique
   ! t   masse_tot   qdm_tot   energie_tot   u_moy   sigma_moy   u_moy(verif)   sigma_moy(verif)
   write(70,100) dble(i-1)*dt,masse_an_tot(i),qdm_an_tot(i),energie_an_tot(i),u_an_moy(i),&
           sigma_an_moy(i),qdm_an_tot(i)/masse_an_tot(i),energie_an_tot(i)/masse_an_tot(i)-&
           (qdm_an_tot(i)/masse_an_tot(i))**2.d0

   !Passage au pas de temps suivant
   rpar(1)=rpar(1)+dt

enddo

close(70)

write(*,*) 'Ecriture des fichiers contenant les resultats section par section'

! Grace a ces fichiers, je trace les grandeurs conservatives en fonction de la surface des gouttes.
open(71,file='Result/solution_analytique_masse_par_section.dat',form='formatted')
open(72,file='Result/solution_analytique_qdm_par_section.dat',form='formatted')
open(73,file='Result/solution_analytique_energie_par_section.dat',form='formatted')

! Ecrire la masse, la qdm et l'energie a chaque pas de temps
do k=1,Nsec
   write(71,100) S(k-1), (masse_an(i,k),i=1,Nt+1)
   write(71,100) S(k), (masse_an(i,k),i=1,Nt+1)
   write(72,100) S(k-1), (qdm_an(i,k),i=1,Nt+1)
   write(72,100) S(k), (qdm_an(i,k),i=1,Nt+1)
   write(73,100) S(k-1), (energie_an(i,k),i=1,Nt+1)
   write(73,100) S(k), (energie_an(i,k),i=1,Nt+1)
enddo

write(71,100) S(nsec), (0.d0,i=1,Nt+1)
write(72,100) S(nsec), (0.d0,i=1,Nt+1)
write(73,100) S(nsec), (0.d0,i=1,Nt+1)

close(71)
close(72)
close(73)

! Ecrire les valeurs analytiques de u et sigma dans chaque section et a chaque pas de temps
open(74,file='Result/solution_analytique_u_par_section.dat',form='formatted')
open(75,file='Result/solution_analytique_sigma_par_section.dat',form='formatted')

do k=1,Nsec
   write(74,100) S(k-1), (u_an(i,k),i=1,Nt+1)
   write(74,100) S(k), (u_an(i,k),i=1,Nt+1)
   write(75,100) S(k-1), (sigma_an(i,k),i=1,Nt+1)
   write(75,100) S(k), (sigma_an(i,k),i=1,Nt+1)
enddo

write(74,100) S(nsec), (0.d0,i=1,Nt+1)
write(75,100) S(nsec), (0.d0,i=1,Nt+1)

close(74)
close(75)

!Sinta et Sintb ne servent a rien pour l'instant
!etre appelees par la sous-routine d'evaporation
do k = 1, Nsec
   Sm52=(S(k)**2.5d0-S(k-1)**2.5d0)/2.5d0
   Sm72=(S(k)**3.5d0-S(k-1)**3.5d0)/3.5d0
   Sinta(k) = S(k)*Sm52-Sm72
   Sintb(k) = Sm72-S(k-1)*Sm52
end do

t=0.d0

open(80,file='Result/solution_numerique.dat',form='formatted')

!Ecriture du premier pas de temps
! t   masse_tot   qdm_tot   energie_tot   u_moy   sigma_moy   u_moy(verif)   sigma_moy(verif)
write(80,100) t,masse_num_tot(1),qdm_num_tot(1),energie_num_tot(1),u_num_moy(1),sigma_num_moy(1)&
            ,qdm_num_tot(1)/masse_num_tot(1),energie_num_tot(1)/masse_num_tot(1)-&
           (qdm_num_tot(1)/masse_num_tot(1))**2.d0

do i=1,Nt
   write(*,*) 'pas de temps numerique', i, '/', Nt
   t=dble(i)*dt
   call evap_drag_aff(Nvit,Nene,nsec,S,Sinta,Sintb,dt,ug,nk,mk,uk,sigmak,nkp,mkp,ukp,sigmakp)
   nk = nkp
   mk = mkp
   uk = ukp
   sigmak = sigmakp

   !Ecriture des masse, qdm et energie section par section
   do k=1,Nsec
      masse_num(i+1,k) = mk(k)
      qdm_num(i+1,k) = mk(k)*uk(1,k)
      energie_num(i+1,k) = mk(k)*(uk(1,k)**2.d0+sigmak(1,k))
   enddo

   !Calcul des masse, qdm et energies totales
   masse_num_tot(i+1) = sum(masse_num(i+1,:))
   qdm_num_tot(i+1) = sum(qdm_num(i+1,:))
   energie_num_tot(i+1) = sum(energie_num(i+1,:))

   !Ecriture de u et sigma (section par section puis valeurs moyennes)
   do k=1,Nsec
      if (masse_num(i+1,k).gt.0.d0) then
         u_num(i+1,k) = qdm_num(i+1,k)/masse_num(i+1,k)
         sigma_num(i+1,k) = energie_num(i+1,k)/masse_num(i+1,k)-u_num(i+1,k)**2.d0
      else
         u_num(i+1,k) = 0.d0
         sigma_num(i+1,k) = 0.d0
      endif

      if (masse_num_tot(i+1).gt.0.d0) then
         u_num_moy(i+1) = qdm_num_tot(i+1)/masse_num_tot(i+1)
         sigma_num_moy(i+1) = energie_num_tot(i+1)/masse_num_tot(i+1)-u_num_moy(i+1)**2.d0
      else
         u_num_moy(i+1) = 0.d0
         sigma_num_moy(i+1) = 0.d0
      endif
   enddo

   !Ecriture de la masse, la qdm et l'energie obtenues avec le schema numerique
   ! t   masse_tot   qdm_tot   energie_tot   u_moy   sigma_moy   u_moy(verif)   sigma_moy(verif)

   write(80,100) t,masse_num_tot(i+1),qdm_num_tot(i+1),energie_num_tot(i+1),&
         u_num_moy(i+1),sigma_num_moy(i+1),&
         qdm_num_tot(i+1)/masse_num_tot(i+1),energie_num_tot(i+1)/masse_num_tot(i+1)-&
           (qdm_num_tot(i+1)/masse_num_tot(i+1))**2.d0

enddo
close(80)

open(81,file='Result/solution_numerique_masse_par_section.dat',form='formatted')
open(82,file='Result/solution_numerique_qdm_par_section.dat',form='formatted')
open(83,file='Result/solution_numerique_energie_par_section.dat',form='formatted')

do k=1,Nsec
   write(81,100) S(k-1), (masse_num(i,k),i=1,Nt+1)
   write(81,100) S(k), (masse_num(i,k),i=1,Nt+1)
   write(82,100) S(k-1), (qdm_num(i,k),i=1,Nt+1)
   write(82,100) S(k), (qdm_num(i,k),i=1,Nt+1)
   write(83,100) S(k-1), (energie_num(i,k),i=1,Nt+1)
   write(83,100) S(k), (energie_num(i,k),i=1,Nt+1)
enddo

close(81)
close(82)
close(83)

! Ecrire les valeurs numeriques de u et sigma dans chaque section et a chaque pas de temps
open(84,file='Result/solution_numerique_u_par_section.dat',form='formatted')
open(85,file='Result/solution_numerique_sigma_par_section.dat',form='formatted')

do k=1,Nsec
   write(84,100) S(k-1), (u_num(i,k),i=1,Nt+1)
   write(84,100) S(k), (u_num(i,k),i=1,Nt+1)
   write(85,100) S(k-1), (sigma_num(i,k),i=1,Nt+1)
   write(85,100) S(k), (sigma_num(i,k),i=1,Nt+1)
enddo

write(84,100) S(nsec), (0.d0,i=1,Nt+1)
write(85,100) S(nsec), (0.d0,i=1,Nt+1)

close(84)
close(85)

!Calcul de l'erreur sur la masse, la qdm et l'energie
open(unit=86,file='Result/erreur_quantites_conservatives.dat',form='formatted')

masse_initiale = sum(masse_an(1,:))
qdm_initiale = sum(qdm_an(1,:))
energie_initiale = sum(energie_an(1,:))

do i=1,Nt+1
   write(86,100) dble(i-1)*dt, abs((sum(masse_an(i,:))-sum(masse_num(i,:)))/masse_initiale),&
      abs((sum(qdm_an(i,:))-sum(qdm_num(i,:)))/qdm_initiale),&
      abs((sum(energie_an(i,:))-sum(energie_num(i,:)))/energie_initiale)
enddo
   
close(86)

open(unit=87,file='Result/erreur_u_et_sigma.dat',form='formatted')
! A ecrire mais pour l'instant u et sigma font tellement n'importe quoi que ca ne sert a rien
close(87)

!Comparaison de la masse, la qdm et l'energie obtenues analytiquement
!a celles obtenues avec le schema numerique.

!Creation d'un fichier pour trouver le pb sur u et sigma
! t   masse_tot   qdm_tot   energie_tot   u_moy   energie_tot/masse_tot   sigma_moy (valeurs analytiques)
! t   masse_tot   qdm_tot   energie_tot   u_moy   energie_tot/masse_tot   sigma_moy (valeurs numeriques)
open(unit=88,file='Result/debug.dat',form='formatted')
do i=1,Nt+1
   write(88,100) dble(i-1)*dt, masse_an_tot(i), qdm_an_tot(i), energie_an_tot(i),&
        qdm_an_tot(i)/masse_an_tot(i),energie_an_tot(i)/masse_an_tot(i),&
        energie_an_tot(i)/masse_an_tot(i)-(qdm_an_tot(i)/masse_an_tot(i))**2.d0
   write(88,100) dble(i-1)*dt, masse_num_tot(i), qdm_num_tot(i), energie_num_tot(i),&
        qdm_num_tot(i)/masse_num_tot(i),energie_num_tot(i)/masse_num_tot(i),&
        energie_num_tot(i)/masse_num_tot(i)-(qdm_num_tot(i)/masse_num_tot(i))**2.d0
enddo
close(88)

!Trace des courbes
call system('cd Result ; gnuplot ../trace.gnu')

contains

function nbre_sections_non_vides(S_lim,Nsec,rpar)
   double precision, intent(in) :: rpar(*),S_lim
   integer, intent(in) :: Nsec
   integer :: nbre_sections_non_vides

   nbre_sections_non_vides = Nsec-floor(rpar(1)*rpar(2)*Nsec/S_lim)
   return
end function

function get_nk_JL(S,rpar)
   double precision, intent(in) :: S,rpar(*)
   double precision :: get_nk_JL

   get_nk_JL = phi(S)

   return
end function

function get_mk_JL(S,rpar)
   double precision, intent(in) :: S,rpar(*)
   double precision :: Rs,t
   double precision :: get_mk_JL

   t=rpar(1)
   Rs=rpar(2)
   get_mk_JL=S**1.5d0*phi(S+Rs*t)

   return
end function

function get_mkuk_JL(S,rpar)
   double precision, intent(in) :: S,rpar(*)
   double precision :: Rs,t,ug,alpha_drag
   double precision :: get_mkuk_JL

   t=rpar(1)
   Rs=rpar(2)
   alpha_drag=rpar(3)
   ug=rpar(4)

   if (S.eq.0) then
      get_mkuk_JL=0.d0
   else
      get_mkuk_JL=S**1.5d0*phi(S+Rs*t)*(ug+(1.d0+Rs*t/S)**(-alpha_drag/Rs)*u_moy_sans_ug(S+Rs*t))
   end if

   return
end function

function get_energie_JL(S,rpar)
   double precision, intent(in) :: S,rpar(*)
   double precision :: Rs,t,ug,alpha_drag
   double precision :: get_energie_JL

   t=rpar(1)
   Rs=rpar(2)
   alpha_drag=rpar(3)
   ug=rpar(4)

   if (S.eq.0) then
      get_energie_JL=0.d0
   else
      get_energie_JL=S**1.5d0*phi(S+Rs*t)*(ug**2.d0+2.d0*ug*(1.d0+Rs*t/S)**(-alpha_drag/Rs)*&
      u_moy_sans_ug(S+Rs*t)+(1.d0+Rs*t/S)**(-2.d0*alpha_drag/Rs)*(sigma(S+Rs*t)+&
      u_moy_sans_ug(S+Rs*t)**2.d0))
   end if

   return
end function

end
