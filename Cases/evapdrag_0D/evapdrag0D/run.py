"""
Evaporation and drag case: J. Lagarde PdS2015
To postprocess, run:
./plot_sol.py --sol solution.h5 --var rho --probe probe_0 --ascii
"""

casetype = "evap&drag0D"   # Case type
scheme = 'relax-ag-o1'   # Scheme name
CFL = 0.5               # CFL number
tf = 0.05               # simulation final time
maxite = 0              # simulation final time (O for time-limited)
Lx = 0.05               # Domain length
N = 5                   # Number of points
nsect = 25              # number of sections
Smax = 11310.           # Max size [micro-m^2]
dt_fixed = 0.00284170854271  # Constant time step = Smax/(Rs*1.d12)/N_ite
dt_store = 0.0          # Storage time interval
dt_plot = 0.0           # Plotting time interval
is2D = False            # 2D test case
isSecST = False         # ST for each system?
isAllST = True          # ST coupling all systems?
miniplot = True
# Add probes:
probe = []
probe.append({'position': 0., 'comment': 'First point'})
# probe.append({'position': (0.5), 'comment': 'Second point'})

# Tolerance error:
err_tol = {'mtot': 4.77590680543e-06,
           'qdmtot': 0.00010077443375550942,
           'Etot': 0.00018475722261490621}

