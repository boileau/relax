"""
2D test case corresponding to Fig. 5c of Boileau et al. SISC2015 paper
:cite:`Boileau:2015`.
"""

import math

casetype = "Figure5"      # Case type
scheme = "relax-iso-o2"   # Scheme name
CFL = 0.5                 # CFL number
tf = 0.33                 # simulation final time
maxite = 0                # simulation final time
Lx = 1.0                  # Domain length
N = 200                   # Number of points
nsect = 0                 # number of sections
dt_store = 0.0            # Storage time interval
dt_plot = 0.0             # Plotting time interval
isSecST = True            # ST?
stokes = 0.9/(8*math.pi)  # Stokes number
miniplot = True
err_tol = {'rho': 1e-14, 'u': 1e-15, 'v': 1e-15, 'e_11': 1e-14, 'p_11': 1e-15, 'nk': 1e-15}  # Error tolerance
tol_type = 'lower'

