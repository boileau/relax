  
    do j=1,Ny
            
            
      ! LEFT INJECTION
      Ninj=0
      Uinj=0
      Vinj=0
      sigmainj=0.d0
      if ( (Ycell(1,j).gt.0.4).and.(Ycell(1,j).lt.0.6) ) then
        Ninj=1
        Uinj=0.2
        Vinj=0
        sigmainj=0.d0
      elseif ( (Ycell(1,j).gt.1.4).and.(Ycell(1,j).lt.1.6) ) then
        Ninj=1
        Uinj=0.2
        Vinj=0
        sigmainj=0.d0
      endif
      mom(1:10)=moments_monogauss( Ninj, Uinj, Vinj, sigmainj )
      M0(:,1,j) = mom(1:6)
      M0(:,2,j) = mom(1:6)
      ! RIGHT INJECTION   
      Ninj=0
      Uinj=0
      Vinj=0
      sigmainj=0.d0
      mom(1:10)=moments_monogauss( Ninj, Uinj, Vinj, sigmainj )
      M0(:,Nx,j) = mom(1:6)
      M0(:,Nx-1,j) = mom(1:6)    
    end do

    do i=1,Nx
      ! DOWN INJECTION   
      Ninj=0
      Uinj=0
      Vinj=0
      sigmainj=0.d0      
      mom(1:10)=moments_monogauss( Ninj, Uinj, Vinj, sigmainj )
      M0(:,i,1) = mom(1:6)
      M0(:,i,2) = mom(1:6)
      ! UP INJECTION   
      Ninj=0
      Uinj=0
      Vinj=0
      sigmainj=0.d0
      mom(1:10)=moments_monogauss( Ninj, Uinj, Vinj, sigmainj )
      M0(:,i,Ny-1) = mom(1:6)
      M0(:,i,Ny) = mom(1:6)
    end do 