module flux_erf

implicit none
contains

!****************************************************!!
!!  MOMENTS INCOMPLETS EN U DE LA GAUSSIENNE 2D !!   
!!****************************************************!!
subroutine moment_sigma10_U_plusminus( w, u, v, sig, momp, momm )
!
! CAS sigma12 = 0
!
! calcule les 10 moments d'une gaussienne 
! avec u variant
!     entre z et plus l'infini  pour momp
!     entre moins l'infini et z pour momm
! et v variant sur R entier
! sigma=(s1,s12,s2) où s1 est la variance selon U, s2 la variance selon V et s12 la covariance
!
![1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5]  
![0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 0 1 2 3 4 0 1 2 3 4 0 1 2 3 4 0 1 2 3 4]  
![0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 2 2 2 2 2 3 3 3 3 3 4 4 4 4 4 5 5 5 5 5]  
!
   real*8, intent(in) :: w, u, v, sig(3)
   real*8 :: momp(10),momm(10)

   integer :: i1(10), j1(10),i ,j ,k, no, alpha
   real*8 :: s1, s12, s2, x, integp(0:4),integm(0:4), mom(0:4), sumpk(0:4), summk(0:4)
   real*8 :: C(0:9,0:9),s2cond,vcond

! coefficients binomiaux
   data C(0,0:9)/1.d0, 0.d0, 0.d0,  0.d0,  0.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
   data C(1,0:9)/1.d0, 1.d0, 0.d0,  0.d0,  0.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
   data C(2,0:9)/1.d0, 2.d0, 1.d0,  0.d0,  0.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
   data C(3,0:9)/1.d0, 3.d0, 3.d0,  1.d0,  0.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
   data C(4,0:9)/1.d0, 4.d0, 6.d0,  4.d0,  1.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
   data C(5,0:9)/1.d0, 5.d0, 10.d0, 10.d0, 5.d0,   1.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
   data C(6,0:9)/1.d0, 6.d0, 15.d0, 20.d0, 15.d0,  6.d0,   1.d0,  0.d0,  0.d0, 0.d0 /
   data C(7,0:9)/1.d0, 7.d0, 21.d0, 35.d0, 35.d0,  21.d0,  7.d0,  1.d0,  0.d0, 0.d0 /
   data C(8,0:9)/1.d0, 8.d0, 28.d0, 56.d0, 70.d0,  56.d0,  28.d0, 8.d0,  1.d0, 0.d0 /
   data C(9,0:9)/1.d0, 9.d0, 36.d0, 84.d0, 126.d0, 126.d0, 84.d0, 36.d0, 9.d0, 1.d0 /

! coefficients des moments
   i1 = (/0, 1, 0, 2, 1, 0, 3, 2, 1, 0 /)
   j1 = (/0, 0, 1, 0, 1, 2, 0, 1, 2, 3 /)


! matrice de covariance
   s1 = sig(1)
   s12 = sig(2)
   s2 = sig(3)

! initialisation, calcul des quantites intermediaires
   momp = 0.d0
   momm = 0.d0


      x = -u/(dsqrt(s1)+1.d-16)
      call moments_incomplets_gaussienne1D(4,x,integp(:),integm(:))
      call moments_gaussienne1D(4,v,s2,mom(:))

! calcul des flux
   do i= 0, 4
         sumpk(i) = 0.d0
         summk(i) = 0.d0
         do k = 0, i
            sumpk(i) = sumpk(i) + C(i,k)*u**(i-k)*dsqrt(s1)**k*integp(k)
            summk(i) = summk(i) + C(i,k)*u**(i-k)*dsqrt(s1)**k*integm(k)
         end do
   end do
      
      
      
      
     s1=s1+1.d-16
     
     s2cond=s2-s12**2/s1

    momp(1)=sumpk(0)
    momm(1)=summk(0)
    
    momp(2)=sumpk(1)
    momm(2)=summk(1)

    momp(3)=sumpk(0)*v  + s12/s1*( sumpk(1) -sumpk(0)*u  )!sumpk(0)*mom(1)
    momm(3)=summk(0)*v  + s12/s1*( summk(1) -summk(0)*u  )!summk(0)*mom(1)
    
    momp(4)=sumpk(2)
    momm(4)=summk(2)
    
    momp(5)=sumpk(1)*v  + s12/s1*( sumpk(2) -sumpk(1)*u  )
    momm(5)=summk(1)*v  + s12/s1*( summk(2) -summk(1)*u  )
    
    momp(6)=sumpk(0)*(v**2+s2cond)  +  2.d0*v*s12/s1*( sumpk(1)-u*sumpk(0) )&
                                     + (s12/s1)**2*( sumpk(2)-2*u*sumpk(1)+sumpk(0)*u**2  )
    momm(6)=summk(0)*(v**2+s2cond)  +  2.d0*v*s12/s1*( summk(1)-u*summk(0) )&
                                     + (s12/s1)**2*( summk(2)-2*u*summk(1)+summk(0)*u**2  )
    
    momp(7)=sumpk(3)
    momm(7)=summk(3)
    
    momp(8)=sumpk(2)*v  + s12/s1*( sumpk(3) -sumpk(2)*u  )
    momm(8)=summk(2)*v  + s12/s1*( summk(3) -summk(2)*u  )
    
    momp(9)=sumpk(1)*(v**2+s2cond)  +  2.d0*v*s12/s1*( sumpk(2)-u*sumpk(1) )&
                                     + (s12/s1)**2*( sumpk(3)-2*u*sumpk(2)+sumpk(1)*u**2  )
    momm(9)=summk(1)*(v**2+s2cond)  +  2.d0*v*s12/s1*( summk(2)-u*summk(1) )&
                                     + (s12/s1)**2*( summk(3)-2*u*summk(2)+summk(1)*u**2  )
    
    momp(10)=sumpk(0)*(v**3+3.d0*v*s2cond)  +  3.d0*s12/s1*(v**2+s2cond)*( sumpk(1)-u*sumpk(0) )&
                                            +  3.d0*v*(s12/s1)**2*( sumpk(2)-2*u*sumpk(1)+sumpk(0)*u**2 )&
                                             + (s12/s1)**3*( sumpk(3)-3*u*sumpk(2)+3*sumpk(1)*u**2-sumpk(0)*u**3  )
                                     
                                     
    momp(10)=summk(0)*(v**3+3.d0*v*s2cond)  +  3.d0*s12/s1*(v**2+s2cond)*( summk(1)-u*summk(0) )&
                                            +  3.d0*v*(s12/s1)**2*( summk(2)-2*u*summk(1)+summk(0)*u**2 )&
                                             + (s12/s1)**3*( summk(3)-3*u*summk(2)+3*summk(1)*u**2-summk(0)*u**3  )
    
    momp=momp*w
    momm=momm*w
    
end subroutine



!****************************************************!!
!!  MOMENTS INCOMPLETS EN U DE LA GAUSSIENNE 2D !!   
!!****************************************************!!
subroutine moment_sigma10_V_plusminus( w, u, v, sig, momp, momm )
!
! CAS sigma12 = 0
!
! calcule les 10 moments d'une gaussienne 
! avec u variant
!     entre z et plus l'infini  pour momp
!     entre moins l'infini et z pour momm
! et v variant sur R entier
! sigma=(s1,s12,s2) où s1 est la variance selon U, s2 la variance selon V et s12 la covariance
!
![1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5]  
![0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 0 1 2 3 4 0 1 2 3 4 0 1 2 3 4 0 1 2 3 4]  
![0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 2 2 2 2 2 3 3 3 3 3 4 4 4 4 4 5 5 5 5 5]  
!
   real*8, intent(in) :: w, u, v, sig(3)
   real*8 :: momp(10),momm(10)

   integer :: i1(10), j1(10),i ,j ,k, no, alpha
   real*8 :: s1, s12, s2, x, integp(0:4),integm(0:4), mom(0:4), sumpk(0:4), summk(0:4)
   real*8 :: C(0:9,0:9),s1cond,ucond

! coefficients binomiaux
   data C(0,0:9)/1.d0, 0.d0, 0.d0,  0.d0,  0.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
   data C(1,0:9)/1.d0, 1.d0, 0.d0,  0.d0,  0.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
   data C(2,0:9)/1.d0, 2.d0, 1.d0,  0.d0,  0.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
   data C(3,0:9)/1.d0, 3.d0, 3.d0,  1.d0,  0.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
   data C(4,0:9)/1.d0, 4.d0, 6.d0,  4.d0,  1.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
   data C(5,0:9)/1.d0, 5.d0, 10.d0, 10.d0, 5.d0,   1.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
   data C(6,0:9)/1.d0, 6.d0, 15.d0, 20.d0, 15.d0,  6.d0,   1.d0,  0.d0,  0.d0, 0.d0 /
   data C(7,0:9)/1.d0, 7.d0, 21.d0, 35.d0, 35.d0,  21.d0,  7.d0,  1.d0,  0.d0, 0.d0 /
   data C(8,0:9)/1.d0, 8.d0, 28.d0, 56.d0, 70.d0,  56.d0,  28.d0, 8.d0,  1.d0, 0.d0 /
   data C(9,0:9)/1.d0, 9.d0, 36.d0, 84.d0, 126.d0, 126.d0, 84.d0, 36.d0, 9.d0, 1.d0 /

! coefficients des moments
   i1 = (/0, 1, 0, 2, 1, 0, 3, 2, 1, 0 /)
   j1 = (/0, 0, 1, 0, 1, 2, 0, 1, 2, 3 /)


! matrice de covariance
   s1 = sig(1)
   s12 = sig(2)
   s2 = sig(3)

! initialisation, calcul des quantites intermediaires
   momp = 0.d0
   momm = 0.d0


      x = -v/(dsqrt(s2)+1.d-16)
      call moments_incomplets_gaussienne1D(4,x,integp(:),integm(:))
      call moments_gaussienne1D(4,u,s1,mom(:))

! calcul des flux
   do i= 0, 4
         sumpk(i) = 0.d0
         summk(i) = 0.d0
         do k = 0, i
            sumpk(i) = sumpk(i) + C(i,k)*v**(i-k)*dsqrt(s2)**k*integp(k)
            summk(i) = summk(i) + C(i,k)*v**(i-k)*dsqrt(s2)**k*integm(k)
         end do
   end do
      
      
      
       s2=s2+1.d-16
      
     s1cond=s1-s12**2/s2

    momp(1)=sumpk(0)
    momm(1)=summk(0)
    
    momp(2)=sumpk(0)*u  + s12/s2*( sumpk(1) -sumpk(0)*v  )
    momm(2)=summk(0)*u  + s12/s2*( summk(1) -summk(0)*v  )
    
    momp(3)=sumpk(1)
    momm(3)=summk(1)
    
    momp(4)=sumpk(0)*(u**2+s1cond)  +  2.d0*u*s12/s2*( sumpk(1)-v*sumpk(0) )&
                                     + (s12/s2)**2*( sumpk(2)-2*v*sumpk(1)+sumpk(0)*v**2  )
    momm(4)=summk(0)*(u**2+s1cond)  +  2.d0*u*s12/s2*( summk(1)-v*summk(0) )&
                                     + (s12/s2)**2*( summk(2)-2*v*summk(1)+summk(0)*v**2  )
    
    
    momp(5)=sumpk(1)*u  + s12/s2*( sumpk(2) -sumpk(1)*v  )
    momm(5)=summk(1)*u  + s12/s2*( summk(2) -summk(1)*v  )
    
    momp(6)=sumpk(2)
    momm(6)=summk(2)
    
    momp(7)=sumpk(0)*(u**3+3.d0*u*s1cond)  +  3.d0*s12/s2*(u**2+s1cond)*( sumpk(1)-v*sumpk(0) )&
                                            +  3.d0*u*(s12/s2)**2*( sumpk(2)-2*v*sumpk(1)+sumpk(0)*v**2 )&
                                             + (s12/s2)**3*( sumpk(3)-3*v*sumpk(2)+3*sumpk(1)*v**2-sumpk(0)*v**3  )
                                     
                                     
    momp(7)=summk(0)*(u**3+3.d0*u*s1cond)  +  3.d0*s12/s2*(u**2+s1cond)*( summk(1)-v*summk(0) )&
                                            +  3.d0*u*(s12/s2)**2*( summk(2)-2*v*summk(1)+summk(0)*v**2 )&
                                             + (s12/s2)**3*( summk(3)-3*v*summk(2)+3*summk(1)*v**2-summk(0)*v**3  )
    
    
    
    
    momp(8)=sumpk(1)*(u**2+s1cond)  +  2.d0*u*s12/s2*( sumpk(2)-v*sumpk(1) )&
                                     + (s12/s2)**2*(sumpk(3)-2*v*sumpk(2)+sumpk(1)*v**2  )
    momm(8)=summk(1)*(u**2+s1cond)  +  2.d0*u*s12/s2*( summk(2)-v*summk(1) )&
                                     + (s12/s2)**2*(summk(3)-2*v*summk(2)+summk(1)*v**2  )
    
    momp(9)=sumpk(2)*u  + s12/s2*( sumpk(3) -sumpk(2)*v  )
    momm(9)=summk(2)*u  + s12/s2*( summk(3) -summk(2)*v  )
    
    momp(10)=sumpk(3)
    momm(10)=summk(3)
    
    momp=momp*w
    momm=momm*w
    

end subroutine




!!!****************************************************!!
!!!  MOMENTS INCOMPLETS EN U DE LA MULTI-GAUSSIENNE 2D !!   
!!!****************************************************!!
!subroutine moment_sigma48_U_decor( w, u, v, sig, momp, momm )
!!
!! CAS sigma12 = 0
!!
!! calcule les 48 moments d'une sommme de 4 gaussiennes en (u(i),v(i)) avec des poids w(i)
!! avec u variant
!!     entre z et plus l'infini  pour momp
!!     entre moins l'infini et z pour momm
!! et v variant sur R entier
!! sigma=(s1,s21,s22) où s1 est la variance selon U et s21 et s22 les variances selon V pour les directions conditionnées
!!
!![1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5]  
!![0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 0 1 2 3 4 0 1 2 3 4 0 1 2 3 4 0 1 2 3 4]  
!![0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 2 2 2 2 2 3 3 3 3 3 4 4 4 4 4 5 5 5 5 5]  
!!
!   real*8, intent(in) :: w, u, v, sig(3)
!   real*8 :: momp(10),momm(10)
!
!   integer :: i1(48), j1(48),i ,j ,k, no, alpha
!   real*8 :: s1, s21, s22, x, integp(0:7,4),integm(0:7,4), mom(0:7,4), sumpk, summk
!   real*8 :: C(0:9,0:9)
!
!! coefficients binomiaux
!   data C(0,0:9)/1.d0, 0.d0, 0.d0,  0.d0,  0.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
!   data C(1,0:9)/1.d0, 1.d0, 0.d0,  0.d0,  0.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
!   data C(2,0:9)/1.d0, 2.d0, 1.d0,  0.d0,  0.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
!   data C(3,0:9)/1.d0, 3.d0, 3.d0,  1.d0,  0.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
!   data C(4,0:9)/1.d0, 4.d0, 6.d0,  4.d0,  1.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
!   data C(5,0:9)/1.d0, 5.d0, 10.d0, 10.d0, 5.d0,   1.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
!   data C(6,0:9)/1.d0, 6.d0, 15.d0, 20.d0, 15.d0,  6.d0,   1.d0,  0.d0,  0.d0, 0.d0 /
!   data C(7,0:9)/1.d0, 7.d0, 21.d0, 35.d0, 35.d0,  21.d0,  7.d0,  1.d0,  0.d0, 0.d0 /
!   data C(8,0:9)/1.d0, 8.d0, 28.d0, 56.d0, 70.d0,  56.d0,  28.d0, 8.d0,  1.d0, 0.d0 /
!   data C(9,0:9)/1.d0, 9.d0, 36.d0, 84.d0, 126.d0, 126.d0, 84.d0, 36.d0, 9.d0, 1.d0 /
!
!! coefficients des moments
!   i1 = (/0, 1, 0, 2, 1, 0, 3, 2, 1, 0 /)
!   j1 = (/0, 0, 1, 0, 1, 2, 0, 1, 2, 3 /)
!
!
!! matrice de covariance
!   s1 = sig(1)
!   s21 = sig(2)
!   s22 = sig(3)
!
!! initialisation, calcul des quantites intermediaires
!   momp = 0.d0
!   momm = 0.d0
!
!   do alpha = 1, 2
!      x = -u(alpha)/(dsqrt(s1)+1.d-16)
!      call moments_incomplets_gaussienne1D(7,x,integp(:,alpha),integm(:,alpha))
!      call moments_gaussienne1D(7,v(alpha),s21,mom(:,alpha))
!   end do
!   do alpha = 3, 4
!      x = -u(alpha)/(dsqrt(s1)+1.d-16)
!      call moments_incomplets_gaussienne1D(7,x,integp(:,alpha),integm(:,alpha))
!      call moments_gaussienne1D(7,v(alpha),s22,mom(:,alpha))
!   end do
!
!! calcul des flux
!   do no = 1, 48
!      i = i1(no)
!      j = j1(no)
!      do alpha = 1, 4
!         sumpk = 0.d0
!         summk = 0.d0
!         do k = 0, i
!            sumpk = sumpk + C(i,k)*u(alpha)**(i-k)*dsqrt(s1)**k*integp(k,alpha)
!            summk = summk + C(i,k)*u(alpha)**(i-k)*dsqrt(s1)**k*integm(k,alpha)
!         end do
!         momp(no) = momp(no) + w(alpha)*mom(j,alpha)*sumpk
!         momm(no) = momm(no) + w(alpha)*mom(j,alpha)*summk
!      end do
!   end do
!
!end subroutine
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!****************************************************!!
!!!  MOMENTS INCOMPLETS EN V DE LA MULTI-GAUSSIENNE 2D !!   
!!!****************************************************!!
!subroutine moment_sigma48_V_decor( w, u, v, sig, momp, momm )
!!
!! CAS sigma12 = 0
!!
!! calcule les 35 moments d'une sommme de 4 gaussiennes en (u(i),v(i)) avec des poids w(i)
!! avec v variant
!!     entre z et plus l'infini  pour momp
!!     entre moins l'infini et z pour momm
!! et u variant sur R entier
!!
!!
!![1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5]  
!![0 0 0 0 0 0 0 0 0 0 1 1 1 1 1 2 2 2 2 2 3 3 3 3 3 4 4 4 4 4 5 5 5 5 5]  
!![0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 0 1 2 3 4 0 1 2 3 4 0 1 2 3 4 0 1 2 3 4]  
!!
!   real*8, intent(in) :: w(4), u(4), v(4), sig(3)
!   real*8 :: momp(48),momm(48)
!
!   integer :: i1(48), j1(48),i ,j ,k, no, alpha
!   real*8 :: s11, s12, s2, x, integp(0:7,4),integm(0:7,4), mom(0:7,4), sumpk, summk
!   real*8 :: C(0:9,0:9)
!
!! coefficients binomiaux
!   data C(0,0:9)/1.d0, 0.d0, 0.d0,  0.d0,  0.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
!   data C(1,0:9)/1.d0, 1.d0, 0.d0,  0.d0,  0.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
!   data C(2,0:9)/1.d0, 2.d0, 1.d0,  0.d0,  0.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
!   data C(3,0:9)/1.d0, 3.d0, 3.d0,  1.d0,  0.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
!   data C(4,0:9)/1.d0, 4.d0, 6.d0,  4.d0,  1.d0,   0.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
!   data C(5,0:9)/1.d0, 5.d0, 10.d0, 10.d0, 5.d0,   1.d0,   0.d0,  0.d0,  0.d0, 0.d0 /
!   data C(6,0:9)/1.d0, 6.d0, 15.d0, 20.d0, 15.d0,  6.d0,   1.d0,  0.d0,  0.d0, 0.d0 /
!   data C(7,0:9)/1.d0, 7.d0, 21.d0, 35.d0, 35.d0,  21.d0,  7.d0,  1.d0,  0.d0, 0.d0 /
!   data C(8,0:9)/1.d0, 8.d0, 28.d0, 56.d0, 70.d0,  56.d0,  28.d0, 8.d0,  1.d0, 0.d0 /
!   data C(9,0:9)/1.d0, 9.d0, 36.d0, 84.d0, 126.d0, 126.d0, 84.d0, 36.d0, 9.d0, 1.d0 /
!
!! coefficients des moments
!   i1 = (/0, 1, 0, 2, 1, 0, 3, 2, 1, 0, 4, 3, 2, 1, 0, 5, 4, 3, 2, 1, 0,&
!          6, 5, 4, 3, 2, 1, 0, 7, 6, 5, 4, 3, 2, 1, 0, 7, 6, 5, 3, 2, 1, 7, 6, 3, 2, 7, 3/)      
!   j1 = (/0, 0, 1, 0, 1, 2, 0, 1, 2, 3, 0, 1, 2, 3, 4, 0, 1, 2, 3, 4, 5,&
!          0, 1, 2, 3, 4, 5, 6, 0, 1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 5, 6, 7, 2, 3, 6, 7, 3, 7/) 
!
!! matrice de covariance
!   s2 = sig(3)
!   s11 = sig(1)
!   s12 = sig(2)   
!
!! initialisation, calcul des quantites intermediaires
!   momp = 0.d0
!   momm = 0.d0
!
!   do alpha = 1, 2
!      x = -v(alpha)/(dsqrt(s2)+1.d-16)
!      call moments_incomplets_gaussienne1D(7,x,integp(:,alpha),integm(:,alpha))
!      call moments_gaussienne1D(7,u(alpha),s11,mom(:,alpha))
!   end do
!   do alpha = 3, 4
!      x = -v(alpha)/(dsqrt(s2)+1.d-16) 
!      call moments_incomplets_gaussienne1D(7,x,integp(:,alpha),integm(:,alpha))
!      call moments_gaussienne1D(7,u(alpha),s12,mom(:,alpha))
!   end do
!   
!
!! calcul des flux
!   do no = 1, 35
!      i = i1(no)
!      j = j1(no)
!      do alpha = 1, 4
!         sumpk = 0.d0
!         summk = 0.d0
!         do k = 0, j
!            sumpk = sumpk + C(j,k)*v(alpha)**(j-k)*dsqrt(s2)**k*integp(k,alpha)
!            summk = summk + C(j,k)*v(alpha)**(j-k)*dsqrt(s2)**k*integm(k,alpha)
!         end do
!         momp(no) = momp(no) + w(alpha)*mom(i,alpha)*sumpk
!         momm(no) = momm(no) + w(alpha)*mom(i,alpha)*summk
!      end do
!   end do
!
!end subroutine
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!******************************************!!
!!  MOMENTS INCOMPLETS D'UNE GAUSSIENNE 1D  !!   
!!******************************************!!
subroutine moments_incomplets_gaussienne1D(N,z,integp,integm)
! calcule les moments d'ordre 0 à N de la gaussienne centree reduite
!     entre z et plus l'infini  pour integp
!     entre moins l'infini et z pour integm
   integer, intent(in)  :: N
   double precision, intent(in)  :: z
   double precision, intent(out) :: integp(0:N),integm(0:N)
   double precision  :: pi
   parameter(pi = 3.14159265358979323846d0)
   integer :: k

   integp(0) = 0.5d0*(1.d0-erf(z/dsqrt(2.d0)))
   integm(0) = 0.5d0*(1.d0+erf(z/dsqrt(2.d0)))
   if(N>0) then
      integp(1) = dexp(-0.5d0*z*z)/dsqrt(2.d0*pi)
      integm(1) = -dexp(-0.5d0*z*z)/dsqrt(2.d0*pi)
      do k = 2, N
         integp(k) = z**(k-1)*integp(1) +dble(k-1)*integp(k-2)
         integm(k) = z**(k-1)*integm(1) +dble(k-1)*integm(k-2)
      end do
   end if
   
end subroutine


!!*******************************!!
!!  MOMENTS D'UNE GAUSSIENNE 1D  !!   
!!*******************************!!
subroutine moments_gaussienne1D(N,mu,sigma2,mom)
! calcule les moments de 0 à N de la gaussienne centrée en mu et de variance sigma2
   integer, intent(in)  :: N
   double precision, intent(in)  :: mu,sigma2
   double precision, intent(out) :: mom(0:N)
   integer :: k

   mom(0) = 1.d0
   if(N>0) then
      mom(1) = mu
      do k = 2, N
         mom(k) = mu*mom(k-1)+dble(k-1)*sigma2*mom(k-2)
      end do
   end if
   
end subroutine

!!**************************************************************!!
!!  ERF(X) = 2/SQRT(PI) * INTEGRAL FROM 0 TO X OF EXP(-T^2) dT  !!   
!!**************************************************************!!

FUNCTION erf(x)
    double precision :: erf
    double precision, INTENT(IN) :: x
    INTEGER :: jjint

    jjint=0
    CALL CALERF(x, erf, jjint)
    
END FUNCTION erf
!!******************** FIN FONCTION ERF ******************


!!******************************************!!
!! PROCEDURE CALERF SERVANT A CALCULER ERF  !!
!!******************************************!!

SUBROUTINE Calerf(arg, resultat, jint)
    INTEGER :: I
    INTEGER, INTENT(IN) :: jint
    double precision, INTENT(IN) :: arg
    double precision, INTENT(OUT) :: resultat
    double precision, PARAMETER :: FOUR=4.0D0, ONE=1.0D0, HALF=0.5D0, TWO=20.D0, ZERO=0.0D0, SQRPI=5.6418958354775628695D-1
    double precision, PARAMETER :: THRESH=0.46875D0,SIXTEN=16.0D0
    double precision, PARAMETER :: XINF=1.79D308,XNEG=-26.628D0, XBIG=26.543D0,XHUGE=6.71D7,XMAX=2.53D307
    double precision, PARAMETER :: XSMALL=1.11D-16 !!XSMALL=9.629649721936179265279889712924637D-35
    double precision :: DEL, X, XDEN, XNUM, Y, YSQ
    double precision, DIMENSION(4) :: B
    double precision, DIMENSION(5) :: A, Q
    double precision, DIMENSION(6) :: P
    double precision, DIMENSION(8) :: D
    double precision, DIMENSION(9) :: C



    !!  ON INITIALISE LES VALEURS DES COEFFICIENTS  
    !!********************************************

    DATA A(1:2)/3.16112374387056560D00,1.13864154151050156D02/
    DATA A(3:4)/3.77485237685302021D02,3.20937758913846947D03/
    DATA A(5)/1.85777706184603153D-1/

    DATA B(1:2)/2.36012909523441209D01,2.44024637934444173D02/
    DATA B(3:4)/1.28261652607737228D03,2.84423683343917062D03/

    DATA C(1:2)/5.64188496988670089D-1,8.88314979438837594D0/
    DATA C(3:4)/6.61191906371416295D01,2.98635138197400131D02/
    DATA C(5:6)/8.81952221241769090D02,1.71204761263407058D03/
    DATA C(7:8)/2.05107837782607147D03,1.23033935479799725D03/
    DATA C(9)/2.15311535474403846D-8/

    DATA D(1:2)/1.57449261107098347D01,1.17693950891312499D02/
    DATA D(3:4)/5.37181101862009858D02,1.62138957456669019D03/
    DATA D(5:6)/3.29079923573345963D03,4.36261909014324716D03/
    DATA D(7:8)/3.43936767414372164D03,1.23033935480374942D03/

    DATA P(1:2)/3.05326634961232344D-1,3.60344899949804439D-1/
    DATA P(3:4)/1.25781726111229246D-1,1.60837851487422766D-2/
    DATA P(5:6)/6.58749161529837803D-4,1.63153871373020978D-2/

    DATA Q(1:2)/2.56852019228982242D00,1.87295284992346047D00/
    DATA Q(3:4)/5.27905102951428412D-1,6.05183413124413191D-2/
    DATA Q(5)/2.33520497626869185D-3/

	
	!! ON COMMENCE LE CALCUL
	!!**********************

    X=arg
    Y=ABS(X)
    IF (Y .LE. THRESH) THEN
       YSQ=ZERO
       IF (Y .GT. XSMALL) THEN
          YSQ=Y*Y
       END IF
       XNUM= A(5)*YSQ
       XDEN=YSQ
       DO I=1,3
          XNUM=(XNUM+A(I))*YSQ
          XDEN=(XDEN+B(I))*YSQ
       END DO
       resultat=X*(XNUM+A(4))/(XDEN+B(4))
       IF (jint .NE. 0) THEN 
          resultat= ONE-resultat 
       END IF
       IF (jint .EQ. 2) THEN
          resultat=DEXP(YSQ)*resultat
       END IF
    ELSE
       IF (Y .LE. FOUR) THEN
          XNUM= C(9)*Y
          XDEN=Y
          DO I=1,7
             XNUM= (XNUM+C(I))*Y
             XDEN= (XDEN+D(I))*Y
          END DO
          resultat= (XNUM+C(8))/(XDEN+D(8))
          IF (jint .NE. 2) THEN
             YSQ=AINT(Y*SIXTEN)/SIXTEN
             DEL= (Y-YSQ)*(Y+YSQ)
             resultat=DEXP(-YSQ*YSQ)*DEXP(-DEL)*resultat
          END IF
       ELSE
          resultat=ZERO
          IF (Y .GE. XBIG) THEN
             IF ((jint .EQ. 2) .AND. (Y .LT. XMAX)) THEN
                IF (Y .GE. XHUGE) THEN
                   resultat=SQRPI/Y
                END IF
             END IF
          ELSE
             YSQ=ONE/(Y*Y)
             XNUM=P(6)*YSQ
             DO I=1,4
                XNUM=(XNUM+P(I))*YSQ
                XDEN=(XDEN+Q(I))*YSQ
             END DO
             resultat=YSQ*(XNUM+P(5))/(XDEN+Q(5))
             resultat=(SQRPI-resultat)/Y
             IF (jint .NE. 2) THEN
                YSQ=AINT(Y*SIXTEN)/SIXTEN
                DEL=(Y-YSQ)*(Y+YSQ)
                resultat=DEXP(-YSQ*YSQ)*DEXP(-DEL)*resultat
             END IF
          END IF
       END IF
        
       IF (jint .EQ. 0) THEN
          resultat=(HALF-resultat)+HALF
          IF (X .LT. ZERO) THEN
             resultat=-resultat
          END IF
       ELSE 
          IF (jint .EQ. 1) THEN
             IF (X .LT. ZERO) THEN 
                resultat=TWO-resultat
             END IF
          ELSE
             IF (X .LT. ZERO) THEN
                IF (X .LT. XNEG) THEN
                   resultat=XINF
                ELSE
                   YSQ=AINT(X*SIXTEN)/SIXTEN
                   DEL=(X-YSQ)*(X+YSQ)
                   Y=DEXP(YSQ*YSQ)*DEXP(DEL)
                   resultat=(Y+Y)-resultat
                END IF
             END IF
          END IF
       END IF
    END IF

END SUBROUTINE Calerf
!!*********** FIN PROCEDURE CALERF ********************

end module flux_erf