program Gaussian2D
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!            MGaussian quadrature method 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!
!! This version transports 6 moments of the 2 velocity components:
!![1 2 3 4 5 6] 
!![0 1 0 2 1 0] 
!![0 0 1 0 1 2]   




  use nr_complet
  use AG2D_quadrature
  use AG2D_forceterms
  use AG2D_transport_fluxsplit	
  use AG2D_transport_HLL
  use AG2D_transport_MUSCL_HLL
  use AG2D_transport_MUSCL_fluxsplit
  use AG2D_interface
  use AG2D_inout
  use AG2D_diffoperator
  use AG2D_sharedvariables
  use Lagrangian2D_module
  
  Implicit none
  
   integer  Nx,Ny,Ns,nite,nitemax,nitemin,force,scheme, adim, advection,Nsauv,method,restart
   integer :: distrib,irestart,ipost,Nquery,Nmodes,outputlag,ialternatelie,periolag,gaslimitedcfl
   real*8 :: Xmin,Xmax,Ymin,Ymax
  
   real*8 :: St,Dx,Dy, Hx, Hy, deltatsauv,Tsauv,deltatquery,tquery
   real*8 :: Tmax,Tmin,cfl,t,dt,Umaxg,Vmaxg,mom(10)
   real*8,dimension(:,:,:),allocatable :: M0,M1,Mtemp,sigmaf
   real*8,dimension(:,:),allocatable :: uf,vf,Xcell,Ycell
   real*8,dimension(:), allocatable :: time,Minjleft,Minjright,Minjup,Minjdown
   real*8 :: coeff,time1,time2
   real*8 :: Dtconv,dtloc,Nmin,limiter,omega1,alpha_sinus,alpha_x,alpha_y
       
   real*8, dimension(:,:), allocatable :: n,u,v
   real*8, dimension(:,:,:),allocatable :: sigma,sigma0
       
   !lagrangian variables
   integer :: Np,iinterp,Nmom,imom(6),jmom(6)
   real*8, dimension(:), allocatable :: Xp,Yp,Up,Vp
   real*8 :: dtlag,Ninj,Uinj,Vinj,sigmainj(3)
       
  integer :: i,j,k, k1, ndxtest, errortest,ite,itemax,isauv,testcase,Ncut,iquery
  
  character*12 :: filemom(0:1000),filevtk(0:1000),filelag(0:1000)
  character*15 :: filequad(0:1000),filetimequery
  character*50 :: filegas
  character*50 :: outdir

  !moments de sortie
  Nmom=6
  !velx
  imom(1:Nmom)=(/ 0,1,0,2,1,0  /)
  !vely
  jmom(1:Nmom)=(/ 0,0,1,0,1,2  /)	


                                                              
call read_data(Hx,Hy,Nx,Ny,nitemin,nitemax,testcase,model,cfl,coeff,force,scheme,St,&
               Nsauv,distrib,restart,irestart,isauv,tmax,diffop,realcorr,Xmin,Xmax,Ymin,Ymax,&
               outdir,Nmin,limiter,Np,iinterp,dtlag,omega1,alpha_sinus,ipost,Nquery,Nmodes,&
               outputlag,alpha_x,alpha_y,periolag,gaslimitedcfl)

if (model.ne.11) then
  allocate(M0(6,Nx,Ny),M1(6,Nx,Ny),Mtemp(6,Nx,Ny),Xcell(Nx,Ny),Ycell(Nx,Ny),uf(Nx,Ny),vf(Nx,Ny),sigmaf(3,Nx,Ny))
elseif (model.eq.11) then
  allocate(M0(6,Nx,Ny),Xcell(Nx,Ny),Ycell(Nx,Ny),uf(Nx,Ny),vf(Nx,Ny),sigmaf(3,Nx,Ny))
  allocate(Xp(Np),Yp(Np),Up(Np),Vp(Np))
endif


if (testcase.eq.2) then
  allocate(time(nitemax))
endif
!if (testcase.eq.5) then
!allocate(Minjleft(6,Ny),Minjright(6,Ny),Minjup(6,Nx),Minjdown(6,Nx))
!endif



call initialisation(Nx,Ny,Hx,Hy,testcase,nitemin,nitemax,restart,irestart,Nsauv,Xmin,Xmax,Ymin,Ymax,distrib,    &
                    filemom,filequad,filevtk,filelag,Dx,Dy,Xcell,Ycell,uf,vf,M0,Tmin,Tmax,deltatsauv,tsauv,isauv,advection,t,ite,  &
                    time,Nmin,Np,iinterp,Xp,Yp,Up,Vp,omega1,alpha_sinus,Nquery,tquery,deltatquery,iquery,Nmodes)
if (model.eq.11) then
  call lag_to_eul(Np,Xp,Yp,Up,Vp,Nx,Ny,xcell,ycell,dx,dy,Nmom,imom,jmom,M0) 
endif
                    
                    
call output(Nx,Ny,Dx,Dy,St,model,Xcell,Ycell,M0,filemom(isauv),filequad(isauv),filevtk(isauv),outdir)
if ((model.eq.11).and.(outputlag.eq.1)) then
  call output_lagrangian(Np,Xp,Yp,Up,Vp,Nx,Ny,Uf,Vf,dx,dy,filelag(isauv),outdir)
endif


call print_info(testcase,model,scheme,St,cfl,coeff,Nx,Ny,adim,Np)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Time loop
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
ite=nitemin
write(*,*)'__________________________________________________________________'
write(*,*)'COMPUTATION (ite, t, dt)'

filetimequery='timequery.dat'

open(unit=200,form='formatted',file=trim(outdir)//'/'//filetimequery)


ialternatelie=0
call CPU_TIME(time1)
!write(*,*)tmax,t,itemax,ite
do while( (t < Tmax).and.(ite<nitemax) )
    !
    if (testcase.eq.2) then
      write(*,*)'READ GAS'
      call readgas_dat(ite,Nx,Ny,Dx,Dy,uf,vf,sigmaf)
    elseif (testcase.eq.3) then
      vf=0.d0
      do j=1,Nx
      do i=1,Ny
      uf(j,i) =    -sin(omega1*(Xcell(j,i)-Hx/2.d0+alpha_sinus*t) )
      end do
      end do
    elseif (testcase.eq.-1) then
    
	do i=1,Ny
	    do j=1,Nx
	        uf(j,i) =    sin((Xcell(j,i)-alpha_x*t)*2.d0*pi)*cos((Ycell(j,i)-alpha_y*t)*2.d0*pi)  
	        vf(j,i) =  - sin((Ycell(j,i)-alpha_y*t)*2.d0*pi)*cos((Xcell(j,i)-alpha_x*t)*2.d0*pi)  
	    end do
	end do
    
    endif
    
    
    if (model.ne.11) then
      !EULERIAN SOLVER
      if (testcase.eq.5) then
        include "injection.inc"
      endif
      
      if (gaslimitedcfl.eq.1) then
      Umaxg=maxval(abs(uf))
      Vmaxg=maxval(abs(vf))
      else
      Umaxg=0.d0
      Vmaxg=0.d0
      endif
      call timestep(M0,Nx,Dx,Ny,Dy,cfl,coeff, Umaxg,Vmaxg,Dt,t,Dtconv, St, ite,time,nitemax,scheme,testcase,dtlag,model)
       
       
      if (ialternatelie.eq.0) then
       
        call convection(Nx,Ny,Dx,Dy,Dt,Dtconv,M0,scheme,coeff,St,advection,M1,testcase,limiter)
        
        if ( (model.gt.1).and.(diffop.eq.1) ) then
            M0 = M1
            call diffusion(Nx,Ny,Dx,Dy,Dt,Dtconv,M0,St,diffop,M1)
        endif
         
        if (force==1) then
            M0 = M1
            M1 = moment_dragforce( M0, Nx, Ny, Dx, Dy, Dt, uf, vf, St) 
        end if
        M0 = M1
        ialternatelie=1
      elseif (ialternatelie.eq.1) then
           
        if (force==1) then
            write(*,*)'FORCE TERM'
            M1 = moment_dragforce( M0, Nx, Ny, Dx, Dy, Dt, uf, vf, St) 
        end if
        
        M0 = M1
        call convection(Nx,Ny,Dx,Dy,Dt,Dtconv,M0,scheme,coeff,St,advection,M1,testcase,limiter)
        
        if ( (model.gt.1).and.(diffop.eq.1) ) then
            M0=M1
            write(*,*)'DIFF OPERATOR'
            call diffusion(Nx,Ny,Dx,Dy,Dt,Dtconv,M0,St,diffop,M1)
        endif
        
        M0 = M1
        ialternatelie=0
      endif
      
    elseif (model.eq.11) then
      ! LAGRANGIAN SOLVER
      call timestep(M0,Nx,Dx,Ny,Dy,cfl,coeff, Umaxg,Vmaxg,Dt,t,Dtconv, St, ite,time,nitemax,scheme,testcase,dtlag,model)
      if (testcase.ne.0) then
        call lagrangian_solver_RK4_classic(Np,Xp,Yp,Up,Vp,Nx,Ny,dx,dy,Hx,Hy,Uf,Vf,St,dt,periolag)
      else
        call lagrangian_solver_RK4_TG(Np,Xp,Yp,Up,Vp,Nx,Ny,dx,dy,Hx,Hy,Uf,Vf,St,dt)
      endif
      call lag_to_eul(Np,Xp,Yp,Up,Vp,Nx,Ny,xcell,ycell,dx,dy,Nmom,imom,jmom,M0)
    endif
      
    if (t>=tsauv) then
      write(*,*)'save',isauv+1
      tsauv=tsauv+deltatsauv
      isauv=isauv+1
      call output(Nx,Ny,Dx,Dy,St,model,Xcell,Ycell,M0,filemom(isauv),filequad(isauv),filevtk(isauv),outdir)
      
      if ((model.eq.11).and.(outputlag.eq.1)) then
        call output_lagrangian(Np,Xp,Yp,Up,Vp,Nx,Ny,Uf,Vf,dx,dy,filelag(isauv),outdir)
      endif
    
    endif
    
    if (ipost.eq.1) then
      if (t>=tquery) then
        write(*,*)'query',iquery+1
        tquery=tquery+deltatquery
        iquery=iquery+1
        call postproc_query(Nx,Ny,Dx,Dy,St,model,Xcell,Ycell,M0,t,uf,vf)
      endif
    endif

end do
call CPU_TIME(time2)
write(*,*)'__________________________________________________________________'
write(*,*)'CPU time=',time2-time1


  

close(200)

end program Gaussian2D
