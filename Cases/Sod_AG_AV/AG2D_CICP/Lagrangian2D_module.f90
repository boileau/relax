module Lagrangian2D_module

use AG2D_generic
implicit none


contains


subroutine generate_particles(Np,Nx,Ny,dx,dy,xc,yc,Lx,Ly,Ug,Vg,iinterp,Xp0,Yp0,Up0,Vp0,velinit,Xmin,Xmax,Ymin,Ymax,distrib)


integer,intent(in) :: Np,Nx,Ny,iinterp,velinit,distrib
real*8, intent(in) :: dx,dy,Lx,Ly,xc(Nx,Ny),yc(Nx,Ny),Ug(Nx,Ny),Vg(Nx,Ny),Xmin,Xmax,Ymin,Ymax
real*8, intent(out), dimension(Np) :: Xp0,Yp0,Up0,Vp0

integer :: k

   if (distrib.ge.0 ) then
      do k=1,Np
         call random_number(Xp0(k))
         call random_number(Yp0(k))
         if (distrib.eq.1) then
           Xp0(k)=Xp0(k)*(Xmax-Xmin)+Xmin
           Yp0(k)=Yp0(k)*(Ymax-Ymin)+Ymin
         else
           Xp0(k)=Xp0(k)*Lx
           Yp0(k)=Yp0(k)*Ly
         end if
      end do
      
      if (velinit.eq.1) then
      call gas_at_particles(Np,Nx,Ny,dx,dy,xc,yc,Xp0,Yp0,iinterp,Ug,Vg,Up0,Vp0)
      elseif(velinit.eq.0) then
      Up0=0.d0
      Vp0=0.d0
      endif
   
   else
      include "initpersolag.inc"
   endif
   
end subroutine


subroutine gas_at_particles(Np,Nx,Ny,dx,dy,xc,yc,Xp0,Yp0,iinterp,Ug,Vg,Up0,Vp0)

integer, intent(in) :: Np,Nx,Ny,iinterp
real*8, intent(in) :: dx,dy,xc(Nx,Ny),yc(Nx,Ny)
real*8, intent(in) :: Xp0(Np),Yp0(Np),Ug(Nx,Ny),Vg(Nx,Ny)

real*8, intent(out) :: Up0(Np),Vp0(Np)

integer :: i,j,k,im1,ip1,jm1,jp1
real*8 :: Ax(3),Ay(3)


      do k=1,Np

        i=floor(Xp0(k)/dx)+1
        j=floor(Yp0(k)/dy)+1
        ip1=modulo(i+1-1,Nx)+1
        jp1=modulo(j+1-1,Ny)+1
        im1=modulo(i-1-1,Nx)+1
        jm1=modulo(j-1-1,Ny)+1
  
        
        if (iinterp.eq.0) then
          Up0(k) = Ug(i,j)
          Vp0(k) = Vg(i,j)
        elseif (iinterp.eq.1) then
          call quadratic_interpolate(Ug(i,j),Ug(im1,j),Ug(ip1,j),Ug(i,jp1),Ug(i,jm1),dx,dy,Ax,Ay)
          Up0(k)=(Ax(1)*(Xp0(k)-xc(i,j))**2+Ax(2)*(Xp0(k)-xc(i,j))+Ax(3))*(Ay(1)*(Yp0(k)-yc(i,j))**2+Ay(2)*(Yp0(k)-yc(i,j))+Ax(3))
          
          call quadratic_interpolate(Vg(i,j),Vg(im1,j),Vg(ip1,j),Vg(i,jp1),Vg(i,jm1),dx,dy,Ax,Ay)
          Vp0(k)=(Ax(1)*(Xp0(k)-xc(i,j))**2+Ax(2)*(Xp0(k)-xc(i,j))+Ax(3))*(Ay(1)*(Yp0(k)-yc(i,j))**2+Ay(2)*(Yp0(k)-yc(i,j))+Ax(3))
        endif
      
      end do
      
      
end subroutine



subroutine lag_to_eul(Np,Xp,Yp,Up,Vp,Nx,Ny,xc,yc,dx,dy,Nmom,imom,jmom,M)

integer, intent(in) :: Np,Nx,Ny,Nmom,imom(Nmom),jmom(Nmom)
real*8, intent(in) :: Xp(Np),Yp(Np),Up(Np),Vp(Np),xc(Nx,Ny),yc(Nx,Ny),dx,dy

real*8, intent(out) :: M(Nmom,Nx,Ny)

integer :: i,j,k,l
  M=0.d0

  do k=1,Np
    i=floor(Xp(k)/dx)+1
    j=floor(Yp(k)/dy)+1
    if (i>Nx) then
      i = Nx
    endif
    if (j>Ny) then
      j = Ny
    endif
    if ( (i.gt.0).and.(j.gt.0) )then
    do l=1,Nmom
            M(l,i,j)=M(l,i,j)+Up(k)**imom(l)*Vp(k)**jmom(l)
    enddo
    endif
  end do
  M=M/Np*Nx*Ny
end subroutine



subroutine lagrangian_solver(Np,Xp,Yp,Up,Vp,Nx,Ny,dx,dy,Lx,Ly,Ug,Vg,St,dt)

integer, intent(in) :: Np,Nx,Ny
real*8, intent(in)  :: Ug(Nx,Ny),Vg(Nx,Ny),St,dx,dy,dt,Lx,Ly
real*8, intent(inout), dimension(Np) :: Xp,Yp,Up,Vp

integer :: i,j,k
real*8 :: Xpnew,Ypnew,Upnew,Vpnew


  do k=1,Np
    i=floor(Xp(k)/dx)+1
    j=floor(Yp(k)/dy)+1 
   
    Xpnew=Xp(k) + dt*Ug(i,j) - St*( Up(k)-Ug(i,j) )*(exp(-dt/St)-1.d0)
    Ypnew=Yp(k) + dt*Vg(i,j) - St*( Vp(k)-Vg(i,j) )*(exp(-dt/St)-1.d0)
    Upnew=( Up(k)-Ug(i,j) )*exp(-dt/St) + Ug(i,j)
    Vpnew=( Vp(k)-Vg(i,j) )*exp(-dt/St) + Vg(i,j)       

     !periodicity
    if ( (Xpnew.ge.0.d0).and.(Xpnew.le.Lx) ) then
      Xp(k)=Xpnew
    elseif (Xpnew.lt.0.d0) then
      Xp(k)=Xpnew+Lx
    elseif (Xpnew.gt.Lx) then
      Xp(k)=Xpnew-Lx
    endif
    if ( (Ypnew.ge.0.d0).and.(Ypnew.le.Ly) ) then
      Yp(k)=Ypnew
    elseif (Ypnew.lt.0.d0) then
      Yp(k)=Ypnew+Ly
    elseif (Ypnew.gt.Ly) then
      Yp(k)=Ypnew-Ly
    endif           
    
    Up(k)=Upnew
    Vp(k)=Vpnew
  enddo

end subroutine


subroutine lagrangian_solver_o2TG(Np,Xp,Yp,Up,Vp,Nx,Ny,dx,dy,Lx,Ly,Ug,Vg,St,dt)

integer, intent(in) :: Np,Nx,Ny
real*8, intent(in)  :: Ug(Nx,Ny),Vg(Nx,Ny),St,dx,dy,dt,Lx,Ly
real*8, intent(inout), dimension(Np) :: Xp,Yp,Up,Vp

integer :: i,j,k
real*8 :: Xpnew,Ypnew,Upnew,Vpnew,KU,KV,AU,AV


  do k=1,Np
    i=floor(Xp(k)/dx)+1
    j=floor(Yp(k)/dy)+1 
    
    KU=cos(2*pi*Xp(k))*cos(2*pi*Yp(k))*Up(k)-sin(2*pi*Xp(k))*sin(2*pi*Yp(k))*Vp(k)
    KV=sin(2*pi*Xp(k))*sin(2*pi*Yp(k))*Up(k)-cos(2*pi*Xp(k))*cos(2*pi*Yp(k))*Vp(k)
    KU=2*pi*KU
    KV=2*pi*KV
    KU=0.d0
    KV=0.d0
    
    Xpnew=Xp(k) + dt*sin(2*pi*Xp(k))*cos(2*pi*Yp(k)) - St*( Up(k)-sin(2*pi*Xp(k))*cos(2*pi*Yp(k)))*(exp(-dt/St)-1.d0)&
                + KU*dt**2-KU*St*dt-KU*St**2*(exp(-dt/St)-1)
    Ypnew=Yp(k) - dt*cos(2*pi*Xp(k))*sin(2*pi*Yp(k)) - St*( Vp(k)+cos(2*pi*Xp(k))*sin(2*pi*Yp(k)))*(exp(-dt/St)-1.d0)&
                + KV*dt**2-KV*St*dt-KV*St**2*(exp(-dt/St)-1)
    Upnew=( Up(k)-sin(2*pi*Xp(k))*cos(2*pi*Yp(k)) )*exp(-dt/St) + sin(2*pi*Xp(k))*cos(2*pi*Yp(k)) +KU*(dt-St*(1-exp(-dt/St)))
    Vpnew=( Vp(k)+cos(2*pi*Xp(k))*sin(2*pi*Yp(k)) )*exp(-dt/St) - cos(2*pi*Xp(k))*sin(2*pi*Yp(k)) +KV*(dt-St*(1-exp(-dt/St)))

     !periodicity
    if ( (Xpnew.ge.0.d0).and.(Xpnew.le.Lx) ) then
      Xp(k)=Xpnew
    elseif (Xpnew.lt.0.d0) then
      Xp(k)=Xpnew+Lx
    elseif (Xpnew.gt.Lx) then
      Xp(k)=Xpnew-Lx
    endif
    if ( (Ypnew.ge.0.d0).and.(Ypnew.le.Ly) ) then
      Yp(k)=Ypnew
    elseif (Ypnew.lt.0.d0) then
      Yp(k)=Ypnew+Ly
    elseif (Ypnew.gt.Ly) then
      Yp(k)=Ypnew-Ly
    endif           
    
    Up(k)=Upnew
    Vp(k)=Vpnew
  enddo

end subroutine



subroutine lagrangian_solver_RK2(Np,Xp,Yp,Up,Vp,Nx,Ny,dx,dy,Lx,Ly,Ug,Vg,St,dt)

integer, intent(in) :: Np,Nx,Ny
real*8, intent(in)  :: Ug(Nx,Ny),Vg(Nx,Ny),St,dx,dy,dt,Lx,Ly
real*8, intent(inout), dimension(Np) :: Xp,Yp,Up,Vp

integer :: i,j,k
real*8 :: Xpnew,Ypnew,Upnew,Vpnew,Xpstar,Ypstar,Upstar,Vpstar


  do k=1,Np
    i=floor(Xp(k)/dx)+1
    j=floor(Yp(k)/dy)+1 
    
    
    Xpstar=Xp(k) + 0.5d0*dt*Ug(i,j) - St*( Up(k)-Ug(i,j) )*(exp(-0.5d0*dt/St)-1.d0)
    Ypstar=Yp(k) + 0.5d0*dt*Vg(i,j) - St*( Vp(k)-Vg(i,j) )*(exp(-0.5d0*dt/St)-1.d0)
    Upstar=( Up(k)-Ug(i,j) )*exp(-0.5d0*dt/St) + Ug(i,j) 
    Vpstar=( Vp(k)-Vg(i,j) )*exp(-0.5d0*dt/St) + Vg(i,j)
    
    i=floor(Xpstar/dx)+1
    j=floor(Ypstar/dy)+1
    
    Xpnew=Xp(k) + dt*Ug(i,j) - St*( Upstar-Ug(i,j) )*(exp(-dt/St)-1.d0)
    Ypnew=Yp(k) + dt*Vg(i,j) - St*( Vpstar-Vg(i,j) )*(exp(-dt/St)-1.d0)
    Upnew=( Up(k)-Ug(i,j) )*exp(-dt/St) + Ug(i,j) 
    Vpnew=( Vp(k)-Vg(i,j) )*exp(-dt/St) + Vg(i,j)
    

     !periodicity
    if ( (Xpnew.ge.0.d0).and.(Xpnew.le.Lx) ) then
      Xp(k)=Xpnew
    elseif (Xpnew.lt.0.d0) then
      Xp(k)=Xpnew+Lx
    elseif (Xpnew.gt.Lx) then
      Xp(k)=Xpnew-Lx
    endif
    if ( (Ypnew.ge.0.d0).and.(Ypnew.le.Ly) ) then
      Yp(k)=Ypnew
    elseif (Ypnew.lt.0.d0) then
      Yp(k)=Ypnew+Ly
    elseif (Ypnew.gt.Ly) then
      Yp(k)=Ypnew-Ly
    endif           
    
    Up(k)=Upnew
    Vp(k)=Vpnew
  enddo

end subroutine




subroutine lagrangian_solver_RK2_classic(Np,Xp,Yp,Up,Vp,Nx,Ny,dx,dy,Lx,Ly,Ug,Vg,St,dt,periolag)

integer, intent(in) :: Np,Nx,Ny,periolag
real*8, intent(in)  :: Ug(Nx,Ny),Vg(Nx,Ny),St,dx,dy,dt,Lx,Ly
real*8, intent(inout), dimension(Np) :: Xp,Yp,Up,Vp

integer :: i,j,k
real*8 :: Xpnew,Ypnew,Upnew,Vpnew,Xpstar,Ypstar,Upstar,Vpstar


  do k=1,Np
    i=floor(Xp(k)/dx)+1
    j=floor(Yp(k)/dy)+1
    
    Xpstar=Xp(k) + 0.5d0*dt*Up(k)
    Ypstar=Yp(k) + 0.5d0*dt*Vp(k)
    if ( (i.gt.0).and.(j.gt.0) ) then
    Upstar=Up(k) + 0.5d0*dt*(Ug(i,j)-Up(k))/St
    Vpstar=Vp(k) + 0.5d0*dt*(Vg(i,j)-Vp(k))/St
    else
    Upstar=Up(k)
    Vpstar=Vp(k)
    endif
    i=floor(Xpstar/dx)+1
    j=floor(Ypstar/dy)+1
    
    Xpnew=Xp(k) + dt*Upstar
    Ypnew=Yp(k) + dt*Vpstar
    if ( (i.gt.0).and.(j.gt.0) ) then
    Upnew=Up(k) + dt*(Ug(i,j)-Upstar)/St
    Vpnew=Vp(k) + dt*(Vg(i,j)-Vpstar)/St
    else
    Upnew=Up(k)
    Vpnew=Vp(k)
    endif
    

    if (periolag.eq.1) then
     !periodicity
    if ( (Xpnew.ge.0.d0).and.(Xpnew.le.Lx) ) then
      Xp(k)=Xpnew
    elseif (Xpnew.lt.0.d0) then
      Xp(k)=Xpnew+Lx
    elseif (Xpnew.gt.Lx) then
      Xp(k)=Xpnew-Lx
    endif
    if ( (Ypnew.ge.0.d0).and.(Ypnew.le.Ly) ) then
      Yp(k)=Ypnew
    elseif (Ypnew.lt.0.d0) then
      Yp(k)=Ypnew+Ly
    elseif (Ypnew.gt.Ly) then
      Yp(k)=Ypnew-Ly
    endif
    else
    Xp(k)=Xpnew
    Yp(k)=Ypnew
    Up(k)=Upnew
    Vp(k)=Vpnew
    endif
  enddo

end subroutine



subroutine lagrangian_solver_RK4_classic(Np,Xp,Yp,Up,Vp,Nx,Ny,dx,dy,Lx,Ly,Ug,Vg,St,dt,periolag)

integer, intent(in) :: Np,Nx,Ny,periolag
real*8, intent(in)  :: Ug(Nx,Ny),Vg(Nx,Ny),St,dx,dy,dt,Lx,Ly
real*8, intent(inout), dimension(Np) :: Xp,Yp,Up,Vp

integer :: i,j,k,i1,j1,i2,j2,i3,j3
real*8 :: Xpnew,Ypnew,Upnew,Vpnew,Xp1,Yp1,Up1,Vp1,Xp2,Yp2,Up2,Vp2,Xp3,Yp3,Up3,Vp3,Xp4,Yp4,Up4,Vp4
real*8 :: k1x,k1y,k1u,k1v,k2x,k2y,k2u,k2v,k3x,k3y,k3u,k3v,k4x,k4y,k4u,k4v


  do k=1,Np
    i=floor(Xp(k)/dx)+1
    j=floor(Yp(k)/dy)+1

    
    
    k1x=Up(k)
    k1y=Vp(k)
    if ( (i.le.0).or.(j.le.0) ) then
    k1u=0.d0
    k1v=0.d0
    else
    k1u=(Ug(i,j)-Up(k))/St
    k1v=(Vg(i,j)-Vp(k))/St
    endif
    Xp1=Xp(k) + 0.5d0*dt*k1x
    Yp1=Yp(k) + 0.5d0*dt*k1y
    Up1=Up(k) + 0.5d0*dt*k1u
    Vp1=Vp(k) + 0.5d0*dt*k1v
    
    i1=floor(Xp1/dx)+1
    j1=floor(Yp1/dy)+1
    
    k2x=Up1
    k2y=Vp1
    if ( (i1.le.0).or.(j1.le.0) ) then
    k2u=0.d0
    k2v=0.d0
    else
    k2u=(Ug(i1,j1)-Up1)/St
    k2v=(Vg(i1,j1)-Vp1)/St
    endif
    
    
    Xp2=Xp(k) + 0.5d0*dt*k2x
    Yp2=Yp(k) + 0.5d0*dt*k2y
    Up2=Up(k) + 0.5d0*dt*k2u
    Vp2=Vp(k) + 0.5d0*dt*k2v
    
    i2=floor(Xp2/dx)+1
    j2=floor(Yp2/dy)+1
    
    k3x=Up2
    k3y=Vp2
    if ( (i2.le.0).or.(j2.le.0) ) then
      k3u=0.d0
      k3v=0.d0
    else
      k3u=(Ug(i2,j2)-Up2)/St
      k3v=(Vg(i2,j2)-Vp2)/St
    endif
  
    
    Xp3=Xp(k) + dt*k3x
    Yp3=Yp(k) + dt*k3y
    Up3=Up(k) + dt*k3u
    Vp3=Vp(k) + dt*k3v
    
    i3=floor(Xp3/dx)+1
    j3=floor(Yp3/dy)+1
    
    k4x=Up3
    k4y=Vp3
    if ( (i3.le.0).or.(j3.le.0) ) then
      k4u=0.d0
      k4v=0.d0
    else
      k4u=(Ug(i3,j3)-Up3)/St
      k4v=(Vg(i3,j3)-Vp3)/St
    endif
    
    
    
    Xpnew=Xp(k) + dt/6.d0*(k1x+2.d0*k2x+2.d0*k3x+k4x)
    Ypnew=Yp(k) + dt/6.d0*(k1y+2.d0*k2y+2.d0*k3y+k4y)
    Upnew=Up(k) + dt/6.d0*(k1u+2.d0*k2u+2.d0*k3u+k4u)
    Vpnew=Vp(k) + dt/6.d0*(k1v+2.d0*k2v+2.d0*k3v+k4v)
    
    if (periolag.eq.1) then
     !periodicity
    if ( (Xpnew.ge.0.d0).and.(Xpnew.le.Lx) ) then
      Xp(k)=Xpnew
    elseif (Xpnew.lt.0.d0) then
      Xp(k)=Xpnew+Lx
    elseif (Xpnew.gt.Lx) then
      Xp(k)=Xpnew-Lx
    endif
    if ( (Ypnew.ge.0.d0).and.(Ypnew.le.Ly) ) then
      Yp(k)=Ypnew
    elseif (Ypnew.lt.0.d0) then
      Yp(k)=Ypnew+Ly
    elseif (Ypnew.gt.Ly) then
      Yp(k)=Ypnew-Ly
    endif
    else
    Xp(k)=Xpnew
    Yp(k)=Ypnew
    Up(k)=Upnew
    Vp(k)=Vpnew
    endif
  enddo

end subroutine


subroutine lagrangian_solver_RK4_TG(Np,Xp,Yp,Up,Vp,Nx,Ny,dx,dy,Lx,Ly,Ug,Vg,St,dt)

integer, intent(in) :: Np,Nx,Ny
real*8, intent(in)  :: Ug(Nx,Ny),Vg(Nx,Ny),St,dx,dy,dt,Lx,Ly
real*8, intent(inout), dimension(Np) :: Xp,Yp,Up,Vp

integer :: i,j,k,i1,j1,i2,j2,i3,j3
real*8 :: Xpnew,Ypnew,Upnew,Vpnew,Xp1,Yp1,Up1,Vp1,Xp2,Yp2,Up2,Vp2,Xp3,Yp3,Up3,Vp3,Xp4,Yp4,Up4,Vp4
real*8 :: k1x,k1y,k1u,k1v,k2x,k2y,k2u,k2v,k3x,k3y,k3u,k3v,k4x,k4y,k4u,k4v


  do k=1,Np
    i=floor(Xp(k)/dx)+1
    j=floor(Yp(k)/dy)+1 
    
    
    k1x=Up(k)
    k1y=Vp(k)
    k1u=(sin(2*pi*Xp(k))*cos(2*pi*Yp(k))-Up(k))/St
    k1v=(-cos(2*pi*Xp(k))*sin(2*pi*Yp(k))-Vp(k))/St
    
    Xp1=Xp(k) + 0.5d0*dt*k1x
    Yp1=Yp(k) + 0.5d0*dt*k1y
    Up1=Up(k) + 0.5d0*dt*k1u
    Vp1=Vp(k) + 0.5d0*dt*k1v
    
    i1=floor(Xp1/dx)+1
    j1=floor(Yp1/dy)+1
    
    k2x=Up1
    k2y=Vp1
    k2u=(sin(2*pi*Xp1)*cos(2*pi*Yp1)-Up1)/St
    k2v=(-cos(2*pi*Xp1)*sin(2*pi*Yp1)-Vp1)/St
    
    
    Xp2=Xp(k) + 0.5d0*dt*k2x
    Yp2=Yp(k) + 0.5d0*dt*k2y
    Up2=Up(k) + 0.5d0*dt*k2u
    Vp2=Vp(k) + 0.5d0*dt*k2v
    
    i2=floor(Xp2/dx)+1
    j2=floor(Yp2/dy)+1
    
    k3x=Up2
    k3y=Vp2
    k3u=(sin(2*pi*Xp2)*cos(2*pi*Yp2)-Up2)/St
    k3v=(-cos(2*pi*Xp2)*sin(2*pi*Yp2)-Vp2)/St
  
    
    Xp3=Xp(k) + dt*k3x
    Yp3=Yp(k) + dt*k3y
    Up3=Up(k) + dt*k3u
    Vp3=Vp(k) + dt*k3v
    
    i3=floor(Xp3/dx)+1
    j3=floor(Yp3/dy)+1
    
    k4x=Up3
    k4y=Vp3
    k4u=(sin(2*pi*Xp3)*cos(2*pi*Yp3)-Up3)/St
    k4v=(-cos(2*pi*Xp3)*sin(2*pi*Yp3)-Vp3)/St
    
    
    
    Xpnew=Xp(k) + dt/6.d0*(k1x+2.d0*k2x+2.d0*k3x+k4x)
    Ypnew=Yp(k) + dt/6.d0*(k1y+2.d0*k2y+2.d0*k3y+k4y)
    Upnew=Up(k) + dt/6.d0*(k1u+2.d0*k2u+2.d0*k3u+k4u)
    Vpnew=Vp(k) + dt/6.d0*(k1v+2.d0*k2v+2.d0*k3v+k4v)
    
    
     !periodicity
    if ( (Xpnew.ge.0.d0).and.(Xpnew.le.Lx) ) then
      Xp(k)=Xpnew
    elseif (Xpnew.lt.0.d0) then
      Xp(k)=Xpnew+Lx
    elseif (Xpnew.gt.Lx) then
      Xp(k)=Xpnew-Lx
    endif
    if ( (Ypnew.ge.0.d0).and.(Ypnew.le.Ly) ) then
      Yp(k)=Ypnew
    elseif (Ypnew.lt.0.d0) then
      Yp(k)=Ypnew+Ly
    elseif (Ypnew.gt.Ly) then
      Yp(k)=Ypnew-Ly
    endif           
    
    Up(k)=Upnew
    Vp(k)=Vpnew
  enddo

end subroutine



subroutine output_lagrangian(Np,Xp,Yp,Up,Vp,Nx,Ny,Ug,Vg,dx,dy,filelag,outdir)

integer, intent(in) :: Np,Nx,Ny
real*8, intent(in)  :: Ug(Nx,Ny),Vg(Nx,Ny),dx,dy
real*8, intent(inout), dimension(Np) :: Xp,Yp,Up,Vp
character*12,intent(in) :: filelag
character*50, intent(in) :: outdir

integer :: i,j,k
real*8 :: Ugatp,Vgatp
character*1000 :: ligne


    open(unit=100,form='formatted',file=trim(outdir)//'/'//filelag) 
    
    do k=1,Np
        i=floor(Xp(k)/dx)+1
        j=floor(Yp(k)/dy)+1
        if ( (i.gt.0).and.(j.gt.0) ) then
        Ugatp=Ug(i,j)    
        Vgatp=Vg(i,j)
        else
        Ugatp=Up(k)   
        Vgatp=Vp(k)
        endif
        !write(100,*) Xp(k),Yp(k),Up(k),Vp(k),Ugatp,Vgatp
        write(ligne,*) Xp(k),Yp(k),Up(k),Vp(k),Ugatp,Vgatp
        write(100,'(a)') trim(ligne) 
    end do

    
   close(100) 


end subroutine output_lagrangian




end module Lagrangian2D_module