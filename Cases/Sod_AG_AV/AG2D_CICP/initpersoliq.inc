  
    do j=1,Ny
        do i=1,Nx
                if (Xcell(i,j)<1) then
                  N=1.0d0
                  U=0.0d0
                  V=0.0d0
                  sigma(1)=2d0/N
                  sigma(2)=0.05d0/N
                  sigma(3)=0.6d0/N
                else
                  N=0.125d0
                  U=0.0d0
                  V=0.0d0
                  sigma(1)=0.2d0/N
                  sigma(2)=0.1d0/N
                  sigma(3)=0.2d0/N
                endif
                
                !N=1+exp(-(xcell(i,j)-0.5d0)**2/0.1d0**2)
                !U=1
                !V=0.d0
                !sigma(1)=0.5d0
                !sigma(2)=0.25d0
                !sigma(3)=0.5d0
                !sigma=0.d0
                
            mom(1:10)=moments_monogauss( N, U, V, sigma )
            M0(:,i,j) = mom(1:6)        
        end do 
    end do
