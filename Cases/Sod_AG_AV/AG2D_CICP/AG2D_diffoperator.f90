module AG2D_diffoperator

use AG2D_quadrature


implicit none
contains


function diff_operator(M0,Nx,Ny,Dx,Dy,Dt,St)

integer, intent(in) :: Nx,Ny
real*8, intent(in) :: Dx,Dy,M0(6,Nx,Ny),Dt,St
real*8 :: M1(6,Nx,Ny)

real*8 :: n(Nx,Ny),u(Nx,Ny),v(Nx,Ny),sigma(3,Nx,Ny),nsigmastar(3,Nx,Ny)
real*8 :: Diff(6,Nx,Ny),diff_operator(6,Nx,Ny)

!local
integer :: i,j,k,im1,ip1,jm1,jp1

do j=1,Nx
    do i=1,Ny
        call moment_inversion_2D_gaussian(M0(:,j,i),n(j,i),u(j,i),v(j,i),sigma(:,j,i))  	    
    end do
end do

sigma=sigFEM(Nx,Ny,dx,dy,sigma,n,u,v,St)  

do i=1,Nx
  do j=1,Ny
    nsigmastar(1,i,j)=n(i,j)*( sigma(1,i,j)-(sigma(1,i,j)+sigma(3,i,j))/2.d0 )
    nsigmastar(2,i,j)=n(i,j)*( sigma(2,i,j) )
    nsigmastar(3,i,j)=n(i,j)*( sigma(3,i,j)-(sigma(1,i,j)+sigma(3,i,j))/2.d0 )
  end do
end do

Diff=0.d0
do i=1,Nx
    im1=modulo(i-1-1,Nx)+1
    ip1=modulo(i+1-1,Nx)+1
	do j=1,Ny
        jm1=modulo(j-1-1,Ny)+1
        jp1=modulo(j+1-1,Ny)+1
                if (n(i,j).gt.mtol_diff) then
                !diff on number density (nada)
                Diff(1,i,j)=0.d0
                
                !diff on qdm
		Diff(2,i,j)=-1.d0/(2.d0*dx)*(nsigmastar(1,ip1,j)-nsigmastar(1,im1,j))  -  1.d0/(2.d0*dy)*(nsigmastar(2,i,jp1)-nsigmastar(2,i,jm1)) 
		Diff(3,i,j)=-1.d0/(2.d0*dx)*(nsigmastar(2,ip1,j)-nsigmastar(2,im1,j))  -  1.d0/(2.d0*dy)*(nsigmastar(3,i,jp1)-nsigmastar(3,i,jm1))
                
                
                
		Diff(4,i,j)=-1.d0/(2.d0*dx)*(3*u(ip1,j)*nsigmastar(1,ip1,j)-3*u(im1,j)*nsigmastar(1,im1,j)) &
                            -1.d0/(2.d0*dy)*(2.d0*u(i,jp1)*nsigmastar(2,i,jp1)-2.d0*u(i,jm1)*nsigmastar(2,i,jm1))  &
                            -1.d0/(2.d0*dy)*(v(i,jp1)*nsigmastar(1,i,jp1)-v(i,jm1)*nsigmastar(1,i,jm1))
                            
		Diff(5,i,j)=-1.d0/(2.d0*dx)*(2*u(ip1,j)*nsigmastar(2,ip1,j)-2*u(im1,j)*nsigmastar(2,im1,j)) &
                            -1.d0/(2.d0*dx)*(v(ip1,j)*nsigmastar(1,ip1,j)-v(im1,j)*nsigmastar(1,im1,j)) &
                            -1.d0/(2.d0*dy)*(2.d0*v(i,jp1)*nsigmastar(2,i,jp1)-2.d0*v(i,jm1)*nsigmastar(2,i,jm1))  &
                            -1.d0/(2.d0*dy)*(u(i,jp1)*nsigmastar(3,i,jp1)-u(i,jm1)*nsigmastar(3,i,jm1))
                
                
		Diff(6,i,j)=-1.d0/(2.d0*dy)*(3*v(i,jp1)*nsigmastar(3,i,jp1)-3*v(i,jm1)*nsigmastar(3,i,jm1)) &
                            -1.d0/(2.d0*dx)*(2.d0*v(ip1,j)*nsigmastar(2,ip1,j)-2.d0*v(im1,j)*nsigmastar(2,im1,j))  &
                            -1.d0/(2.d0*dx)*(u(ip1,j)*nsigmastar(3,ip1,j)-u(im1,j)*nsigmastar(3,im1,j)) 
              else
                Diff(:,i,j)=0.d0
              endif
              
              
              M1(:,i,j)=M0(:,i,j)+Dt*Diff(:,i,j)
              !if (abs(Diff(3,i,j)*Dt/M0(3,i,j)).gt.0.01d0) write(*,*)Diff(3,i,j)*Dt/M0(3,i,j)
              
        end do
end do

diff_operator=M1




end function





end module AG2D_diffoperator