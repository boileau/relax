module AG2D_inout

use AG2D_quadrature
use AG2D_generic
use Lagrangian2D_module

implicit none

character :: gasfolder*100,gastemplate*100
integer :: velinit,typegastemplate,switchloop,Nxprojmin,Nyprojmin,Nxprojmax,Nyprojmax

real*8 :: Uinit,Vinit,sigmainit(3),Stc
contains


subroutine read_data(Hx,Hy,Nx,Ny,nitemin,nitemax,testcase,model,cfl,coeff,force,scheme,  &
                     St,Nsauv,distrib,restart,irestart,isauv,tmax,diffop,realcorr,       &
                     Xmin,Xmax,Ymin,Ymax,outdir,Nmin,limiter,Np,iinterp,Dtlag,omega1,    &
                     alpha_sinus,ipost,Nquery,Nmodes,outputlag,alpha_x,alpha_y,periolag, &
                     gaslimitedcfl)


integer,intent(out) :: Nx,Ny,nitemin,nitemax,testcase,model,force,scheme,Nsauv,distrib,restart,Np
integer,intent(out) :: irestart,isauv,diffop,realcorr,iinterp,ipost,Nquery,Nmodes,outputlag,periolag,gaslimitedcfl
real*8, intent(out) :: Hx,Hy,cfl,coeff,tmax,St,Xmin,Xmax,Ymin,Ymax,Nmin,limiter,Dtlag,omega1,alpha_sinus
real*8, intent(out) :: alpha_x,alpha_y
character :: outdir*50
real*8 :: ratio,tmin
integer :: ttype

open(unit=100,form='formatted',file='input.file')
read(100,*) !GAUSSIAN 2D INPUT FILE
read(100,*) 
read(100,*)  !TEST CASE
read(100,*) testcase      ! test case (0=Taylor Green, 1=Frozen gas field 2= Dynamic gas field,3=sinus field,5=injection)
Nmodes=0
omega1=0
alpha_sinus=0
if (testcase.eq.4) then
  read(100,*) Nmodes
elseif (testcase.eq.-1) then
 read(100,*) alpha_x,alpha_y
else
  read(100,*) omega1,alpha_sinus  ! parameters for testcase=3
endif
read(100,*) gasfolder     ! gas folder
read(100,*) gastemplate   !
read(100,*) typegastemplate !
read(100,*) switchloop
read(100,*) 
read(100,*) !MODEL
read(100,*) model      ! -2 aniso without s12 -1=monokinetic, 0=anisotrope, 1=isotrope, 2=VISCO, 3=AXISY, 4=2PhiEASM
                       ! 11 lagrangian
read(100,*) 
read(100,*) !PHYSICAL PARAMETERS
read(100,*) force       ! activate drag force 
read(100,*) Stc         ! Critical Stokes number (need to be given if testcase~=0) 
read(100,*) ratio       ! Stokes number
if ( (testcase.eq.0).or.(testcase.eq.-1) ) then
	Stc=1.d0/(8.d0*pi)
endif
St=ratio*Stc

read(100,*) distrib        ! initial distribution 0=uniform 1= square distribution
if ( (distrib.eq.1).or.(distrib.eq.2) ) then
  read(100,*) Xmin,Xmax,Ymin,Ymax ! Xmin Xmax Ymin Ymax if distrib=1
  read(100,*) Nmin
else
  read(100,*)
  read(100,*) 
endif
read(100,*) velinit
read(100,*) Uinit,Vinit,sigmainit(1),sigmainit(2),sigmainit(3)
read(100,*)          
read(100,*) !TIME PARAMETERS
read(100,*) restart    ! restart or not
read(100,*) irestart   ! restart solution
read(100,*) ttype      ! ttype
read(100,*) Tmin       ! Tmin
read(100,*) Tmax       ! Tmax
read(100,*) nitemin    ! nitemin
read(100,*) nitemax    ! nitemax

if (ttype.eq.1) then
  Tmin=St*Tmin
  Tmax=St*Tmax
endif

read(100,*) 
read(100,*) !DOMAIN
read(100,*) Hx   ! Hx
read(100,*) Hy   ! Hy
read(100,*) Nx    ! Nx
read(100,*) Ny    ! Ny
if ( (testcase.eq.5).and.(model.ne.11)) then
  Nx=Nx+4
  Ny=Ny+4
endif
read(100,*)
read(100,*) !LAGRANGIAN PARAMETERS
read(100,*) Np
read(100,*) iinterp
read(100,*) Dtlag
read(100,*) periolag
read(100,*) outputlag
read(100,*)
read(100,*) !NUMERICAL METHODS
read(100,*) CFL    ! CFL
read(100,*) gaslimitedcfl
read(100,*) coeff  ! parameter for time step ( umax=umax(u1,u2)+coeff*sigma(1),umin=umin(u1,u2)-coeff*sigma(1) )
read(100,*) scheme      ! scheme ( 2=Flux SPlit, 3=HLL, 4=MUSCL/HLL, 5=MUSCL/Fluxsplit )
read(100,*) limiter
read(100,*) diffop      ! diffusive operator if model >1 choose between convective or diffusive operator for the stress tensor
read(100,*) realcorr      ! realizability correction on the stress tensor, automoatic if diffop=0, user-defined if diffop=1
read(100,*) mtol       ! nitemax
read(100,*) mtol_sigFEM  
read(100,*) mtol_diff
read(100,*) islim
read(100,*) alpha
read(100,*) Smax
read(100,*)
read(100,*) !OUTPUT
read(100,*) Nsauv     ! Number of output solutions
read(100,*) outdir
read(100,*)
read(100,*) !POSTPROC
read(100,*) ipost  ! yes or not
if (ipost.eq.1) then
 if ( log(real(Nx,8))/log(2.d0).ne.floor(log(real(Nx,8))/log(2.d0)) ) then
    write(*,*) 'ERROR: for segregation output, Nx and Ny must be a power of 2'
    write(*,*) 'EXIT CODE'
    STOP
  endif
endif
read(100,*) Nquery ! Number of time queries
read(100,*) Nxprojmin,Nyprojmin,Nxprojmax,Nyprojmax ! projection
close(100)
if (Nxprojmax.gt.Nx) Nxprojmax=Nx
if (Nyprojmax.gt.Ny) Nyprojmax=Ny


end subroutine read_data



subroutine initialisation(Nx,Ny,Hx,Hy,testcase,nitemin,nitemax,restart,irestart,Nsauv,Xmin,Xmax,Ymin,Ymax,distrib,    &
                          filemom,filequad,filevtk,filelag,Dx,Dy,Xcell,Ycell,uf,vf,M0,Tmin,Tmax,deltatsauv,tsauv,isauv,advection,&
                          t,ite,time,Nmin,Np,iinterp,Xp,Yp,Up,Vp,omega1,alpha,Nquery,tquery,deltatquery,iquery,Nmodes)
                 
integer, intent(in) :: Nx,Ny, testcase, nitemin,nitemax,restart,irestart,Nsauv,distrib,Nquery,Nmodes
real*8, intent(in) :: Hx,Hy,Xmin,Xmax,Ymin,Ymax,Nmin,omega1,alpha
integer, intent(out) :: isauv,advection,ite,iquery
real*8, intent(out) :: Dx,Dy,Xcell(Nx,Ny),Ycell(Nx,Ny),uf(Nx,Ny),vf(Nx,Ny),M0(6,Nx,Ny),deltatsauv,tsauv,t,deltatquery,tquery
real*8, intent(inout) :: Tmin,Tmax
character*12, intent(out) :: filemom(Nsauv),filevtk(Nsauv),filelag(Nsauv)
character*15, intent(out) :: filequad(Nsauv)
real*8, intent(out), optional :: time(nitemax)

! lagrangian
integer, intent(in) :: Np,iinterp
real*8 :: Xp(Np),Yp(Np),Up(Np),Vp(Np),A(Nmodes),C,sumk
      
                          
integer :: i,j                     
real*8 :: N,U,V,sigma(3),mom(10),sigmaf(3,Nx,Ny)


if ((testcase==1).or.(testcase.eq.2)) then
	if ( ( (Nx.eq.256).and.(Ny.eq.256) ).or.( (Nx.eq.512).and.(Ny.eq.512) )&
                  .or.( (Nx.eq.1024).and.(Ny.eq.1024) )  &
                  .or.( (Nx.eq.2048).and.(Ny.eq.2048) ) &
                  .or.( (Nx.eq.64).and.(Ny.eq.64) ) &       
                  .or.( (Nx.eq.128).and.(Ny.eq.128) ) ) then
	else
		write(*,*)'use "a power of 2" cells per direction for frozen HIT'
		return
	endif
endif


!Tmax = 4.d0   ! final time

if (testcase.eq.2) then
  open(unit=100,form='formatted',file=trim(gasfolder)//'/time.dat')
  do i=1,nitemax
    read(100,*)time(i)
  end do
  close(100)
  Tmin=time(nitemin)
  Tmax=time(nitemax)
endif



deltatsauv=(Tmax-Tmin)/Nsauv
tsauv=Tmin+deltatsauv
isauv=0

deltatquery=(Tmax-Tmin)/Nquery
tquery=Tmin+deltatquery
iquery=0

if ( (testcase.eq.5).and.(model.ne.11) ) then
  Dy = Hy/(Ny-4)
  Dx = Hx/(Nx-4) 
  do i=1,Nx
      do j=1,Ny
          Xcell(i,j) = -3.d0*Dx/2.d0 + (i-1)*Dx    
          Ycell(i,j) = -3.d0*Dy/2.d0 + (j-1)*Dy 
      enddo
  enddo
else
  Dy = Hy/Ny 
  Dx = Hx/Nx  
  do i=1,Nx
      do j=1,Ny
          Xcell(i,j) = Dx/2.d0 + (i-1)*Dx    
          Ycell(i,j) = Dy/2.d0 + (j-1)*Dy 
      enddo
  enddo
endif


call generate_filename(Nsauv,filemom,filequad,filevtk,filelag)


if ( (testcase.eq.-2).or.(testcase.eq.5) ) then
	uf = 0.d0  
	vf = 0.d0 
       include "initpersogas.inc"
elseif ( (testcase.eq.0).or.(testcase.eq.-1) ) then
        !write(*,*) 'frozen gas field: TG vortices'
	uf = 0.d0  
	vf = 0.d0  
	do i=1,Ny
	    do j=1,Nx
	        uf(j,i) =    sin(Xcell(j,i)*2.d0*pi)*cos(Ycell(j,i)*2.d0*pi)  
	        vf(j,i) =  - sin(Ycell(j,i)*2.d0*pi)*cos(Xcell(j,i)*2.d0*pi)  
	    end do
	end do
elseif (testcase.eq.1) then
        !write(*,*) 'frozen gas field: HIT'
        call readgas_dat(nitemin,Nx,Ny,Dx,Dy,uf,vf,sigmaf)
elseif (testcase.eq.2) then
        !write(*,*) 'dynamics gas field: HIT'
        call readgas_dat(nitemin,Nx,Ny,Dx,Dy,uf,vf,sigmaf)
elseif (testcase.eq.3) then
	uf = 0.d0  
	vf = 0.d0  
	do i=1,Ny
	    do j=1,Nx
	        uf(j,i) =    -sin(omega1*(Xcell(j,i)-Hx/2.d0) )
	    end do
	end do
elseif (testcase.eq.4) then
!        !write(*,*) 'frozen gas field: TG vortices'
!	uf = 0.d0  
!	vf = 0.d0
!         sumk=0.d0
!         do k=1,Nmodes
!           sumk=sumk+k**(-5.d0/3.d0)
!         end do
!         C=(4*pi-(sin(4*pi)**2/4*pi))\sumk
!         
!         do k=1,Nmodes
!          A(k)=sqrt(   (C*k**(-5.d0/3.d0)) / ( 2*pi*(k+1)-(sin(2*pi*(k+1)))**2/(2*pi*(k+1)) )   )
!         enddo
!         
!          do i=1,Ny
!              do j=1,Nx
!                do k=1,N
!                  uf(j,i) = uf(j,i) + A(k)*sin(Xcell(j,i)*pi*(k+1))*cos(Ycell(j,i)*pi*(k+1))  
!                  vf(j,i) = vf(j,i) - A(k)*sin(Ycell(j,i)*pi*(k+1))*cos(Xcell(j,i)*pi*(k+1))
!                end do
!              end do
!          end do
!        end do
endif






if (model.ne.11) then
  if (distrib.ge.0) then
    !
    ! Initializing the grid 
    M0 = 0.d0    ! 16 moments
    N = 0.d0  ! weights
    U = 0.d0   ! U velocity 
    V = 0.d0   ! V velocity
    sigma = 0.d0   ! covariances
    
    
    do j=1,Ny
        do i=1,Nx
            if (distrib.eq.0) then
                !uniform
                N=1.d0
            elseif (distrib.eq.1) then
                ! localized
                if ( (Xcell(i,j)>=Xmin).and.(Xcell(i,j)<=Xmax).and.(Ycell(i,j)>=Ymin).and.(Ycell(i,j)<=Ymax) ) then
                  N=1.d0
                else
                  N=Nmin
                endif
            elseif (distrib.eq.2) then
                ! localized
                  N=exp(-(Xcell(i,j)-Xmin)**2/2/Ymin**2)*exp(-(Ycell(i,j)-Xmax)**2/2/Ymax**2)
            endif
            if (velinit.eq.0 ) then
              V = 0.d0 
              U = 0.d0
              sigma=0.d0
            elseif (velinit.eq.1) then
              V = vf(i,j)  
              U = uf(i,j)
             ! sigma=sigmaf(:,i,j)
             elseif (velinit.eq.2) then
              V=Vinit
              U=Uinit
              sigma=sigmainit
            endif
            mom(1:10)=moments_monogauss( N, U, V, sigma )
            M0(:,i,j) = mom(1:6)        
        end do 
    end do
  else
    M0 = 0.d0    ! 16 moments
    N = 0.d0  ! weights
    U = 0.d0   ! U velocity 
    V = 0.d0   ! V velocity
    sigma = 0.d0   ! covariances
    include "initpersoliq.inc"
  endif
  
elseif (model.eq.11) then
    call generate_particles(Np,Nx,Ny,dx,dy,xcell,ycell,Hx,Hy,uf,vf,iinterp,Xp,Yp,Up,Vp,velinit,Xmin,Xmax,Ymin,Ymax,distrib)
endif

!
t = tmin
ite=nitemin
!

 
advection = 0


end subroutine








subroutine print_info(testcase,model,scheme,St,cfl,coeff,Nx,Ny,adim,Np)

integer, intent(in):: testcase,model,scheme,Nx,Ny,adim,Np
real*8 :: St,cfl,coeff


    write(*,*)'__________________________________________________________________'
    write(*,*)'                  2D Anisotropic Gaussian Quadrature              '
    if (testcase.eq.0) then
    write(*,*)'                      Taylor Green Vortices                       '
    elseif (testcase.eq.1) then
    write(*,*)'                            Frozen HIT                            '
    elseif (testcase.eq.2) then
    write(*,*)'                           Dynamic HIT                            '
    elseif (testcase.eq.3) then
    write(*,*)'                      Time-dependent sinus                        '
    endif
    write(*,*)'                                                                  '
    
    
    if (model.eq.0) then
            write(*,*)'                             ANISOTROPIC                           '
    elseif (model.eq.-1) then
            write(*,*)'                              MONOKINETIC                         '
    elseif (model.eq.1) then
            write(*,*)'                              ISOTROPIC                           '
    elseif (model.eq.2) then
            write(*,*)'                         ISOTROPIC + ACBMM-VISCO                     '
    elseif (model.eq.3) then
            write(*,*)'                         ISOTROPIC + ACBMM-AXISY                     '
    elseif (model.eq.4) then
            write(*,*)'                       ISOTROPIC + ACBMM-2-Phi-EASM                  '
    elseif (model.eq.11) then
            write(*,*)'                              LAGRANGIAN                             '        
    endif
    write(*,*)'__________________________________________________________________'
    
    if (model.ne.11) then
      write(*,*)''
      if ( scheme==2 ) then
              write(*,*) '1st order flux splitting kinetic scheme'
      elseif ( scheme==3 ) then
              write(*,*) '1st order two state HLL kinetic scheme'
      elseif ( scheme==4 ) then
              write(*,*) '2nd order MUSCL scheme with HLL flux'
      elseif ( scheme==5 ) then
              write(*,*) '2nd order MUSCL scheme with kinetic fluxes'
      endif
      write(*,'(a,e9.2)')'CFL=',cfl
      if ( scheme==2 ) then
      write(*,'(a,e9.2)')'coeff=',coeff
      endif
    elseif (model.eq.11) then
      write(*,*)'Nparticles = ',Np
    endif
    
    
    
    write(*,'(a,i4,i4)')'Ncellx,y=',Nx,Ny
    write(*,*)''
    if (testcase==0) then
    write(*,'(a,e10.3)') 'St/Stc=',St*pi*8.d0
    else
    write(*,'(a,e10.3)') 'St=',St/Stc
    endif
    
    



end subroutine





subroutine generate_filename(Nsauv,filemom,filequad,filevtk,filelag)

integer, intent(in) :: Nsauv
character :: one*1,two*2,three*3,four*4
character*12 :: filemom(0:1000),filevtk(0:1000),filelag(0:1000),temp12,temp12_2,temp12_3
character*15 :: filequad(0:1000),temp15

integer :: i

filelag(:)='lag_0000.dat'
filemom(:)='mom_0000.dat'
filequad(:)='quad_0000.dat'
filevtk(:)='mom_0000.vtk'

do i=1,Nsauv
	temp12='mom_0000.dat'
        temp12_2='mom_0000.vtk'
        temp12_3='lag_0000.dat'
	temp15='quad_0000.dat'
	if (i<10) then
		write(one,'(i1)')i
		temp12(8:8)=one
		temp15(9:9)=one
                temp12_2(8:8)=one
                temp12_3(8:8)=one
	elseif (i<100) then
		write(two,'(i2)')i
		temp12(7:8)=two
		temp15(8:9)=two
                temp12_2(7:8)=two
                temp12_3(7:8)=two
	elseif (i<1000) then
		write(three,'(i3)')i
		temp12(6:8)=three
		temp15(7:9)=three
                temp12_2(6:8)=three
                temp12_3(6:8)=three
	else
		write(four,'(i4)')i
		temp12(5:8)=four
		temp15(6:9)=four
                temp12_2(5:8)=four
                temp12_3(5:8)=four
	endif
	filemom(i)=temp12
        filevtk(i)=temp12_2
        filelag(i)=temp12_3
	filequad(i)=temp15
enddo

end subroutine generate_filename


subroutine output(Nx,Ny,Dx,Dy,St,model,Xcell,Ycell,M0,filemom,filequad,filevtk,outdir)

integer, intent(in) :: Nx,Ny,model
real*8,intent(in) :: Dx,Dy,Xcell(Nx,Ny),Ycell(Nx,Ny),M0(6,Nx,Ny),St
character*12,intent(in) :: filemom,filevtk
character*15,intent(in) :: filequad
character*50, intent(in) :: outdir

integer :: i,j
real*8 :: nout(Nx,Ny),uout(Nx,Ny),vout(Nx,Ny),sigout(3,Nx,Ny)
character*1000 :: ligne


    open(unit=100,form='formatted',file=trim(outdir)//'/'//filemom) 
    open(unit=101,form='formatted',file=trim(outdir)//'/'//filequad)
    open(unit=102,form='formatted',file=trim(outdir)//'/'//filevtk)
    
    write(102,'(A, 1X)')trim(adjustl('# vtk DataFile Version 3.0'))
    write(102,'(A, 1X)')'Exemple STRUCTURED_GRID'
    write(102,'(A, 1X)')'ASCII'
    write(102,'(A, 1X)')'DATASET STRUCTURED_GRID'
    write(102,'(A, 1X, I7, 1X, I7, 1X, A)')'DIMENSIONS',Nx,Ny,'1'
    write(102,'(A, 1X, I9, 1X, A)')'POINTS ',Nx*Ny,'float'
    
    
    do j=1,Nx
      do i=1,Ny
        write(ligne,*) Xcell(j,i),Ycell(j,i),M0(:,j,i)
        write(100,'(a)') trim(ligne) 
       ! write(100,*) Xcell(j,i),Ycell(j,i),M0(:,j,i)
        write(102,*) Xcell(j,i),Ycell(j,i),0
        call moment_inversion_2D_gaussian( M0(:,j,i), nout(j,i), uout(j,i), vout(j,i), sigout(:,j,i))
        if (model.le.1) then
        write(101,*) Xcell(j,i),Ycell(j,i),nout(j,i), uout(j,i), vout(j,i), sigout(:,j,i)
        endif
      enddo
    end do
    write(102,'(A, 1X, I9 )')'POINT_DATA',Nx*Ny
    write(102,'(A, 1X)')'SCALARS Number float'
    write(102,'(A, 1X)')'LOOKUP_TABLE default'
    do j=1,Nx
      do i=1,Ny
        write(102,*)M0(1,j,i)
      end do
    end do
    if (model.gt.1) then
        
        sigout=sigFEM(Nx,Ny,Dx,Dy,sigout,nout,uout,vout,St)
      do j=1,Nx
        do i=1,Ny
          write(ligne,*) Xcell(j,i),Ycell(j,i),nout(j,i), uout(j,i), vout(j,i), sigout(:,j,i)
          write(101,'(a)') trim(ligne) 
          !write(101,*) Xcell(j,i),Ycell(j,i),nout(j,i), uout(j,i), vout(j,i), sigout(:,j,i)
        enddo
      end do
    endif
    
   close(100) 
   close(101)
   close(102)

end subroutine output










subroutine readgas_dat(ite,Nx,Ny,dx,dy,uf,vf,sigmaf)

integer,intent(in):: ite
integer, intent(in):: Nx,Ny

real*8 :: uf(Nx,Ny),vf(Nx,Ny),u(256,256),v(256,256)
real*8, intent(out), optional :: sigmaf(3,Nx,Ny)

integer :: i,j,k,l,pos,im1,ip1,jm1,jp1,ii,jj
character :: one*1,two*2,three*3,four*4,temp*100
real*8 :: temp1,temp2,Aux(3),Auy(3),Avx(3),Avy(3),x,y,dx,dy,dxloc,dyloc,Uadd,ratio


uf=0.d0
vf=0.d0
sigmaf=0.d0

	if (typegastemplate.eq.1 ) then
            temp=trim(gasfolder)//'/'//trim(gastemplate)
            pos=len_trim(gasfolder)+1+len_trim(gastemplate)+1
            if (ite<10) then 
                    write(one,'(i1)')ite
                    temp(pos:pos)=one
            elseif (ite<100) then
                    write(two,'(i2)')ite
                    temp(pos:pos+1)=two
            elseif (ite<1000) then
                    write(three,'(i3)')ite
                    temp(pos:pos+2)=three
            else
                    write(four,'(i4)')ite
                    temp(pos:pos+3)=four
            endif
            temp=trim(temp)//'.dat'
        !temp=trim(gasfolder)//'/gas_thi2D_256_Iter0000.dat'
        elseif (typegastemplate.eq.0 ) then
            temp=trim(gasfolder)//'/'//trim(gastemplate)//'0000.dat'
            pos=len_trim(gasfolder)+1+len_trim(gastemplate)+4
            if (ite<10) then 
                    write(one,'(i1)')ite
                    temp(pos:pos)=one
            elseif (ite<100) then
                    write(two,'(i2)')ite
                    temp(pos-1:pos)=two
            elseif (ite<1000) then
                    write(three,'(i3)')ite
                    temp(pos-2:pos)=three
            else
                    write(four,'(i4)')ite
                    temp(pos-3:pos)=four
            endif
        endif
        
        
        
      ratio= real(Nx)/256.d0
      
      open(unit=100,form='formatted',file=temp)
      if (switchloop==0) then
        do i=1,256
                do j=1,256
                        read(100,*)u(i,j),v(i,j)
                end do
        end do
      elseif (switchloop==1) then
        do j=1,256
                do i=1,256
                        read(100,*)u(i,j),v(i,j)
                end do
        end do
      endif
      
      close(100)
      
      ! to avoid interpolation problems when u or v is 0. do not forget to substract Uadd at the end
      Uadd=10
      
      !u=u+Uadd
      !v=v+Uadd
      
      do i=1,256
        do j=1,256
          ip1=modulo(i+1-1,256)+1
          jp1=modulo(j+1-1,256)+1
          im1=modulo(i-1-1,256)+1
          jm1=modulo(j-1-1,256)+1
          
          !call quadratic_interpolate(u(i,j),u(im1,j),u(ip1,j),u(i,jp1),u(i,jm1),dx,dy,Aux,Auy)
          !call quadratic_interpolate(v(i,j),v(im1,j),v(ip1,j),v(i,jp1),v(i,jm1),dx,dy,Avx,Avy)
          ! beware initial deltax and deltay are set to 1
          if (ratio.ge.1) then
            ! higher number of cells required
            do k=0,int(ratio)-1
              do l=0,int(ratio)-1
                dxloc=dx/ratio
                dyloc=dy/ratio
                x=-0.5*dx+dxloc/2+k*dxloc
                y=-0.5*dy+dyloc/2+l*dyloc
                uf(int(ratio)*i-k,int(ratio)*j-l)=u(i,j)!-Uadd!(Aux(1)*x**2+Aux(2)*x+Aux(3))*(Auy(1)*y**2+Auy(2)*y+Auy(3)) -Uadd
                vf(int(ratio)*i-k,int(ratio)*j-l)=v(i,j)!-Uadd!(Avx(1)*x**2+Avx(2)*x+Avx(3))*(Avy(1)*y**2+Avy(2)*y+Avy(3)) -Uadd
                
                !sigmaf(1,ratio*i-k,ratio*j-l)= (Aux(1)**2*dx**4/80.d0+(2*Aux(1)*Aux(3)+Aux(2)**2)/12.d0*dx**2+Aux(3)**2)&
                !                              *(Auy(1)**2*dy**4/80.d0+(2*Auy(1)*Auy(3)+Auy(2)**2)/12.d0*dy**2+Auy(3)**2)&
                !                              -(Aux(1)*dx**2/12.d0+Aux(3))**2*(Auy(1)*dy**2/12.d0+Auy(3))**2
                !
                !sigmaf(2,ratio*i-k,ratio*j-l)=0.d0 
                !
                !sigmaf(3,ratio*i-k,ratio*j-l)= (Avx(1)**2*dx**4/80.d0+(2*Avx(1)*Avx(3)+Avx(2)**2)/12.d0*dx**2+Avx(3)**2)&
                !                              *(Avy(1)**2*dy**4/80.d0+(2*Avy(1)*Avy(3)+Avy(2)**2)/12.d0*dy**2+Avy(3)**2)&
                !                              -(Avx(1)*dx**2/12.d0+Avx(3))**2*(Avy(1)*dy**2/12.d0+Avy(3))**2
                
              end do
            end do
          else
            !lower number of cells required
            ii=ceiling(real(i,8)*ratio)
            jj=ceiling(real(j,8)*ratio)

            uf(ii,jj)=uf(ii,jj)+u(i,j)*ratio**2
            Vf(ii,jj)=Vf(ii,jj)+V(i,j)*ratio**2
            
          endif
          
          
        end do
      end do

        
        
        

end subroutine




subroutine postproc_query(Nx,Ny,Dx,Dy,St,model,Xcell,Ycell,M0,t,uf,vf)

integer, intent(in) :: Nx,Ny,model
real*8,intent(in) :: Dx,Dy,Xcell(Nx,Ny),Ycell(Nx,Ny),M0(6,Nx,Ny),St,t,uf(Nx,Ny),vf(Nx,Ny)

integer :: i,j,k,Nxp(100),Nyp(100),Nproj,q
real*8 :: nout(Nx,Ny),uout(Nx,Ny),vout(Nx,Ny),sigout(3,Nx,Ny)
real*8 :: Ntot,N2tot,segreg(100),GranularT(100),GranularE(100),GranularEg(100)
real*8 :: M0proj(6,Nx,Ny),ufproj(Nx,Ny),vfproj(Nx,Ny)
  
    
    segreg     = 0.d0
    GranularT  = 0.d0
    GranularE  = 0.d0
    GranularEg = 0.d0
      

    
    
    Nproj=int((dlog(real(Nxprojmax,8))-dlog(real(Nxprojmin,8)))/dlog(2.d0))+1
    q=int(dlog(real(Nxprojmin,8))/dlog(2.d0))-1
    do k=1,Nproj
      q=q+1
      Nxp(k)=2**q
      Nyp(k)=2**q
      !write(*,*)k,q,Nxp(k),Nyp(k)
      call projdata(Nx,Ny,6,M0,Nxp(k),Nyp(k),M0proj(1:6,1:Nxp(k),1:Nyp(k)))
      call projvel(Nx,Ny,uf,Nxp(k),Nyp(k),ufproj(1:Nxp(k),1:Nyp(k)))
      call projvel(Nx,Ny,vf,Nxp(k),Nyp(k),vfproj(1:Nxp(k),1:Nyp(k)))
      
      do j=1,Nxp(k)
        do i=1,Nyp(k)
          call moment_inversion_2D_gaussian( M0proj(:,j,i), nout(j,i), uout(j,i), vout(j,i), sigout(:,j,i))
        enddo
      end do
      if ((model.gt.1).and.(model.ne.11)) then
        sigout(1:3,1:Nxp(k),1:Nyp(k))=sigFEM( Nxp(k),Nyp(k),Dx,Dy,sigout(1:3,1:Nxp(k),1:Nyp(k)), &
                                              nout(1:Nxp(k),1:Nyp(k)),uout(1:Nxp(k),1:Nyp(k)),vout(1:Nxp(k),1:Nyp(k)),St)
      endif
      
      
      Ntot=0.d0
      N2tot=0.d0
      GranularT(k)  = 0.d0
      GranularE(k)  = 0.d0
      GranularEg(k) = 0.d0
      
      do j=1,Nxp(k)
        do i=1,Nyp(k)
          Ntot=Ntot+M0proj(1,j,i)/Nxp(k)/Nyp(k)
          N2tot=N2tot+M0proj(1,j,i)**2/Nxp(k)/Nyp(k)
          GranularT(k)  = GranularT(k)  + nout(j,i)*(sigout(1,j,i)+sigout(3,j,i))/2
          GranularE(k)  = GranularE(k)  + M0proj(4,j,i)+M0proj(6,j,i)
          GranularEg(k) = GranularEg(k) + M0proj(1,j,i)*ufproj(j,i)**2+M0proj(1,j,i)*vfproj(j,i)**2
        end do
      end do
      segreg(k)=N2tot/Ntot**2
      GranularT(k)=GranularT(k)/Ntot/Nxp(k)/Nyp(k)
      GranularE(k)=GranularE(k)/2/Ntot/Nxp(k)/Nyp(k)
      GranularEg(k)=GranularEg(k)/2/Ntot/Nxp(k)/Nyp(k)
    end do

    write(200,*)t,segreg(1:Nproj),GranularT(1:Nproj),GranularE(1:Nproj),GranularEg(1:Nproj)

end subroutine postproc_query





subroutine projdata(Nxin,Nyin,Nmom,Mom_in,Nxout,Nyout,Mom_out)

integer, intent(in):: Nxin,Nyin,Nxout,Nyout,Nmom

real*8, intent(in)  :: Mom_in(Nmom,Nxin,Nyin)
real*8, intent(out) :: Mom_out(Nmom,Nxout,Nyout)

integer :: i,j,k,l,pos,im1,ip1,jm1,jp1,ii,jj,m

real*8 :: temp1,temp2,x,y,dx,dy,dxloc,dyloc,ratio


Mom_out=0.d0
 
ratio= real(Nxout)/Nxin
if (ratio.ne.(real(Nyout)/Nyin)) then
    write(*,*)'projdata error: not the same ratio in each direction'
    return
end if


    do m=1,Nmom
      do i=1,Nxin
        do j=1,Nyin
          
          if (ratio.ge.1) then
            ! higher number of cells required
            do k=0,int(ratio)-1
              do l=0,int(ratio)-1
                Mom_out(m,int(ratio)*i-k,int(ratio)*j-l)=Mom_in(m,i,j)

                
              end do
            end do
          else
            !lower number of cells required
            ii=ceiling(real(i,8)*ratio)
            jj=ceiling(real(j,8)*ratio)

            Mom_out(m,ii,jj)=Mom_out(m,ii,jj)+Mom_in(m,i,j)*ratio**2

            
          endif
          
          
        end do
      end do
    end do

 end subroutine projdata

subroutine projvel(Nxin,Nyin,velin,Nxout,Nyout,velout)

integer, intent(in):: Nxin,Nyin,Nxout,Nyout

real*8, intent(in)  :: velin(Nxin,Nyin)
real*8, intent(out) :: velout(Nxout,Nyout)

integer :: i,j,k,l,pos,im1,ip1,jm1,jp1,ii,jj,m

real*8 :: temp1,temp2,x,y,dx,dy,dxloc,dyloc,ratio


velout=0.d0
 
ratio= real(Nxout)/Nxin
if (ratio.ne.(real(Nyout)/Nyin)) then
    write(*,*)'projdata error: not the same ratio in each direction'
    return
end if

      do i=1,Nxin
        do j=1,Nyin
          
          if (ratio.ge.1) then
            ! higher number of cells required
            do k=0,int(ratio)-1
              do l=0,int(ratio)-1
                velout(int(ratio)*i-k,int(ratio)*j-l)=velin(i,j)

                
              end do
            end do
          else
            !lower number of cells required
            ii=ceiling(real(i,8)*ratio)
            jj=ceiling(real(j,8)*ratio)

            velout(ii,jj)=velout(ii,jj)+velin(i,j)*ratio**2

            
          endif
          
          
        end do
      end do
    
 end subroutine projvel


end module AG2D_inout

