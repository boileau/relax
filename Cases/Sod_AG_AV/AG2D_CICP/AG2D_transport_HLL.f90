module AG2D_transport_HLL


use nr_complet
use AG2D_quadrature
use AG2D_sharedvariables

implicit none

contains

function moment_advection_2Dx_HLL( M0, order, Nx, Dx, Ny, Dy, dt,coeff,taup)
! moment_advection_2Dxy updates 6 moments in 2D due to spatial fluxes using
! x-y splitting
!
integer, intent(in) :: order, Nx, Ny
real*8, intent(in) :: M0(6,Nx,Ny), Dx, Dy, Dt, coeff,taup
real*8 :: M(6,Nx,Ny),moment_advection_2Dx_HLL(6,Nx,Ny)

integer :: i,j,k,kmom(1:6),ixflux(1:6),iyflux(1:6),im1,ip1,jm1,jp1,jj,ii
real*8 :: momminus(10,Nx,Ny), momplus(10,Nx,Ny), lambdamin(Nx+1,Ny+1), lambdamax(Nx+1,Ny+1)
real*8 :: mplus(6,Nx,Ny),mminus(6,Nx,Ny)
real*8 :: n(Nx,Ny), u(Nx,Ny), v(Nx,Ny), sigma(3,Nx,Ny)
real*8 :: umin(Nx,Ny),umax(Nx,Ny),vmin(Nx,Ny),vmax(Nx,Ny)
real*8 :: Fx(10,Nx+1,Ny),Fy(10,Nx,Ny+1),momstarx(10,Nx+1,Ny),momstary(10,Nx,Ny+1),mom(10,Nx,Ny)
![1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8]
![0 1 0 2 1 0 3 2 1 0 4 3 2 1 0 5 4 3 2 1 0 6 5 4 3 2 1 0 7 6 5 4 3 2 1 0 7 6 5 3 2 1 7 6 3 2 7 3]
![0 0 1 0 1 2 0 1 2 3 0 1 2 3 4 0 1 2 3 4 5 0 1 2 3 4 5 6 0 1 2 3 4 5 6 7 1 2 3 5 6 7 2 3 6 7 3 7]

!![1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16] 
!![0 1 0 2 1 0 3 2 1  0  4  3  1  0  4  1] 
!![0 0 1 0 1 2 0 1 2  3  0  1  3  4  1  4]
   
!!xflux
!![1 2 3 4 5 6  7  8  9 10 11 12 13 14 15 16]
!![2 4 5 7 8 9 11 12 13 14 16 17 19 20 23 26]

!!yflux
!![1 2 3 4 5  6  7  8  9 10 11 12 13 14 15 16]
!![3 5 6 8 9 10 12 13 14 15 17 18 20 21 24 27]


ixflux=(/2,4,5,7,8,9/)
iyflux=(/3,5,6,8,9,10/)

M = M0  


!
momminus = 0.d0
momplus  = 0.d0 
mom      = 0.d0
momstarx = 0.d0
Fx       = 0.d0

!
! Evaluate quadrature in x direction


do j=1,Nx
    do i=1,Ny
        call moment_inversion_2D_gaussian(M(:,j,i),n(j,i),u(j,i),v(j,i),sigma(:,j,i))  
        if ( model.le.1 ) then
	      	mom(1:10,j,i) = moments_monogauss( n(j,i), u(j,i), v(j,i), sigma(:,j,i) )
		    umin(j,i)=u(j,i)-coeff*sqrt(sigma(1,j,i))
		    umax(j,i)=u(j,i)+coeff*sqrt(sigma(1,j,i)) 
		endif		    
    end do
end do

if ( (model.gt.1).and.(diffop.eq.0) ) then
    sigma=sigFEM(Nx,Ny,dx,dy,sigma,n,u,v,taup)
	do j=1,Nx
	    do i=1,Ny 
		    mom(1:10,j,i) = moments_monogauss( n(j,i), u(j,i), v(j,i), sigma(:,j,i) )
			umin(j,i)=u(j,i)-coeff*sqrt(sigma(1,j,i)) 
			umax(j,i)=u(j,i)+coeff*sqrt(sigma(1,j,i)) 	    
	    end do
	end do    
endif


! Evaluate intermediate state
do j=1,Nx+1
    do i=1,Ny
        jm1=modulo(j-1-1,Nx)+1
        jj =modulo(j-1,Nx)+1
        jp1=modulo(j+1-1,Nx)+1
        lambdamin(j,i)=min(umin(jm1,i),umin(jj,i))
        lambdamax(j,i)=max(umax(jm1,i),umax(jj,i))
        call intermediatestate_HLL(0,lambdamin(j,i),lambdamax(j,i), mom(:,jm1,i), mom(:,jj,i),momstarx(:,j,i) )
        do k=1,6
           Fx(k,j,i)=  0.5d0*(mom(ixflux(k),jm1,i)+mom(ixflux(k),jj,i))&
                     - 0.5d0*abs(lambdamin(j,i))*(momstarx(k,j,i)-mom(k,jm1,i))&
           			 - 0.5d0*abs(lambdamax(j,i))*(mom(k,jj,i)-momstarx(k,j,i))
        end do
    end do
end do


! Evaluate new moments due to x flux
do j=1,Nx
    do i=1,Ny
        do k=1,6
	        M(k,j,i)  = mom(k,j,i)  - dt/Dx*( Fx(k,j+1,i)    -  Fx(k,j,i) )   
        end do
    end do
end do


 moment_advection_2Dx_HLL=M
 
end function

function moment_advection_2Dy_HLL( M0, order, Nx, Dx, Ny, Dy, dt,coeff,taup)
! moment_advection_2Dxy updates 6 moments in 2D due to spatial fluxes using
! x-y splitting
!
integer, intent(in) :: order, Nx, Ny
real*8, intent(in) :: M0(6,Nx,Ny), Dx, Dy, Dt, coeff,taup
real*8 :: M(6,Nx,Ny),moment_advection_2Dy_HLL(6,Nx,Ny)

integer :: i,j,k,kmom(1:6),ixflux(1:6),iyflux(1:6),im1,ip1,jm1,jp1,jj,ii
real*8 :: momminus(10,Nx,Ny), momplus(10,Nx,Ny), lambdamin(Nx+1,Ny+1), lambdamax(Nx+1,Ny+1)
real*8 :: mplus(6,Nx,Ny),mminus(6,Nx,Ny)
real*8 :: n(Nx,Ny), u(Nx,Ny), v(Nx,Ny), sigma(3,Nx,Ny)
real*8 :: umin(Nx,Ny),umax(Nx,Ny),vmin(Nx,Ny),vmax(Nx,Ny)
real*8 :: Fx(10,Nx+1,Ny),Fy(10,Nx,Ny+1),momstarx(10,Nx+1,Ny),momstary(10,Nx,Ny+1),mom(10,Nx,Ny)
![1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8]
![0 1 0 2 1 0 3 2 1 0 4 3 2 1 0 5 4 3 2 1 0 6 5 4 3 2 1 0 7 6 5 4 3 2 1 0 7 6 5 3 2 1 7 6 3 2 7 3]
![0 0 1 0 1 2 0 1 2 3 0 1 2 3 4 0 1 2 3 4 5 0 1 2 3 4 5 6 0 1 2 3 4 5 6 7 1 2 3 5 6 7 2 3 6 7 3 7]

!![1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16] 
!![0 1 0 2 1 0 3 2 1  0  4  3  1  0  4  1] 
!![0 0 1 0 1 2 0 1 2  3  0  1  3  4  1  4]
   
!!xflux
!![1 2 3 4 5 6  7  8  9 10 11 12 13 14 15 16]
!![2 4 5 7 8 9 11 12 13 14 16 17 19 20 23 26]

!!yflux
!![1 2 3 4 5  6  7  8  9 10 11 12 13 14 15 16]
!![3 5 6 8 9 10 12 13 14 15 17 18 20 21 24 27]


ixflux=(/2,4,5,7,8,9/)
iyflux=(/3,5,6,8,9,10/)

M = M0  


!
momminus = 0.d0
momplus  = 0.d0 
mom      = 0.d0
momstary = 0.d0
Fy       = 0.d0

!
! Evaluate quadrature in y direction


do j=1,Nx
    do i=1,Ny
        call moment_inversion_2D_gaussian(M(:,j,i),n(j,i),u(j,i),v(j,i),sigma(:,j,i))  
        if ( model.le.1 ) then
	      	mom(1:10,j,i) = moments_monogauss( n(j,i), u(j,i), v(j,i), sigma(:,j,i) )
		    vmin(j,i)=v(j,i)-coeff*sqrt(sigma(3,j,i))
		    vmax(j,i)=v(j,i)+coeff*sqrt(sigma(3,j,i)) 
		endif		    
    end do
end do

if ( (model.gt.1).and.(diffop.eq.0) ) then
    sigma=sigFEM(Nx,Ny,dx,dy,sigma,n,u,v,taup)
	do j=1,Nx
	    do i=1,Ny 
		    mom(1:10,j,i) = moments_monogauss( n(j,i), u(j,i), v(j,i), sigma(:,j,i) )
			vmin(j,i)=v(j,i)-coeff*sqrt(sigma(3,j,i)) 
			vmax(j,i)=v(j,i)+coeff*sqrt(sigma(3,j,i)) 	    
	    end do
	end do    
endif






!mom=momplus+momminus

! Evaluate intermediate state
do j=1,Nx
    do i=1,Ny+1
        im1=modulo(i-1-1,Ny)+1
        ii =modulo(i-1,Ny)+1
        ip1=modulo(i+1-1,Ny)+1
        lambdamin(j,i)=min(vmin(j,im1),vmin(j,ii))
        lambdamax(j,i)=max(vmax(j,im1),vmax(j,ii))
        call intermediatestate_HLL(1,lambdamin(j,i),lambdamax(j,i), mom(:,j,im1), mom(:,j,ii), momstary(:,j,i) )
        do k=1,6
           Fy(k,j,i)=  0.5d0*(mom(iyflux(k),j,im1)+mom(iyflux(k),j,ii))&
                     - 0.5d0*abs(lambdamin(j,i))*(momstary(k,j,i)-mom(k,j,im1))&
           			 - 0.5d0*abs(lambdamax(j,i))*(mom(k,j,ii)-momstary(k,j,i))
        end do
    end do
end do


! Evaluate new moments due to x flux
do j=1,Nx
    do i=1,Ny
        do k=1,6
	        M(k,j,i)  = mom(k,j,i)  - dt/Dy*( Fy(k,j,i+1)    -  Fy(k,j,i) )   
        end do
    end do
end do

 moment_advection_2Dy_HLL=M
 
end function






subroutine intermediatestate_HLL(direction,ll,lr,moml,momr,momstar)

integer, intent(in) :: direction
real*8, intent(in) :: ll,lr,moml(10),momr(10)
real*8, intent(out) :: momstar(6)

integer :: k,ixflux(6),iyflux(6)
ixflux=(/2,4,5,7,8,9/)
iyflux=(/3,5,6,8,9,10/)

if ( abs(ll-lr).gt.1.d-12 ) then
	if (direction.eq.0) then
		!momstar x
		do k=1,6
			momstar(k)= ( lr*momr(k)-ll*moml(k) +moml(ixflux(k))-momr(ixflux(k)) )  / (lr-ll)
		enddo
	elseif (direction.eq.1) then
		!momstar x
		do k=1,6
			momstar(k)= ( lr*momr(k)-ll*moml(k) +moml(iyflux(k))-momr(iyflux(k)) )  / (lr-ll)
		enddo
	endif
else 
	momstar=0.d0
endif

end subroutine





end module AG2D_transport_HLL