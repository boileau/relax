module AG2D_transport_fluxsplit


use nr_complet
use AG2D_quadrature
use flux_erf
use AG2D_sharedvariables

implicit none
real*8 :: dtntol
parameter(dtntol=0.d-10)
contains

function moment_advection_2Dx_fluxsplit( M0, order, Nx, Dx, Ny, Dy, dt,St)
! moment_advection_2Dxy updates 15 moments in 2D due to spatial fluxes using
! x-y splitting
!
integer, intent(in) :: order, Nx, Ny
real*8, intent(in) :: M0(6,Nx,Ny), Dx, Dy, Dt,St
real*8 :: M(6,Nx,Ny),Mtemp(6,Nx,Ny),moment_advection_2Dx_fluxsplit(6,Nx,Ny)

integer :: i,j,k,kmom(6),ixflux(6),iyflux(6),im1,ip1,jm1,jp1
real*8 :: momminus(10,Nx,Ny), momplus(10,Nx,Ny)
real*8 :: mplus(6,Nx,Ny),mminus(6,Nx,Ny)
real*8 :: n(Nx,Ny), u(Nx,Ny), v(Nx,Ny), sigma(3,Nx,Ny)

![1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8]
![0 1 0 2 1 0 3 2 1 0 4 3 2 1 0 5 4 3 2 1 0 6 5 4 3 2 1 0 7 6 5 4 3 2 1 0 7 6 5 3 2 1 7 6 3 2 7 3]
![0 0 1 0 1 2 0 1 2 3 0 1 2 3 4 0 1 2 3 4 5 0 1 2 3 4 5 6 0 1 2 3 4 5 6 7 1 2 3 5 6 7 2 3 6 7 3 7]

!![1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16] 
!![0 1 0 2 1 0 3 2 1  0  4  3  1  0  4  1] 
!![0 0 1 0 1 2 0 1 2  3  0  1  3  4  1  4]
   
!!xflux
!![1 2 3 4 5 6  7  8  9 10 11 12 13 14 15 16]
!![2 4 5 7 8 9 11 12 13 14 16 17 19 20 23 26]

!!yflux
!![1 2 3 4 5  6  7  8  9 10 11 12 13 14 15 16]
!![3 5 6 8 9 10 12 13 14 15 17 18 20 21 24 27]



kmom=(/1,2,3,4,5,6/)
ixflux=(/2,4,5,7,8,9/)
iyflux=(/3,5,6,8,9,10/)

M = M0  


!
momminus = 0.d0
momplus  = 0.d0 

!
! Evaluate quadrature in x direction


do j=1,Nx
    do i=1,Ny
        call moment_inversion_2D_gaussian(M(:,j,i),n(j,i),u(j,i),v(j,i),sigma(:,j,i))
        if (model.le.1) then
        call moment_sigma10_U_plusminus( n(j,i), u(j,i), v(j,i), sigma(:,j,i), momplus(:,j,i), momminus(:,j,i) )
        endif
        !write(*,*)momplus(3,j,i)
    end do
end do
if (model.gt.1) then
  sigma=sigFEM(Nx,Ny,Dx,Dy,sigma,n,u,v,St)
  do j=1,Nx
      do i=1,Ny
          call moment_sigma10_U_plusminus( n(j,i), u(j,i), v(j,i), sigma(:,j,i), momplus(:,j,i), momminus(:,j,i) )
      end do
  end do
endif


! Evaluate new moments due to x flux
do j=1,Nx
    do i=1,Ny
        jm1=modulo(j-1-1,Nx)+1
        jp1=modulo(j+1-1,Nx)+1
        do k=1,6
	        mplus(k,j,i)  = momplus(kmom(k),j,i)  - dt/Dx*( momplus(ixflux(k),j,i)    -  momplus(ixflux(k),jm1,i) ) 
	        mminus(k,j,i) = momminus(kmom(k),j,i) - dt/Dx*( momminus(ixflux(k),jp1,i) -  momminus(ixflux(k),j,i)  )         
	        M(k,j,i)      = mplus(k,j,i) + mminus(k,j,i)  
        end do
    end do
end do


moment_advection_2Dx_fluxsplit=M
end function




function moment_advection_2Dy_fluxsplit( M0, order, Nx, Dx, Ny, Dy, dt,St)
! moment_advection_2Dxy updates 15 moments in 2D due to spatial fluxes using
! x-y splitting
!
integer, intent(in) :: order, Nx, Ny
real*8, intent(in) :: M0(6,Nx,Ny), Dx, Dy, Dt,St
real*8 :: M(6,Nx,Ny),Mtemp(6,Nx,Ny),moment_advection_2Dy_fluxsplit(6,Nx,Ny)

integer :: i,j,k,kmom(6),ixflux(6),iyflux(6),im1,ip1,jm1,jp1
real*8 :: momminus(10,Nx,Ny), momplus(10,Nx,Ny)
real*8 :: mplus(6,Nx,Ny),mminus(6,Nx,Ny)
real*8 :: n(Nx,Ny), u(Nx,Ny), v(Nx,Ny), sigma(3,Nx,Ny)

![1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8]
![0 1 0 2 1 0 3 2 1 0 4 3 2 1 0 5 4 3 2 1 0 6 5 4 3 2 1 0 7 6 5 4 3 2 1 0 7 6 5 3 2 1 7 6 3 2 7 3]
![0 0 1 0 1 2 0 1 2 3 0 1 2 3 4 0 1 2 3 4 5 0 1 2 3 4 5 6 0 1 2 3 4 5 6 7 1 2 3 5 6 7 2 3 6 7 3 7]

!![1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16] 
!![0 1 0 2 1 0 3 2 1  0  4  3  1  0  4  1] 
!![0 0 1 0 1 2 0 1 2  3  0  1  3  4  1  4]
   
!!xflux
!![1 2 3 4 5 6  7  8  9 10 11 12 13 14 15 16]
!![2 4 5 7 8 9 11 12 13 14 16 17 19 20 23 26]

!!yflux
!![1 2 3 4 5  6  7  8  9 10 11 12 13 14 15 16]
!![3 5 6 8 9 10 12 13 14 15 17 18 20 21 24 27]



kmom=(/1,2,3,4,5,6/)
ixflux=(/2,4,5,7,8,9/)
iyflux=(/3,5,6,8,9,10/)

M = M0  

momminus = 0.d0 
momplus = 0.d0
mplus=0.d0
mminus=0.d0
n=0.d0
u=0.d0
v=0.d0
sigma=0.d0    


!
! Evaluate quadrature in y direction (from partially updated moments)
do j=1,Nx
    do i=1,Ny
        call moment_inversion_2D_gaussian(M(:,j,i),n(j,i),u(j,i),v(j,i),sigma(:,j,i))
        if (model.le.1) then
        call moment_sigma10_V_plusminus( n(j,i), u(j,i), v(j,i), sigma(:,j,i), momplus(:,j,i), momminus(:,j,i) )
        endif
        !write(*,*)momplus(3,j,i)
    end do
end do
if (model.gt.1) then
  sigma=sigFEM(Nx,Ny,Dx,Dy,sigma,n,u,v,St)
  do j=1,Nx
      do i=1,Ny
          call moment_sigma10_V_plusminus( n(j,i), u(j,i), v(j,i), sigma(:,j,i), momplus(:,j,i), momminus(:,j,i) )
      end do
  end do
endif



do j=1,Nx
    do i=1,Ny
        im1=modulo(i-1-1,Ny)+1
        ip1=modulo(i+1-1,Ny)+1
        do k=1,6
	        mplus(k,j,i)=momplus(kmom(k),j,i)    - dt/Dy*(momplus(iyflux(k),j,i)-momplus(iyflux(k),j,im1)) 
	        mminus(k,j,i)=momminus(kmom(k),j,i)  - dt/Dy*(momminus(iyflux(k),j,ip1)-momminus(iyflux(k),j,i))         
	        M(k,j,i)= mplus(k,j,i) + mminus(k,j,i)  
        end do
    end do
end do



moment_advection_2Dy_fluxsplit=M
end function


end module AG2D_transport_fluxsplit