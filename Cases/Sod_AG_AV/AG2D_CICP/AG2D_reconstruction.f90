module AG2D_reconstruction


implicit none
!real*8 :: dtntol
!parameter(dtntol=0.d-10)
contains

!---------------------------------------------------------------------------------------!
!!!! AV blindee
!!!! -bonne condition sur le determinant
!!!! -valeurs barrées ok pour tout le monde
!!!! -ok sur les vitesses
!!!! -pente itérative pour les sigmas
!---------------------------------------------------------------------------------------!
subroutine recons_O2(vp,mcg,mcd,Dx,Dt,limiter)
 real*8 :: Dx,Dt
 real*8,intent(in) :: vp(1:3,1:6),limiter
 real*8 :: vr(1:6),pent(1:6),pente_g,pente_d,pente_c
 real*8 :: mcg(1:6),mcd(1:6)
 real*8 :: corr
 integer :: k,l(6),m(6),n(6)
 integer :: i,aie
 real*8 :: XX,det,temp1,temp2,gamma,beta(10)

 l=(/ 0, 0, 0, 2, 2, 3 /)
 m=(/ 0, 0, 0, 2, 3, 3 /)
 n=(/ 0, 4, 6, 0, 0, 0 /)

  
    mcg=0.d0
    mcd=0.d0
    vr=0.d0
    
    do k=1,10
     beta(k)=1d0-0.1d0*k
    end do
 
 
    ! V primitives reconstruites 2*(6)
    ! reconstruction lineaire : VPk(x) = vr(k) + pent(k)*x
    if (vp(2,1).lt.1.d-10) then
      mcg(1:6)=vp(2,1:6)
      mcd(1:6)=vp(2,1:6)
      pent=0.d0
      return
    endif
    
    pent=0.d0
    
    !!!!!! densite
    vr(1)=vp(2,1)
    pent(1)=pente_limitee_n(vp(1,1),vp(2,1),vp(3,1),Dx,limiter)
    corr=-pent(1)/vr(1)*Dx**2/12D0

    temp2=Dx**2/12*(1.d0+pent(1)**2/vp(2,1)**2*Dx**2/12.d0)

    !!!!!! vitesse k
   do k=2,3
    pente_g=(vp(2,k)-vp(1,k))/(Dx-2D0*corr)
    pente_d=(vp(3,k)-vp(2,k))/(Dx+2D0*corr)
    pente_c=(pente_g+pente_d)/2.d0
    !pent(k)=sign(5D-1,vp(3,k)-vp(2,k))+sign(5D-1,vp(2,k)-vp(1,k))
    !pent(k)=pent(k)*min(DABS(pente_g),DABS(pente_d),1/Dt,  &
    !                 DSQRT(vp(2,n(k))/temp2) )
                     
    pent(k)=minmodg(limiter*pente_g,pente_c,limiter*pente_d)          
    pent(k)=sign(1.d0,pent(k))*min(dabs(pent(k)),dabs(DSQRT(vp(2,n(k)))/temp2),1/Dt)                 
                     
                     
   end do
    
    
    
    
   !on verifie que les pentes ne degommeront pas le sigma12
   temp1=vp(2,4)*pent(3)**2+vp(2,6)*pent(2)**2-2.d0*vp(2,5)*pent(3)*pent(2)
   
   if (temp1.le.1.d-15) then
      do k=2,3
         vr(k)=vp(2,k)+pent(k)*corr
      end do
   else
      det=vp(2,4)*vp(2,6)-vp(2,5)**2+1.d-15
      gamma=sqrt(det/temp2/temp1)
      !write(*,*)'gamma=',gamma,temp1,temp2,(vp(2,4)*vp(2,6)-vp(2,5)**2)
      if (gamma>1.d0) gamma=1.d0 
      do k=2,3
        pent(k)=gamma*pent(k)
        vr(k)=vp(2,k)+pent(k)*corr
      end do
   endif
   

   aie=0
100 k=0

  ! SIGMA11 et SIGMA22
    !!!!!! sigma_l**2    attention le carré est deja dans le vecteur vp(4:6)   
   do i=1,2
    k=4+2*(i-1)
    vr(k)=vp(2,k) + pent(l(k))*pent(m(k))*temp2
    if (vr(k).lt.-1D-15) write(*,*) 'Probleme vr',k,vr(k)
    vr(k)=max(vr(k),0D0)
    pente_g=(vr(k)-vp(1,k))/(Dx-2D0*corr)
    pente_d=(vp(3,k)-vr(k))/(Dx+2D0*corr)
    pent(k)=sign(5D-1,vp(2,k)-vp(1,k))+sign(5D-1,vp(3,k)-vp(2,k))
    pent(k)=pent(k)*min(DABS(pente_g),DABS(pente_d),DABS(vr(k)/(-corr+5D-1*Dx)),DABS(vr(k)/(-corr-5D-1*Dx)))
    !pent(k)=0.d0
    if (aie.gt.0)then
      if (aie.gt.10) then
      pent(k)=0.d0
      else
      pent(k)=beta(aie)*pent(k)
      endif
    endif
    
    vr(k)=vr(k)+pent(k)*corr
   end do
   
   
    do k=1,6
       mcg(k)=vr(k) - pent(k)*Dx*5D-1
       mcd(k)=vr(k) + pent(k)*Dx*5D-1 
    end do


   !SIGMA 12
   k=5
    vr(k)=vp(2,k) + pent(l(k))*pent(m(k))*temp2

    pente_g=(vr(k)-vp(1,k))/(Dx-2D0*corr)
    pente_d=(vp(3,k)-vr(k))/(Dx+2D0*corr)
    pent(k)=sign(5D-1,vp(2,k)-vp(1,k))+sign(5D-1,vp(3,k)-vp(2,k))
    pent(k)=pent(k)*min(DABS(pente_g),DABS(pente_d))
    !pent(k)=0.d0
    if (aie.gt.0)then
      if (aie.gt.10) then
      pent(k)=0.d0
      else
      pent(k)=beta(aie)*pent(k)
      endif
    endif
    vr(k)=vr(k)+pent(k)*corr
    mcg(k)=vr(k) - pent(k)*Dx*5D-1
    mcd(k)=vr(k) + pent(k)*Dx*5D-1 
    if (aie.eq.10) then
       
    elseif ( (mcg(4)*mcg(6)-mcg(5)**2.lt.-1D-15).or.(mcd(4)*mcd(6)-mcd(5)**2.lt.-1D-15) ) then
       aie=aie+1
       go to 100
    end if
    
    !if (aie.eq.10) write(*,*),'aie = 10'


   !!! Reconstruction MC 2*(6)
    do k=1,6
       mcg(k)=vr(k) - pent(k)*Dx*5D-1
       mcd(k)=vr(k) + pent(k)*Dx*5D-1 
    end do

    
    
    ! verification ultime de la realisabilité
    
    if (mcg(4).lt.-1D-15) write(*,*) 'Probleme etat gauche, s11=',mcg(4)
    if (mcg(6).lt.-1D-15) write(*,*) 'Probleme etat gauche, s22=',mcg(6)
    if (mcd(4).lt.-1D-15) write(*,*) 'Probleme etat droite, s11=',mcd(4)
    if (mcd(6).lt.-1D-15) write(*,*) 'Probleme etat droite, s22=',mcd(6)
    
    if (mcg(4)*mcg(6)-mcg(5)**2.lt.-1D-15) then
      write(*,*) 'Probleme etat gauche, det=',mcg(4)*mcg(6)-mcg(5)**2,vp(2,4)*vp(2,6)-vp(2,5)**2
      write(*,*)  pent
    endif
    if (mcd(4)*mcd(6)-mcd(5)**2.lt.-1D-15) then
      write(*,*) 'Probleme etat droite, det=',mcd(4)*mcd(6)-mcd(5)**2,vp(2,4)*vp(2,6)-vp(2,5)**2
      write(*,*)  pent
    endif
    
    
    mcg(4)=max(0.d0,mcg(4))
    mcg(6)=max(0.d0,mcg(6))
    !mcg(5)=sqrt( max(mcg(4)*mcg(6),mcg(5)**2) )
    
    mcd(4)=max(0.d0,mcd(4))
    mcd(6)=max(0.d0,mcd(6))
    !mcd(5)=sqrt( max(mcd(4)*mcd(6),mcd(5)**2) )
    
 
    
end subroutine

!---------------------------------------------------------------------------------------!
!!!! FD entropies
subroutine recons_O2_old1(vp,mcg,mcd,Dx,Dt)
 real*8 :: Dx,Dt
 real*8,intent(in) :: vp(1:3,1:6)
 real*8 :: vr(1:6),pent(1:6),pente_g,pente_d
 real*8 :: mcg(1:6),mcd(1:6)
 real*8 :: corr
 integer :: k,l(6),m(6),n(6)
 integer :: i
 real*8 :: XX,det
 real*8 :: entropie(1:3,1:3)
 real*8 :: entropie_g(1:3),entropie_d(1:3)
 real*8 :: entrop_min=1D-10

 l=(/ 0, 0, 0, 2, 2, 3 /)
 m=(/ 0, 0, 0, 2, 3, 3 /)
 n=(/ 0, 4, 6, 0, 0, 0 /)

    ! V primitives reconstruites 2*(6)
    ! reconstruction lineaire : VPk(x) = vr(k) + pent(k)*x
  
    !!!!!! densite
    vr(1)=vp(2,1)
    pent(1)=pente_limitee(vp(1,1),vp(2,1),vp(3,1),Dx)
    corr=-pent(1)/vr(1)*Dx**2/12D0

    !!!!!! vitesse k
   do k=2,3
    pente_g=(vp(2,k)-vp(1,k))/(Dx-2D0*corr)
    pente_d=(vp(3,k)-vp(2,k))/(Dx+2D0*corr)
    pent(k)=sign(5D-1,vp(3,k)-vp(2,k))+sign(5D-1,vp(2,k)-vp(1,k))
    pent(k)=pent(k)*min(DABS(pente_g),DABS(pente_d),1/Dt)  
    vr(k)=vp(2,k)+pent(k)*corr
   end do

   do i=1,3
      entropie(i,1)=max(vp(i,4)/vp(i,1)**2,entrop_min)
      entropie(i,2)=max((DSQRT(vp(i,4)*vp(i,6)) - vp(i,5))/vp(i,1)**2,entrop_min)
      entropie(i,3)=max(vp(i,6)/vp(i,1)**2,entrop_min)
   end do
   do k=1,3
      pente_g=(entropie(2,k)-entropie(1,k))/Dx
      pente_d=(entropie(3,k)-entropie(2,k))/Dx
      pent(k)=sign(5D-1,pente_g)+sign(5D-1,pente_d)
      pent(k)=pent(k)*min(DABS(pente_g),DABS(pente_d),2D0*(entropie(2,k)-entrop_min)/Dx) 
      entropie_g(k)=entropie(2,k)-pent(k)*Dx/2D0
      entropie_d(k)=entropie(2,k)+pent(k)*Dx/2D0
   end do

  !!! Reconstruction MC 2*(6)
    do k=1,3
       mcg(k)=vr(k) - pent(k)*Dx*5D-1
       mcd(k)=vr(k) + pent(k)*Dx*5D-1
    end do
    mcg(4)=entropie_g(1)*mcg(1)**2
    mcd(4)=entropie_d(1)*mcd(1)**2
    mcg(6)=entropie_g(3)*mcg(1)**2
    mcd(6)=entropie_d(3)*mcd(1)**2
    mcg(5)=DSQRT(mcg(4)*mcg(6))-entropie_g(2)*mcg(1)**2
    mcd(5)=DSQRT(mcd(4)*mcd(6))-entropie_d(2)*mcd(1)**2

    !do k=1,6
    !   mcg(k)=vp(2,k)
    !   mcd(k)=vp(2,k)
    !end do

    if (mcg(4).lt.-1D-15) write(*,*) 'Probleme etat gauche, s11=',mcg(4)
    if (mcg(6).lt.-1D-15) write(*,*) 'Probleme etat gauche, s22=',mcg(6)
    if (mcd(4).lt.-1D-15) write(*,*) 'Probleme etat droite, s11=',mcd(4)
    if (mcd(6).lt.-1D-15) write(*,*) 'Probleme etat droite, s22=',mcd(6)
    !write(*,*) '-                                                       -'

    det=DSQRT(mcg(4)*mcg(6))-mcg(5)
    if (det.lt.0D0) then 
       write(*,*) 'Probleme etat gauche, det=',det,entropie_g(:)
       write(*,*) '1',entropie(1,1),entropie_g(1),entropie(2,1),entropie_d(1),entropie(3,1)
       write(*,*) '2',entropie(1,2),entropie_g(2),entropie(2,2),entropie_d(3),entropie(3,3)
       write(*,*) '3',entropie(1,3),entropie_g(3),entropie(2,3),entropie_d(3),entropie(3,3)
    end if
    det=DSQRT(mcd(4)*mcd(6))-mcd(5)
    if (det.lt.0D0) then
       write(*,*) 'Probleme etat droite, det=',det,entropie_d(:)
       write(*,*) '1',entropie(1,1),entropie_g(1),entropie(2,1),entropie_d(1),entropie(3,1)
       write(*,*) '2',entropie(1,2),entropie_g(2),entropie(2,2),entropie_d(3),entropie(3,3)
       write(*,*) '3',entropie(1,3),entropie_g(3),entropie(2,3),entropie_d(3),entropie(3,3)
    end if
    return

    !if (vp(2,4).gt.0.1d0) then
    !if (vp(2,5).gt.0.005d0) then
    !if (vp(2,6).gt.0.15d0) then
    if ((DABS(vp(2,1)).gt.1.5D0*DABS(vp(1,1))).and.(DABS(vp(3,1)).gt.1.5D0*DABS(vp(2,1)))) then
        write(10,*) -Dx,(vp(1,:)+vp(2,:))/2D0,vp(1,:)
        write(10,*) -Dx/2D0,(vp(1,:)+vp(2,:))/2D0,vp(1,:)
        do i=0,20
           xx=Dx*(i-10D0)/20D0
           write(10,*) xx,vr(:) + pent(:)*XX,vp(2,:)
        end do       
        write(10,*) Dx/2D0,(vp(3,:)+vp(2,:))/2D0,vp(3,:)
        write(10,*) Dx,(vp(3,:)+vp(2,:))/2D0,vp(3,:) 
        stop
    end if    

end subroutine
!----------------------------------------------------------------------------------------!
!!!! FD sigma carres
subroutine recons_O2_fd(vp,mcg,mcd,Dx,Dt)
 real*8 :: Dx,Dt
 real*8,intent(in) :: vp(1:3,1:6)
 real*8 :: vr(1:6),pent(1:6),pente_g,pente_d
 real*8 :: mcg(1:6),mcd(1:6)
 real*8 :: corr
 integer :: k,l(6),m(6),n(6)
 integer :: i
 real*8 :: XX,det

 l=(/ 0, 0, 0, 2, 2, 3 /)
 m=(/ 0, 0, 0, 2, 3, 3 /)
 n=(/ 0, 4, 6, 0, 0, 0 /)

    ! V primitives reconstruites 2*(6)
    ! reconstruction lineaire : VPk(x) = vr(k) + pent(k)*x
  
    !!!!!! densite
    vr(1)=vp(2,1)
    pent(1)=pente_limitee(vp(1,1),vp(2,1),vp(3,1),Dx)
    corr=-pent(1)/vr(1)*Dx**2/12D0

    !!!!!! vitesse k
   do k=2,3
    pente_g=(vp(2,k)-vp(1,k))/(Dx-2D0*corr)
    pente_d=(vp(3,k)-vp(2,k))/(Dx+2D0*corr)
    pent(k)=sign(5D-1,vp(3,k)-vp(2,k))+sign(5D-1,vp(2,k)-vp(1,k))
    pent(k)=pent(k)*min(DABS(pente_g),DABS(pente_d),1/Dt ,  &
                     DSQRT(vp(2,n(k))/( Dx**2/12D0 - corr**2 )) )    
                     !1/Dx*DABS(DSQRT(-12D0*vp(2,n(k))/( Dx**2/12D0*(pent(1)/vr(1))**2 - 1D0 )))) 
    vr(k)=vp(2,k)+pent(k)*corr
   end do

   do i=1,2
    k=4+2*(i-1)
    !vr(k)=vp(2,k) + vp(2,l(k))*vp(2,m(k))
    !vr(k)=vr(k)   - vr(l(k))*vr(m(k))
    !vr(k)=vr(k)   - pent(l(k))*pent(m(k))*Dx**2/12D0
    !vr(k)=vr(k)   + corr*(vr(l(k))*pent(m(k)) + vr(m(k))*pent(l(k)))
    vr(k)=vp(2,k) + pent(l(k))*pent(m(k))*( corr**2 - Dx**2/12D0 )
    if (vr(k).lt.-1D-15) write(*,*) 'Probleme vr',k,vr(k)
    vr(k)=max(vr(k),0D0)
    pente_g=(vr(k)-vp(1,k))/(Dx-2D0*corr)
    pente_d=(vp(3,k)-vr(k))/(Dx+2D0*corr)
    pent(k)=sign(5D-1,vp(2,k)-vp(1,k))+sign(5D-1,vp(3,k)-vp(2,k))
    pent(k)=pent(k)*min(DABS(pente_g),DABS(pente_d),DABS(vr(k)/(-corr+5D-1*Dx)),DABS(vr(k)/(-corr-5D-1*Dx)))
    !pent(k)=0D0
    vr(k)=vr(k)+pent(k)*corr
   end do
   k=5
    vr(k)=vp(2,k) + pent(l(k))*pent(m(k))*( corr**2 - Dx**2/12D0 )
    pente_g=(vr(k)-vp(1,k))/(Dx-2D0*corr)
    pente_d=(vp(3,k)-vr(k))/(Dx+2D0*corr)
    pent(k)=sign(5D-1,vp(2,k)-vp(1,k))+sign(5D-1,vp(3,k)-vp(2,k))
    pent(k)=pent(k)*min(DABS(pente_g),DABS(pente_d))
    vr(k)=vr(k)+pent(k)*corr
    !pent(k)=0D0
    !vr(k)=0D0

   !!! Reconstruction MC 2*(6)
    do k=1,6
       mcg(k)=vr(k) - pent(k)*Dx*5D-1
       mcd(k)=vr(k) + pent(k)*Dx*5D-1
    end do

    !return

    !do k=1,6
    !   mcg(k)=vp(2,k)
    !   mcd(k)=vp(2,k)
    !end do

    !if (vr(4).lt.-1D-15) write(*,*) 'Probleme vr4=',vr(4)
    !if (vr(6).lt.-1D-15) write(*,*) 'Probleme vr6=',vr(6)
    if (mcg(4).lt.-1D-15) write(*,*) 'Probleme etat gauche, s11=',mcg(4)
    if (mcg(6).lt.-1D-15) write(*,*) 'Probleme etat gauche, s22=',mcg(6)
    if (mcd(4).lt.-1D-15) write(*,*) 'Probleme etat droite, s11=',mcd(4)
    if (mcd(6).lt.-1D-15) write(*,*) 'Probleme etat droite, s22=',mcd(6)
    !write(*,*) '-                                                       -'
    det=DSQRT(vr(4)*vr(6))-vr(5)
    if (det.lt.-1D-4) write(*,*) 'Probleme etat barre , det=',det
    det=DSQRT(mcg(4)*mcg(6))-mcg(5)
    if (det.lt.-1D-4) write(*,*) 'Probleme etat gauche, det=',det
    det=DSQRT(mcd(4)*mcd(6))-mcd(5)
    if (det.lt.-1D-4) write(*,*) 'Probleme etat droite, det=',det

    return

    !if (vp(2,4).gt.0.1d0) then
    !if (vp(2,5).gt.0.005d0) then
    !if (vp(2,6).gt.0.15d0) then
    if ((DABS(vp(2,1)).gt.1.5D0*DABS(vp(1,1))).and.(DABS(vp(3,1)).gt.1.5D0*DABS(vp(2,1)))) then
        write(10,*) -Dx,(vp(1,:)+vp(2,:))/2D0,vp(1,:)
        write(10,*) -Dx/2D0,(vp(1,:)+vp(2,:))/2D0,vp(1,:)
        do i=0,20
           xx=Dx*(i-10D0)/20D0
           write(10,*) xx,vr(:) + pent(:)*XX,vp(2,:)
        end do       
        write(10,*) Dx/2D0,(vp(3,:)+vp(2,:))/2D0,vp(3,:)
        write(10,*) Dx,(vp(3,:)+vp(2,:))/2D0,vp(3,:) 
        stop
    end if    

end subroutine
!---------------------------------------------------------------------------------------!
function pente_limitee(vl,vm,vr,h)

real*8, intent(in) :: vl, vm, vr, h
real*8 :: p, pente_limitee

real*8 :: pl, pr
pl = (vm - vl)/h
pr = (vr - vm)/h
p  = minmod(pl, pr)

pente_limitee=p
end function

function pente_limitee_n(vl,vm,vr,h,limiter)

real*8, intent(in) :: vl, vm, vr, h, limiter
real*8 :: p, pente_limitee_n

real*8 :: pl, pr,pc,r,RR
pl = (vm - vl)/h
pr = (vr - vm)/h

pc=(pl+pr)/2

p=minmodg(limiter*pl,pc,limiter*pr)
!p=minmod(limiter*pl,limiter*pr)
!p=0.d0


pente_limitee_n=p

end function

function minmod(a,b)

real*8, intent(in) :: a, b
real*8 :: c, minmod

real*8 :: p, r
p = a*b 
r = sign(0.5d0,p) + 0.5d0  
c = sign(r,a) * min(dabs(a),dabs(b))
 
minmod=c

end function

function minmodg(a,b,c)

real*8, intent(in) :: a, b,c
real*8 ::  minmodg

real*8 :: p, r

if ( (sign(1.d0,a).eq.sign(1.d0,b)).and.(sign(1.d0,b).eq.sign(1.d0,c)) ) then
  if (sign(1.d0,a).eq.1.d0) then
    minmodg=min(a,b,c)
  else
    minmodg=max(a,b,c)
  endif
else
  minmodg=0.d0
endif


end function
!---------------------------------------------------------------------------------------!

end module AG2D_reconstruction