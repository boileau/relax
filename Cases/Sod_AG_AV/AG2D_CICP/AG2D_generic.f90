module AG2D_generic


use AG2D_quadrature

implicit none

contains

subroutine timestepsize( M0,Nx,Dx,Ny,Dy,cfl,coeff, Dt, Umaxg, Vmaxg,taup  )
 !timestepsize  HLLC Aymeric
 !  
integer, intent(in) :: Nx, Ny
real*8, intent(in) :: M0(6,Nx,Ny), Dx, Dy, cfl, coeff,taup,Umaxg,Vmaxg
real*8, intent(out) :: Dt

integer :: i,j,k
real*8 :: Vmax,Umax,maxN
real*8 :: N1(Nx,Ny),U1(Nx,Ny),V1(Nx,Ny),sigma1(3,Nx,Ny),mom(10)
real*8 :: umin1,umax1,vmin1,vmax1
 
Vmax = 0 
Umax = 0
!Vmaxg = 0  
!Umaxg = 0  
 ! 
do j=1,Nx
    do i=1,Ny
        call moment_inversion_2D_gaussian(M0(:,j,i),N1(j,i),U1(j,i),V1(j,i),sigma1(:,j,i))
    end do
end do
        
if ( (model.gt.1).and.(diffop.eq.0) )then
  sigma1=sigFEM(Nx,Ny,Dx,Dy,sigma1,N1,U1,V1,taup) 
endif
do j=1,Nx
    do i=1,Ny
        umin1=u1(j,i)-coeff*sqrt(sigma1(1,j,i)) 
        umax1=u1(j,i)+coeff*sqrt(sigma1(1,j,i)) 
        vmin1=v1(j,i)-coeff*sqrt(sigma1(3,j,i)) 
      	vmax1=v1(j,i)+coeff*sqrt(sigma1(3,j,i))
	    Umax=max(Umax,abs(umax1),abs(umin1)) 
	    Vmax=max(Vmax,abs(vmax1),abs(vmin1))
    end do
end do

Vmax = max(Vmaxg,Vmax,1.d-15)  
Umax = max(Umaxg,Umax,1.d-15) 
write(*,*)'Umax,Vmax=',Umax,Vmax
Dt = cfl*min( Dy/Vmax, Dx/Umax )  
 
 !
end subroutine



subroutine quadratic_interpolate(Ucenter,Uleft,Uright,Uup,Udown,deltax,deltay,Ax,Ay)

real*8, intent(in) :: Ucenter,Uleft,Uright,Uup,Udown
real*8, intent(out) :: Ax(3),Ay(3)

 real*8 :: A1,A2,A3,B1,B2,B3,Left(2),Mat(2,2),detM,deltax,deltay
 

 
      A3=sqrt(abs(Ucenter))
      B3=sign(1.d0,Ucenter)*A3
      Left(1)=Uright/B3-A3
      Left(2)=Uleft/B3-A3
      Mat(1,1)=deltax**2
      Mat(1,2)=deltax
      Mat(2,1)=deltax**2
      Mat(2,2)=-deltax
      detM=Mat(1,1)*Mat(2,2)-Mat(1,2)*Mat(2,1)
      A1=1/detM*(Mat(2,2)*Left(1)-Mat(1,2)*Left(2))
      A2=1/detM*(-Mat(2,1)*Left(1)+Mat(1,1)*Left(2))
      
      Left(1)=Uup/A3-B3
      Left(2)=Udown/A3-B3
      Mat(1,1)=deltay**2
      Mat(1,2)=deltay
      Mat(2,1)=deltay**2
      Mat(2,2)=-deltay
      detM=Mat(1,1)*Mat(2,2)-Mat(1,2)*Mat(2,1)
      B1=1/detM*(Mat(2,2)*Left(1)-Mat(1,2)*Left(2))
      B2=1/detM*(-Mat(2,1)*Left(1)+Mat(1,1)*Left(2))

      Ax(1)=A1
      Ax(2)=A2
      Ax(3)=A3
      Ay(1)=B1
      Ay(2)=B2
      Ay(3)=B3

end subroutine

end module AG2D_generic
