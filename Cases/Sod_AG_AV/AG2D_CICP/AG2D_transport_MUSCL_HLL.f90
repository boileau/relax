module AG2D_transport_MUSCL_HLL

use AG2D_quadrature
use AG2D_reconstruction
use AG2D_sharedvariables
  
implicit none

contains

function moment_advection_2Dx_MUSCL_HLL( M0, order, Nx, Dx, Ny, Dy, dt,coeff,taup,limiter)
! moment_advection_2Dxy updates 15 moments in 2D due to spatial fluxes using
! x-y splitting
!
integer, intent(in) :: order, Nx, Ny
real*8, intent(in) :: M0(6,Nx,Ny), Dx, Dy, Dt, coeff,taup,limiter
real*8 :: M1(6,Nx,Ny),M2(6,Nx,Ny),moment_advection_2Dx_MUSCL_HLL(6,Nx,Ny)

integer :: i,j,k
real*8 :: Fx(10,1:Nx+1,1:Ny),Fy(10,1:Nx,1:Ny+1)

!----------------------------------------------------------!
!    Reconstruction des paramètres (moments centrés)       !
!----------------------------------------------------------!
!
! Moments (6)
M1=M0
!
! Evaluate quadrature in x direction
call Fluxx_MUSCL_HLL(M1,Nx,Ny,Fx,Dx,Dy,Dt,coeff,taup,limiter)
! Evaluate new moments due to x flux 1st step RK2
do j=1,Nx
    do i=1,Ny
        do k=1,6
	        M1(k,j,i)  = M0(k,j,i)  - ( Fx(k,j+1,i)    -  Fx(k,j,i) )/Dx*dt*0.5D0
        end do
    end do
end do
!
! Evaluate quadrature in x direction
call Fluxx_MUSCL_HLL(M1,Nx,Ny,Fx,Dx,Dy,Dt,coeff,taup,limiter)
! Evaluate new moments due to x flux 2nd step RK2
do j=1,Nx
    do i=1,Ny
        do k=1,6
	        M1(k,j,i)  = M0(k,j,i)  - ( Fx(k,j+1,i)    -  Fx(k,j,i) )/Dx*dt
        end do
    end do
end do


moment_advection_2Dx_MUSCL_HLL=M1

end function


!---------------------------------------------------------------------------------------!
subroutine Fluxx_MUSCL_HLL(MM,Nx,Ny,Fx,Dx,Dy,Dt,coeff,taup,limiter)

integer, intent(in) :: Nx,Ny
real*8, intent(in) :: MM(1:6,1:Nx,1:Ny),Dx,Dy,Dt,coeff,taup,limiter
real*8 :: Fx(1:10,1:Nx+1,1:Ny)

integer :: i,j,k,ixflux(1:6)
integer :: jm1,jj,jp1
real*8 :: n(1:Nx,1:Ny), u(1:Nx,1:Ny), v(1:Nx,1:Ny), sigma(1:3,1:Nx,1:Ny)
real*8 :: umin_g(1:Nx,1:Ny),umax_g(1:Nx,1:Ny)
real*8 :: umin_d(1:Nx,1:Ny),umax_d(1:Nx,1:Ny)
real*8 :: momrec_d(1:10,1:Nx,1:Ny), momrec_g(1:10,1:Nx,1:Ny),momstarx(1:6,1:Nx+1,1:Ny)
real*8 :: lambdamin,lambdamax
real*8 :: vp(3,6)
real*8 :: mcg(6),mcd(6)

ixflux=(/2,4,5,7,8,9/)
!
! Evaluate left and right states in x direction
!
do j=1,Nx
  do i=1,Ny
      call moment_inversion_2D_gaussian(MM(:,j,i),n(j,i),u(j,i),v(j,i),sigma(:,j,i))  
  end do
end do


if ( (model.gt.1).and.(diffop.eq.0) ) then
    sigma=sigFEM(Nx,Ny,Dx,Dy,sigma,n,u,v,taup) 
endif




!
do j=1,Nx+1
jm1=modulo(j-1-1,Nx)+1
jj =modulo(j-1,Nx)+1
jp1=modulo(j-1+1,Nx)+1
do i=1,Ny
    vp(1,1)=n(jm1,i)
    vp(1,2)=u(jm1,i)
    vp(1,3)=v(jm1,i)
    vp(2,1)=n(jj,i)
    vp(2,2)=u(jj,i)
    vp(2,3)=v(jj,i)
    vp(3,1)=n(jp1,i)
    vp(3,2)=u(jp1,i)
    vp(3,3)=v(jp1,i)
    do k=1,3
       vp(1,k+3)=sigma(k,jm1,i)
       vp(2,k+3)=sigma(k,jj,i)
       vp(3,k+3)=sigma(k,jp1,i)
    end do

    !!! Reconstruction Moments centrés gauche et droite
    call recons_o2(vp,mcg,mcd,Dx,Dt,limiter)

    
    !if (mcg(4)*mcg(5)-mcg(6)**2.lt.0D0) write(*,*) 'Probleme etat gauche direction x cellule',jj,i
    !if (mcd(4)*mcd(5)-mcd(6)**2.lt.0D0) write(*,*) 'Probleme etat droite direction x cellule',jj,i
    !!! Calcul Moments gacuhe et droite 2*(10)
    momrec_g(1:10,jj,i) = moments_monogauss( mcg(1), mcg(2), mcg(3), mcg(4:6) )     
    momrec_d(1:10,jj,i) = moments_monogauss( mcd(1), mcd(2), mcd(3), mcd(4:6) )
    umin_g(jj,i)=mcg(2)-coeff*dsqrt(max(mcg(4),0D0))
    umax_g(jj,i)=mcg(2)+coeff*dsqrt(max(mcg(4),0D0))
    umin_d(jj,i)=mcd(2)-coeff*dsqrt(max(mcd(4),0D0))
    umax_d(jj,i)=mcd(2)+coeff*dsqrt(max(mcd(4),0D0))
    !temp
    !umin_g(jj,i)=vp(2,2)-coeff*dsqrt(vp(2,4))
    !umax_g(jj,i)=vp(2,2)+coeff*dsqrt(vp(2,4))
    !umin_d(jj,i)=vp(2,2)-coeff*dsqrt(vp(2,4))
    !umax_d(jj,i)=vp(2,2)+coeff*dsqrt(vp(2,4)) 
end do
end do

!
! Evaluate left interface flux in x direction
!
do j=1,Nx+1
jm1=modulo(j-1-1,Nx)+1
jj =modulo(j-1,Nx)+1
jp1=modulo(j-1+1,Nx)+1
do i=1,Ny
    !!! Flux(6)
    ! intermediate state
    lambdamin=min(umin_d(jm1,i),umin_g(jj,i))
    lambdamax=max(umax_d(jm1,i),umax_g(jj,i))
    call intermediatestate_HLL(0,lambdamin,lambdamax, momrec_d(:,jm1,i), momrec_g(:,jj,i),momstarx(:,j,i) )
    do k=1,6
       Fx(k,j,i)=  0.5d0*(momrec_d(ixflux(k),jm1,i)+momrec_g(ixflux(k),jj,i))&
                 - 0.5d0*dabs(lambdamin)*(momstarx(k,j,i)-momrec_d(k,jm1,i))&
       			 - 0.5d0*dabs(lambdamax)*(momrec_g(k,jj,i)-momstarx(k,j,i))
    end do   
end do
end do


end subroutine Fluxx_MUSCL_HLL
!---------------------------------------------------------------------------------------!
subroutine Fluxy_MUSCL_HLL(MM,Nx,Ny,Fy,Dx,Dy,Dt,coeff,taup,limiter)

integer, intent(in) :: Nx,Ny
real*8, intent(in) :: MM(1:6,1:Nx,1:Ny),Dx,Dy,Dt,coeff,taup,limiter
real*8 :: Fy(1:10,1:Nx,1:Ny+1)

integer :: i,j,k,iyflux(1:6)
integer :: im1,ii,ip1
real*8 :: n(1:Nx,1:Ny), u(1:Nx,1:Ny), v(1:Nx,1:Ny), sigma(1:3,1:Nx,1:Ny)
real*8 :: vmin_g(1:Nx,1:Ny),vmax_g(1:Nx,1:Ny)
real*8 :: vmin_d(1:Nx,1:Ny),vmax_d(1:Nx,1:Ny)
real*8 :: momrec_d(1:10,1:Nx,1:Ny), momrec_g(1:10,1:Nx,1:Ny),momstary(1:6,1:Nx,1:Ny+1)
real*8 :: lambdamin,lambdamax
real*8 :: vp(3,6)
real*8 :: mcg(6),mcd(6)

iyflux=(/3,5,6,8,9,10/)
!
! Evaluate left and right states in y direction
do j=1,Nx
do i=1,Ny
    call moment_inversion_2D_gaussian(MM(:,j,i),n(j,i),u(j,i),v(j,i),sigma(:,j,i))  
end do
end do

if ( (model.gt.1).and.(diffop.eq.0) ) then
    sigma=sigFEM(Nx,Ny,Dx,Dy,sigma,n,u,v,taup) 
endif


!
do j=1,Nx
do i=1,Ny+1
    im1=modulo(i-1-1,Ny)+1
    ii =modulo(i-1,Ny)+1
    ip1=modulo(i-1+1,Ny)+1
    vp(1,1)=n(j,im1)
    vp(1,2)=u(j,im1)
    vp(1,3)=v(j,im1)
    vp(2,1)=n(j,ii)
    vp(2,2)=u(j,ii)
    vp(2,3)=v(j,ii)
    vp(3,1)=n(j,ip1)
    vp(3,2)=u(j,ip1)
    vp(3,3)=v(j,ip1)
    do k=1,3
       vp(1,k+3)=sigma(k,j,im1)
       vp(2,k+3)=sigma(k,j,ii)
       vp(3,k+3)=sigma(k,j,ip1)
    end do
    !!! Reconstruction Moments centrés gauche et droite
    call recons_o2(vp,mcg,mcd,Dy,Dt,limiter)
    !if (mcg(4)*mcg(5)-mcg(6)**2.lt.0D0) write(*,*) 'Probleme etat gauche direction y cellule',j,i
    !if (mcd(4)*mcd(5)-mcd(6)**2.lt.0D0) write(*,*) 'Probleme etat droite direction y cellule',j,i
    !!! Reconstruction Moments 2*(10)
    momrec_g(1:10,j,ii) = moments_monogauss( mcg(1), mcg(2), mcg(3), mcg(4:6) )     
    momrec_d(1:10,j,ii) = moments_monogauss( mcd(1), mcd(2), mcd(3), mcd(4:6) )
    vmin_g(j,ii)=mcg(3)-coeff*dsqrt(max(mcg(6),0D0))
    vmax_g(j,ii)=mcg(3)+coeff*dsqrt(max(mcg(6),0D0))
    vmin_d(j,ii)=mcd(3)-coeff*dsqrt(max(mcd(6),0D0))
    vmax_d(j,ii)=mcd(3)+coeff*dsqrt(max(mcd(6),0D0))
    !temp
    !vmin_g(j,ii)=vp(2,3)-coeff*dsqrt(vp(2,6))
    !vmax_g(j,ii)=vp(2,3)+coeff*dsqrt(vp(2,6))
    !vmin_d(j,ii)=vp(2,3)-coeff*dsqrt(vp(2,6))
    !vmax_d(j,ii)=vp(2,3)+coeff*dsqrt(vp(2,6)) 
end do
end do
!
! Evaluate left interface flux in y direction
!
do j=1,Nx
do i=1,Ny+1
    im1=modulo(i-1-1,Ny)+1
    ii =modulo(i-1,Ny)+1
    ip1=modulo(i-1+1,Ny)+1
    !!! Flux(6)
    ! intermediate state
    lambdamin=min(vmin_d(j,im1),vmin_g(j,ii))
    lambdamax=max(vmax_d(j,im1),vmax_g(j,ii))
    call intermediatestate_HLL(1,lambdamin,lambdamax, momrec_d(:,j,im1), momrec_g(:,j,ii),momstary(:,j,i) )
    do k=1,6
       Fy(k,j,i)=  0.5d0*(momrec_d(iyflux(k),j,im1)+momrec_g(iyflux(k),j,ii))&
                 - 0.5d0*dabs(lambdamin)*(momstary(k,j,i)-momrec_d(k,j,im1))&
       			 - 0.5d0*dabs(lambdamax)*(momrec_g(k,j,ii)-momstary(k,j,i))
    end do
end do
end do

end subroutine Fluxy_MUSCL_HLL
!---------------------------------------------------------------------------------------!
function moment_advection_2Dy_MUSCL_HLL( M0, order, Nx, Dx, Ny, Dy, dt,coeff,taup,limiter)
! moment_advection_2Dyx updates 15 moments in 2D due to spatial fluxes using
! y-x splitting
!
integer, intent(in) :: order, Nx, Ny
real*8, intent(in) :: M0(6,Nx,Ny), Dx, Dy, Dt, coeff,taup,limiter
real*8 :: M1(6,Nx,Ny),M2(6,Nx,Ny),moment_advection_2Dy_MUSCL_HLL(6,Nx,Ny)

integer :: i,j,k
real*8 :: Fx(10,Nx+1,Ny),Fy(10,Nx,Ny+1)
!
!----------------------------------------------------------!
!    Reconstruction des paramètres (moments centrés)       !
!----------------------------------------------------------!
!
! Moments (6)
M1=M0
!
! Evaluate quadrature in y direction
call Fluxy_MUSCL_HLL(M1,Nx,Ny,Fy,Dx,Dy,Dt,coeff,taup,limiter)
! Evaluate new moments due to y flux 1st step RK2
do j=1,Nx
    do i=1,Ny
        do k=1,6
	        M1(k,j,i)  = M0(k,j,i)  - ( Fy(k,j,i+1)    -  Fy(k,j,i) )/Dy*dt*0.5D0 
        end do
    end do
end do
!
! Evaluate quadrature in y direction
call Fluxy_MUSCL_HLL(M1,Nx,Ny,Fy,Dx,Dy,Dt,coeff,taup,limiter)
! Evaluate new moments due to y flux 2nd step RK2
do j=1,Nx
    do i=1,Ny
        do k=1,6
	        M1(k,j,i)  = M0(k,j,i)  - ( Fy(k,j,i+1)    -  Fy(k,j,i) )/Dy*dt
        end do
    end do
end do
!

moment_advection_2Dy_MUSCL_HLL=M1
 
end function moment_advection_2Dy_MUSCL_HLL
!-----------------------------------!
subroutine intermediatestate_HLL(direction,ll,lr,moml,momr,momstar)

integer, intent(in) :: direction
real*8, intent(in) :: ll,lr,moml(10),momr(10)
real*8, intent(out) :: momstar(6)

integer :: k,ixflux(6),iyflux(6)
!kmom=(/1,2,3,4,5,6,7,8,9,10,11,12,14,15,17,20/)
ixflux=(/2,4,5,7,8,9/)
iyflux=(/3,5,6,8,9,10/)

if ( dabs(ll-lr).gt.1.d-12 ) then
	if (direction.eq.0) then
		!momstar x
		do k=1,6
			momstar(k)= ( lr*momr(k)-ll*moml(k) +moml(ixflux(k))-momr(ixflux(k)) )  / (lr-ll)
		enddo
	elseif (direction.eq.1) then
		!momstar x
		do k=1,6
			momstar(k)= ( lr*momr(k)-ll*moml(k) +moml(iyflux(k))-momr(iyflux(k)) )  / (lr-ll)
		enddo
	endif
else 
	momstar=0.d0
endif

end subroutine

!---------------------------------------------!




end module AG2D_transport_MUSCL_HLL