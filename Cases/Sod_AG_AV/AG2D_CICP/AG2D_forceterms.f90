module AG2D_forceterms

use AG2D_quadrature

implicit none
contains

function moment_dragforce( M0, Nx, Ny, Dx, Dy, Dt, uf, vf, St )
!

integer, intent(in) :: Nx, Ny
real*8, intent(in) :: M0(6,Nx,Ny), Dt, uf(Nx,Ny), vf(Nx,Ny), St,Dx, Dy
real*8 :: M1(6,Nx,Ny),Mtemp(6,Nx,Ny),moment_dragforce(6,Nx,Ny)

integer :: i, j, k, ndx, errorq
real*8 :: uf1, vf1, n(Nx,Ny), u(Nx,Ny), v(Nx,Ny), sigma(3,Nx,Ny),sigma1(3),sigma0(3,Nx,Ny), mom(48),sigmatemp(3)
real*8 :: vrel, re1, Cd, taud, kap, kap0, kap1,mtol

mtol=1.d-10

M1 = M0 

  do i=1,Ny
      do j=1,Nx
          call moment_inversion_2D_gaussian(M0(:,j,i),n(j,i),u(j,i),v(j,i),sigma0(:,j,i))
      end do
  end do

    if (model.gt.1) then
        sigma=sigFEM(Nx,Ny,Dx,Dy,sigma0,n,u,v,St)
    else
        sigma=sigma0
    endif


  do i=1,Ny
      do j=1,Nx
          uf1 = uf(j,i) 
          vf1 = vf(j,i) 
          
            !write(*,*) 'la'
            kap = exp(-Dt/St) 
            if (n(j,i).gt.mtol )then
              u(j,i)  = kap*u(j,i) + (1-kap)*uf1 
              v(j,i)  = kap*v(j,i) + (1-kap)*vf1
            else
              u(j,i)  = 0.d0!uf1
              v(j,i)  = 0.d0!vf1
            endif
                
            sigma(:,j,i)= sigma(:,j,i)*kap**2
            
            mom(1:10) = moments_monogauss( n(j,i), u(j,i), v(j,i),sigma(:,j,i) ) 
            M1(1:6,j,i)=mom(1:6)
      end do
  end do

moment_dragforce=M1
end function



end module AG2D_forceterms
