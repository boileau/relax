module AG2D_quadrature
! Anisotropic gaussian 2D
! module for quadrature and moments


use nr_complet
use AG2D_reconstruction

implicit none

real*8 ::  mtol,deltacorr,mtol_diff,mtol_sigFEM
integer :: model,Ncorr,diffop,realcorr,islim
real*8 :: Smax,alpha

contains


subroutine moment_inversion_2D_gaussian( mom, n, u, v, sig, uf, vf )
 ! Invert 6 moments to find Anisotropic Gaussian
 !
!![1 2 3 4 5 6] 
!![0 1 0 2 1 0] 
!![0 0 1 0 1 2] 
 !
 real*8, intent(in) :: mom(6)
 real*8, intent(in), optional :: uf,vf
 real*8, intent(out) :: n, u, v, sig(3)
 
real*8 :: sigiso,det,alpha,beta
 
n = 0.d0
if (.not.present(uf)) then
  u = 0.d0  
  v = 0.d0
else
  u = uf 
  v = vf
endif
sig = 0.d0  

 
 ! check for vacuum state
if (mom(1) <=0.d0) then
 	return
elseif ( mom(1) < mtol ) then
    n=mom(1)
    return
end if

n=mom(1)
u=mom(2)/mom(1)
v=mom(3)/mom(1)

if ( model.eq.-1) then 
   sig=0.d0
elseif ( ( model.eq.0).or.( model.eq.11) ) then !11=lagrangian but just for postprocessing purpose 
	sig=(/  max(0.d0,mom(4)/mom(1)-u**2), mom(5)/mom(1)-v*u , max(0.d0,mom(6)/mom(1)-v**2)    /)	
elseif ( model.eq.-2) then
        sig=(/  max(0.d0,mom(4)/mom(1)-u**2), 0.d0 , max(0.d0,mom(6)/mom(1)-v**2)    /)	
else
	sigiso=0.5d0*max(0.d0,mom(4)/mom(1)-u**2+mom(6)/mom(1)-v**2)
	sig=(/  sigiso, 0.d0 , sigiso    /)
endif

if (islim==1 ) then
  alpha=1
  beta=1
  det=sig(1)*sig(3)-sig(2)**2
  if (sig(1)/n**alpha.gt.Smax) then
      !sig(1)=Smax*n**alpha
      alpha=Smax*n**alpha/sig(1)
  endif
  if (sig(3)/n**alpha.gt.Smax) then
      !sig(3)=Smax*n**alpha
      beta=Smax*n**alpha/sig(3)
  endif
  sig(1)=min(alpha,beta)*sig(1)
  sig(3)=min(alpha,beta)*sig(3)
  sig(2)=min(alpha,beta)*sig(2)
endif



  
if ( sig(1)*sig(3)-sig(2)**2<0.d0 ) then
 !write(*,*)'aie s12 quad 0',sig(1)*sig(3)-sig(2)**2
  !write(*,*)sig
  if ( (sig(1).lt.1.d-15).or.(sig(3).lt.1.d-15)  )then
    sig(2)=0.d0
  else
    if (sig(2).gt.0) then
    sig(2)=sqrt(sig(1)*sig(3))-1.d-7
    else
    sig(2)=-sqrt(sig(1)*sig(3))+1.d-7
    endif
  endif
  if ( (sig(1)*sig(3)-sig(2)**2).lt.0.d0) then
  !write(*,*)sig(1)*sig(3)-sig(2)**2
  sig(2)=0.d0
  endif
endif

end subroutine


function moments_monogauss( n, u, v, sigma )

real*8, intent(in) :: n, u, v, sigma(3)
real*8 :: mom(10), moments_monogauss(10)

![1 2 3 4 5 6 7 8 9 10]
![0 1 0 2 1 0 3 2 1  0]
![0 0 1 0 1 2 0 1 2  3]  

mom(1)=n
mom(2)=n*u
mom(3)=n*v
mom(4)=n*(u**2 + sigma(1))
mom(5)=n*(u*v  + sigma(2))
mom(6)=n*(v**2 + sigma(3))
mom(7)=n*(u**3 + 3*u*sigma(1))
mom(8)=n*(u**2*v  + 2*u*sigma(2) + v*sigma(1))
mom(9)=n*(v**2*u  + 2*v*sigma(2) + u*sigma(3))
mom(10)=n*(v**3 + 3*v*sigma(3))

moments_monogauss=mom

end function



function sigFEM(Nx,Ny,dx,dy,sigin,n,u,v,taup)

real*8, intent(in) :: n(Nx,Ny),u(Nx,Ny),v(Nx,Ny),sigin(3,Nx,Ny),dx,dy,taup
real*8 :: S(3,Nx,Ny),sigFEM(3,Nx,Ny),nu,nucorr
real*8 :: dev(3,Nx,Ny),O(3,Nx,Ny),T1(2,2),T2(2,2),T3(2,2),ST(2,2),OM(2,2)
real*8 :: eta1,eta2,L1,L2,L3,L4,trS,trT3,absdev

integer, intent(in) :: Nx,Ny
integer :: i,j,k,im1,ii,ip1,jm1,jj,jp1,evaldiff

real*8 :: G1,G2,G3
real*8 :: vp(3,6),mcg(6),mcd(6),mcb(6),mch(6),pent(6)
                        
real*8:: Rstar_uu,Rstar_vv,Rstar_uv,sigma_uu,sigma_uv,sigma_vv,corre
real*8 :: b_uu,b_uv,b_vv,RUME
logical :: correction

evaldiff=0

do j=1,Ny
    jm1=modulo(j-1-1,Ny)+1
    jp1=modulo(j+1-1,Ny)+1
	do i=1,Nx
        im1=modulo(i-1-1,Nx)+1
        ip1=modulo(i+1-1,Nx)+1
        if (n(i,j).gt.mtol_sigFEM) then
        
                if (evaldiff.eq.1) then
                  ! using the reconstruction strategy to evaluate slopes
                  vp(1,1)=n(im1,j)
                  vp(1,2)=u(im1,j)
                  vp(1,3)=v(im1,j)
                  vp(2,1)=n(i,j)
                  vp(2,2)=u(i,j)
                  vp(2,3)=v(i,j)
                  vp(3,1)=n(ip1,j)
                  vp(3,2)=u(ip1,j)
                  vp(3,3)=v(ip1,j)
                  do k=1,3
                     vp(1,k+3)=sigin(k,im1,j)
                     vp(2,k+3)=sigin(k,i,j)
                     vp(3,k+3)=sigin(k,ip1,j)
                  end do
                  !!! Reconstruction Moments centr�s gauche et droite
                  call recons_o2(vp,mcg,mcd,Dx,1.d0,1.d0)
                  vp(1,1)=n(i,jm1)
                  vp(1,2)=u(i,jm1)
                  vp(1,3)=v(i,jm1)
                  vp(2,1)=n(i,j)
                  vp(2,2)=u(i,j)
                  vp(2,3)=v(i,j)
                  vp(3,1)=n(i,jp1)
                  vp(3,2)=u(i,jp1)
                  vp(3,3)=v(i,jp1)
                  do k=1,3
                     vp(1,k+3)=sigin(k,i,jm1)
                     vp(2,k+3)=sigin(k,i,j)
                     vp(3,k+3)=sigin(k,i,jp1)
                  end do
                  call recons_o2(vp,mcb,mch,Dy,1.d0,1.d0)
                  
                  S(1,i,j)=(1.d0/(dx)*(mcd(2)-mcg(2)) - 1.d0/(dy)*(mch(3)-mcb(3)))/2.d0
                  S(2,i,j)=(1.d0/(dy)*(mch(2)-mcb(2)) + 1.d0/(dx)*(mcd(3)-mcg(3)))/2.d0
                  S(3,i,j)=-S(1,i,j)
  
                  O(1,i,j)=0.d0
                  O(2,i,j)=(1.d0/(dy)*(mch(2)-mcb(2)) - 1.d0/(dx)*(mcd(3)-mcg(3)))/2.d0
                  O(3,i,j)=0.d0		
                  eta1=S(1,i,j)**2+S(3,i,j)**2+2.d0*S(2,i,j)**2
                  eta2=-2*O(2,i,j)**2
                  L1=2.d0
                  L2=-1.d0
                  L3=-1.d0
                  L4=-1.d0
                  trS=1.d0/(dx)*(mcd(2)-mcg(2))+1.d0/(dy)*(mch(3)-mcb(3)) 
                 
                 
                 
                elseif (evaldiff.eq.0) then
                  ! classical centered scheme
                  S(1,i,j)=(1.d0/(2.d0*dx)*(u(ip1,j)-u(im1,j)) - 1.d0/(2.d0*dy)*(v(i,jp1)-v(i,jm1)))/2.d0
                  S(2,i,j)=(1.d0/(2.d0*dy)*(u(i,jp1)-u(i,jm1)) + 1.d0/(2.d0*dx)*(v(ip1,j)-v(im1,j)))/2.d0
                  S(3,i,j)=-S(1,i,j)
  
                  O(1,i,j)=0.d0
                  O(2,i,j)=(1.d0/(2.d0*dy)*(u(i,jp1)-u(i,jm1)) - 1.d0/(2.d0*dx)*(v(ip1,j)-v(im1,j)))/2.d0
                  O(3,i,j)=0.d0		
                  eta1=S(1,i,j)**2+S(3,i,j)**2+2.d0*S(2,i,j)**2
                  eta2=-2*O(2,i,j)**2
                  L1=2.d0
                  L2=-1.d0
                  L3=-1.d0
                  L4=-1.d0
                  trS=1.d0/(2.d0*dx)*(u(ip1,j)-u(im1,j))+1.d0/(2.d0*dy)*(v(i,jp1)-v(i,jm1)) 
                
                end if
                
                
                ST(1,1)=S(1,i,j)
                ST(2,2)=S(3,i,j)
                ST(1,2)=S(2,i,j)
                ST(2,1)=S(2,i,j)

                OM(1,1)=O(1,i,j)
                OM(2,2)=O(3,i,j)
                OM(1,2)=O(2,i,j)
                OM(2,1)=-O(2,i,j)

                T1(:,:)=ST(:,:)

                T2=MATMUL(ST,OM)-MATMUL(OM,ST)
                T3=MATMUL(ST,ST)
                trT3=T3(1,1)+T3(2,2)
                T3(1,1)=T3(1,1)-0.5d0*trT3
                T3(2,2)=T3(2,2)-0.5d0*trT3

! alternative to MATMUL function if one may want to verify
!               do n=1,2
!                  do m=1,2
!                    T2(n,m)=SUM(ST(n,:)*OM(:,m))-SUM(ST(n,:)*OM(:,m))
!                 enddo
!                enddo
                  
                corre=sign(1.d0,(S(1,i,j)*S(3,i,j)-S(2,i,j)**2))
                correction=.false.
		if (model.eq.2) then
                
                        if(  ((diffop.eq.1).and.(realcorr.eq.1)).or.(diffop.eq.0) ) then
			   nu=min(  taup*sigin(1,i,j),0.99d0*sigin(1,i,j)/abs(S(1,i,j)) ) ! classical FEM-VISCO
                        else
                           nu=taup*sigin(1,i,j)
                        endif
                        
                        sigFEM(:,i,j)=sigin(:,i,j)-nu*S(:,i,j)
		elseif (model.eq.3) then 
		    !masi FEM-AXYSI
                elseif (model.eq.4) then
                          if(eta1.gt.1e-15) then
                            G2=L4/(eta1*L1)
                            if((eta1+eta2).gt.0) then
                            G1=-sqrt(2.d0*eta1+2.d0*eta2)/(2.d0*eta1)
                            else
                            G1=0.d0
                            endif
                         else
                            G1=0.d0
                            G2=0.d0
                         endif

! Rstar_uu/2energy
                        b_uu=G1*T1(1,1)+G2*T2(1,1)
! Rstar_vv/2energy
                        b_vv=-b_uu !!G1*T1(2,2)+G2*T2(2,2)
! Rstar_uv/2energy
                        b_uv=G1*T1(1,2)+G2*T2(1,2)

			!XB(1)=b_uu
                        !XB(2)=b_uv

                        !call mnewt(1000,XB,2,0.0001d0,0.0001d0,XS,XO)

                        !b_uu=XB(1)
                        !b_vv=-b_uu
                        !b_uv=XB(2)

                        RUME=sigin(1,i,j)

                        Rstar_uu=b_uu*RUME*2.d0
                        Rstar_vv=b_vv*RUME*2.d0
                        Rstar_uv=(b_uv*RUME*2.d0)


                        sigma_uu=Rstar_uu+RUME
                        sigma_uv=Rstar_uv
                        sigma_vv=Rstar_vv+RUME

                        if(  ((diffop.eq.1).and.(realcorr.eq.1)).or.(diffop.eq.0) ) then
                          if(sigma_uu.lt.0.) then
                            !write(*,*)Rstar_uu,Rstar_vv,RUME
                            Ncorr=Ncorr+1
                            deltacorr=deltacorr+abs(sigma_uu)
                            Rstar_uu=-RUME
                            Rstar_vv=-Rstar_uu
                          elseif(sigma_vv.lt.0.) then
                           ! write(*,*)Rstar_uu,Rstar_vv,RUME
                            Ncorr=Ncorr+1
                            deltacorr=deltacorr+abs(sigma_uu)
                            Rstar_vv=-RUME
                            Rstar_uu=-Rstar_vv
                          endif
  
                          sigma_uu=Rstar_uu+RUME
                          sigma_vv=Rstar_vv+RUME
                          corre=sigma_uu*sigma_vv-sigma_uv**2 
                          if(corre.lt.0) then
                          !Ncorr=Ncorr+1
                          sigma_uv=sign(1.d0,sigma_uv)*sqrt(sigma_uu*sigma_vv)
                          endif
                        endif

                        sigFEM(1,i,j)=sigma_uu
                        sigFEM(2,i,j)=sigma_uv
                        sigFEM(3,i,j)=sigma_vv
		endif
                
                
                
	      if ( ((model.eq.2).and.(diffop.eq.0)).or.((model.eq.2).and.(diffop.eq.1).and.(realcorr.eq.1))   ) then
		sigFEM(2,i,j)=sign(1.d0,-S(2,i,j))*min(abs(sigFEM(2,i,j)),sqrt(sigFEM(1,i,j)*sigFEM(3,i,j)))
              endif
                
            else
              sigFEM(:,i,j)=0.D0
            
            endif
	end do
end do

end function sigFEM

end module AG2D_quadrature
