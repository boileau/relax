module AG2D_interface


use AG2D_transport_fluxsplit	
use AG2D_transport_HLL
use AG2D_transport_MUSCL_HLL
use AG2D_transport_MUSCL_fluxsplit
use AG2D_generic
use AG2D_diffoperator
use AG2D_forceterms

implicit none


contains


subroutine timestep(M0,Nx,Dx,Ny,Dy,cfl,coeff, Umaxg,Vmaxg,Dt,t, Dtconv, St, ite,time,nitemax,scheme,testcase,Dtlag,model)

real*8, intent(in) :: M0(6,Nx,Ny),Dx,Dy,cfl,St,coeff,time(nitemax),dtlag
integer, intent(in) :: Nx,Ny,nitemax,scheme,testcase,model
real*8, intent(inout)::t
integer, intent(inout)::ite
real*8, intent(out) :: Dt,Dtconv

real*8, intent(in) :: Umaxg, Vmaxg


    if (model.ne.11) then
      !Calculate Dt
     call timestepsize( M0,Nx,Dx,Ny,Dy,cfl,coeff, Dt, Umaxg, Vmaxg,St)
     
     write(*,*)'Dt conv',Dt
     Dtconv=Dt
     !
     !if ( (t+Dt)>tmax ) then
     !	Dt=tmax+eps1-t
     !endif
     
     if (testcase.eq.2) then
       Dt=time(ite+1)-time(ite)
     else
       Dt=Dtconv
     endif
         write(*,'(i5,e13.6,e13.6,e13.6)')ite,t, dt, dt/dtconv
    t = t + Dt
    ite=ite+1
    elseif (model.eq.11)  then
     if (testcase.eq.2) then
       Dt=time(ite+1)-time(ite)
     else
       Dt=Dtlag
     endif
      write(*,'(i5,e13.6,e13.6,e13.6)')ite,t, dt
      t = t + Dt
      ite=ite+1
    endif
    
    
    

end subroutine timestep





subroutine convection(Nx,Ny,Dx,Dy,Dt,Dtconv,M0,scheme,coeff,St,advection,M1,testcase,limiter)

integer, intent(in) :: Nx,Ny,scheme,testcase
integer, intent(inout) :: advection
real*8, intent(in)  :: Dx,Dy,Dt,Dtconv,M0(6,Nx,Ny),coeff,St,limiter
real*8, intent(out) :: M1(6,Nx,Ny)
real*8 :: Mtemp(6,Nx,Ny),Mtemp2(6,Nx,Ny)

integer :: ncut,k
real*8 :: dtloc

    ! Evaluation of the new moments using RK2SPP for 1 step of 2-D spatial transport
    !
    Mtemp2=M0
    Dtloc=Dt
    Ncut=ceiling(dtloc/dtconv)
    Dtloc=Dtloc/Ncut
    do k=1,Ncut
      
      if ( advection == 0 ) then
          if ( scheme == 3 ) then
              write(*,*)'ADVECTION XY HLL'              
              Mtemp = moment_advection_2Dx_HLL( Mtemp2, scheme, Nx, Dx, Ny, Dy, Dtloc, coeff,St)
              M1    = moment_advection_2Dy_HLL( Mtemp, scheme, Nx, Dx, Ny, Dy, Dtloc, coeff,St)
              advection = 1
          elseif ( scheme == 2 ) then
              write(*,*)'ADVECTION XY FLUXSPLIT'
              Mtemp = moment_advection_2Dx_fluxsplit( Mtemp2, scheme, Nx, Dx, Ny, Dy, Dtloc,St )
              M1    = moment_advection_2Dy_fluxsplit( Mtemp, scheme, Nx, Dx, Ny, Dy, Dtloc,St )
              advection = 1
          elseif ( scheme == 4 ) then
              write(*,*)'ADVECTION XY MUSCL/HLL'
              Mtemp = moment_advection_2Dx_MUSCL_HLL( Mtemp2, scheme, Nx, Dx, Ny, Dy, Dtloc, coeff, St,limiter)
              M1    = moment_advection_2Dy_MUSCL_HLL( Mtemp, scheme, Nx, Dx, Ny, Dy, Dtloc, coeff, St,limiter)  
              advection = 1
          elseif ( scheme == 5 ) then
              write(*,*)'ADVECTION XY MUSCL/Fluxsplit'
              Mtemp = moment_advection_2Dx_MUSCL_fluxsplit( Mtemp2, scheme, Nx, Dx, Ny, Dy, Dtloc,limiter )
              M1    = moment_advection_2Dy_MUSCL_fluxsplit( Mtemp, scheme, Nx, Dx, Ny, Dy, Dtloc,limiter )  
              advection = 1
          end if
      else
          if ( scheme == 3 ) then
              write(*,*)'ADVECTION YX HLL' 
              Mtemp = moment_advection_2Dy_HLL( Mtemp2, scheme, Nx, Dx, Ny, Dy, Dtloc, coeff,St)
              M1    = moment_advection_2Dx_HLL( Mtemp, scheme, Nx, Dx, Ny, Dy, Dtloc, coeff,St)
              advection = 0
          elseif ( scheme == 2 ) then
              write(*,*)'ADVECTION YX FLUXSPLIT'
              Mtemp = moment_advection_2Dy_fluxsplit( Mtemp2, scheme, Nx, Dx, Ny, Dy, Dtloc,St )
              M1    = moment_advection_2Dx_fluxsplit( Mtemp, scheme, Nx, Dx, Ny, Dy, Dtloc,St )
              advection = 0
          elseif ( scheme == 5 ) then
              write(*,*)'ADVECTION YX MUSCL/Fluxsplit'
              Mtemp = moment_advection_2Dy_MUSCL_fluxsplit( Mtemp2, scheme, Nx, Dx, Ny, Dy, Dtloc,limiter )
              M1    = moment_advection_2Dx_MUSCL_fluxsplit( Mtemp, scheme, Nx, Dx, Ny, Dy, Dtloc ,limiter)  
              advection = 0
          elseif ( scheme == 4 ) then
              write(*,*)'ADVECTION YX MUSCL/HLL'
              Mtemp = moment_advection_2Dy_MUSCL_HLL( Mtemp2, scheme, Nx, Dx, Ny, Dy, Dtloc, coeff, St,limiter)
              M1    = moment_advection_2Dx_MUSCL_HLL( Mtemp, scheme, Nx, Dx, Ny, Dy, Dtloc, coeff, St,limiter)  
              advection = 0  
          end if
      end if
      Mtemp2=M1
      
     end do



end subroutine convection


subroutine diffusion(Nx,Ny,Dx,Dy,Dt,Dtconv,M0,St,diffop,M1)

integer, intent(in) :: Nx,Ny,diffop
real*8, intent(in) :: Dx,Dy,M0(6,Nx,Ny),Dt,St,Dtconv
real*8, intent(out) :: M1(6,Nx,Ny)

!local
integer :: ncut,k
real*8 :: dtloc
real*8 :: Mtemp(6,Nx,Ny)

  Mtemp=M0
  !write(*,*)'DIFF OPERATOR'
  Dtloc=Dt
  Ncut=ceiling(dtloc/dtconv)
  Dtloc=Dtloc/Ncut
  M1=Mtemp
  
  do k=1,Ncut
   write(*,*)'DIFF OPERATOR'
   Mtemp=M1
   M1 = diff_operator(Mtemp,Nx,Ny,Dx,Dy,Dtloc,St)
  end do
  

end subroutine diffusion


subroutine forceterms(Nx,Ny,Dx,Dy,Dt,M0,St,uf,vf,iforceop,M1)

integer, intent(in) :: Nx,Ny,iforceop
real*8, intent(in) :: Dx,Dy,M0(6,Nx,Ny),Dt,St,uf(Nx,Ny),vf(Nx,Ny)
real*8, intent(out) :: M1(6,Nx,Ny)

!local
   write(*,*)'FORCE TERM'
   M1 = moment_dragforce( M0, Nx, Ny, Dx, Dy, Dt, uf, vf, St) 
  

end subroutine forceterms




end module AG2D_interface
