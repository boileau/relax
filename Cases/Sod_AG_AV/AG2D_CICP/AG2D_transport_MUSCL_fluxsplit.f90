module AG2D_transport_MUSCL_fluxsplit

use AG2D_quadrature
use AG2D_reconstruction
use AG2D_transport_MUSCL_HLL
use flux_erf
use AG2D_sharedvariables

implicit none

contains

function moment_advection_2Dx_MUSCL_fluxsplit( M0, order, Nx, Dx, Ny, Dy, dt,limiter)
! moment_advection_2Dxy updates 15 moments in 2D due to spatial fluxes using
! x-y splitting
!
integer, intent(in) :: order, Nx, Ny
real*8, intent(in) :: M0(6,Nx,Ny), Dx, Dy, Dt,limiter
real*8 :: M1(6,Nx,Ny),M2(6,Nx,Ny),moment_advection_2Dx_MUSCL_fluxsplit(6,Nx,Ny)
real*8 :: Mcorr(6,Nx,Ny)

integer :: i,j,k,jm1,jp1,im1,ip1
real*8 :: Fx(10,1:Nx+1,1:Ny),Fy(10,1:Nx,1:Ny+1)

!----------------------------------------------------------!
!    Reconstruction des paramètres (moments centrés)       !
!----------------------------------------------------------!
!
! Moments (6)
M1=M0
!

! Evaluate quadrature in x direction
call Fluxx_MUSCL_fluxsplit(M1,Nx,Ny,Fx,Dx,Dt,Mcorr,limiter)
! Evaluate new moments due to x flux 1st step RK2
do j=1,Nx
    do i=1,Ny
        jm1=modulo(j-1-1,Nx)+1
        jp1=modulo(j+1-1,Nx)+1
        do k=1,6
	        M1(k,j,i)  = Mcorr(k,j,i)  - ( Fx(k,jp1,i)    -  Fx(k,j,i) )/Dx*dt*0.5D0
        end do
    end do
end do

!
! Evaluate quadrature in x direction
call Fluxx_MUSCL_fluxsplit(M1,Nx,Ny,Fx,Dx,Dt,M2,limiter)
! Evaluate new moments due to x flux 2nd step RK2
do j=1,Nx
    do i=1,Ny
        jm1=modulo(j-1-1,Nx)+1
        jp1=modulo(j+1-1,Nx)+1
        do k=1,6
	        M1(k,j,i)  = Mcorr(k,j,i)  - ( Fx(k,jp1,i)    -  Fx(k,j,i) )/Dx*dt
        end do
    end do
end do

moment_advection_2Dx_MUSCL_fluxsplit=M1

end function
!


!---------------------------------------------------------------------------------------!
subroutine Fluxx_MUSCL_fluxsplit(MM,Nx,Ny,Fx,Dx,Dt,M0,limiter)

integer, intent(in) :: Nx,Ny
real*8, intent(in) :: MM(1:6,1:Nx,1:Ny),Dx,Dt,limiter
real*8 :: Fx(1:10,1:Nx+1,1:Ny)
real*8, intent(out) :: M0(1:6,1:Nx,1:Ny)

integer :: i,j,k,ixflux(1:6)
integer :: jm1,jj,jp1
real*8 :: n(1:Nx,1:Ny), u(1:Nx,1:Ny), v(1:Nx,1:Ny), sigma(1:3,1:Nx,1:Ny)
real*8 :: momplusg(1:10,Nx,Ny), momminusg(1:10,Nx,Ny)
real*8 :: momplusd(1:10,Nx,Ny), momminusd(1:10,Nx,Ny)
real*8 :: vp(3,6)
real*8 :: mcg(6),mcd(6)

ixflux=(/2,4,5,7,8,9/)
!
! Evaluate left and right states in x direction
!
do j=1,Nx
do i=1,Ny
    call moment_inversion_2D_gaussian(MM(:,j,i),n(j,i),u(j,i),v(j,i),sigma(:,j,i))
    call moment_sigma10_U_plusminus( n(j,i), u(j,i),v(j,i), sigma(:,j,i), momplusg(1:10,j,i), momminusg(1:10,j,i) )
    M0(:,j,i)=momplusg(1:6,j,i)+momminusg(1:6,j,i)
end do
end do
!
do j=1,Nx
jm1=modulo(j-1-1,Nx)+1
jj =modulo(j-1,Nx)+1
jp1=modulo(j-1+1,Nx)+1
do i=1,Ny
    vp(1,1)=n(jm1,i)
    vp(1,2)=u(jm1,i)
    vp(1,3)=v(jm1,i)
    vp(2,1)=n(jj,i)
    vp(2,2)=u(jj,i)
    vp(2,3)=v(jj,i)
    vp(3,1)=n(jp1,i)
    vp(3,2)=u(jp1,i)
    vp(3,3)=v(jp1,i)
    do k=1,3
       vp(1,k+3)=sigma(k,jm1,i)
       vp(2,k+3)=sigma(k,jj,i)
       vp(3,k+3)=sigma(k,jp1,i)
    end do
    !!! Reconstruction Moments centrés gauche et droite
!    if ( n(jj,i).gt.1.d-10) then
      call recons_o2(vp,mcg,mcd,Dx,Dt,limiter)
    !else
    !  mcg(1)=n(j,i)
    !  mcg(2)=u(j,i)
    !  mcg(3)=v(j,i)
    !  mcg(4:6)=sigma(1:3,j,i)
    !  mcd=mcg
    !endif
    !if (sqrt(mcg(4)*mcg(6))-mcg(5).lt.0D0) write(*,*) 'Probleme etat gauche direction x cellule',jj,i
    !if (sqrt(mcd(4)*mcd(6))-mcd(5).lt.0D0) write(*,*) 'Probleme etat droite direction x cellule',jj,i
    !!! Calcul Moments gacuhe et droite 2*(10)
    call moment_sigma10_U_plusminus( mcg(1), mcg(2), mcg(3), mcg(4:6), momplusg(1:10,j,i), momminusg(1:10,j,i) ) 
    call moment_sigma10_U_plusminus( mcd(1), mcd(2), mcd(3), mcd(4:6), momplusd(1:10,j,i), momminusd(1:10,j,i) )
end do
end do

!
! Evaluate left interface flux in x direction
!
! Evaluate new moments due to x flux
do j=1,Nx
do i=1,Ny
    jm1=modulo(j-1-1,Nx)+1
    jp1=modulo(j+1-1,Nx)+1
    do k=1,6
        !Fx(k,j,i) = momplusg(ixflux(k),j,i) - momplusd(ixflux(k),jm1,i) + momminusg(ixflux(k),jp1,i) -  momminusd(ixflux(k),j,i)
        Fx(k,j,i) = momplusd(ixflux(k),jm1,i) + momminusg(ixflux(k),j,i) 
    end do
end do
end do

end subroutine Fluxx_MUSCL_fluxsplit
!---------------------------------------------------------------------------------------!
subroutine Fluxy_MUSCL_fluxsplit(MM,Nx,Ny,Fy,Dy,Dt,M0,limiter)

integer, intent(in) :: Nx,Ny
real*8, intent(in) :: MM(1:6,1:Nx,1:Ny),Dy,Dt,limiter
real*8 :: Fy(1:10,1:Nx,1:Ny+1)
real*8, intent(out) :: M0(1:6,1:Nx,1:Ny)

real*8 :: momplusg(1:10,Nx,Ny+1), momminusg(1:10,Nx,Ny+1)
real*8 :: momplusd(1:10,Nx,Ny+1), momminusd(1:10,Nx,Ny+1)
integer :: i,j,k,iyflux(1:6)
integer :: im1,ii,ip1
real*8 :: n(1:Nx,1:Ny), u(1:Nx,1:Ny), v(1:Nx,1:Ny), sigma(1:3,1:Nx,1:Ny)

real*8 :: vp(3,6)
real*8 :: mcg(6),mcd(6)

iyflux=(/3,5,6,8,9,10/)


momplusg=0.d0
momminusg=0.d0
momplusd=0.d0
momminusd=0.d0
!
! Evaluate left and right states in y direction
do j=1,Nx
do i=1,Ny
    call moment_inversion_2D_gaussian(MM(:,j,i),n(j,i),u(j,i),v(j,i),sigma(:,j,i))
    call moment_sigma10_U_plusminus( n(j,i), u(j,i),v(j,i), sigma(:,j,i), momplusg(1:10,j,i), momminusg(1:10,j,i) )
    M0(:,j,i)=momplusg(1:6,j,i)+momminusg(1:6,j,i)
end do
end do
!
do j=1,Nx
do i=1,Ny
    im1=modulo(i-1-1,Ny)+1
    ii =modulo(i-1,Ny)+1
    ip1=modulo(i-1+1,Ny)+1
    vp(1,1)=n(j,im1)
    vp(1,2)=u(j,im1)
    vp(1,3)=v(j,im1)
    vp(2,1)=n(j,ii)
    vp(2,2)=u(j,ii)
    vp(2,3)=v(j,ii)
    vp(3,1)=n(j,ip1)
    vp(3,2)=u(j,ip1)
    vp(3,3)=v(j,ip1)
    do k=1,3
       vp(1,k+3)=sigma(k,j,im1)
       vp(2,k+3)=sigma(k,j,ii)
       vp(3,k+3)=sigma(k,j,ip1)
    end do
    !!! Reconstruction Moments centrés gauche et droite
    !if ( n(j,ii).gt.1.d-10) then
      call recons_o2(vp,mcg,mcd,Dy,Dt,limiter)
    !else
    !  mcg(1)=n(j,i)
    !  mcg(2)=u(j,i)
    !  mcg(3)=v(j,i)
    !  mcg(4:6)=sigma(1:3,j,i)
    !  mcd=mcg
    !endif
    
    
    !if (mcg(4)*mcg(5)-mcg(6)**2.lt.0D0) write(*,*) 'Probleme etat gauche direction y cellule',j,i
    !if (mcd(4)*mcd(5)-mcd(6)**2.lt.0D0) write(*,*) 'Probleme etat droite direction y cellule',j,i
    !!! Reconstruction Moments 2*(10)
    call moment_sigma10_V_plusminus( mcg(1), mcg(2), mcg(3), mcg(4:6), momplusg(1:10,j,i), momminusg(1:10,j,i) )
    call moment_sigma10_V_plusminus( mcd(1), mcd(2), mcd(3), mcd(4:6), momplusd(1:10,j,i), momminusd(1:10,j,i) )

end do
end do

!
! Evaluate left interface flux in y direction
!
do j=1,Nx
do i=1,Ny
    im1=modulo(i-1-1,Ny)+1
    ip1=modulo(i-1+1,Ny)+1
    do k=1,6
      ! Fy(k,j,i)= momplusg(iyflux(k),j,i) - momplusd(iyflux(k),j,im1) + momminusg(iyflux(k),j,ip1) -  momminusd(iyflux(k),j,i)  
       Fy(k,j,i)= momplusg(iyflux(k),j,im1) + momminusd(iyflux(k),j,i) 
    end do
end do
end do

end subroutine Fluxy_MUSCL_fluxsplit
!---------------------------------------------------------------------------------------!
function moment_advection_2Dy_MUSCL_fluxsplit( M0, order, Nx, Dx, Ny, Dy, dt,limiter)
! moment_advection_2Dyx updates 15 moments in 2D due to spatial fluxes using
! y-x splitting
!
integer, intent(in) :: order, Nx, Ny
real*8, intent(in) :: M0(6,Nx,Ny), Dx, Dy, Dt,limiter
real*8 :: M1(6,Nx,Ny),M2(6,Nx,Ny),moment_advection_2Dy_MUSCL_fluxsplit(6,Nx,Ny)

integer :: i,j,k
real*8 :: Fx(10,Nx+1,Ny),Fy(10,Nx,Ny+1)
real*8 :: Mcorr(6,Nx,Ny)
!
!----------------------------------------------------------!
!    Reconstruction des paramètres (moments centrés)       !
!----------------------------------------------------------!
!
! Moments (6)
M1=M0
!
! Evaluate quadrature in y direction
call Fluxy_MUSCL_fluxsplit(M1,Nx,Ny,Fy,Dy,Dt,Mcorr,limiter)
! Evaluate new moments due to y flux 1st step RK2
do j=1,Nx
    do i=1,Ny
        do k=1,6
	        M1(k,j,i)  = M0(k,j,i)  - ( Fy(k,j,i+1)    -  Fy(k,j,i) )/Dy*dt*0.5D0 
        end do
    end do
end do
!
! Evaluate quadrature in y direction
call Fluxy_MUSCL_fluxsplit(M1,Nx,Ny,Fy,Dy,Dt,M2,limiter)
! Evaluate new moments due to y flux 2nd step RK2
do j=1,Nx
    do i=1,Ny
        do k=1,6
	        M1(k,j,i)  = M0(k,j,i)  - ( Fy(k,j,i+1)    -  Fy(k,j,i) )/Dy*dt
        end do
    end do
end do
!

moment_advection_2Dy_MUSCL_fluxsplit=M1
 
end function moment_advection_2Dy_MUSCL_fluxsplit
!-----------------------------------!




end module AG2D_transport_MUSCL_fluxsplit