# -*- coding: utf-8 -*-
"""
Plot relax and reference solution from AG2D A. Vie's code [CICP2015]
"""

import imp
from Relax import data_process as dproc

# Load sod_process module
mod_name = 'sod_process'
tup = imp.find_module(mod_name, ['../post'])
sp = imp.load_module(mod_name, *tup)


if __name__ == '__main__':

    # Load and plot initial solution
    ref = sp.load_ref('mom_0000.dat')
    sim = sp.load_sim(0)
    dproc.plot_ref_sim(ref, sim, varnames=sp.varnames)

    # Load and plot last solution
    ref = sp.load_ref('mom_0010.dat')
    sim = sp.load_sim()
    dproc.plot_ref_sim(ref, sim, varnames=sp.varnames)
