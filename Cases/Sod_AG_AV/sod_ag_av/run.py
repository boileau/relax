"""
SOD test case from CICP2015 Vie et al.
"""

scheme = 'relax-ag-o1'    # Scheme name
casetype = "SOD-AG"       # Case type
epsi_e = 1.0e-10          # Energy threshold for PGD/Gas dynamics
gamma = 3.0               # Polytropic coefficient
CFL = 0.5                 # CFL number
tf = 0.1                  # simulation final time
maxite = 0                # maximum number of iterations
Lx = 2.0                  # Domain length
N = 1000                  # Number of points
nsect = 0                 # number of sections
dt_store = 0.1            # Storage time interval
dt_plot = 0.0             # Plotting time interval
miniplot = True           # PLot first and last solution ?

# Add probes:
#probe = []
#probe.append({'position': 0.1, 'comment': 'First point'})
#probe.append({'position': (0.5), 'comment': 'Second point'})

err_tol = {'p_11': 0.00048389359806515154,
           'p_12': 0.0063339884532189715,
	   'p_22': 0.006855811760096267,
	   'u': 0.0017726893046028193,
	   'rho': 0.010638777949356146,
	   'v': 0.012088362982809738}
tol_type = 'lower'

