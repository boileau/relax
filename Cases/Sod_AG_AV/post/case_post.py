# -*- coding: utf-8 -*-
"""
Compute relative error
Reference code is in: Cases/AG_TSM/Code_JL
"""

import numpy as np
from Relax import data_process as dproc


# Default tolerance
tol = {'p_11': 6.4288705557380812,
       'p_12': 3.1462728090784577,
       'p_22': 3.6991444901951906,
       'u': 14.87699138149555,
       'v': 5.5204471206658967,
       'rho': 4.2723259119645736}

varnames = 'rho', 'u', 'v', 'p_11', 'p_12', 'p_22'


def filter_x(x):
    """return a filter to restrict domain"""
    return np.logical_and(x > 0.5, x <= 1.5)


def load_ref(filename):
    """return a reference data dict"""
    ref = {}
    path = "Ref/" + filename
    ref_data = np.loadtxt(path)

    x = ref_data[:, 0]
    x_filter = filter_x(x)
    ref['x'] = x[x_filter]

    rho = ref_data[:, 2][x_filter]
    u = ref_data[:, 3][x_filter]/rho
    v = ref_data[:, 4][x_filter]/rho

    ref['rho'] = rho
    ref['u'] = u
    ref['v'] = v
    ref['p_11'] = ref_data[:, 5][x_filter] - rho*u*u
    ref['p_12'] = ref_data[:, 6][x_filter] - rho*u*v
    ref['p_22'] = ref_data[:, 7][x_filter] - rho*v*v

    return ref


def load_sim(ite='last'):
    """return a simulation data dict"""
    sim = {}
    f = dproc.open_solution()  # Open solution.h5
    x = dproc.get_grid(f)

    x_filter = filter_x(x)
    sim['x'] = x[x_filter]

    vardict = dproc.get_sol_dict(f, ite=ite)
    sim.update({k: var[x_filter] for k, var in vardict.items()
                if k != 't'})
    sim['t'] = vardict['t']
    return sim


def interpolate(ref, sim):
    """interpolate ref on sim"""
    n_ref = len(ref['x'])
    n_sim = len(sim['x'])
    if (n_ref % n_sim) != 0:
        exit("Number of elements in reference ({}) must be a multiple of \
              number in simulation ({}).".format(n_ref, n_sim))
    else:
        jump = n_ref // n_sim
        for k, v in ref.items():
            ref[k] = v[::jump]
        return ref


def compute(sol_path="solution.h5", verbose=True):
    """Load data and compute error"""
    error = {}
    ref = load_ref('mom_0010.dat')
    sim = load_sim()

    for var in varnames:
        error[var] = dproc.L2_err(sim[var], ref[var], norm=ref[var].max())

    if verbose:
        for k, v in error.items():
            print("Error({}) = {}".format(k, v))

    return error


def get_error_status(tol, tol_type, verbose=True):
    """Compute error and return status"""
    error = compute(verbose=verbose)
    return dproc.check(error, tol, tol_type, verbose=verbose)
