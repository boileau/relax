module TSM_aff_evap_drag
! transport das l'espace des phases avec TSM
implicit none

double precision, parameter :: pi=3.14159265358979324d0
double precision, private, parameter :: epsi=1.d-12


contains

subroutine check_S(nsec,S,rapportmn,nl,ml,nn)
! routine qui verifie qu'on est dans l'espace des moments et projette si les valeurs sont 'petites'
  integer, intent(in) :: nsec
  double precision, intent(in) :: S(0:nsec)
   double precision, intent(in) :: rapportmn ! rhol/(6*sqrt(pi))
  double precision, intent(inout) :: nl(nsec),ml(nsec)
  integer, intent(in) :: nn
  integer :: k
  double precision :: val,ss
  do k = 1, nsec
     if(ml(k)<rapportmn*S(k-1)**1.5d0*nl(k).or.ml(k)>rapportmn*S(k)**1.5d0*nl(k)) then
        if(ml(k)>epsi) then
           print*,'sortie de l espace des moments',nn,k,rapportmn*S(k-1)**1.5d0*nl(k),ml(k),&
           											rapportmn*S(k)**1.5d0*nl(k)
           print*,'nl,ml',nl(k),ml(k)
           stop
        else if(ml(k)>1.d-50) then
           if(ml(k)>1.d-20) print*,'projection',nn,k,rapportmn*S(k-1)**1.5d0*nl(k),ml(k),rapportmn*S(k)**1.5d0*nl(k)
           if(ml(k)>1.d-20) stop
           ss = S(k)
           if(k==nsec) ss = 2.d0*S(k-1)-S(k-2)
           nl(k) = ml(k)*2.5d0*(ss-S(k-1))/(rapportmn*(ss**2.5d0-S(k-1)**2.5d0))
           if(ml(k)<rapportmn*S(k-1)**1.5d0*nl(k).or.ml(k)>rapportmn*S(k)**1.5d0*nl(k))then
              print*,'erreur projection',S(k-1)**1.5d0,ml(k)/rapportmn/nl(k),S(k)**1.5d0
              print*,k,S(k-1),S(k),nl(k),ml(k),2.5d0*(S(k)-S(k-1))/(S(k)**2.5d0-S(k-1)**2.5d0)
              stop
           end if
        else
           nl(k) = 0.d0
           ml(k) = 0.d0
        end if
     end if
     if(isnan(nl(k))) then
        print*,'n = NAN, k=',k
        print*,nl(k),ml(k),nn
        stop
     end if
  end do
end subroutine

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!  PAS DE TEMPS EVAPORATION TRAINEE   !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine evap_drag_aff(nsec,S,Sinta,Sintb,Rs,rapportmn,alpha_drag,dt,ug,nk,mk,uk,sigmak,nkp,mkp,ukp,sigmakp)
! un pas de temps d'evaporation avec l'algorithme du SIAP un peu ameliore
! entrees:
!    dt: pas de temps
!    S(k-1),S(k): bornes de la section k, k=1,..,nsec (variable globale)
!    ug : vitesse du gaz
!    nk(nsec): vecteur de moments d'ordre 0 dans les nsec sections a l'instant t
!    mk(nsec): vecteur de moments d'ordre 3/2 dans les nsec sections a l'instant t
!    uk(nsec): vecteur de vitesse dans les nsec sections a l'instant t
!    sigma(nsec): vecteur de moment d'ordre 2 dans les nsec a l'instant t
! sorties:
!    nkp(nsec): vecteur de moments d'ordre 0 dans les nsec sections a l'instant t+dt
!    mkp(nsec): vecteur de moments d'ordre 3/2 dans les nsec sections a l'instant t+dt
!    ukp(nsec): vecteur de vitesse dans les nsec sections a l'instant t+dt
!    sigmakp(nsec): vecteur de moments d'ordre 3/2 dans les nsec a l'instant t+dt
!
   integer, intent(in) :: nsec ! nombre de section
   double precision, intent(in) :: S(0:nsec) ! bornes des sections (surfaces en micron^2)
   double precision, intent(in) :: Sinta(Nsec),Sintb(Nsec) ! bornes interne pour la reconstruction affine
   double precision, intent(in) :: Rs ! taux d'evaporation en m^2/s
   double precision, intent(in) :: rapportmn ! rhol/(6*sqrt(pi))
   double precision, intent(in) :: alpha_drag ! coefficient pour la trainee en m^2/s
   double precision, intent(in) :: dt,ug,nk(nsec),mk(nsec),uk(nsec),sigmak(nsec)
   double precision, intent(out) :: nkp(nsec),mkp(nsec),ukp(nsec),sigmakp(nsec)
   double precision :: Sa(nsec),Sb(nsec),alpha(nsec),beta(nsec),mm(0:3),mmp(0:3),wei(2),absc(2)
   double precision :: dd,SS,m1,m2,u1,u2,v1,v2,sigma1,sigma2,delta,coef1,coef2,dtl,dtmax
   double precision, parameter :: Xsmall = 1.d-30
   integer :: i,j,k,ipar(1),niter,ni,err

   integer, parameter :: methode_choisie = 2     ! 1 : reconstruction a un moment
                                                 ! 2 : reconstruction a deux moments
!
! discretization
   dtmax = dt+1.d0
   do k = 1, nsec-1
      dtmax = min(dtmax,(S(k)-S(k-1))/(Rs*1.d12))
   end do
   niter = int(dt/dtmax) + 1
   dtl = dt/dble(niter)
   if(niter>1) print*,'sous discretisation',niter,dt,dtmax,dtl
   !print*,'CFL',(Rs*dtl/(S(k)-S(k-1))*1.d12,k=1,nsec-1)
!
! boucle en temps
   nkp = nk
   mkp = mk
   ukp = uk
   sigmakp = sigmak
   if (methode_choisie.eq.2) call check_S(nsec,S,rapportmn,nkp,mkp,-2)
   do ni = 1, niter
!
! reconstruction
   do k = 1, nsec
      ! cas TSM
      if (methode_choisie.eq.2) then   ! Reconstruction a deux moments
         call reconstruct_affine(S(k-1),S(k),Sinta(k),Sintb(k),nkp(k),mkp(k)/rapportmn,Sa(k),Sb(k),alpha(k),beta(k))
      else                             ! Reconstruction a un moment
      ! cas OSM
         Sa(k) = S(k-1)
         Sb(k) = S(k)
         alpha(k) = mk(k)/(rapportmn*(S(k)**2.5d0-S(k-1)**2.5d0)/2.5d0)
         beta(k) = alpha(k)
      end if
   end do
! 
   dd = Rs*dtl*1.d12 ! attention: conversion d'unite
   mmp = 0.d0
   do k = 1, nsec-1
      ! integrales de S(k-1)+dd a S(k) de S**i*frecons(S)
      mm = 0.d0
      SS = S(k-1) + dd
      if(SS<Sa(k)) SS = Sa(k)
      if(SS<Sb(k)) then
         delta = Sb(k)-SS
         do i = 0, 3
            mm(i) = -(beta(k)-alpha(k))/(Sb(k)-Sa(k))*delta**(i+2)/dble(i+2)+beta(k)*delta**(i+1)/dble(i+1)
         end do
      end if
      ! quadrature et evolution des abscisses de la quadrature
      if(mm(0)>Xsmall) then
         call quadrature_2nodes(0.d0,delta,mm,absc,wei,err)
         if(err.ne.0) then
            print*,'erreur quadrature 1',k
            print*,'alpha,beta,Sa,Sb,delta',alpha(k),beta(k),Sa(k),Sb(k),delta
            print*,'mm',mm
            stop
         end if
         absc = Sb(k)-absc
         m1 = wei(1)*(absc(1)-dd)**1.5d0+wei(2)*(absc(2)-dd)**1.5d0
         if(m1.le.S(k-1)**1.5d0*mm(0).or.m1.ge.(S(k)-dd)**1.5d0*mm(0)) then
            print*,'interm 1: sortie de l espace des moments',k
            print*,S(k-1)**1.5d0*mm(0),m1,(S(k)-dd)**1.5d0*mm(0)
            if(m1>1.d-50)stop
         end if

         v1 = ug+(ukp(k)-ug)*(1.d0-dd/absc(1))**(alpha_drag/Rs)
         v2 = ug+(ukp(k)-ug)*(1.d0-dd/absc(2))**(alpha_drag/Rs)


         u1 = wei(1)*(absc(1)-dd)**1.5d0*v1 &
             +wei(2)*(absc(2)-dd)**1.5d0*v2

         sigma1 = wei(1)*(absc(1)-dd)**1.5d0*(v1**2.d0+(1.d0-dd/absc(1))**(2.d0*alpha_drag/Rs)*sigmakp(k)) &
                 +wei(2)*(absc(2)-dd)**1.5d0*(v2**2.d0+(1.d0-dd/absc(2))**(2.d0*alpha_drag/Rs)*sigmakp(k))
         
         u1 = u1/m1
         m1 = m1*rapportmn
      else
         m1 = 0.d0
         u1 = ug
         sigma1 = 0.d0
         mm = 0.d0
      end if
      ! integrales de S(k) a S(k)+dd de S**i*frecons(S)
      mmp = 0.d0
      SS = S(k) + dd
      if(SS>Sb(k+1)) SS = Sb(k+1)
      if(SS>Sa(k+1)) then
         delta = SS-Sa(k+1)
         do i = 0, 3
            mmp(i) = (beta(k+1)-alpha(k+1))/(Sb(k+1)-Sa(k+1))*delta**(i+2)/dble(i+2)+alpha(k+1)*delta**(i+1)/dble(i+1)
         end do
      end if
      ! quadrature et evolution des abscisses de la quadrature
      if(mmp(0)>Xsmall) then
         call quadrature_2nodes(0.d0,delta,mmp,absc,wei,err)
!         if(err.ne.0) then
!            print*,'erreur quadrature 2',k+1
!            print*,'alpha,beta,Sa,Sb,delta',alpha(k+1),beta(k+1),Sa(k+1),Sb(k+1),delta
!            print*,'mm',mmp
!            stop
!         end if
         absc = absc + Sa(k+1)
         m2 = wei(1)*(absc(1)-dd)**1.5d0+wei(2)*(absc(2)-dd)**1.5d0
!         if(m2.le.(S(k)-dd)**1.5d0*mmp(0).or.m2.ge.S(k)**1.5d0*mmp(0)) then
!            print*,'interm 2: sortie de l espace des moments',k+1
!            print*,(S(k)-dd)**1.5d0*mmp(0),m2,S(k)**1.5d0*mmp(0)
!            if(m2>1.d-50)stop
!         end if

         v1 = ug+(ukp(k+1)-ug)*(1.d0-dd/absc(1))**(alpha_drag/Rs)
         v2 = ug+(ukp(k+1)-ug)*(1.d0-dd/absc(2))**(alpha_drag/Rs)

         u2 = wei(1)*(absc(1)-dd)**1.5d0*v1 &
             +wei(2)*(absc(2)-dd)**1.5d0*v2
         sigma2 = wei(1)*(absc(1)-dd)**1.5d0*(v1**2.d0+(1.d0-dd/absc(1))**(2.d0*alpha_drag/Rs)*sigmakp(k+1)) &
                 +wei(2)*(absc(2)-dd)**1.5d0*(v2**2.d0+(1.d0-dd/absc(2))**(2.d0*alpha_drag/Rs)*sigmakp(k+1))

         u2 = u2/m2
         m2 = m2*rapportmn
      else
         m2 = 0.d0
         u2 = ug
         sigma2 = 0.d0
         mmp = 0.d0
      end if
      nkp(k) = mm(0)+ mmp(0)
      mkp(k) = m1 + m2
      if((mkp(k).le.S(k-1)**1.5d0*nkp(k)*rapportmn.or.mkp(k).ge.S(k)**1.5d0*nkp(k)*rapportmn).and.(mkp(k)+nkp(k).ne.0.d0)) then
            print*,'final: sortie de l espace des moments',k
            print*,S(k-1)**1.5d0*nkp(k)*rapportmn,mkp(k),S(k)**1.5d0*nkp(k)*rapportmn
            print*,mm(0),mmp(0)
            print*,m1,m2
            print*,S(k-1)**1.5d0*rapportmn,m1/mm(0),m2/mmp(0),S(k)**1.5d0*rapportmn
            if(mkp(k)>1.d-50)stop
      end if
      if(mkp(k).ne.0.d0) then
         ukp(k) = (m1*u1 + m2*u2)/(m1 + m2)
         sigmakp(k)=rapportmn*(sigma1 + sigma2)/(m1 + m2) - ukp(k)**2!Est-ce que ca ne serait pas mieux d'ecrire sigmak=(E_to)/mkp(k)-ukp(k)**2 ? A essayer
      else
         ukp(k) = ug
         sigmakp(k) = 0
      end if
   end do
!
   k = nsec
   ! integrales de S(k-1)+dd a S(k) de S**i*frecons(S)
   mm = 0.d0
   SS = S(k-1) + dd
   if(SS<Sa(k)) SS = Sa(k)
   if(SS<Sb(k)) then
      delta = Sb(k)-SS
      do i = 0, 3
         mm(i) = -(beta(k)-alpha(k))/(Sb(k)-Sa(k))*delta**(i+2)/dble(i+2)+beta(k)*delta**(i+1)/dble(i+1)
      end do
   end if
   nkp(k) = mm(0)
   ! quadrature et evolution des abscisses de la quadrature
   if(mm(0)>Xsmall) then
      call quadrature_2nodes(0.d0,delta,mm,absc,wei,err)
      if(err.ne.0) then
         print*,'erreur quadrature 3',k
         print*,'alpha,beta,Sa,Sb,delta',alpha(k),beta(k),Sa(k),Sb(k),delta
         print*,'mm',mm
         stop
      end if
      absc = Sb(k)-absc
      mkp(k) = wei(1)*(absc(1)-dd)**1.5d0+wei(2)*(absc(2)-dd)**1.5d0
      if(mkp(k).le.S(k-1)**1.5d0*mm(0).or.mkp(k).ge.(S(k)-dd)**1.5d0*mm(0)) then
         print*,'interm 1f: sortie de l espace des moments',k
         print*,S(k-1)**1.5d0*mm(0),mkp(k),(S(k)-dd)**1.5d0*mm(0)
         if(mkp(k)>1.d-50)stop
      end if

      v1 = ug+(ukp(k)-ug)*(1.d0-dd/absc(1))**(alpha_drag/Rs)
      v2 = ug+(ukp(k)-ug)*(1.d0-dd/absc(2))**(alpha_drag/Rs)


      ukp(k) = wei(1)*(absc(1)-dd)**1.5d0*v1 &
              +wei(2)*(absc(2)-dd)**1.5d0*v2
      sigmakp(k) = wei(1)*(absc(1)-dd)**1.5d0*(v1**2.d0+(1.d0-dd/absc(1))**(2.d0*alpha_drag/Rs)*sigmakp(k)) &
                 +wei(2)*(absc(2)-dd)**1.5d0*(v2**2.d0+(1.d0-dd/absc(2))**(2.d0*alpha_drag/Rs)*sigmakp(k))
      
      ukp(k) = ukp(k)/mkp(k)
      sigmakp(k)=sigmakp(k)/mkp(k)
      sigmakp(k)=sigmakp(k)-ukp(k)**2
      mkp(k) = mkp(k)*rapportmn
   else
      mkp(k) = 0.d0
      ukp(k) = ug
      sigmakp(k) = 0.d0
   end if
!
   do k = 1, nsec
      if(isnan(mkp(k))) then
           print*,'NaN pour mk, k=',k
           stop
      end if
      if(isnan(ukp(k))) then
           print*,'NaN pour uk, k=',k
           stop
      end if
   end do
   if (methode_choisie.eq.2) call check_S(nsec,S,rapportmn,nkp,mkp,-1)
! fin boucle en temps
   end do
   return
!                                                                                                  
end subroutine

subroutine quadrature_2nodes(a,b,mom,absc,wei,err)
   double precision, intent(in)  :: a,b,mom(4)
   double precision, intent(out) :: absc(2),wei(2)
   integer, intent(out) :: err
   double precision :: mm(3),moy,sigma2,q,e,x,val,abmoy
   double precision :: p1,p2,p3,c1,c2,c3
   integer          :: i,j,k
   double precision, parameter :: mommin=0.d0, epsi=1.d-10
!
   err = 0
   wei = 0.5d0*mom(1)*(/1.d0,1.d0/)
   absc = 0.5d0*(a+b)*(/1.d0,1.d0/)
   if (mom(1).le.mommin) return
   mm = mom(2:4)/mom(1)
!
   c1 = (mm(1)-a)/(b-a)
   c2 = (mm(2)-2.d0*a*mm(1)+a**2)/(b-a)**2
   c3 = (mm(3)-3.d0*a*mm(2)+3.d0*a**2*mm(1)-a**3)/(b-a)**3
   !p1 = (mm(1)-a)/(b-a)
   !p2 = (mm(2)-mm(1)**2)/(mm(1)-a)/(b-mm(1))
   !p3 = (b-mm(1))*(mm(3)*(mm(1)-a)-(mm(2)-a*mm(1)-a**2)*mm(2)-a**2*mm(1)**2)/((b-a)*(mm(2)-mm(1)**2)*(-mm(2)+(a+b)*mm(1)-a*b))
   p1 = c1
   p2 = (c2-c1**2)/c1/(1-c1)
   p3 = (1-c1)*(c1*c3-c2**2)/(c2-c1**2)/(c1-c2)
   if(p1.le.0.D0.or.p1.ge.1.d0.or.p2.le.0.D0.or.p2.ge.1.d0.or.p3.le.0.D0.or.p3.ge.1.d0) then
      print*,'moments canoniques',p1,p2,p3
      print*,'a,b',a,b
      print*,'moments sur [ab]',mm
      print*,'moments sur [01]',c1,c2,c3
      err = 1
   end if
!
   moy = mm(1)
   sigma2 = mm(2)-mm(1)**2
   if(sigma2.le.0.d0) sigma2 = 1.d-12
   !q = mm(3)-mm(1)**3-3.d0*sigma2*moy
   q = mm(3)-3.d0*mm(2)*mm(1)+2.d0*mm(1)**3
   e = q/sigma2
   x = 0.5d0*e/dsqrt(e*e+4.d0*sigma2)
   wei(1) = 0.5d0+x
   wei(2) = 0.5d0-x
   val = (e+dsqrt(e*e+4.d0*sigma2))**2/4.d0
   absc(1) = moy -dsqrt(sigma2**2/val)
   absc(2) = moy +dsqrt(val)
   if(absc(1)<a) then
      if(wei(1)*(a-absc(1))>epsi) print*,'changt absc1 a',absc(1),a,a-absc(1),wei(1)
      absc(1) = a
   end if
   if(absc(2)<a) then
      if(wei(2)>epsi) print*,'changt absc2 a+eps',absc(2),a+epsi
      absc(2) = a+epsi
   end if
   if(absc(2)>b) then
      if(wei(2)*(b-absc(2))>epsi) print*,'changt absc2 b',absc(2),b,b-absc(2),wei(2)
      absc(2) = b
   end if
   if(absc(1)>b) then
      if(wei(1)>epsi) print*,'changt absc1 b-eps',absc(1),b-epsi
      absc(1) = b-epsi
   end if
   abmoy = 0.5d0*(a+b)
   if(dabs(1.d0-wei(1)-wei(2))>epsi.or.dabs(mm(1)-wei(1)*absc(1)-wei(2)*absc(2))>epsi*max(1.d0,mm(1)) &
          .or.dabs(mm(2)-wei(1)*absc(1)**2-wei(2)*absc(2)**2)>epsi*max(1.d0,mm(2))&
          .or.dabs(mm(3)-wei(1)*absc(1)**3-wei(2)*absc(2)**3)>epsi*max(1.d0,mm(3))) then
      print*,'erreur quadrature_2nodes'
      print*,'m0',1.d0,wei(1)+wei(2),1.d0-wei(1)-wei(2)
      print*,'m1',mm(1),wei(1)*absc(1)+wei(2)*absc(2),(mm(1)-wei(1)*absc(1)-wei(2)*absc(2))/mm(1)
      print*,'m2',mm(2),wei(1)*absc(1)**2+wei(2)*absc(2)**2,(mm(2)-wei(1)*absc(1)**2-wei(2)*absc(2)**2)/mm(2)
      print*,'m3',mm(3),wei(1)*absc(1)**3+wei(2)*absc(2)**3,(mm(3)-wei(1)*absc(1)**3-wei(2)*absc(2)**3)/mm(3)
      print*,'sigma',mm(2)-mm(1)**2
      print*,'a,b',a,b
      print*,'mc',p1,p2,p3
      err=2
   end if
   wei = wei*mom(1)
   return
end subroutine 

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!  RECONSTRUCTION AFFINE   !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine reconstruct_affine(Skm1,Sk,Sintak,Sintbk,nk,mk,Sa,Sb,alpha,beta)
! Recontruction d'une fonction fa(S) à partir de ses moments d'ordre 0 (nk) 
! et 3/2 (mk) sur la section [Skm1,Sk].
! fa(S) est une fonction affine sur [Sa,Sb] et nulle en dehors de l'intervalle
! et est caractérisee par: fa(Sa)=alpha et fa(Sb)=beta
! 3 cas seulement sont possibles:
! - [Sa,Sb]=[Skm1,Sk]   <=> mk/nk dans [Sintak,Sintbk]
! - Sa=Skm1 et beta=0   <=> mk/nk dans ]Skm1,Sintak[
! - Sb=Sk   et alpha=0  <=> mk/nk dans ]Sintbk,Sk[
!
   double precision, intent(in) :: Skm1,Sk,Sintak,Sintbk,nk,mk
   double precision, intent(out) :: Sa,Sb,alpha,beta
   double precision :: dS,det,Rkm1,Rk,Ra,Rb,r,rpar(2),valpoly
   !double precision, parameter :: Racc = 1.d-12, coeff = 0.5d0*35.d0**(1.d0/3.d0)
   double precision, parameter :: Racc = 1.d-12, coeff = 0.5d0*35.d0**(1.d0/3.d0)
   integer :: i,j,k,ipar(1)
!
   ipar(1) = 0
   dS = Sk-Skm1
   det = 0.5d0*(Sintbk-Sintak)
   if(mk*dS**2.ge.2.d0*Sintak*nk.and.mk*dS**2.le.2.d0*Sintbk*nk) then
      ! cas [Sa,Sb]=[Skm1,Sk]
      Sa = Skm1
      Sb = Sk
      alpha = (Sintbk/dS*nk  -0.5d0*dS*mk)/det
      beta  = (-Sintak/dS*nk +0.5d0*dS*mk)/det
   else if(mk>Skm1**1.5d0*nk.and.mk*dS**2.lt.2.d0*Sintak*nk) then
      ! cas Sa=Skm1 et beta=0
      Rkm1 = dsqrt(Skm1)
      Rk   = dsqrt(Sk)
      beta = 0.d0
      Sa = Skm1
      rpar(1) = Rkm1
      rpar(2) = mk/nk
      if(Skm1.ne.0.d0) then
         r = rpar(2)**(1.d0/3.d0)
         !Rb = zriddr(Poly1,Rkm1,Rk,1.d-12,rpar,ipar)
         ipar(1) = 0
         call muller_modifie(Poly1,r,min(coeff*r,Rk),Racc,rpar,ipar,Rb,valpoly)
      else
         Rb = (35.d0*rpar(2)/8.d0)**(1.d0/3.d0)
      end if
      Sb = Rb**2
      alpha = 2.d0*nk/(Sb-Skm1)
   elseif(mk<Sk**1.5d0*nk.and.mk*dS**2>2.d0*Sintbk*nk) then
      ! cas Sb=Sk   et alpha=0
      Rkm1 = dsqrt(Skm1)
      Rk   = dsqrt(Sk)
      alpha = 0.d0
      Sb = Sk
      rpar(1) = Rk
      rpar(2) = mk/nk
      !Ra = zriddr(Poly3,Rkm1,Rk,1.d-12,rpar,ipar)
      ipar(1) = 0
      r = rpar(2)**(1.d0/3.d0)
      call muller_modifie(Poly3,Rkm1,r,Racc,rpar,ipar,Ra,valpoly)
      Sa = Ra**2
      beta = 2.d0*nk/(Sk-Sa)
   else
      if(mk>1.d-20) print*,'sortie de l espace des moments',nk,mk,Skm1,(mk/nk)**(2.d0/3.d0),Sk
      if(mk>epsi) then 
         print*,'pb recons!',nk,mk,Skm1,(mk/nk)**(2.d0/3.d0),Sk
         stop
      end if
      Sa = Skm1
      Sb = Sk
      alpha = 5.d0*mk/(2.d0*(Sk**2.5d0-Skm1**2.5d0))
      beta  = alpha
   end if
   return
!                                                                                                  
end subroutine

!!!!!!!!!!!!!!!!!!!!!!!!!!
!!  POLYNOME DU CAS 1   !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

function Poly1(X,rpar,ipar)
! polynome P
   double precision, intent(in) :: X, rpar(*)
   integer, intent(inout) :: ipar(*)
   double precision :: Poly1
   double precision :: Rkm,Vmoy
!
   ipar(1) = ipar(1) + 1
   Rkm = rpar(1)
   Vmoy = rpar(2)
   Poly1 = (5.d0*Rkm**3-8.75*Vmoy)*Rkm**2 + X*((10.d0*Rkm**3-17.5*Vmoy)*Rkm &
            + X*((8.d0*Rkm**3-8.75*Vmoy) + X*(6.d0*Rkm**2 + X*(4.d0*Rkm + 2.d0*X))) )
   return
end function
!     
!!!!!!!!!!!!!!!!!!!!!!!!!!
!!  POLYNOME DU CAS 1   !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

function Poly3(X,rpar,ipar)
! polynome Q
   double precision, intent(in) :: X, rpar(*)
   integer, intent(inout) :: ipar(*)
   double precision :: Poly3
   double precision :: Rk,Vmoy
!
   ipar(1) = ipar(1) + 1
   Rk = rpar(1)
   Vmoy = rpar(2)
   Poly3 = (5.d0*Rk**3-8.75*Vmoy)*Rk**2 + X*((10.d0*Rk**3-17.5*Vmoy)*Rk &
            + X*((8.d0*Rk**3-8.75*Vmoy) + X*(6.d0*Rk**2 + X*(4.d0*Rk +2.d0*X))))
!
   return
end function
!     
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!  FONCTION RECONSTRUITE   !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

function affine(S,rpar,ipar)
! fonction reconstruite fa(S)
   double precision, intent(in) :: S, rpar(*)
   integer, intent(in) :: ipar(*)
   double precision :: affine
   double precision :: Sa,Sb,alpha,beta,k,val
!
   k = rpar(1)
   Sa = rpar(2)
   Sb = rpar(3)
   alpha = rpar(4)
   beta  = rpar(5)
   if(S.ge.Sa.and.S.le.Sb) then
      val = (alpha*(Sb-S)+beta*(S-Sa))/(Sb-Sa)
   else
      val = 0.d0
   end if
   affine = val*S**k
!
   return
end function

!!!!!!!!!!!!!!!!
!!  MULLER   !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine muller_modifie(func,x1,x2,xacc,rpar,ipar,racine,valfunc)
! USES func! Using modfied Muller’s method, return the root of a function func known to lie between x1 and x2. 
! The root, returned as racine, will be refined to an approximate accuracy xacc.
! valfunc if the falue of func at the root
   double precision, intent(in) :: x1,x2,xacc,rpar(*)
   integer, intent(inout) :: ipar(*)
   double precision, external :: func
   double precision, intent(out) :: racine,valfunc
   integer, parameter :: MAXIT=100
   double precision, parameter :: UNUSED=-1.11E30
   double precision :: xa,xb,xc,xd,r1,r2,fa,fb,fc,fcb,fba,fcba,gamma
   integer ::j
   fa = func(x1,rpar,ipar)
   fb = func(x2,rpar,ipar)
   if(fa*fb<0.d0) then
      if(fa<0.d0) then
         xa=x1
         xb=x2
      else
         fc = fa
         xa = x2
         fa = fb
         xb = x1
         fb = fc
      end if
      racine=UNUSED ! Any highly unlikely value, to simplify logic below.
      do j=1,MAXIT 
         xc=0.5d0*(xa+xb)
         fc=func(xc,rpar,ipar) ! First of two function evaluations per itseration.
         fcb = (fc-fb)/(xc-xb)
         fba = (fb-fa)/(xb-xa)
         fcba = (fcb-fba)/(xc-xa)
         gamma = fcb+(xc-xb)*fcba
         if(gamma**2<4.d0*fc*fcba) then
            print*,'pb dans la recherche de racine de f'
            stop
         end if
         r1 = xc -2.d0*fc/(gamma + dsqrt(gamma**2-4.d0*fc*fcba)) ! Updating formula 1.
         r2 = xc -2.d0*fc/(gamma - dsqrt(gamma**2-4.d0*fc*fcba)) ! Updating formula 2.
         if((r1.ge.xa.and.r1.le.xb).or.(r1.ge.xb.and.r1.le.xa)) then
         !if((r1-xa)/(xb-xa).ge.0.d0.and.(r1-xa)/(xb-xa).le.1.d0) then
            xd = r1
         elseif((r2.ge.xa.and.r2.le.xb).or.(r2.ge.xb.and.r2.le.xa)) then
         !elseif((r2-xa)/(xb-xa).ge.0.d0.and.(r2-xa)/(xb-xa).le.1.d0) then
            xd = r2
         else
            print*,'pb',xa,r1,r2,xb,r1-xa,r2-xa,xb-xa
         endif
         if (dabs(xd-racine).le.xacc) return
         racine  = xd
         valfunc=func(racine,rpar,ipar) ! Second of two function evaluations per iteration
         if(valfunc<0.d0) then ! Bookkeeping to keep the root bracketed on next iteration.
            xa = racine
            fa = valfunc
            if(fc>0.d0) then
               xb = xc
               fb = fc
            end if
         else if (valfunc>0.d0) then
            xb = racine
            fb = valfunc
            if(fc<0.d0)  then
               xa = xc
               fa = fc
            end if
         else
            return
         end if
         if(dabs(xa-xb).le.xacc) return
      enddo
      print*, 'muller_modifie exceed maximum iterations'
   else if (fa.eq.0.d0) then
      racine  = x1
      valfunc = fa
   else if (fb.eq.0.d0) then
      racine  = x2
      valfunc = fb
   else
      print*, 'root must be bracketed in muller_modifie',fa,fb,x1,x2
      stop
   endif
   return
end subroutine


end module 
