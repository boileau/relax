set terminal png
set output 'masse_tot.png'
set xlabel 'temps (s)'
set ylabel 'masse totale (kg)'
plot 'solution_analytique.dat' using 1:2 with line title 'Solution analytique', \
 'solution_numerique.dat' using 1:2 with line title 'Solution numerique'

set output 'qdm_tot.png'
set xlabel 'temps (s)'
set ylabel 'quantite de mouvement totale (kg.m.s-1)'
plot 'solution_analytique.dat' using 1:3 with line title 'Solution analytique', \
 'solution_numerique.dat' using 1:3 with line title 'Solution numerique'

set output 'energie_tot.png'
set xlabel 'temps (s)'
set ylabel 'energie totale (kg.m2.s-2)'
plot 'solution_analytique.dat' using 1:4 with line title 'Solution analytique', \
 'solution_numerique.dat' using 1:4 with line title 'Solution numerique'

set output 'u_moy.png'
set xlabel 'temps (s)'
set ylabel 'vitesse moyenne (m.s-1)'
plot 'solution_analytique.dat' using 1:7 with line title 'Solution analytique', \
 'solution_numerique.dat' using 1:7 with line title 'Solution numerique'

set output 'sigma_moy.png'
set xlabel 'temps (s)'
set ylabel 'sigma moyen (m2.s-2)'
plot 'solution_analytique.dat' using 1:8 with line title 'Solution analytique', \
 'solution_numerique.dat' using 1:8 with line title 'Solution numerique'


set output 'evolution_masse_section_par_section.png'
set xlabel 'surface'
set ylabel 'masse de la section (kg)'
plot 'solution_analytique_masse_par_section.dat' using 1:2 with line title 'pas de temps analytique 1', \
 'solution_numerique_masse_par_section.dat' using 1:2 with line title 'pas de temps numerique 1', \
 'solution_analytique_masse_par_section.dat' using 1:11 with line title 'pas de temps analytique 10', \
 'solution_numerique_masse_par_section.dat' using 1:11 with line title 'pas de temps numerique 10', \
 'solution_analytique_masse_par_section.dat' using 1:21 with line title 'pas de temps analytique 20', \
 'solution_numerique_masse_par_section.dat' using 1:21 with line title 'pas de temps numerique 20'

set output 'evolution_qdm_section_par_section.png'
set xlabel 'surface'
set ylabel 'qdm de la section (kg.m.s-1)'
plot 'solution_analytique_qdm_par_section.dat' using 1:2 with line title 'pas de temps analytique 1', \
 'solution_numerique_qdm_par_section.dat' using 1:2 with line title 'pas de temps numerique 1', \
 'solution_analytique_qdm_par_section.dat' using 1:11 with line title 'pas de temps analytique 10', \
 'solution_numerique_qdm_par_section.dat' using 1:11 with line title 'pas de temps numerique 10', \
 'solution_analytique_qdm_par_section.dat' using 1:21 with line title 'pas de temps analytique 20', \
 'solution_numerique_qdm_par_section.dat' using 1:21 with line title 'pas de temps numerique 20'

set output 'evolution_energie_section_par_section.png'
set xlabel 'surface'
set ylabel 'energie de la section (kg.m2.s2)'
plot 'solution_analytique_energie_par_section.dat' using 1:2 with line title 'pas de temps analytique 1', \
 'solution_numerique_energie_par_section.dat' using 1:2 with line title 'pas de temps numerique 1', \
 'solution_analytique_energie_par_section.dat' using 1:11 with line title 'pas de temps analytique 10', \
 'solution_numerique_energie_par_section.dat' using 1:11 with line title 'pas de temps numerique 10', \
 'solution_analytique_energie_par_section.dat' using 1:21 with line title 'pas de temps analytique 20', \
 'solution_numerique_energie_par_section.dat' using 1:21 with line title 'pas de temps numerique 20'

set output 'evolution_u_section_par_section.png'
set xlabel 'surface'
set ylabel 'vitesse de la section (m.s-1)'
plot 'solution_analytique_u_par_section.dat' using 1:2 with line title 'pas de temps analytique 1', \
 'solution_numerique_u_par_section.dat' using 1:2 with line title 'pas de temps numerique 1', \
 'solution_analytique_u_par_section.dat' using 1:11 with line title 'pas de temps analytique 10', \
 'solution_numerique_u_par_section.dat' using 1:11 with line title 'pas de temps numerique 10', \
 'solution_analytique_u_par_section.dat' using 1:21 with line title 'pas de temps analytique 20', \
 'solution_numerique_u_par_section.dat' using 1:21 with line title 'pas de temps numerique 20'

set output 'evolution_sigma_section_par_section.png'
set xlabel 'surface'
set ylabel 'sigma de la section (m2.s-2)'
plot 'solution_analytique_sigma_par_section.dat' using 1:2 with line title 'pas de temps analytique 1', \
 'solution_numerique_sigma_par_section.dat' using 1:2 with line title 'pas de temps numerique 1', \
 'solution_analytique_sigma_par_section.dat' using 1:11 with line title 'pas de temps analytique 10', \
 'solution_numerique_sigma_par_section.dat' using 1:11 with line title 'pas de temps numerique 10', \
 'solution_analytique_sigma_par_section.dat' using 1:21 with line title 'pas de temps analytique 20', \
 'solution_numerique_sigma_par_section.dat' using 1:21 with line title 'pas de temps numerique 20'

set output 'erreur.png'
set xlabel 't'
set ylabel 'erreur'
plot 'erreur_quantites_conservatives.dat' using 1:2 with line title 'masse totale', \
 'erreur_quantites_conservatives.dat' using 1:3 with line title 'qdm totale', \
 'erreur_quantites_conservatives.dat' using 1:4 with line title 'energie totale'

