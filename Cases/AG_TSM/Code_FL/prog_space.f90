module fonction_f

implicit none

contains

function phi(S,Slim)
   double precision, intent(in) :: S, Slim
   double precision :: phi

   if (S.gt.0.d0.and.S.lt.Slim) then
      phi = (1.d0+8.d0*(S/Slim))*(1.d0-(S/Slim))**2.d0*dexp(0.001d0*(1.d0-1.d0/(1.d0-(S/Slim))**2.d0))
  else
      phi = 0.d0
   end if

   return
end function

function u_moy_sans_x(S,Slim,xmax)
   double precision, intent(in) :: S, Slim, xmax
   double precision :: u_moy_sans_x

   if (S.gt.0.d0.and.S.lt.Slim) then
      u_moy_sans_x = (S/Slim)*(2.d0-(S/Slim)) !Polynome d'ordre 2. Les grosses gouttes ont une vitesse plus grande car sont moins ralenties.
   else
      u_moy_sans_x = 0.d0
   end if
   u_moy_sans_x = u_moy_sans_x *3.d0 / xmax

   return
end function

function sigma(S,Slim)
   double precision, intent(in) :: S, Slim
   double precision :: sigma

   if (S.gt.0.d0.and.S.lt.Slim) then
      sigma = (S/Slim)*(2.d0-S/Slim) !Polynome d'ordre 2. Les grosses gouttes ont une vitesse plus grande car sont moins ralenties.
   else
      sigma = 0.d0
   end if

   return
end function

function get_nk(S,rpar,ipar)
   double precision, intent(in) :: S,rpar(*)
   integer, intent(in) :: ipar(*)
   double precision :: get_nk, Slim

   Slim = rpar(5)
   get_nk = phi(S,Slim)

   return
end function

function get_mk(S,rpar,ipar)
   double precision, intent(in) :: S,rpar(*)
   integer, intent(in) :: ipar(*)
   double precision :: get_mk

   get_mk = S**1.5d0*get_nk(S,rpar,ipar)

   return
end function

function get_mkuk(S,rpar,ipar)
   double precision, intent(in) :: S,rpar(*)
   integer, intent(in) :: ipar(*)
   double precision :: Slim, x, xmax
   double precision :: get_mkuk

   x    = rpar(4)
   Slim = rpar(5)
   xmax = rpar(6)
   get_mkuk = S**1.5d0*phi(S,Slim)*x*u_moy_sans_x(S,Slim,xmax)

   return
end function

function get_energie(S,rpar,ipar)
   double precision, intent(in) :: S,rpar(*)
   integer, intent(in) :: ipar(*)
   double precision :: Slim, x, xmax
   double precision :: get_energie

   x    = rpar(4)
   Slim = rpar(5)
   xmax = rpar(6)
   get_energie = S**1.5d0*phi(S,Slim)*((x*u_moy_sans_x(S,Slim,xmax))**2.d0+sigma(S,Slim))

   return
end function

function coef1(t,S,Slim,alpha_drag,Rs)
   double precision, intent(in) :: t,S,Slim,alpha_drag,Rs
   double precision :: coef1
   coef1 = (S+Rs*t)**(alpha_drag/Rs)*phi(S+Rs*t,Slim)
end function

function coef2(t,S,Slim,alpha_drag,Rs,xmax)
   double precision, intent(in) :: t,S,Slim,alpha_drag,Rs,xmax
   double precision :: coef2
   coef2 = (S+Rs*t)**(alpha_drag/Rs) &
     - u_moy_sans_x(S+Rs*t,Slim,xmax)/(alpha_drag+Rs)*(S**(alpha_drag/Rs+1.d0)-(S+Rs*t)**(alpha_drag/Rs+1.d0))
end function

function get_nk_t(S,rpar,ipar)
   double precision, intent(in) :: S,rpar(*)
   integer, intent(in) :: ipar(*)
   double precision :: t,x,Slim,alpha_drag,Rs,xmax
   double precision :: get_nk_t

   t    = rpar(1)
   Rs   = rpar(2)
   alpha_drag=rpar(3)
   x    = rpar(4)
   Slim = rpar(5)
   xmax = rpar(6)
   get_nk_t=coef1(t,S,Slim,alpha_drag,Rs)/coef2(t,S,Slim,alpha_drag,Rs,xmax)

   return
end function

function get_mk_t(S,rpar,ipar)
   double precision, intent(in) :: S,rpar(*)
   integer, intent(in) :: ipar(*)
   double precision :: Rs,t
   double precision :: get_mk_t

   get_mk_t=S**1.5d0*get_nk_t(S,rpar,ipar)

   return
end function

function get_mkuk_t(S,rpar,ipar)
   double precision, intent(in) :: S,rpar(*)
   integer, intent(in) :: ipar(*)
   double precision :: t,x,Slim,alpha_drag,Rs,xmax
   double precision :: get_mkuk_t

   t    = rpar(1)
   Rs   = rpar(2)
   alpha_drag=rpar(3)
   x    = rpar(4)
   Slim = rpar(5)
   xmax = rpar(6)

   get_mkuk_t = S**1.5d0*coef1(t,S,Slim,alpha_drag,Rs)/coef2(t,S,Slim,alpha_drag,Rs,xmax)**2&
                *x*u_moy_sans_x(S+Rs*t,Slim,xmax)*S**(alpha_drag/Rs)

   return
end function

function get_energie_t(S,rpar,ipar)
   double precision, intent(in) :: S,rpar(*)
   integer, intent(in) :: ipar(*)
   double precision :: t,x,Slim,alpha_drag,Rs,xmax
   double precision :: get_energie_t

   t    = rpar(1)
   Rs   = rpar(2)
   alpha_drag=rpar(3)
   x    = rpar(4)
   Slim = rpar(5)
   xmax = rpar(6)

   get_energie_t = S**1.5d0*coef1(t,S,Slim,alpha_drag,Rs)/coef2(t,S,Slim,alpha_drag,Rs,xmax)**3 &
                *( sigma(S+Rs*t,Slim) + (x*u_moy_sans_x(S+Rs*t,Slim,xmax))**2 )*S**(2.d0*alpha_drag/Rs)

   return
end function

function nbre_sections_non_vides(Slim,Nsec,rpar,ipar)
   double precision, intent(in) :: rpar(*),Slim
   integer, intent(in) :: ipar(*),Nsec
   integer :: nbre_sections_non_vides

   nbre_sections_non_vides = Nsec-floor(rpar(1)*rpar(2)*Nsec/Slim)
   return
end function 

SUBROUTINE Gauss_50(func,a,b,integ,rpar,ipar)
! calcul de l'integrale de func, x dans [a,b]
! quadrature de Gauss a 24 points 
  double precision, intent(in) :: a,b,rpar(*)
  integer, parameter :: ordre=50
  integer, intent(inout) :: ipar(*)
  double precision, external :: func
  double precision, intent(out) :: integ
  double precision :: xi
  double precision :: weig(ordre) 
  double precision :: absc(ordre) 
  integer          :: i
!
  weig(1:ordre)    = (/ 1.4543112765773515d-3,   3.3798995978727396d-3,   5.2952741918255233d-3,&
     7.1904113807281398d-3,   9.0577803567421060d-3,   1.0890121585061753d-2,   1.2680336785006062d-2,   1.4421496790267552d-2,&
     1.6106864111788976d-2,   1.7729917807573041d-2,   1.9284378306293828d-2,   2.0764231545073817d-2,   2.2163752169401633d-2,&
     2.3477525651974220d-2,   2.4700469224733193d-2,   2.5827851534790579d-2,   2.6855310944498136d-2,   2.7778872403106270d-2,&
     2.8594962823864197d-2,   2.9300424906611226d-2,   2.9892529352132730d-2,   3.0368985420885106d-2,   3.0727949795158378d-2,&
     3.0968033710341607d-2,   3.1088308327673626d-2,   3.1088308327673626d-2,   3.0968033710341607d-2,   3.0727949795158378d-2,&
     3.0368985420885106d-2,   2.9892529352132730d-2,   2.9300424906611226d-2,   2.8594962823864197d-2,   2.7778872403106270d-2,&
     2.6855310944498136d-2,   2.5827851534790579d-2,   2.4700469224733193d-2,   2.3477525651974220d-2,   2.2163752169401633d-2,&
     2.0764231545073817d-2,   1.9284378306293828d-2,   1.7729917807573041d-2,   1.6106864111788976d-2,   1.4421496790267552d-2,&
     1.2680336785006062d-2,   1.0890121585061753d-2,   9.0577803567421060d-3,   7.1904113807281398d-3,   5.2952741918255233d-3,&
     3.3798995978727396d-3,   1.4543112765773515d-3 /)
  absc(1:ordre)   = (/ 5.6679778996449048d-4,   2.9840152839546441d-3,   7.3229579759970798d-3,&
     1.3567807446653979d-2,   2.1694522378596037d-2,   3.1671690527561025d-2,   4.3460721672104075d-2,   5.7016010238193471d-2,&
     7.2285115285026957d-2,   8.9208964570332006d-2,   0.10772208354980040d0,   0.12775284888696575d0,   0.14922376564658890d0,&
     0.17205176715728032d0,   0.19614853640752489d0,   0.22142084774267495d0,   0.24777092754626789d0,   0.27509683251298062d0,&
     0.30329284405121743d0,   0.33224987729028133d0,   0.36185590311023397d0,   0.39199638156197913d0,   0.42255470500092707d0,&
     0.45341264921995694d0,   0.48445083083640555d0,   0.51554916916359439d0,   0.54658735078004306d0,   0.57744529499907293d0,&
     0.60800361843802087d0,   0.63814409688976603d0,   0.66775012270971867d0,   0.69670715594878252d0,   0.72490316748701944d0,&
     0.75222907245373216d0,   0.77857915225732510d0,   0.80385146359247517d0,   0.82794823284271968d0,   0.85077623435341110d0,&
     0.87224715111303430d0,   0.89227791645019960d0,   0.91079103542966799d0,   0.92771488471497299d0,   0.94298398976180653d0,&
     0.95653927832789587d0,   0.96832830947243898d0,   0.97830547762140396d0,   0.98643219255334602d0,   0.99267704202400298d0,&
     0.99701598471604536d0,   0.99943320221003551d0  /)
  integ = 0.d0
  do i = 1, ordre
      xi = a + (b-a)*absc(i)
      integ = integ + weig(i)*func(xi,rpar,ipar)
  end do
  integ = integ*(b-a)
!
END SUBROUTINE

SUBROUTINE IntRomberg(fn,a,b,eps,aire,rpar,ipar)
! Integration par la methode de Romberg
! Dominique Lefebvre fevrier 2007
! Inspire du code de A.Garcia dans Numerical Methods for Physics
!
! Modifiee le 30/08/09 suite à une erreur d'estimation de la precision
! signalee par Maelle Nodet (Universite de Grenoble)
! 
! a = borne inferieure d'integration
! b = borne superieure d'integration
! eps = précision souhaitee
! aire = surface retournee
  double precision, intent(in) :: a,b,eps,rpar(*)
  integer, intent(inout) :: ipar(*)
  double precision, external :: fn
  double precision, intent(out) :: aire
  INTEGER  :: Nmax
!  PARAMETER (Nmax=30)
  PARAMETER (Nmax=25)
!  PARAMETER (Nmax=15)
  double precision :: h,xi, delta, sum, c, fotom, t(1000)
  integer          :: out, pieces, i, nx(Nmax+1), nt, ii, n, nn, l, ntra, k, m, j, l2
  LOGICAL :: fini

! Calcul du premier terme
    h = b-a
    pieces = 1
    nx(1) = 1
    delta = h / pieces
    c = (fn(a,rpar,ipar) + fn(b,rpar,ipar)) / 2.d0
    sum = c
    t(1) = delta * c
    n = 1
    nn = 2
 
    fini =.false.
    DO WHILE (.not. fini .and. n<Nmax)
        n = n + 1
        fotom = 4.d0
        nx(n) = nn
        pieces = pieces * 2
        l = pieces - 1
        l2 = (l+1) / 2
        delta = h / pieces

! Evaluation par la methode des trapezes 
        DO ii = 1, l2
            i = ii * 2 - 1
            xi = a + delta * i
            sum = sum + fn(xi,rpar,ipar)
        ENDDO
        t(nn) = sum * delta
        ntra = nx(n-1)
        k = n-1

! Calcul de la nieme colonne du tableau 
        DO m = 1, k
            j = nn + m
            nt = nx(n-1) + m - 1
            t(j) = (fotom * t(j-1) - t(nt)) / (fotom - 1)
            fotom = fotom * 4
        ENDDO

! Estimation de la precision
        IF (n .lt. 5) THEN
            nn = j+1
        ELSE IF (t(nn+1) .eq. 0.0) THEN
            nn = j+1
        ELSE
            IF (abs(t(ntra+1)-t(nn+1)) .le. abs(t(nn+1)*eps)) THEN 
                fini = .true.
            ELSEIF (abs(t(nn-1) - t(j)) .le. abs(t(j)*eps)) THEN
                fini = .true.
            ELSE
                nn = j+1
            ENDIF
        ENDIF 
    ENDDO

! On retourne la valeur finale de l'aire
    aire = t(j)
    if(nn.ge.1000) print*,'nn>1000 dans IntRomberg',nn
    if(n.ge.Nmax.and.(fn(a,rpar,ipar)>1.d-20.and.fn(b,rpar,ipar)>1.d-20)) &
       print*,'n=Nmax dans IntRomberg',a,b,aire,fn(a,rpar,ipar),fn(b,rpar,ipar)

    RETURN
END SUBROUTINE IntRomberg

end module

!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!
!PROGRAMME TEST
!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!

program test
use fonction_f

implicit none

double precision :: rpar(6)
integer :: ipar(1)

double precision :: eps
double precision :: aire0,aire1,aire2,aire3

integer, parameter :: Nsec = 10, Nt = 50, Npts = 100
integer :: k,i,j,Nmax
double precision :: pi = 3.141592653589793d0
double precision :: t, dt, tmax
double precision :: rmax,smax,Rs,rhol,alpha_drag,rapportmn
double precision :: dS,S(0:Nsec)
double precision :: dx, Xmax, x(0:Npts), xm(Npts)

double precision :: n_an_tot(0:Nt,Npts), m_an_tot(0:Nt,Npts), qdm_an_tot(0:Nt,Npts), e_an_tot(0:Nt,Npts)
double precision :: u_an_moy(0:Nt,Npts), sig_an_moy(0:Nt,Npts)

double precision :: uk_an(0:Nt,Npts,nsec), sigk_an(0:Nt,Npts,nsec)
double precision :: nk_an(0:Nt,Npts,nsec),mk_an(0:Nt,Npts,nsec),qdmk_an(0:Nt,Npts,nsec),ek_an(0:Nt,Npts,nsec)


rhol = 634.239d0              ! densite du liquide (kg/m**3)
alpha_drag = .9680385e-6      ! alpha (m^2/s)
Rs = 1.99e-7                  ! coefficient d'evaporation Rs (m**2/s)
xmax = 0.2d0

!smax = 1.310d3
smax = 11.310d3
rapportmn = rhol/(6.d0*sqrt(pi))
tmax = smax/(Rs*1.d12)
dt = tmax/dble(Nt)
dS = smax/dble(Nsec)
do k = 0, Nsec
   S(k) = dble(k)*dS
end do
dx = 2.d0*xmax/dble(Npts)
do j = 0, Npts
   x(j) = -xmax + dble(j)*dx ! bords des cellules
end do
do j = 1, Npts
   xm(j) = 0.5d0*(x(j)+x(j-1)) ! milieux des cellules
end do

rpar(1) = 0.d0                  ! temps t
rpar(2) = Rs*1.d12              ! Rs - changement d'unite : m**2/s -> micron**2/s
rpar(3) = alpha_drag*1.d12      ! alpha_drag - changement d'unite : m**2/s -> micron**2/s
rpar(5) = smax                  
rpar(6) = xmax                  
ipar(1) = 0                     ! Ne sert a rien dans mon code, mais je veux garder la possibilite
                              ! de passer des arguments integer

eps = 1.d-6                     ! parametre de convergence pour IntRomberg

nk_an   = 0.d0
mk_an   = 0.d0
qdmk_an = 0.d0
uk_an   = 0.d0
ek_an   = 0.d0
sigk_an = 0.d0
u_an_moy   = 0.d0
sig_an_moy = 0.d0

!Initialisation de nk,mk,uk,sigmak
do j = 1, Npts
   rpar(4) = xm(j)
   do k = 1,Nsec
!      call IntRomberg(get_nk_t,S(k-1),S(k),eps,aire0,rpar,ipar)
!      call IntRomberg(get_mk_t,S(k-1),S(k),eps,aire1,rpar,ipar)
!      call IntRomberg(get_mkuk_t,S(k-1),S(k),eps,aire2,rpar,ipar)
!      call IntRomberg(get_energie_t,S(k-1),S(k),eps,aire3,rpar,ipar)
      call Gauss_50(get_nk_t,S(k-1),S(k),aire0,rpar,ipar)
      call Gauss_50(get_mk_t,S(k-1),S(k),aire1,rpar,ipar)
      call Gauss_50(get_mkuk_t,S(k-1),S(k),aire2,rpar,ipar)
      call Gauss_50(get_energie_t,S(k-1),S(k),aire3,rpar,ipar)
      nk_an(0,j,k)   = 1.d-18*aire0 ! Changement d'unite !!!
      mk_an(0,j,k)   = (1.d-18*rapportmn)*aire1   ! Changement d'unite de rapportmn
      qdmk_an(0,j,k) = (1.d-18*rapportmn)*aire2
      uk_an(0,j,k)   = aire2/aire1
      ek_an(0,j,k)   = (1.d-18*rapportmn)*aire3
      sigk_an(0,j,k) = aire3/aire1-(aire2/aire1)**2.d0
   enddo
   m_an_tot(0,j)   = sum(mk_an(0,j,:))
   qdm_an_tot(0,j) = sum(qdmk_an(0,j,:))
   e_an_tot(0,j)   = sum(ek_an(0,j,:))
   if(m_an_tot(0,j)>0.d0) then
      u_an_moy(0,j)   = qdm_an_tot(0,j)/m_an_tot(0,j)
      sig_an_moy(0,j) = e_an_tot(0,j)/m_an_tot(0,j)-u_an_moy(0,j)**2
   end if
enddo

!Ecriture de la solution analytique
do i=1,Nt-1                    ! Boucle temporelle
 t = dble(i)*dt
 rpar(1) = t

 write(*,*) 'pas de temps analytique', i, '/', Nt

 Nmax = nbre_sections_non_vides(smax,Nsec,rpar,ipar)

 do j = 1, Npts
   rpar(4) = xm(j)
   do k = 1,Nmax-1
!      call IntRomberg(get_nk_t,S(k-1),S(k),eps,aire0,rpar,ipar)
!      call IntRomberg(get_mk_t,S(k-1),S(k),eps,aire1,rpar,ipar)
!      call IntRomberg(get_mkuk_t,S(k-1),S(k),eps,aire2,rpar,ipar)
!      call IntRomberg(get_energie_t,S(k-1),S(k),eps,aire3,rpar,ipar)
      call Gauss_50(get_nk_t,S(k-1),S(k),aire0,rpar,ipar)
      call Gauss_50(get_mk_t,S(k-1),S(k),aire1,rpar,ipar)
      call Gauss_50(get_mkuk_t,S(k-1),S(k),aire2,rpar,ipar)
      call Gauss_50(get_energie_t,S(k-1),S(k),aire3,rpar,ipar)
      nk_an(i,j,k)   = 1.d-18*aire0 ! Changement d'unite !!!
      mk_an(i,j,k)   = (1.d-18*rapportmn)*aire1   ! Changement d'unite de rapportmn
      qdmk_an(i,j,k) = (1.d-18*rapportmn)*aire2
      uk_an(i,j,k)   = aire2/aire1
      ek_an(i,j,k)   = (1.d-18*rapportmn)*aire3
      sigk_an(i,j,k) = aire3/aire1-(aire2/aire1)**2
   enddo

   !Derniere section non vide : Nmax-1
   k = Nmax

   if (Nmax.ge.1) then
      if (abs(S(Nsec)-rpar(1)*rpar(2)-S(k-1)).lt.S(1)/1.d10) then
         aire0 = 0.d0
         aire1 = 0.d0
         aire2 = 0.d0
         aire3 = 0.d0
      else
!         call IntRomberg(get_nk_t,S(k-1),S(Nsec)-rpar(1)*rpar(2),eps,aire0,rpar,ipar)
!         call IntRomberg(get_mk_t,S(k-1),S(Nsec)-rpar(1)*rpar(2),eps,aire1,rpar,ipar)
!         call IntRomberg(get_mkuk_t,S(k-1),S(Nsec)-rpar(1)*rpar(2),eps,aire2,rpar,ipar)
!         call IntRomberg(get_energie_t,S(k-1),S(Nsec)-rpar(1)*rpar(2),eps,aire3,rpar,ipar)
         call Gauss_50(get_nk_t,S(k-1),S(Nsec)-rpar(1)*rpar(2),aire0,rpar,ipar)
         call Gauss_50(get_mk_t,S(k-1),S(Nsec)-rpar(1)*rpar(2),aire1,rpar,ipar)
         call Gauss_50(get_mkuk_t,S(k-1),S(Nsec)-rpar(1)*rpar(2),aire2,rpar,ipar)
         call Gauss_50(get_energie_t,S(k-1),S(Nsec)-rpar(1)*rpar(2),aire3,rpar,ipar)
      endif

      nk_an(i,j,k)   = 1.d-18*aire0 ! Changement d'unite !!!
      mk_an(i,j,k)   = (1.d-18*rapportmn)*aire1   ! Changement d'unite de rapportmn
      qdmk_an(i,j,k) = (1.d-18*rapportmn)*aire2
      uk_an(i,j,k)   = aire2/aire1
      ek_an(i,j,k)   = (1.d-18*rapportmn)*aire3
      sigk_an(i,j,k) = aire3/aire1-(aire2/aire1)**2

   else
      write(*,*) 'probleme grave',Nmax
   endif   

   !masse, qdm et energie totales au pas de temps i
   m_an_tot(i,j)   = sum(mk_an(i,j,:))
   qdm_an_tot(i,j) = sum(qdmk_an(i,j,:))
   e_an_tot(i,j)   = sum(ek_an(i,j,:))
   if(m_an_tot(i,j)>0.d0) then
      u_an_moy(i,j)   = qdm_an_tot(i,j)/m_an_tot(i,j)
      sig_an_moy(i,j) = e_an_tot(i,j)/m_an_tot(i,j)-u_an_moy(i,j)**2
   end if

 end do
enddo

open(70,file='x_mtot_analytique.dat',form='formatted')
open(71,file='x_qdmtot_analytique.dat',form='formatted')
open(72,file='x_etot_analytique.dat',form='formatted')
open(73,file='x_umoy_analytique.dat',form='formatted')
open(74,file='x_sigmoy_analytique.dat',form='formatted')
do j = 1, Npts
   write(70,100) xm(j), m_an_tot(:,j)
   write(71,100) xm(j), qdm_an_tot(:,j)
   write(72,100) xm(j), e_an_tot(:,j)
   write(73,100) xm(j), u_an_moy(:,j)
   write(74,100) xm(j), sig_an_moy(:,j)
end do
close(70)
close(71)
close(72)
close(73)
close(74)

open(70,file='t_mtot_analytique.dat',form='formatted')
open(71,file='t_qdmtot_analytique.dat',form='formatted')
open(72,file='t_etot_analytique.dat',form='formatted')
open(73,file='t_umoy_analytique.dat',form='formatted')
open(74,file='t_sigmoy_analytique.dat',form='formatted')
do i = 0, Nt
   write(70,100) dble(i)*dt, m_an_tot(i,:)
   write(71,100) dble(i)*dt, qdm_an_tot(i,:)
   write(72,100) dble(i)*dt, e_an_tot(i,:)
   write(73,100) dble(i)*dt, u_an_moy(i,:)
   write(74,100) dble(i)*dt, sig_an_moy(i,:)
end do
close(70)
close(71)
close(72)
close(73)
close(74)


100 format (300(1x,1pe22.15)) 

end
