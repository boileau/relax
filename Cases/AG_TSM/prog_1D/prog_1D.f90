program test
use TSM_aff_evap_drag

implicit none

double precision, parameter :: mac_prec = 1d-16
double precision :: rpar(6)

include 'param.inc'
character(len=1) :: argument
integer :: nargs
integer :: k,i,j,Nmax
double precision :: t, dt, tmax
double precision :: rmax,rhol,x0,sigmax
double precision :: dS,S(0:Nsec),Sp(0:Nsec)
double precision :: dx, xmax, x(0:Npts), xm(Npts)

double precision :: Sinta(Nsec),Sintb(Nsec)
double precision :: Sm52, Sm72

double precision :: e_kin_tot, e_kin_k(Nsec)
double precision :: n_an_tot(0:Nt,Npts), m_an_tot(0:Nt,Npts), qdm_an_tot(0:Nt,Npts), e_an_tot(0:Nt,Npts)
double precision :: u_an_moy(0:Nt,Npts), sig_an_moy(0:Nt,Npts), S_an_moy(0:Nt,Npts)

double precision :: uk_an(0:Nt,Npts,Nvit,nsec), sigk_an(0:Nt,Npts,Nene,nsec)
double precision :: nk_an(0:Nt,Npts,nsec),mk_an(0:Nt,Npts,nsec),qdmk_an(0:Nt,Npts,Nvit,nsec),ek_an(0:Nt,Npts,Nene,nsec)

character :: nomt*4,nomsec*4,nompts*4
character nb_tag1*1,nb_tag2*2,nb_tag3*3

character(len=1024) :: result_dir

! Read command line argument
!nargs = COMMAND_ARGUMENT_COUNT() 
!if (nargs > 0) then
!    call get_command_argument(1, argument)
!    read(argument,'(i1)') case_type
!endif

write(*,*) "Case type:", case_type

result_dir = "Result_case_"//trim(str(case_type))//"_n"//trim(str(Npts))//"_nsect"//trim(str(Nsec))
CALL system("rm -rf "//trim(result_dir))
CALL system("mkdir "//trim(result_dir))

if(Npts.le.9)   then
   write (nb_tag1,'(I1)') Npts
   nompts = '000'//nb_tag1
else if (Npts.le.99) then
   write (nb_tag2,'(I2)') Npts
   nompts = '00'//nb_tag2
else if (Npts.le.999) then
   write (nb_tag3,'(I3)') Npts
   nompts = '0'//nb_tag3
else if (Npts.le.9999) then
   write (nompts,'(I4)') Npts
else
   write(6,*) 'attention, le nombre de points de doit pas depasser 9999',Npts
   stop
end if

if(Nt.le.9)   then
   write (nb_tag1,'(I1)') Nt
   nomt = '000'//nb_tag1
else if (Nt.le.99) then
   write (nb_tag2,'(I2)') Nt
   nomt = '00'//nb_tag2
else if (Nt.le.999) then
   write (nb_tag3,'(I3)') Nt
   nomt = '0'//nb_tag3
else if (Nt.le.9999) then
   write (nomt,'(I4)') Nt
else
   write(6,*) 'attention, le nombre de pas de temps de doit pas depasser 9999',Nt
   stop
end if

if(Nsec.le.9)   then
   write (nb_tag1,'(I1)') Nsec
   nomsec = '000'//nb_tag1
else if (Nsec.le.99) then
   write (nb_tag2,'(I2)') Nsec
   nomsec = '00'//nb_tag2
else if (Nsec.le.999) then
   write (nb_tag3,'(I3)') Nsec
   nomsec = '0'//nb_tag3
else if (Nsec.le.9999) then
   write (nomsec,'(I4)') Nsec
else
   write(6,*) 'attention, le nombre de sections de doit pas depasser 9999',Nsec
   stop
end if

rhol = 634.239d0              ! densite du liquide (kg/m**3)
alpha_drag = .9680385e-6      ! alpha (m^2/s)
Rs = 1.99e-7                  ! coefficient d'evaporation Rs (m**2/s)

smax = 11.310d3
ratio_mn = rhol/(6.d0*sqrt(pi))
dS = smax/dble(Nsec)
do k = 0, Nsec
   S(k) = dble(k)*dS
end do

if (case_type < 3) then
    x0 = 2.d-2
    sigmax = x0**2/40.d0
    !tmax = smax/(Rs*1.d12)/2.d0
    !dt = tmax/dble(Nt)
    dt = 2.d-5
    tmax = Nt*dt
    write(*,*) 'dt =', dt
    xmax = 0.07d0 !2.d0*x0 + u0*tmax*0.6d0
    dx = xmax/dble(Npts)
    do j = 0, Npts
       x(j) = dble(j)*dx ! bords des cellules
    end do
else
    tmax = 0.06
!    dt = tmax/dble(Nt)
    dt = 3.d-5
    xmax = 0.2d0
    dx = 2.*xmax/dble(Npts)
    do j = 0, Npts
       x(j) = -xmax + dble(j)*dx ! bords des cellules
    end do
endif
do j = 1, Npts
   xm(j) = 0.5d0*(x(j)+x(j-1)) ! milieux des cellules
end do

print*,'xmax',xmax
print*,'tmax',tmax
print*,'CFL evap',Rs*1.d12*dt/dS
print*,'alpha_drag/Rs',alpha_drag/Rs 

rpar(1) = 0.d0                  ! temps t
select case (case_type)
case (1)
    rpar(2) = Rs*1.d12              ! Rs - changement d'unite : m**2/s -> micron**2/s
    rpar(3) = alpha_drag/Rs
    rpar(5) = x0
    rpar(6) = sigmax              
case (2)
    rpar(2) = 0.d0
    rpar(3) = alpha_drag/Rs
    rpar(5) = x0
    rpar(6) = sigmax              
case (3)
    rpar(2) = Rs*1.d12              ! Rs - changement d'unite : m**2/s -> micron**2/s
    rpar(3) = alpha_drag*1.d12      ! alpha_drag - changement d'unite : m**2/s -> micron**2/s
    rpar(5) = xmax
case (4)
    rpar(2) = 0.d0
    rpar(3) = 0.d0
    rpar(5) = xmax
case default
    write(*,*) "Wrong case type:", case_type
    stop
end select

nk_an   = 0.d0
mk_an   = 0.d0
qdmk_an = 0.d0
uk_an   = 0.d0
ek_an   = 0.d0
sigk_an = 0.d0
u_an_moy   = 0.d0
sig_an_moy = 0.d0
S_an_moy = 0.d0

print*, "rpar=", rpar
!Initialisation de nk,mk,uk,sigmak
rpar(1) = 0.d0                  ! temps t
do j = 1, Npts
   rpar(4) = xm(j)
   select case (case_type)
   case (1) 
       call moments_1d(Nsec,S,rpar,nk_an(0,j,:),mk_an(0,j,:),qdmk_an(0,j,:,:),&
                       uk_an(0,j,:,:),ek_an(0,j,:,:),sigk_an(0,j,:,:))
   case (2) 
       call moments_t1d(Nsec,S,rpar,nk_an(0,j,:),mk_an(0,j,:),qdmk_an(0,j,:,:),&
                        uk_an(0,j,:,:),ek_an(0,j,:,:),sigk_an(0,j,:,:))
   case (3) 
       call init_1d(Nsec, S, rpar, nk_an(0,j,:), mk_an(0,j,:), qdmk_an(0,j,1,:),&
                    uk_an(0,j,1,:), ek_an(0,j,1,:), sigk_an(0,j,1,:))
   case (4) 
       call init_t1d(Nsec, S, rpar, nk_an(0,j,:), mk_an(0,j,:), qdmk_an(0,j,1,:),&
                     uk_an(0,j,1,:), ek_an(0,j,1,:), sigk_an(0,j,1,:))
   end select
   n_an_tot(0,j)   = sum(nk_an(0,j,:))
   m_an_tot(0,j)   = sum(mk_an(0,j,:))
   qdm_an_tot(0,j) = sum(qdmk_an(0,j,1,:))
   e_kin_tot       = sum(qdmk_an(0,j,1,:)*qdmk_an(0,j,1,:)/mk_an(0,j,:))
   e_an_tot(0,j)   = sum(ek_an(0,j,1,:))
   if(m_an_tot(0,j)>0.d0) then
      S_an_moy(0,j)   = (m_an_tot(0,j)/n_an_tot(0,j)/rhol)**(2.d0/3.d0)
      u_an_moy(0,j)   = qdm_an_tot(0,j)/m_an_tot(0,j)
      !sig_an_moy(0,j) = e_an_tot(0,j)/m_an_tot(0,j)-u_an_moy(0,j)**2
      sig_an_moy(0,j) = (e_an_tot(0,j) - e_kin_tot)/m_an_tot(0,j)
   end if
end do

!Ecriture de la solution analytique
do i = 1, Nt-1                    ! Boucle temporelle
   t = dble(i)*dt
   rpar(1) = t
   write(*,*) 'pas de temps analytique', i, '/', Nt, ' t =', t

   if (case_type < 3) then
       Nmax = nbre_sections_non_vides(smax, Nsec, rpar)
    
       Sp(0:Nmax-1) = S(0:Nmax-1)
       if (Nmax >= 1) then
          if (abs(S(Nsec) - rpar(1)*rpar(2) - S(Nmax-1)) < S(1)/1.d10) then
             Nmax = Nmax - 1
          else
             Sp(Nmax) = S(Nsec) - rpar(1)*rpar(2)
          end if
       end if
   endif

   do j = 1, Npts
      rpar(4) = xm(j)
      select case (case_type)
      case (1) 
          call moments_1d(Nmax,Sp(0:Nmax),rpar,nk_an(i,j,1:Nmax),mk_an(i,j,1:Nmax),&
                          qdmk_an(i,j,:,1:Nmax),uk_an(i,j,:,1:Nmax),ek_an(i,j,:,1:Nmax),sigk_an(i,j,:,1:Nmax))
      case (2) 
          call moments_t1d(Nmax,Sp(0:Nmax),rpar,nk_an(i,j,1:Nmax),mk_an(i,j,1:Nmax),&
                           qdmk_an(i,j,:,1:Nmax),uk_an(i,j,:,1:Nmax),ek_an(i,j,:,1:Nmax),sigk_an(i,j,:,1:Nmax))
      case (3) 
          call init_1d(Nsec, S, rpar, nk_an(i,j,:), mk_an(i,j,:), qdmk_an(i,j,1,:),&
                       uk_an(i,j,1,:), ek_an(i,j,1,:), sigk_an(i,j,1,:))
      case (4) 
          call init_t1d(Nsec, S, rpar, nk_an(i,j,:), mk_an(i,j,:), qdmk_an(i,j,1,:),&
                        uk_an(i,j,1,:), ek_an(i,j,1,:), sigk_an(i,j,1,:))
      end select

      !masse, qdm et energie totales au pas de temps i
      n_an_tot(i,j)   = sum(nk_an(i,j,:))
      m_an_tot(i,j)   = sum(mk_an(i,j,:))
      qdm_an_tot(i,j) = sum(qdmk_an(i,j,1,:))
      e_kin_k(:)      = qdmk_an(i,j,1,:)*qdmk_an(i,j,1,:)/mk_an(i,j,:)
      do k = 1, Nsec
            if (isnan(e_kin_k(k))) then
                 e_kin_k(k) = 0.d0
            endif
      enddo
      e_kin_tot       = sum(e_kin_k(:))
      e_an_tot(i,j)   = sum(ek_an(i,j,1,:))
      if(m_an_tot(i,j)>0.d0) then
         S_an_moy(i,j)   = (m_an_tot(i,j)/n_an_tot(i,j)/rhol)**(2.d0/3.d0)
         u_an_moy(i,j)   = qdm_an_tot(i,j)/m_an_tot(i,j)
         !sig_an_moy(i,j) = (e_an_tot(i,j)*m_an_tot(i,j)-qdm_an_tot(i,j)**2)/m_an_tot(i,j)**2
         sig_an_moy(i,j) = (e_an_tot(i,j) - e_kin_tot)/m_an_tot(i,j)
      end if
   end do
enddo

where(m_an_tot < mac_prec) m_an_tot = 0.d0
where(m_an_tot < mac_prec) qdm_an_tot = 0.d0
where(m_an_tot < mac_prec) e_an_tot = 0.d0
where(m_an_tot < mac_prec) S_an_moy = 0.d0
where(m_an_tot < mac_prec) u_an_moy = 0.d0
where(m_an_tot < mac_prec) sig_an_moy = 0.d0
!where(abs(qdm_an_tot)<mac_prec) qdm_an_tot = 0.d0
!where(e_an_tot<mac_prec) e_an_tot = 0.d0
!where(S_an_moy<mac_prec) S_an_moy = 0.d0
!where(abs(u_an_moy)<mac_prec) u_an_moy = 0.d0
!where(sig_an_moy<mac_prec) sig_an_moy = 0.d0

open(75,file=trim(result_dir)//'/x_ntot_analytique.dat',form='formatted')
open(70,file=trim(result_dir)//'/x_mtot_analytique.dat',form='formatted')
open(76,file=trim(result_dir)//'/x_Smoy_analytique.dat',form='formatted')
open(71,file=trim(result_dir)//'/x_qdmtot_analytique.dat',form='formatted')
open(72,file=trim(result_dir)//'/x_etot_analytique.dat',form='formatted')
open(73,file=trim(result_dir)//'/x_umoy_analytique.dat',form='formatted')
open(74,file=trim(result_dir)//'/x_sigmoy_analytique.dat',form='formatted')
do j = 1, Npts
   write(75,100) xm(j), n_an_tot(:,j)
   write(70,100) xm(j), m_an_tot(:,j)
   write(76,100) xm(j), S_an_moy(:,j)
   write(71,100) xm(j), qdm_an_tot(:,j)
   write(72,100) xm(j), e_an_tot(:,j)
   write(73,100) xm(j), u_an_moy(:,j)
   write(74,100) xm(j), sig_an_moy(:,j)
end do
close(70)
close(71)
close(72)
close(73)
close(74)
close(75)
close(76)

open(75,file=trim(result_dir)//'/t_ntot_analytique.dat',form='formatted')
open(70,file=trim(result_dir)//'/t_mtot_analytique.dat',form='formatted')
open(76,file=trim(result_dir)//'/t_Smoy_analytique.dat',form='formatted')
open(71,file=trim(result_dir)//'/t_qdmtot_analytique.dat',form='formatted')
open(72,file=trim(result_dir)//'/t_etot_analytique.dat',form='formatted')
open(73,file=trim(result_dir)//'/t_umoy_analytique.dat',form='formatted')
open(74,file=trim(result_dir)//'/t_sigmoy_analytique.dat',form='formatted')
do i = 0, Nt
   write(75,100) dble(i)*dt, n_an_tot(i,:)
   write(70,100) dble(i)*dt, m_an_tot(i,:)
   write(76,100) dble(i)*dt, S_an_moy(i,:)
   write(71,100) dble(i)*dt, qdm_an_tot(i,:)
   write(72,100) dble(i)*dt, e_an_tot(i,:)
   write(73,100) dble(i)*dt, u_an_moy(i,:)
   write(74,100) dble(i)*dt, sig_an_moy(i,:)
end do
close(70)
close(71)
close(72)
close(73)
close(74)
close(76)

where(mk_an < mac_prec) mk_an = 0.d0
where(mk_an < mac_prec) nk_an = 0.d0
where(mk_an < mac_prec) qdmk_an(:,:,1,:) = 0.d0
where(mk_an < mac_prec) ek_an(:,:,1,:) = 0.d0
do k = 1, Nsec
   open(70,file=trim(result_dir)//'/t_mk_'//trim(str(k))//'_analytique.dat',form='formatted')
   open(71,file=trim(result_dir)//'/t_qdmk_'//trim(str(k))//'_analytique.dat',form='formatted')
   open(72,file=trim(result_dir)//'/t_ek_'//trim(str(k))//'_analytique.dat',form='formatted')
   open(73,file=trim(result_dir)//'/t_nk_'//trim(str(k))//'_analytique.dat',form='formatted')
   do i = 0, Nt
      write(70,100) dble(i)*dt, mk_an(i,:,k)
      write(71,100) dble(i)*dt, qdmk_an(i,:,1,k)
      write(72,100) dble(i)*dt, ek_an(i,:,1,k)
      write(73,100) dble(i)*dt, nk_an(i,:,k)
   end do
   close(70)
   close(71)
   close(72)
   close(73)
end do

write(*,*) "Result stored in "//trim(result_dir)

100 format (6000(1x,1pe23.15E3)) 

contains
function nbre_sections_non_vides(Slim,Nsec,rpar)
   double precision, intent(in) :: rpar(*),Slim
   integer, intent(in) :: Nsec
   integer :: nbre_sections_non_vides

   nbre_sections_non_vides = Nsec-floor(rpar(1)*rpar(2)*Nsec/Slim)
   return
end function 

function str(k)
!   "Convert an integer to string."
   integer, intent(in) :: k
   character(len=20) :: str

   write (str, *) k
   str = adjustl(str)
end function str

end
