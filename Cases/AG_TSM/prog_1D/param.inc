integer, parameter :: Nsec = 10
integer, parameter :: Nt = 1000
integer, parameter :: Npts = 100
integer, parameter :: Nvit = 1
integer, parameter :: Nene = 1
integer, parameter :: case_type = 2  ! 1: evap and drag
                                     ! 2: pure transport
                                     ! 3: evap and drag homogeneous
                                     ! 4: pure transport homogeneous
