import math

casetype = "ICMF2016-2D"    # Case type
scheme = 'relax-agh-o1'     # Scheme name

Smax = 11310.           # Max size [micro-m^2]

CFL = 1.0                  # CFL number
tf = 1.1                   # simulation final time
maxite = 0                 # simulation final time
Lx = 1.0                   # Domain length
N = 100                    # Number of points
nsect = 5                  # number of sections
gamma = 3                  # Polytropic coefficient
dt_store = 0.05            # Storage time interval
dt_plot = 0.0              # Plotting time interval
is2D = True                # 2D test case
isSecST = True             # ST?
isAllST = False            # TSM
stockes = 13/(8.*math.pi)
