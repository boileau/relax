import math

casetype = "ICMF2016-2D"    # Case type
scheme = 'relax-ag-o1'     # Scheme name

Smax = 11310.           # Max size [micro-m^2]
Rs = 1.99e-9            # Evaporation coefficient [m^2.s^-1]
# Set alpha_drag such as tau_p_max = 20*tau_p_crit
alpha_drag = Smax*math.pi*1e-12/4.  # alpha_drag coefficient [m2/s]

CFL = 1.0                  # CFL number
CFL_evap = 0.2             # CFL evap number
tf = 1.0                   # simulation final time
maxite = 0                 # simulation final time
Lx = 1.0                   # Domain length
N = 100                   # Number of points
nsect = 10                 # number of sections
gamma = 3.0                # Polytropic coefficient
dt_store = 0.01            # Storage time interval
dt_plot = 0.               # Plotting time interval
is2D = True                # 2D test case
isSecST = False            # ST?
isAllST = True             # TSM

err_tol = 0.

