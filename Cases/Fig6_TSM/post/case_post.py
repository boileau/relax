# -*- coding: utf-8 -*-
"""
Compute relative error
Reference code is in: Relax/Fortran_main/
"""

import matplotlib.pyplot as plt
import numpy as np
from Relax import data_process as dproc
from Relax import plot_sol


FIGNAME = 'Fig2D_ICMF2016'
VARNAME = 'rho'
SECTION = 'sum'
SUM_TOTNAMES = 'mtot', 'ntot', 'qdmtot', 'Etot'
#EXTRA = {'quiverskip': 6, 'nisolevel': 8}  # For first figure
EXTRA = {}
if 'quiverskip' in EXTRA.keys():
    OUTDIR = 'png_quiver_{}_{}'.format(VARNAME, SECTION)
else:
    OUTDIR = 'png_{}_{}'.format(VARNAME, SECTION)


def load_sim_integ(solname='solution.h5'):
    """Return a dict of integrals over space (time data)"""
    sim = {}
    sim['solname'] = solname
    with dproc.open_h5(h5path=solname) as f:
        sim['nsect'] = dproc.get_param(f, 'nsect')
        sim['N'] = dproc.get_param(f, 'N')  # number of grid points
        sim['x'] = []
        for var in SUM_TOTNAMES:
            sim[var] = np.empty(0)
        for ite in dproc.get_ite_list(f):
            t, V_cons = dproc.load_snapshot(f, ite=ite, k='sum')
            sim['x'].append(t)
            sim['mtot'] = np.append(sim['mtot'], np.sum(V_cons[0]))
            #sim['qdmtot'] = np.append(sim['qdmtot'], np.sum(V_cons[1]))
            #sim['Etot'] = np.append(sim['Etot'], np.sum(V_cons[3]))
            #sim['ntot'] = np.append(sim['ntot'], np.sum(V_cons[6]))
        dx = dproc.get_param(f, 'dx')
        for var in SUM_TOTNAMES:
            sim[var] = sim[var]*dx**2
        m_0 = sim['mtot'][0]
        sim['mtot'] = sim['mtot']/m_0

    sim['legend'] = "{} sections".format(sim['nsect'])

    return sim


def save_data(prefix, data):
    filename = prefix + ".dat"
    #out_array = np.array([data['x'], data['mtot']])
    #np.savetxt(filename, out_array, header="t | mtot")

    with open(filename, 'w') as f:
        print "Output in {}".format(filename)
        header = "# x | mtot \n"
        f.write(header)
        for i in range(0, len(data['x'])):
            f.write("{:.8e} {:.8e} \n".format(data['x'][i], data['mtot'][i]))


def plot_mtot_vs_t(ax, data, style):
    ax.plot(data['x'], data['mtot']*1e3, style, lw=1, mfc='k',
            markevery=1, label=data['legend'])


def compute(verbose=False):

    plt.ioff()

    do_animate = True
    if do_animate:

        dproc.create_dir(OUTDIR, erase=True)
        sol_tsm = 'solution.h5'
        with dproc.open_h5(sol_tsm) as f:
            ite_list = dproc.get_ite_list(f)

        for index, ite in enumerate(ite_list):
            plt.close('all')
            sol = plot_sol.Solution(solfile='solution.h5', varname=VARNAME,
                                    figname=FIGNAME, batch=True,
                                    ite_str=ite, isec=SECTION,
                                    extra=EXTRA)

            sol.data.get_data()
            sol.outdir = OUTDIR
            sol.index = index
            if SECTION != 'sum':
                if index == 0:  # Use maximum value of first snapshot
                    vmax = sol.data.sections_all[0].vardict[VARNAME].max()
            else:
                if VARNAME == 'rho':
                    vmax = 0.0018
                elif VARNAME == 'p_11':
                    vmax = 1e-5
                elif VARNAME == 'p_22':
                    vmax = 2e-4
            sol.vmax = vmax
            sol.data.plot_data()

    do_integ = False
    if do_integ:
        plt.close('all')

        tsm_file = "../fig6_tsm/solution.h5"
        tsm_file_10 = "../fig6_tsm_nsect10/solution.h5"
        single_file = "../fig6_tsm_single/solution.h5"

        single_integ = load_sim_integ(solname=single_file)
        tsm_integ = load_sim_integ(solname=tsm_file)
        tsm_integ_10 = load_sim_integ(solname=tsm_file_10)

        fig = plt.figure()
        title = "Time evolution of total mass (normalized by initial value)"
        fig.suptitle(title, fontsize=16)
        ax = fig.add_subplot(111)
        ax.set_xlabel("t", fontsize=16)
        #ax.ticklabel_format(axis='y', style='sci')
        ax.plot(single_integ['x'], single_integ['mtot'], '--', lw=1,
                mfc='k', markevery=1, label=single_integ['legend'])
        ax.plot(tsm_integ['x'], tsm_integ['mtot'], '-', lw=1, mfc='r',
                markevery=1, label=tsm_integ['legend'])
        ax.plot(tsm_integ_10['x'], tsm_integ_10['mtot'], 'o', mfc='None',
                linestyle='', markevery=10, label=tsm_integ_10['legend'],
                markersize=10)
        ax.legend(loc='upper right', frameon=False)
        plt.show()
        prefix = 'integ_mtot'
        filename = prefix + ".pdf"
        fig.savefig(filename, format='pdf')

        # save_data('integ_mtot_tsm', tsm_integ)
        # save_data('integ_mtot_tsm_nsect10', tsm_integ_10)
        # save_data('integ_mtot_single', single_integ)

    error = 0.

    return error


def get_error_status(tol, tol_type, verbose=False):
    """Compute error and return a status and warning message"""
    error = compute(verbose=verbose)
    error = tol
    return dproc.check(error, tol, tol_type, verbose=verbose)
