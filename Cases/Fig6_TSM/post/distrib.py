# -*- coding: utf-8 -*-
"""
Created on Mon May 23 03:26:41 2016

@author: boileau
"""

import numpy as np
import matplotlib.pyplot as plt

s = np.linspace(1e-10, 1.-1e-10, 100)
Smax = 1000.
phi = (1. + 8.*s)*(1. - s)**2 * np.exp(0.001*(1. - 1./(1. - s**2)))

fig = plt.figure()
title = "Normalized initial size distribution of number density"
#fig.suptitle(title, fontsize=16)
ax = fig.add_subplot(111)
ax.set_xlabel(r"$s/S_{max}$", fontsize=16)
ax.plot(s, phi, lw=2)
fig.set_size_inches(3, 2)
fig.subplots_adjust(left=0.16, right=0.9, top=0.92, bottom=0.25)
plt.show()

fig.savefig("normalized_distrib.pdf", format='pdf')
