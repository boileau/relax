"""
2D test case corresponding to Fig. 4b of Boileau et al. SISC2015 paper
:cite:`Boileau:2015`.
"""

import math

casetype = "Figure4"     # Case type
scheme = 'Bouchut-o1'    # Scheme name
CFL = 0.5                # CFL number
tf = 0.8                 # simulation final time
maxite = 0               # simulation maximum iteration (0 if unlimited)
Lx = 1.0                 # Domain length
N = 200                  # Number of points
nsect = 0                # number of sections
dt_store = 0.0           # Storage time interval
dt_plot = 0.0            # Plotting time interval
isSecST = True           # Apply source term?
stokes = 13/(8*math.pi)  # Stokes number
miniplot = True          # Plot first and last solution ?
err_tol = 0.             # Error tolerance

