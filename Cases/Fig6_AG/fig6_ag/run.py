import math

casetype = "Figure6-AG"    # Case type
scheme = 'relax-ag-o1'     # Scheme name
CFL = 0.5                  # CFL number
tf = 1.1                   # simulation final time
maxite = 0                 # simulation final time
Lx = 1.0                   # Domain length
N = 200                    # Number of points
nsect = 0                  # number of sections
dt_store = 0.0             # Storage time interval
dt_plot = 0.1              # Plotting time interval
is2D = True                # 2D test case
isSecST = True             # ST?
stokes = 13./(8.*math.pi)  # Stokes number
