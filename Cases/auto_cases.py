#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Run test cases automatically
"""

import argparse
from collections import OrderedDict
from configparser import ConfigParser
from configparser import NoOptionError
import logging
import imp
from os import system
import os.path
from Relax import error
from Relax import relax
from Relax.plot_sol import Solution
import shlex
from subprocess import Popen, PIPE
import sys


logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler('auto_cases.log', mode='w')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
# add the handlers to logger
logger.addHandler(ch)
logger.addHandler(fh)

ini_filename = "case.ini"
relaxpath = imp.find_module('Relax')[1]


class bcolors:
    """For printing colored output to terminal"""
    HEADER = '\033[35m'
    OKBLUE = '\033[34m'
    OKGREEN = '\033[32m'
    WARNING = '\033[33m'
    FAIL = '\033[31m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# Define output colored status
status_not_checked = bcolors.WARNING + "Not checked" + bcolors.ENDC
status_ok = bcolors.OKGREEN + "OK" + bcolors.ENDC
status_ko = bcolors.FAIL + "KO" + bcolors.ENDC
status_warning = bcolors.WARNING + "Warning" + bcolors.ENDC

status_dict = {'OK': status_ok, 'KO': status_ko, 'Warning': status_warning}


def execute(cmd):
    """Execute a unix command and intercept standard output and error"""

    logger.debug(">>> Executing: " + cmd)

    args = shlex.split(cmd)  # Split cmd into arguments
    p = Popen(args, stderr=PIPE)  # Run the command

    # Log standard output and error
    (stdoutdata, stderrdata) = p.communicate()

    filtered_stderr = set(stderrdata.split("\n"))
    filtered_stderr.discard("created an array from object")
    filtered_stderr.discard("")
    if filtered_stderr:
        logger.info('    Standard error:')
        logger.info(stderrdata)
    else:
        logger.debug(stderrdata)


def read_dim(name, wdir):
    """Read case.ini to get number of dimensions"""
    path = wdir + "/" + name
    os.chdir(path)
    parser = ConfigParser()
    parser.read(ini_filename)
    return int(get_param(parser, 'ndim')[0])


def get_param(parser, param_name):
    """Return parameter value if defined in case file"""
    section = 'Description'
    try:
        param = parser.get(section, param_name).replace(' ', '').split(',')
    except(NoOptionError):
        param = None  # No parameter found
    return param


class TestCase(object):
    """This class handles an automatic test case"""

    def __init__(self, name, wdir):

        self.name = name
        self.path = wdir + "/" + self.name
        os.chdir(self.path)

        self._read_case()

        logger.info("---------------------------------")
        logger.info("Executing test case: " + self.name)
        logger.info("---------------------------------")

        self.runners = []
        self.status = "KO"
        self.msg = ""
        self.runstatus = {}

    def _read_case(self):
        """Read case.ini to get the case parameters"""

        self.parser = ConfigParser()
        self.parser.read(ini_filename)

        self.runlist = get_param(self.parser, 'runs')
        self.varlist = get_param(self.parser, 'variables')
        self.error = get_param(self.parser, 'error')

    def run(self):

        for run_name in self.runlist:
            run = Run(self, run_name)  # Instanciate a run
            self.runners.append(run)  # Add run to runners list
            run.python()  # Run relax.py

    def postprocess(self):
        """Plot figures and compute error if asked"""
        if self.varlist and doplot:
            self.plot()
        self.compute_error()

    def plot(self):
        pass

    def compute_error(self):
        """Compute error from reference data"""

        logger.info("---------------------------------")
        logger.info("  Postproc >>> Processing " + self.name)
        logger.info("---------------------------------")
        msg = ""
        if self.error:
            # Define case_post module to use case error computing method
            with error.set_case_post(self.path) as case_post:
                for run in self.runners:  # Evaluate run status
                    run.compute_error(case_post)
                    if run.status != status_ok:
                        err_msg = "Run {}:\n {}".format(run.dir, run.msg)
                        if msg:  # Add carriage return if msg is not empty
                            msg = msg + "\n" + err_msg
                        else:
                            msg = msg + err_msg
                    self.runstatus[run.dir] = run.status
        else:
            for run in self.runners:
                self.runstatus[run.dir] = status_not_checked

        # Finally evaluate case status
        if status_ko in list(self.runstatus.values()):
            self.status = status_ko
        elif (status_not_checked in list(self.runstatus.values())) or \
             (status_warning in list(self.runstatus.values())):
            self.status = status_warning
        else:
            self.status = status_ok


class TestCase1D(TestCase):
    """
    This class handles an automatic test case for 1D simulations
    using xmgrace for plotting
    """

    def __init__(self, name, wdir):

        # Call parent classe constructor
        super(TestCase1D, self).__init__(name, wdir)

        self.figlist = get_param(self.parser, 'figs')
        self.Dir = "XMG/DAT/"
        self.PDFDir = wdir + "/PDF/" + self.name

        # Create the output directory
        if self.varlist:
            system("rm -rf " + self.Dir + "*")
            system("rm -rf " + self.PDFDir)
            system("mkdir " + self.PDFDir)

    def run(self):

        for run_name in self.runlist:
            run = Run1D(self, run_name)  # Instanciate a run
            self.runners.append(run)  # Add run to runners list
            run.python()  # Run relax.py
            if self.varlist:
                run.output_ascii()  # plot the results in a file

    def plot(self):
        """Plot the test case using xmgrace"""

        mode = "-hardcopy"
        logger.info("---------------------------------")
        for fig in self.figlist:
            prefix = self.name + "_" + fig
            logger.info("  Postproc >>> Processing " + prefix)
            logger.info("---------------------------------")
            cmdpath = "XMG/CMD/" + prefix + ".cmd"
            parpath = "XMG/PAR/" + prefix + ".par"
            cmd = "cat " + cmdpath + " " + parpath + " > batchfile.tmp"
            logger.debug("  Postproc >>> Writing xmgrace batch file:")
            logger.debug("  Postproc >>> " + cmd)
            system(cmd)  # Write the grace script file

            EPS_prefix_path = "XMG/EPS/" + prefix
            EPS_path = EPS_prefix_path + ".eps"
            system("rm -f " + EPS_path)  # Clean the EPS directory
            cmd = "xmgrace " + mode + \
                  " -nosafe -graph 0 -autoscale none " \
                  "-batch ./batchfile.tmp -printfile " \
                  + EPS_path
            logger.debug("  Postproc >>> Launching xmgrace:")
            logger.debug("  Postproc >>> " + cmd)
            system(cmd)  # Run xmgrace
            # Erase the temporary script file
            system("rm -f batchfile.tmp")

            cmd = "epstopdf " + EPS_path
            system(cmd)   # Convert EPS to PDF

            PDF_src_path = EPS_prefix_path + ".pdf"
            cmd = "mv " + PDF_src_path + " " + self.PDFDir
            system(cmd)   # mv PDF file to final output directory
            logger.debug("  Postproc >>> Saving figure in: {}/{}.pdf"
                         .format(self.PDFDir, prefix))
            logger.debug("---------------------------------")


class TestCase2D(TestCase):
    """
    This class handles an automatic test case for 2D simulations using
    matplotlib for plotting
    """

    def __init__(self, name, wdir):

        # Call parent classe constructor
        super(TestCase2D, self).__init__(name, wdir)
        self.DestDir = wdir + "/PNG"

    def plot(self):
        """Plot the test case figures using matplotlib"""
        logger.info("---------------------------------")
        logger.info("  Postproc >>> Processing " + self.name)
        logger.info("---------------------------------")
        for run in self.runners:
            run.plot(self)


class Run(object):

    def __init__(self, case, name):
        self.varlist = case.varlist
        self.dir = name
        self.status = ""
        self.msg = ""

    def python(self):
        """Run relax.py"""
        logger.info("---------------------------------")
        logger.info("  Running: " + self.dir)
        logger.info("---------------------------------")
        # Instanciate a relax simulation
        relax_run = relax.Relax(rundir=self.dir, verbose=0)
        self.elapsed = relax_run.run()  # Run the simulation
        logger.info("---------------------------------")
        logger.info("  Execution time [s]: {}".format(self.elapsed))
        logger.info("---------------------------------")

    def plot(self, case):
        """Plot figure that corresponds to given run"""
        solfile = self.dir + "/solution.h5"
        figname = self.dir
        for var in case.varlist:
            sol = Solution(solfile=solfile, varname=var,
                           figname=figname, batch=True)
            sol.process()
            figfile = figname + ".png"
            cmd = "mv " + figfile + " " + case.DestDir
            system(cmd)

    def compute_error(self, case_post):
        """Compute error from reference data and return status and message"""
        logger.info("---------------------------------")
        logger.info("  Postproc >>> Check error for " + self.dir)
        logger.info("---------------------------------")
        os.chdir(self.dir)  # Go to run directory
        status, self.msg = error.get_status(case_post)
        self.status = status_dict[status]
        print("Error check:", self.status)
        if self.status != status_ok:
            print(self.msg)
        os.chdir('..')  # Go back to case directory


class Run1D(Run):

    def __init__(self, case, name):
        # Call parent classe constructor
        super(Run1D, self).__init__(case, name)

        self.path = case.path + "/" + name
        self.DestDir = case.Dir + "/" + name

    def output_ascii(self):
        """Output results in an ascii file"""
        system("mkdir " + self.DestDir)
        for var in self.varlist:
            solfile = self.path + "/solution.h5"
            sol = Solution(solfile=solfile, varname=var, ascii=True,
                           batch=True)
            sol.process()

            asciifile = var + "*.dat"
            cmd = "mv ./out/" + asciifile + " " + self.DestDir
            system(cmd)


if __name__ == '__main__':

    wdir = os.getcwd()

    cases_0D = 'evapdrag_0D', 'evapdrag_icmf_0D'
    cases_1D = 'Fig1', 'Fig2', 'Fig3.1', 'Fig3.2'
    cases_2D = 'Fig4', 'Fig5', 'Sod_AG_Berthon_2D'
    cases_AG = 'Sod_AG_AV', 'Sod_AG_Berthon', 'Sod_AG_Berthon_2D'
    cases_ext = 'Fig6_TSM', 'Fig6'
    short_cases = cases_0D + cases_1D + cases_2D + cases_AG
    test_suite = list(OrderedDict.fromkeys(short_cases))
    all_cases = list(OrderedDict.fromkeys(short_cases + cases_ext))

    parser = argparse.ArgumentParser(description="Run automatic test cases")
    parser.add_argument('--case', nargs='+', required=False, metavar="Case",
                        help="list of test cases")
    parser.add_argument('--plot', action="store_true", required=False,
                        help="Plot figures")
    args = parser.parse_args()
    doplot = args.plot

    if args.case:
        case = args.case[0]
        if case == "list":
            msg = "Available tests are: {}".format(all_cases)
            sys.exit(msg)
        elif case == "0D":
            cases = cases_0D
        elif case == "1D":
            cases = cases_1D
        elif case == "2D":
            cases = cases_2D
        elif case == "AG":
            cases = cases_AG
        elif case == "extended":
            cases = cases_ext
        else:
            cases = tuple(args.case)

            for case in cases:
                if case not in all_cases:
                    msg = "Available tests are: {}".format(all_cases)
                    sys.exit(msg)

    else:
        cases = test_suite

    testcases = []

    for casename in cases:  # ite selected test cases
        ndim = read_dim(casename, wdir)
        if ndim == 1:
            testcase = TestCase1D(casename, wdir)
        elif ndim == 2:
            testcase = TestCase2D(casename, wdir)
        else:
            sys.exit("Error: ndim = ".format(ndim))

        testcase.run()
        testcase.postprocess()
        testcases.append(testcase)  # Add test case to test case list

    # Finally print the timing statistics
    logger.info("---------------------------------")
    logger.info("Execution summary")
    total_time = 0.
    total_time_string = "Total compute time"

    # ---
    # get first column text width for output alignement
    run_names = []
    for testcase in testcases:
        for run in testcase.runners:
            run_names.append("{} [{}]".format(run.dir, run.status))
    run_names.append(total_time_string)  # do not forget total time
    width = max(len(s) for s in run_names)  # get maximum string length
    # ---

    for testcase in testcases:
        logger.info("---------------------------------")
        logger.info("{} [{}]".format(testcase.name, testcase.status))
        for run in testcase.runners:  # iter on runs
            run_name = run.dir
            run_time = run.elapsed
            run_name_status = "{} [{}]".format(run_name,
                                               testcase.runstatus[run_name])
            logger.info("  {0:<{width}}: {1:>10.6f} s"
                        .format(run_name_status, run.elapsed, width=width))
            total_time += run.elapsed
    logger.info("---------------------------------")
    logger.info("  {0:<{width}}: {1:>10.6f} s".format(total_time_string,
                total_time, width=width))
    logger.info("---------------------------------")
