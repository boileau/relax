HARDCOPY DEVICE "EPS"
DEVICE "EPS" OP "bbox:page"

READ BLOCK "./XMG/REF/fig3_init.ref"
BLOCK XY "1:2"

READ XY "./XMG/DAT/fig3_o2_t04/rho.dat"
READ XY "./XMG/DAT/fig3_o2_t04/rho.dat"
READ XY "./XMG/DAT/fig3_o2_t04/color.dat"
RESTRICT(S1, S3.y > 0)
RESTRICT(S2, S3.y <= 0)
kill S3

READ XY "./XMG/DAT/fig3_o2_t06/rho.dat"
READ XY "./XMG/DAT/fig3_o2_t06/rho.dat"
READ XY "./XMG/DAT/fig3_o2_t06/color.dat"
RESTRICT(S3, S5.y > 0)
RESTRICT(S4, S5.y <= 0)
kill S5

