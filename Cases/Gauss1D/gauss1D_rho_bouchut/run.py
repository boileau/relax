"""
1D transport of a gaussian shape
"""

scheme = 'Bouchut-o2'    # Scheme name
casetype = 'gauss1D'      # Case name
CFL = 0.5, 1.0            # CFL number
tf  = 1.0                 # simulation final time
maxite = 0                # Max number of iterations
Lx = 1.0                  # Domain length
N = 10, 100, 1000                  # Number of points
dt_store = 0.0            # Storage time interval
dt_plot = 0.1             # Plotting time interval
miniplot = True           # PLot first and last solution ?
err_tol = 0.              # Error tolerance
