"""
1D transport of a gaussian shape
"""

scheme = 'relax-ag-o1'    # Scheme name
casetype = 'gauss1D'      # Case name
CFL = 0.5                 # CFL number
tf  = 10.                 # simulation final time
maxite = 0                # Max number of iterations
Lx = 1.0                  # Domain length
N = 10000                 # Number of points
dt_store = 1.0            # Storage time interval
dt_plot = 0.0             # Plotting time interval
miniplot = True           # PLot first and last solution ?
err_tol = 0.              # Error tolerance
solfile = 'solution_{}_nx{}_CFL{}.h5'.format(scheme, N, CFL)
