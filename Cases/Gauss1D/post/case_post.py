# -*- coding: utf-8 -*-
"""
Compute relative error
Reference code is in: Relax/Fortran_main/
"""

import numpy as np
import matplotlib.pyplot as plt
import os
from Relax import data_process as dproc

VARNAMES = 'nk',
FIGFORM = 'pdf'
OUTDIR = 'pdf'


def create_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)


def is_equal(a, b):
    if a != 0.:
        err = abs((a - b)/a)
    elif b != 0.:
        err = abs((a - b)/b)
    else:
        err = abs((a - b))
    return err < 1e-8


def compute_snap_error(ref, sim_name, snap_time, norm):
    """Return an error dict for given snapshot time of given simulation"""
    error_L2 = {}

    with dproc.open_h5(h5path=sim_name) as f:
        index, ite = dproc.get_ite(f, snap_time, get_index=True)
        sim_time = dproc.get_time_list(f)[index]

    if not is_equal(sim_time, snap_time):
        print "WARNING: snap_time = {} | sim_time = {}". \
              format(snap_time, sim_time)

    sim = dproc.load_sim(sol_path=sim_name, ite=ite)

    for var in VARNAMES:
        error_L2[var] = np.linalg.norm(sim[var] - ref[var], ord=2)/norm[var]
    return error_L2


def compute_error(refs, sim_names, time_list):

    errors_L2_time = []
    scheme_dict = {}
    error_dict = {}
    for ref, sim_name in zip(refs, sim_names):
        with dproc.open_h5(h5path=sim_name) as f:
            scheme = dproc.get_param(f, 'scheme')[0]
            CFL = dproc.get_param(f, 'CFL')
            scheme_CFL = "{} - CFL = {}".format(scheme, CFL)
            scheme_dict.setdefault(scheme_CFL, []).append(sim_name)

        legend = "{} - nx = {}".format(scheme_CFL, len(ref['x']))

        # initialize error dict
        error_L2 = {'legend': legend}
        n_time = len(time_list)
        for var in VARNAMES:
            error_L2[var] = np.zeros(n_time)
        error_L2['time'] = np.zeros(n_time)

        # Compute norm
        norm = {}
        for var in VARNAMES:
            norm[var] = np.linalg.norm(ref[var], ord=2)

        print "Compute error for {}".format(sim_name)
        for i, time in enumerate(time_list):
            snap_error_L2 = compute_snap_error(ref, sim_name, time, norm)
            for var in VARNAMES:
                error_L2[var][i] = snap_error_L2[var]
            error_L2['time'][i] = time
        error_dict[sim_name] = error_L2
        errors_L2_time.append(error_L2)

    error = {}
    for scheme_CFL, sim_list in scheme_dict.iteritems():
        n = len(sim_list)
        error[scheme_CFL] = {'dx': np.zeros(n)}
        for var in VARNAMES:
            error[scheme_CFL][var] = np.zeros(n)

        for i, sim_name in enumerate(sim_list):
            with dproc.open_h5(h5path=sim_name) as f:
                scheme = dproc.get_param(f, 'scheme')[0]
                CFL = dproc.get_param(f, 'CFL')
                scheme_CFL = "{} - CFL = {}".format(scheme, CFL)
                error[scheme_CFL]['legend'] = scheme_CFL
                error[scheme_CFL]['dx'][i] = dproc.get_param(f, 'dx')
                for var in VARNAMES:
                    error[scheme_CFL][var][i] = error_dict[sim_name][var][-1]

    return errors_L2_time, error


def plot_time_error(*err_data, **kwargs):
    """plot error vs time"""
    title = kwargs.pop('title', '')
    figname = kwargs.pop('figname', '')
    figform = kwargs.pop('figform', 'pdf')
    if figname:
        filename = "{}.{}".format(figname, figform)
        print "Plotting ", filename

    fig = plt.figure()

    ax = {}
    for index, var in enumerate(VARNAMES):
        ax[var] = fig.add_subplot(1, 2, index+1)

    figtitle = title
    fig.suptitle(figtitle, fontsize=14)

    for var in VARNAMES:
        for data in err_data:
            ax[var].plot(data['time'], data[var], label=data['legend'])
            ax[var].set_title(var)
            ax[var].set_xlabel('t [s]', fontsize=12)

    ax[VARNAMES[0]].legend(loc='right', frameon=False)

    if figname:
        fig.set_size_inches(15, 10)
        fig.savefig(filename, format=figform, dpi=150)
        plt.close(fig)
    else:
        plt.show()


def plot_error(error, **kwargs):
    """plot error vs dx"""
    title = kwargs.pop('title', '')
    figname = kwargs.pop('figname', '')
    figform = kwargs.pop('figform', 'pdf')
    if figname:
        filename = "{}.{}".format(figname, figform)
        print "Plotting ", filename

    fig, ax = plt.subplots(1)

    fig.suptitle(title, fontsize=14)

    for scheme_error in error.itervalues():
        for var in VARNAMES:
            legend = scheme_error['legend'] + " - {}".format(var)
            ax.plot(scheme_error['dx'], scheme_error[var],
                    label=legend, marker='o', mfc='None')

    x_slope = np.array([1e-4, 1e-3])
    y_slope = np.array([1e-1, 1.])
    ax.plot(x_slope, y_slope, 'k--', label="Slope 1")

    ax.legend(loc='lower right', frameon=False)
    ax.set_xlabel('grid size [m]', fontsize=12)
    ax.set_xscale('log')
    ax.set_ylabel('Normalized L2-error', fontsize=12)
    ax.set_yscale('log')

    if figname:
        fig.set_size_inches(15, 10)
        fig.savefig(filename, format=figform, dpi=150)
        plt.close(fig)
    else:
        plt.show()


def compute(verbose=False):

    plt.close('all')
    plt.ioff()

    time_start = 0.0
    time_end = 10.
    dt_store = 1.
    time_list = np.arange(time_start, time_end, dt_store)

    with open('sol_list.dat', 'r') as f:
        sim_names = [line[:-1] for line in f if line[0] != '#']

    refs = [dproc.load_sim(sol_path=name, ite=0) for name in sim_names]

    do_plot = True
    if do_plot:

        time_start = 0
        time_end = 10.0
        dt_store = 1.

        time_list = np.arange(time_start, time_end, dt_store)

        for time in time_list:

            sims = []
            for sim_name in sim_names:
                with dproc.open_h5(h5path=sim_name) as f:
                    ite = dproc.get_ite(f, time)
                    scheme = dproc.get_param(f, 'scheme')[0]
                    CFL = dproc.get_param(f, 'CFL')
                    scheme_CFL = "{} - CFL = {}".format(scheme, CFL)

                sim = dproc.load_sim(sol_path=sim_name, ite=ite)
                print "{} time = {}".format(sim['solname'], sim['t'])
                sim['legend'] = "{}: nx = {}".format(scheme_CFL, len(sim['x']))
                sims.append(sim)

            create_dir(OUTDIR)
            figname = '{}/xprofiles_ite_{}'.format(OUTDIR, ite)
            dproc.plot_ref_sim(refs[0], *sims, varnames=VARNAMES,
                               title="x-profiles ", figname=figname,
                               figform=FIGFORM)

    # ---------------
    # Error computing

    errors_L2_time, error = compute_error(refs, sim_names, time_list)
    figtitle = "L2 error vs time"
    figname = "{}/errors_L2_time".format(OUTDIR)
    create_dir(OUTDIR)
    plot_time_error(*errors_L2_time, title=figtitle,
                    figname=figname, figform=FIGFORM)

    figtitle = "L2 error vs grid size at t = 10"
    figname = "{}/errors_L2_dx".format(OUTDIR)
    plot_error(error, title=figtitle, figname=figname, figform=FIGFORM)

    error = 0.
    # ---------------

    return error


def get_error_status(tol, verbose=False):
    """Compute error and return a status and warning message"""
    error = compute(verbose=verbose)
    error = tol
    return dproc.check(error, tol, verbose=verbose)
