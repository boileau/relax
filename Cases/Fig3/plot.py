# -*- coding: utf-8 -*-
"""
Plot relax and reference solution
"""

import imp
from Relax import data_process as dproc

# Load sod_process module
mod_name = 'case_error'
tup = imp.find_module(mod_name, ['../post'])
ce = imp.load_module(mod_name, *tup)


if __name__ == '__main__':

    # Load and plot last solution
    ref = ce.load_ref('solution_ref.dat')
    sim = dproc.load_sim()
    dproc.plot_ref_sim(ref, sim, varnames=ce.varnames)
