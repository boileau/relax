# -*- coding: utf-8 -*-
"""
Compute error with reference solution
"""

import numpy as np
from Relax import data_process as dproc

varnames = 'rho', 'u', 'v', 'p_11', 'color'
zero_tol = dict(tup for tup in zip(varnames, [0.]*len(varnames)))
tol = zero_tol


def load_ref(filename):
    """Load reference data"""
    ref = {}
    path = "Ref/" + filename
    ref_data = np.loadtxt(path)

    ref['x'] = ref_data[:, 0]
    ref['rho'] = ref_data[:, 1]
    ref['u'] = ref_data[:, 2]
    ref['v'] = ref_data[:, 3]
    ref['p_11'] = ref_data[:, 5]
    ref['color'] = ref_data[:, 6]

    return ref


def compute(sol_path="solution.h5", verbose=True):

    error = {}
    # ref = load_ref('solution_ref.dat')
    ref = dproc.load_sim('Ref/solution_ref.h5')
    sim = dproc.load_sim()

    for var in varnames:
        error[var] = dproc.L2_err(sim[var], ref[var])

    if verbose:
        dproc.plot_ref_sim(ref, sim, varnames=varnames)
        for k, v in error.items():
            print("Error({}) = {}".format(k, v))

    return error


def get_error_status(tol, tol_type, verbose=True):
    """Compute error and return status"""
    error = compute(verbose=verbose)
    return dproc.check(error, tol, tol_type, verbose=verbose)
