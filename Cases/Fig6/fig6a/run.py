import math

casetype = "Figure6"       # Case type
scheme = 'relax-isoh-o2'   # Scheme name
CFL = 0.5                  # CFL number
tf = 0.75                  # simulation final time
maxite = 0                 # simulation final time
Lx = 1.0                   # Domain length
N = 200                    # Number of points
nsect = 0                  # number of sections
dt_store = 0.0             # Storage time interval
dt_plot = 0.0              # Plotting time interval
is2D = True                # 2D test case
isSecST = True             # ST?
stokes = 13./(8.*math.pi)  # Stokes number
err_tol = {'rho': 1e-13, 'u': 1e-12, 'v': 1e-13, 'e_11': 1e-11, 'p_11': 1e-15}  # Error tolerance
tol_type = 'lower'
