HARDCOPY DEVICE "EPS"
DEVICE "EPS" OP "bbox:page"

define seuil
seuil=0.0

READ XY "./XMG/DAT/fig2_o1/p_11.dat"
READ XY "./XMG/DAT/fig2_o1/p_11.dat"
READ XY "./XMG/DAT/fig2_o1/color.dat"
RESTRICT(S0, S2.y > seuil)
RESTRICT(S1, S2.y <= seuil)
kill S2

READ XY "./XMG/DAT/fig2_o2/p_11.dat"
READ XY "./XMG/DAT/fig2_o2/p_11.dat"
READ XY "./XMG/DAT/fig2_o2/color.dat"
RESTRICT(S2, S4.y > seuil)
RESTRICT(S3, S4.y <= seuil)
kill S4

READ BLOCK "./XMG/REF/fig2_exact.ref"                                                                                                           
BLOCK XY "1:5"

