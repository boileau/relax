HARDCOPY DEVICE "EPS"
DEVICE "EPS" OP "bbox:page"

define seuil
seuil=0.0

READ XY "./XMG/DAT/fig2_o1/rho.dat"
READ XY "./XMG/DAT/fig2_o1/rho.dat"
READ XY "./XMG/DAT/fig2_o1/color.dat"
RESTRICT(S0, S2.y > seuil)
RESTRICT(S1, S2.y <= seuil)
kill S2

READ XY "./XMG/DAT/fig2_o2/rho.dat"
READ XY "./XMG/DAT/fig2_o2/rho.dat"
READ XY "./XMG/DAT/fig2_o2/color.dat"
RESTRICT(S2, S4.y > seuil)
RESTRICT(S3, S4.y <= seuil)
kill S4

READ XY "./XMG/REF/fig2_exact.ref"

