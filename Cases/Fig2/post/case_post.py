# -*- coding: utf-8 -*-
"""
Compute relative error
Reference code is in: Cases/AG_TSM/Code_JL
"""

import matplotlib.pyplot as plt
from Relax import data_process as dproc
import sod

varnames = 'rho', 'u', 'p_11'
error = {}
# Default tolerance
tol = {'p_11': 0.13826649827964538,
       'u': 0.56499899691404676,
       'rho': 0.48852196579281587}


def compute(sol_path='solution.h5', verbose=True, plot=False):
    """return error between simulation and reference"""

    ref = {}
    sim = {}
    # Load reference data

    with dproc.open_h5(sol_path) as f:  # Open solution.h5
        t, V = dproc.load_snapshot(f)  # Load last solution
        npts = dproc.get_param(f, 'N')
        gamma = dproc.get_param(f, 'gamma')
    sim = dproc.build_vardict(V, npts, gamma, color=None, varlist=varnames)

    # Compute Sod's analytical solution
    rhol = 1.0
    pl = 1.1
    ul = 0.0
    left_state = pl, rhol, ul

    rhor = 0.125
    pr = 1.e-16
    ur = 0.0
    right_state = pr, rhor, ur

    xstart = 0.0
    xend = 1.0
    xl = xstart + (xend - xstart)/npts
    xr = xend - (xend - xstart)/npts
    xi = 0.5
    geometry = xl, xr, xi

    positions, regions, ref = sod.solve(left_state, right_state, geometry, t,
                                        gamma=gamma, npts=npts)

    for var in varnames:
        error[var] = dproc.L2_err(sim[var], ref[var], norm=ref[var].max())

    if plot:
        plt.close('all')
        fig, axarr = plt.subplots(len(ref)-1, sharex=True)

        axarr[0].plot(ref['x'], ref['p'][:-1], linewidth=1.5, color='b')
        axarr[0].plot(ref['x'], sim['p'][:-1], linewidth=1.5, color='k')
        axarr[0].set_ylabel('pressure')
        axarr[0].set_ylim(0, 1.1)
        axarr[1].plot(ref['x'], ref['rho'][:-1], linewidth=1.5, color='b')
        axarr[1].plot(ref['x'], sim['rho'][:-1], linewidth=1.5, color='k')
        axarr[1].set_ylabel('rho')
        axarr[1].set_ylim(0, 1.1)
        axarr[2].plot(ref['x'], ref['u'][:-1], linewidth=1.5, color='b')
        axarr[2].plot(ref['x'], sim['u'][:-1], linewidth=1.5, color='k')
        axarr[2].set_ylabel('u')
        axarr[2].set_ylim(0, 1.1)
        plt.show()

    if verbose:
        for k, v in error.items():
            print("Error({}) = {}".format(k, v))

    return error


def get_error_status(tol, tol_type, verbose=True):
    """Compute error and return status"""
    error = compute(verbose=verbose)
    tol_type = 'lower'
    return dproc.check(error, tol, tol_type, verbose=verbose)
