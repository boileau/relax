"""
Evaporation and drag case: extension of evapdrag0D.py by F. Laurent
To postprocess, run:
./plot_sol.py --sol solution.h5 --var rho --probe probe_0 --ascii
"""

casetype = "evap&drag1D-homo"  # Case type
scheme = 'relax-ag-o1'   # Scheme name

factor_x = 9

Smax = 11310.           # Max size [micro-m^2]
Rs = 1.99e-7            # Evaporation coefficient [m^2.s^-1]

dt_fixed = 3.e-4/factor_x

CFL = 0.5               # CFL number
tf = 0.06               # simulation final time
xmin = - 0.2
Lx = 0.4           # Domain length
N = 100*factor_x
nsect = 25             # number of sections
tsm_order = 2           # TSM method order
gamma = 3.              # polytropic exponent

#dt_store = 0.0005          # Storage time interval
ite_store = 1*factor_x           # Storage ite interval
dt_plot = 0.0           # Plotting time interval
is2D = False            # 2D test case
isSecST = False         # ST for each system?
isAllST = True          # ST coupling all systems?
miniplot = True
sol_type = 'both'          # Compute reference solution?
# Add probes:
probe = []
#probe.append({'position': -0.102, 'comment': 'xmin'})
#probe.append({'position': 0.000, 'comment': 'Center'})
solfile = 'solution_n{}_nsect{}.h5'.format(N, nsect)
# Error tolerance:
err_tol = 0.
#err_tol = {'mtot': 0.0018402540227463492,
#           'qdmtot': 0.0022860704632382206,
#           'Etot': 0.0045340681470737582,
#           'ntot': 0.0093343137328433749}
