# -*- coding: utf-8 -*-
"""
Created on Sat May  7 01:39:40 2016

@author: boileau
"""
import pandas as pd
import numpy as np

refdir = '../prog_1D/Result_1D_noevapdrag_nx100'


def file_name(var, k=1):
    return "{}/t_{}_{}_analytique.dat".format(refdir, var, k+1)


def read_loadtxt(var):
    return np.loadtxt(file_name(var))


def read_panda(var):
    return pd.read_csv(file_name(var), delim_whitespace=True, header=None).values

read_data = read_panda

times = read_data('mk')[:, 0]

snap_time = 0.01502
ref_ite = np.searchsorted(times, snap_time)

Ek = read_data('ek')[ref_ite, 1:]
mk = read_data('mk')[ref_ite, 1:]
qdmk = read_data('qdmk')[ref_ite, 1:]

uk = qdmk/mk
ek = Ek/mk - uk**2

print ek