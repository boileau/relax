# -*- coding: utf-8 -*-
"""
Compute relative error
Reference code is in: Relax/Fortran_main/
"""

from ConfigParser import ConfigParser
from ConfigParser import NoOptionError
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
from Relax import data_process as dproc
import shutil


scalars = 'ntot', 'mtot'
others = 'qdmtot', 'Etot', 'time'
varnames = 'mtot', 'ntot', 'qdmtot', 'umoy', 'Etot', 'emoy'
kvarnames = 'mk', 'nk', 'qdmk', 'Ek'
kvarnames2 = 'nk', 'mk', 'qdmk', 'uk', 'Ek', 'ek'
sum_totnames = 'mtot', 'ntot', 'qdmtot', 'Etot'
probename = 'probe_0'
FIGFORM = 'pdf'
REFDIR = 'Ref_nsec10'
NSECT = int(REFDIR[8:])
NSECT_OUT = 5
OUTDIR = 'pdf'
NSECT_DICT = {}


def get_file_name(var):
    return "{}/t_{}_analytique.dat".format(REFDIR, var)


def read_loadtxt(file_name):
    return read_data(file_name)


def read_panda(file_name):
    return pd.read_csv(file_name, delim_whitespace=True, header=None).values

read_data = read_panda


def get_ref_time():
    """Return time steps as a numpy array"""
    # Get time only
    filename = get_file_name('ntot')
    return read_data(filename)[:-1, 0]


def get_ref_x():
    # Get space coordinate only
    file_name = '{}/x_mtot_analytique.dat'.format(REFDIR)
    return read_data(file_name)[:, 0]


def load_ref_sum():
    """Return a dict of integrals over space (time data)"""
    ref = {'xlabel': 't [s]'}
    # Get time only
    ref['x'] = get_ref_time()

    x = get_ref_x()
    dx = x[1] - x[0]

    for var in 'ntot', 'mtot', 'qdmtot':
        # store from 2nd column to end of line
        ref[var] = np.sum(read_data(get_file_name(var))[:-1, 1:], axis=1)

    # store from 2nd column to end of line (rename variable)
    ref['Etot'] = np.sum(read_data(get_file_name('etot'))[:-1, 1:], axis=1)
    for var in sum_totnames:
        ref[var] = ref[var] * dx
    return ref


def load_ref(ite=0):
    ref = {'xlabel': 'x [m]'}

    if ite == 'last':
        nite = -1
    else:
        nite = ite

    # Get time only
    filename = get_file_name('ntot')
    ref['t'] = read_data(filename)[nite, 0]

    # Get other variables
    for var in 'ntot', 'mtot', 'qdmtot', 'umoy':
        # store from 2nd column to end of line
        ref[var] = read_data(get_file_name(var))[nite, 1:]

    # store from 2nd column to end of line (rename variable)
    ref['Etot'] = read_data(get_file_name('etot'))[nite, 1:]

    # sstore from 2nd column to end of line (rename variable)
    ref['emoy'] = read_data(get_file_name('sigmoy'))[nite, 1:]

    return ref


def load_ref_allite():
    ref = {}
    # Get other variables
    for var in 'ntot', 'mtot', 'qdmtot':
        # store from 2nd column (skip time) to end of line
        ref[var] = read_data(get_file_name(var))[:, 1:]

    # store from 2nd column (skip time) to end of line (rename variable)
    etot = read_data(get_file_name('etot'))
    ref['time'] = etot[:, 0]  # get time only
    ref['Etot'] = etot[:, 1:]

    return ref


def load_time_ref():
    ref = {'xlabel': 't [s]'}

    # Get time only
    filename = get_file_name('ntot')
    ref['x'] = read_data(filename)[:-1, 0]

    # Get other variables
    for var in 'ntot', 'mtot', 'qdmtot', 'umoy':
        # store from 2nd column to end of line
        ref[var] = read_data(get_file_name(var))[:-1, 1:]

    # store from 2nd column to end of line (rename variable)
    ref['Etot'] = read_data(get_file_name('etot'))[:-1, 1:]

    # sstore from 2nd column to end of line (rename variable)
    ref['emoy'] = read_data(get_file_name('sigmoy'))[:-1, 1:]

    return ref


def load_kref(ite=0, k=0):
    """load reference data for a single section"""
    ref = {'xlabel': 'x [m]'}

    if ite == 'last':
        nite = -1
    else:
        nite = ite

    def get_file_name(var):
        return "{}/t_{}_{}_analytique.dat".format(REFDIR, var, k+1)

    # Get time only
    filename = get_file_name('nk')
    ref['t'] = read_data(filename)[nite, 0]

    ref['x'] = get_ref_x()

    # Get other variables
    for var in 'nk', 'mk', 'qdmk':
        # store from 2nd column to end of line
        ref[var] = read_data(get_file_name(var))[nite, 1:]

    # store from 2nd column to end of line (rename variable)
    ref['Ek'] = read_data(get_file_name('ek'))[nite, 1:]

    return ref


def get_kfile_name(var, k):
    return "{}/t_{}_{}_analytique.dat".format(REFDIR, var, k+1)


def load_kref_allite(k=0):
    ref = {'xlabel': 'x [m]'}

    for var in 'nk', 'mk', 'qdmk':
        # store from 2nd column (skip time) to end of line
        ref[var] = read_data(get_kfile_name(var, k))[:, 1:]

    # store from 2nd column (skip time) to end of line (rename variable)
    etot = read_data(get_kfile_name('ek', k))
    ref['t'] = etot[:, 0]  # get time only
    ref['time'] = ref['t']
    ref['Ek'] = etot[:, 1:]
    ref['uk'] = dproc.divide_and_filter(ref['qdmk'], ref['mk'])
    ref['ek'] = dproc.divide_and_filter(ref['Ek'], ref['mk']) - ref['uk']**2

    return ref


def load_time_kref(k=0):
    """load reference data for a single section"""
    ref = {'xlabel': 't [s]'}

    # Get time only
    filename = get_file_name('ntot')
    ref['x'] = read_data(filename)[:-1, 0]
#    ref['x'] = get_ref_time()

    # Get other variable
    for var in 'nk', 'mk', 'qdmk':
        # store from 2nd column to end of line
        ref[var] = read_data(get_kfile_name(var, k))[:-1, 1:]

    # store from 2nd column to end of line (rename variable)
    ref['Ek'] = read_data(get_kfile_name('ek', k))[:-1, 1:]
    ref['uk'] = dproc.divide_and_filter(ref['qdmk'], ref['mk'])
    ref['ek'] = dproc.divide_and_filter(ref['Ek'], ref['mk']) - ref['uk']**2
    return ref


def init_sim(solname, ite, k):
    sim = {}
    with dproc.open_h5(h5path=solname) as f:
        ndim = dproc.get_param(f, 'ndim')
        if ndim == 1:
            sim['t'], V = dproc.load_snapshot(f, ite=ite, k=k)
        else:
            sim['t'], V_2D = dproc.load_snapshot(f, ite=ite, k=k)
            V = V_2D[:, :, 0]
        sim['x'] = dproc.get_grid(f)
        sim['nsect'] = dproc.get_param(f, 'nsect')
        sim['N'] = dproc.get_param(f, 'N')  # number of grid points

    if k == 'sum' and sim['nsect'] != 0:
        V_out = V
    else:
        args = (subarray[0] for subarray in np.split(V, 7))
        V_cons = dproc.prim_to_cons(*args)  # Convert to conservative variables
        # Concatenate:
        # (rho rhou rhov E_11 E_12 E22 nk) + (u v e_11 e_12 e_22 mu)
        V_out = np.concatenate((V_cons[:7], V[1:]))
    return sim, V_out


def set_legend(solname, data):
    if solname[-7:] == '_ref.h5':
        data['legend'] = 'analytical: nsec = {}'.format(data['nsect'])
    else:
#        data['legend'] = '{}: n = {} - nsec = {}'.format(solname, data['N'],
#                                                         data['nsect'])
        data['legend'] = 'n = {} - nsec = {}'.format(data['N'], data['nsect'])


def load_sim(solname='solution.h5', ite=0):

    sim, V_sum = init_sim(solname, ite, k='sum')
    sim['solname'] = solname
    sim['mtot'] = V_sum[0]
    sim['umoy'] = V_sum[7]
    sim['emoy'] = V_sum[9]
    sim['qdmtot'] = V_sum[1]
    sim['Etot'] = V_sum[3]
    sim['ntot'] = V_sum[6]
    set_legend(solname, sim)

    return sim


def load_ksim(solname='solution.h5', ite=0, k=0):

    sim, V = init_sim(solname, ite, k=k)
    sim['solname'] = solname
    sim['mk'] = V[0]
    sim['qdmk'] = V[1]
    sim['Ek'] = V[3]
    sim['nk'] = V[6]
    sim['uk'] = V[7]
    sim['ek'] = V[9]
    sim['k'] = k
    set_legend(solname, sim)
    return sim


def load_sim_sum(solname='solution.h5'):
    """Return a dict of integrals over space (time data)"""
    sim = {}
    sim['solname'] = solname
    with dproc.open_h5(h5path=solname) as f:
        sim['nsect'] = dproc.get_param(f, 'nsect')
        sim['N'] = dproc.get_param(f, 'N')  # number of grid points
        sim['x'] = []
        for var in sum_totnames:
            sim[var] = np.empty(0)
        for ite in dproc.get_ite_list(f):
            t, V_cons = dproc.load_snapshot(f, ite=ite, k='sum')
            sim['x'].append(t)
            sim['mtot'] = np.append(sim['mtot'], np.sum(V_cons[0]))
            sim['qdmtot'] = np.append(sim['qdmtot'], np.sum(V_cons[1]))
            sim['Etot'] = np.append(sim['Etot'], np.sum(V_cons[3]))
            sim['ntot'] = np.append(sim['ntot'], np.sum(V_cons[6]))
        dx = dproc.get_param(f, 'dx')
        for var in sum_totnames:
            sim[var] = sim[var]*dx
    set_legend(solname, sim)

    return sim


def load_sum_probe(solname='solution.h5', probename=probename):
    """return a dict of sum data at probe position"""
    probe = {}
    with dproc.open_h5(h5path=solname) as f:
        sum_data = dproc.get_probe_data(f, probename, 'sum_data')
        probe['x'] = dproc.get_probe_data(f, probename, 'time')
        probe['coor'] = dproc.get_probe_info(f, probename)[0]
        probe['desc'] = dproc.get_probe_description(f, probename)

    # Store simulation data in a dict
    probe['ntot'] = sum_data[:, 6]
    probe['mtot'] = sum_data[:, 0]
    probe['qdmtot'] = sum_data[:, 1]
    probe['Etot'] = sum_data[:, 3]
    probe['umoy'] = sum_data[:, 7]
    probe['emoy'] = sum_data[:, 9]
    if solname[-7:] == '_ref.h5':
        probe['legend'] = 'analytical'
    else:
        probe['legend'] = solname

    return probe


def load_kprobe(solname='solution.h5', probename=probename, k=0):
    """return a dict of section data at probe position"""
    probe = {}
    with dproc.open_h5(h5path=solname) as f:
        time = dproc.get_probe_data(f, probename, 'time')
        nite = len(time)
        gamma = dproc.get_param(f, 'gamma')
        path = "probes/{}/data".format(probename)
        data = f[path][k, :, :, 0].transpose()
        data_dict = dproc.build_vardict(data, nite, gamma)
        probe['x'] = time
        probe['coor'] = dproc.get_probe_info(f, probename)[0]
        probe['desc'] = dproc.get_probe_description(f, probename)

    args = (subarray[0] for subarray in np.split(data, 7))
    data_cons = dproc.prim_to_cons(*args)  # Convert to conservative variables
    probe['mk'] = data_dict['rho']
    probe['uk'] = data_dict['u']
    probe['ek'] = data_dict['e_11']
    probe['qdmk'] = data_cons[1]
    probe['Ek'] = data_cons[3]
    probe['nk'] = data_dict['nk']
    probe['k'] = k
    probe['legend'] = solname
    return probe


def load_ref_probe(ref, coor, varnames):
    ref_probe = {'xlabel': 't [s]'}

    def find_nearest(array, value):
        return (np.abs(array - value)).argmin()

    x = get_ref_x()
    coor_index = find_nearest(x, coor)
    for var in varnames:
        ref_probe[var] = ref[var][:, coor_index]
    ref_probe['x'] = ref['x'][:]
    return ref_probe


def create_dir(directory, remove=False):
    if os.path.exists(directory):
        if remove:
            shutil.rmtree(directory)
            os.makedirs(directory)
    else:
        os.makedirs(directory)


def is_equal(a, b):
    if a != 0.:
        err = abs((a - b)/a)
    elif b != 0.:
        err = abs((a - b)/b)
    else:
        err = abs((a - b))
    return err < 1e-8


def compute_snap_error(ref_name, sim_name, snap_time, norm, k='sum', trim=0):
    """Return an error dict for given snapshot time of given simulation"""
    error_L2 = {}

    with dproc.open_h5(h5path=sim_name) as f:
        index, ite = dproc.get_ite(f, snap_time, get_index=True)
        N = dproc.get_param(f, 'N')
        skip = N/100
        start = skip//2
        end = start + skip*N
        sim_time = dproc.get_time_list(f)[index]

    if not is_equal(sim_time, snap_time):
        print "WARNING: snap_time = {} | sim_time = {}". \
              format(snap_time, sim_time)

    if k == 'sum':
        varnames = sum_totnames
        ref = load_sim(solname=ref_name, ite=ite)
        sim = load_sim(solname=sim_name, ite=ite)
    else:
        varnames = kvarnames
        ref = load_ksim(solname=ref_name, ite=ite, k=k)
        sim = load_ksim(solname=sim_name, ite=ite, k=k)

    for var in varnames:
        ref_var = ref[var][start:end:skip]
        sim_var = sim[var][start:end:skip]
        if trim != 0:
            ref_var = ref_var[trim:-trim]
            sim_var = sim_var[trim:-trim]
        n = len(ref_var)
        error_L2[var] = np.linalg.norm(sim_var - ref_var, ord=2)/norm[var] / \
            n**0.5
    return error_L2


def compute_error_k(errors_L2, error_time_list, ref_names, sim_names, k='sum'):
    """Compute error for given section (or sum)"""
    if k == 'sum':
        varnames = sum_totnames
        ref = load_sim(solname=ref_names[0], ite=0)
    else:
        varnames = kvarnames
        ref = load_ksim(solname=ref_names[0], ite=0, k=k)

    # Compute norm
    norm = {}
    n = len(ref[varnames[0]])
    for var in varnames:
        norm[var] = np.linalg.norm(ref[var], ord=2)/n**0.5

    for ref_name, sim_name in zip(ref_names, sim_names):
        if k == 'sum':
            legend = sim_name
        else:
            legend = "{} - k = {}".format(sim_name, k)
        # initialize error dict
        error_L2 = {'legend': legend}
        n_time = len(error_time_list)
        for var in varnames:
            error_L2[var] = np.zeros(n_time)
        error_L2['time'] = np.zeros(n_time)

        print "Compute error for {}".format(sim_name)
        for i, time in enumerate(error_time_list):
            snap_error_L2 = compute_snap_error(ref_name, sim_name, time,
                                               norm, k=k, trim=15)
            for var in varnames:
                error_L2[var][i] = snap_error_L2[var]
            error_L2['time'][i] = time
        errors_L2.append(error_L2)


def compute_error(ref_names, sim_names, error_time_list, nsect_out=0):
    """Compute L2 error for sum or sections"""
    sim_to_ref = dict(zip(sim_names, ref_names))
    figtitle = "Time evolution of L2-error normalized by the initial L2-norm"

    if nsect_out:
        for ref_nsect, sim_plot_names in NSECT_DICT.iteritems():
            errors_L2 = []
            kskip = ref_nsect // nsect_out
            sections = range(0, ref_nsect, kskip)
            figname = "{}/nsect_{}_error_L2_sections".format(OUTDIR, ref_nsect)
            for k in sections:
                print "Loading section-{}".format(k)
                ref_plot_names = [sim_to_ref[name] for name in sim_plot_names]
                compute_error_k(errors_L2, error_time_list, ref_plot_names,
                                sim_plot_names, k=k)
            plot_time_error(*errors_L2, varnames=kvarnames, title=figtitle,
                            figname=figname, figform=FIGFORM)

    else:
        errors_L2 = []
        print "Loading sum ref from {}:".format(REFDIR)
        varnames = sum_totnames
        figname = "{}/error_L2_sum".format(OUTDIR)
        compute_error_k(errors_L2, error_time_list, ref_names, sim_names,
                        k='sum')
        create_dir(OUTDIR)
        plot_time_error(*errors_L2, varnames=varnames, title=figtitle,
                        figname=figname, figform=FIGFORM)


def plot_time_error(*err_data, **kwargs):
    """plot error vs time"""
    loc_varnames = kwargs.pop('varnames', varnames)
    title = kwargs.pop('title', '')
    figname = kwargs.pop('figname', '')
    figform = kwargs.pop('figform', 'pdf')
    if figname:
        filename = "{}.{}".format(figname, figform)
        print "Plotting ", filename

    arr = np.arange(4).reshape(2, 2)  # array to locate subplots
    fig, axarr = plt.subplots(2, 2, sharex=True)

    figtitle = title
    fig.suptitle(figtitle, fontsize=14)

    flatiter = arr.flat  # flat iterator
    coor = flatiter.coords  # get coordinates of first element
    for i in flatiter:
        if i < len(loc_varnames):
            var = loc_varnames[i]
            for data in err_data:
                label = data.get('legend', 'no_legend')
                axarr[coor].plot(data['time'], data[var], label=label)
            axarr[coor].set_title(var)

        coor = flatiter.coords  # get next element's coordinates

    coor = arr.flat.coords  # reset flat iterator to add legend to first plot
    axarr[coor].legend(loc='upper right', frameon=False)
    axarr[1, 0].set_xlabel('t [s]', fontsize=12)
    axarr[1, 1].set_xlabel('t [s]', fontsize=12)

    if figname:
        fig.set_size_inches(15, 10)
        fig.savefig(filename, format=figform, dpi=150)
        plt.close(fig)
    else:
        plt.show()


def get_list(parser, param_name):
    """Return parameter value if defined in case file"""
    section = 'Description'
    try:
        param = parser.get(section, param_name).replace(' ', '').split(',')
    except(NoOptionError):
        param = None  # No parameter found
    return param


def get_param(*args):
    return get_list(*args)[0]


def get_float(*args):
    return float(get_param(*args))


def compute(verbose=False):

    global NSECT_OUT, OUTDIR

    plot_all = True
    plt.close('all')
    plt.ioff()

    parser = ConfigParser()
    parser.read('post.ini')

    OUTDIR = get_param(parser, 'OUTDIR')
    time_end = get_float(parser, 'time_end')
    dt_store = get_float(parser, 'dt_store')
    sim_names = get_list(parser, 'sim_names')
    do_plot = get_param(parser, 'do_plot')
    do_sum = get_param(parser, 'do_sum')
    do_probe = get_param(parser, 'do_probe')
    do_error = get_param(parser, 'do_error')

    print "Printing to output dir: " + OUTDIR
    create_dir(OUTDIR, remove=True)
    ref_names = [sim_name[:-3] + '_ref.h5' for sim_name in sim_names]
    sim_to_ref = dict(zip(sim_names, ref_names))

    # Get initial time
    with dproc.open_h5(h5path=ref_names[0]) as f:
        t0 = dproc.get_time(f, 0)

    time_start = t0
    print "time_start = {}".format(time_start)
    print "time_end = {}".format(time_end)
    print "dt_store = {}".format(dt_store)

    for sim_name in sim_names:
        with dproc.open_h5(h5path=sim_name) as f:
            nsect = dproc.get_param(f, 'nsect')
        if nsect == 0:
            nsect = 10  # Handle monodisperse case
            NSECT_OUT = 1
        if nsect in NSECT_DICT.keys():
            NSECT_DICT[nsect].append(sim_name)
        else:
            NSECT_DICT[nsect] = [sim_name]

    if do_sum and plot_all:
        print ">>> Computing space integrals"
        ref_sums = [load_sim_sum(solname=ref_name) for ref_name in ref_names]
        sim_sums = [load_sim_sum(solname=sim_name) for sim_name in sim_names]

        sum_title = "Time evolution of integral values"
        figname = OUTDIR + '/space_integrals'
        dproc.plot_ref_sim(ref_sums[0], *sim_sums, varnames=sum_totnames,
                           title=sum_title, figname=figname)

    if do_plot and plot_all:

        print ">>> Plotting x-profiles"
        # time_start = t0
        # time_end = 0.002
        # dt_store = 0.0002
        # dt_store = 5.e-4
        plot_time_list = np.arange(time_start, time_end, dt_store)

        for time in plot_time_list:

            ref_name = ref_names[0]
            with dproc.open_h5(h5path=ref_name) as f:
                ite = dproc.get_ite(f, time)
            ref = load_sim(solname=ref_name, ite=ite)

            sims = []
            for sim_name in sim_names:
                with dproc.open_h5(h5path=sim_name) as f:
                    ite = dproc.get_ite(f, time)
                sims.append(load_sim(solname=sim_name, ite=ite))

            for sim in sims:
                print "{} time = {}".format(sim['solname'], sim['t'])
            sum_title = 'Sum over sections - ite {} - '.format(ite)
            figname = '{}/xprofiles_sum_ite_{}'.format(OUTDIR, ite)
            dproc.plot_ref_sim(ref, *sims, varnames=varnames,
                               title=sum_title, figname=figname,
                               figform=FIGFORM)

            for ref_nsect, plot_names in NSECT_DICT.iteritems():
                kskip = ref_nsect // NSECT_OUT
                sections = range(0, ref_nsect, kskip)
                ref_name = sim_to_ref[plot_names[0]]
                with dproc.open_h5(h5path=ref_name) as f:
                    ite_ref = dproc.get_ite(f, time)
                for k in sections:
                    kref = load_ksim(solname=ref_name, ite=ite_ref, k=k)
                    ksims = []

                    for sim_name in plot_names:
                        with dproc.open_h5(h5path=sim_name) as f:
                            ite = dproc.get_ite(f, time)
                            sim_nsect = dproc.get_param(f, 'nsect')
                        if sim_nsect == sim_nsect:
                            ksims.append(load_ksim(solname=sim_name, ite=ite,
                                                   k=k))
                    k_title = 'Section {} - t = {} - '.format(k, time)
                    sectiondir = "{}/nsect_{}/section_{}".format(OUTDIR,
                                                                 ref_nsect, k)
                    create_dir(sectiondir)
                    figname = '{}/xprofiles_section_{}_t_{:.6f}' \
                              .format(sectiondir, k, time)
                    dproc.plot_ref_sim(kref, *ksims, varnames=kvarnames2,
                                       title=k_title, figname=figname,
                                       figform=FIGFORM)

    if do_error:
        # ---------------
        # Error computing
        # time_start = t0
        # time_end = 0.01
        # dt_store = 2.e-4
        error_time_list = np.arange(time_start, time_end, dt_store)
        # Error for sum over sections
        compute_error(ref_names, sim_names, error_time_list)
        # Error for sections
        compute_error(ref_names, sim_names, error_time_list,
                      nsect_out=NSECT_OUT)

    error = 0.
    # ---------------

    if do_probe and plot_all:
        probes = [load_sum_probe(solname=solname) for solname in sim_names]
        ref_probe = load_sum_probe(solname=ref_names[0])
        description = probes[0]['desc']
        sum_title = "Time evolution of probe {}".format(description)
        figname = OUTDIR + '/probe_sum'
        dproc.plot_ref_sim(ref_probe, *probes, varnames=varnames,
                           title=sum_title, figname=figname)

        for ref_nsect, plot_names in NSECT_DICT.iteritems():
            kskip = ref_nsect // NSECT_OUT
            sections = range(0, ref_nsect, kskip)
            ref_name = sim_to_ref[plot_names[0]]
            for k in sections:
                kref_probe = load_kprobe(solname=ref_name, k=k)
                kprobes = []
                for plot_name in plot_names:
                    kprobes.append(load_kprobe(solname=plot_name, k=k))
                k_title = 'Time evolution of probe {} for section {} - '. \
                    format(description, k)
                figname = '{}/nsect_{}_probe_section_{}'.format(OUTDIR,
                                                                ref_nsect, k)
                dproc.plot_ref_sim(kref_probe, *kprobes, varnames=kvarnames2,
                                   title=k_title, figname=figname)

    return error


def get_error_status(tol, verbose=False):
    """Compute error and return a status and warning message"""
    error = compute(verbose=verbose)
    error = tol
    return dproc.check(error, tol, verbose=verbose)
