"""
Evaporation and drag case: extension of evapdrag0D.py by F. Laurent
To postprocess, run:
./plot_sol.py --sol solution.h5 --var rho --probe probe_0 --ascii
"""

casetype = "evap&drag1D"  # Case type
scheme = 'relax-ag-o1'   # Scheme name

factor_Ngrid = 27  # <--- change this param only

# -----------------
# Compute time step
Smax = 11310.           # Max size [micro-m^2]
Rs = 1.99e-7            # Evaporation coefficient [m^2.s^-1]
Nt = 50                 # Number of time iterations
#dt_fixed = 2.e-5/factor_Ngrid
# -----------------

CFL = 1.0               # CFL number
CFL_evap = 0.2          # CFL number
tf = 0.01               # simulation final time
t0 = 0.0015              # simulation initial time
#maxite = 357
Lx = 0.07              # Domain length
N_base = 100  # DO NOT MODIFY
#xmin = - 0.07
#Lx = 0.14              # Domain length
#N_base = 200
N = int(N_base*factor_Ngrid)                 # Number of points
nsect = 25              # number of sections
tsm_order = 2           # TSM method order
gamma = 3.              # polytropic exponent

dt_store = 5.e-4           # Storage time interval
#ite_store = 1*factor_Ngrid           # Storage time interval
dt_plot = 0.0           # Plotting time interval
is2D = False            # 2D test case
isSecST = False         # ST for each system?
isAllST = True          # ST coupling all systems?
miniplot = True
sol_type = 'both'
# Add probes:
probe = []
#probe.append({'position': 0.02, 'comment': 'Gaussian initial center'})
solfile = 'solution_n{}_nsect{}.h5'.format(N, nsect)
# Error tolerance:
err_tol = 0.
#err_tol = {'mtot': 0.0018402540227463492,
#           'qdmtot': 0.0022860704632382206,
#           'Etot': 0.0045340681470737582,
#           'ntot': 0.0093343137328433749}
