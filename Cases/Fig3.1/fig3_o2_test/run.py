"""
1D test case corresponding to Fig. 3 of Boileau et al. SISC2015 paper
:cite:`Boileau:2015`.
"""

scheme = 'relax-isoh-o2'  # Scheme name
casetype = "SISC-fig3"    # Case name
epsi_e = 1.0e-10          # Energy threshold for PGD/Gas dynamics
CFL = 0.5                 # CFL number
#tf  = 0.1                 # simulation final time
tf  = 0.101777020013      # simulation final time
maxite = 1                # Max number of iterations
Lx  = 2.0                 # Domain length
N   = 160                 # Number of points
nsect = 0                 # number of sections
dt_store = 0.1            # Storage time interval
dt_plot = 0.0             # Plotting time interval
miniplot = True           # PLot first and last solution ?
err_tol = 0.              # Error tolerance
