# -*- coding: utf-8 -*-
"""
Compute relative error
Reference code is in: Relax/Fortran_main/
"""

import numpy as np
import sys
import matplotlib.pyplot as plt
from matplotlib import rc
from Relax import data_process as dproc
from Relax import plot_sol
import os
import pandas as pd

rc('figure.subplot', left=0.1, right=0.95, top=0.8, bottom=0.1666)

scalars = 'ntot', 'mtot'
others = 'qdmtot', 'Etot', 'time'
varplotnames = 'ntot', 'mtot', 'qdmtot', 'Etot'
vardict = {'qdmtot': 'Momentum',
           'mtot': 'Mass',
           'ntot': 'Number density',
           'Etot': 'Energy'}
FIGFORM = 'png'
OUTDIR = FIGFORM
LINES = "-", "--", ":", "-."


def read_panda(file_name):
    return pd.read_csv(file_name, delim_whitespace=True, header=None).values

read_data = read_panda


def load_ref(suffix='analytique'):
    ref = {}

    def get_file_name(var, suffix='analytique'):
        return "Ref/t_{}_{}.dat".format(var, suffix)

    # Get time only
    filename = get_file_name('ntot', suffix=suffix)
    ref['time'] = read_data(filename)[::2, 0]

    # Get other variable
    for var in scalars:
        # store 2nd column only
        ref[var] = read_data(get_file_name(var, suffix=suffix))[::2, 1]

    var = 'qdmtot'
    # store 2nd and 3rd columns
    ref[var] = read_data(get_file_name(var, suffix=suffix))[::2, 1:3]

    # store 2nd and 4rd columns
    ref['Etot'] = read_data(get_file_name('etot', suffix=suffix))[::2, 1:4]

    return ref


def load_kref(prefix='analytique', suffix='Ns0010', verbose=False):
    """Read reference data file and return dictionary"""
    ref = {}

    def get_file_name(var, suffix=suffix):
        return "Ref/S_{}_analytique_{}.dat".format(var, suffix)

    refnames = 'mk', 'uk1', 'uk2', 'sigmak11', 'sigmak12', 'sigmak22', 'nk'
    varnames = 'rho', 'u', 'v', 'e_11', 'e_12', 'e_22', 'nk'
    for ref_name, var_name in zip(refnames, varnames):
        file_name = get_file_name(ref_name, suffix=suffix)
        print("File: {}".format(file_name))
        ref[var_name] = read_data(file_name)[1::2, 1::2]

    return ref


def load_sim_ascii(ref):
    sim = {}
    sim_data = read_data('out/probe_0_sum_data.dat')
    time = sim_data[:, 0]

    # Use only common data
    commonlen = min(len(ref['time']), len(sim_data))
    sim_data = sim_data[:commonlen]
    sim['time'] = time[:commonlen]

    # Store simulation data in a dict
    sim['ntot'] = sim_data[:, 7]
    sim['mtot'] = sim_data[:, 1]
    sim['qdmtot'] = sim_data[:, 2:4]
    sim['Etot'] = sim_data[:, 4:7]

    return sim, commonlen


def load_sim_h5(ref):
    sim = {}
    with dproc.open_h5() as f:
        probename = 'probe_0'
        sim_data = dproc.get_probe_data(f, probename, 'sum_data')
        time = dproc.get_probe_data(f, probename, 'time')
        nsect = dproc.get_param(f, 'nsect')
        tsm_order = dproc.get_param(f, 'tsm_order')

    dt_sim = time[1] - time[0]
    dt_ref = ref['time'][1] - ref['time'][0]
    if dt_ref % dt_sim > 1e-8:
        msg = "dt_ref = {} is not a multiple of dt_sim = {}\n".format(dt_ref,
                                                                      dt_sim)
        msg = msg + "dt_ref/dt_sim = {}".format(dt_ref/dt_sim)
        sys.exit(msg)
    else:
        ite_skip = int(dt_ref/dt_sim)
        sim_data = sim_data[::ite_skip]
        time = time[::ite_skip]
        sim['ite_skip'] = ite_skip

    # Use only common data
    commonlen = min(len(ref['time']), len(sim_data))
    sim_data = sim_data[:commonlen]
    sim['time'] = time[:commonlen]

    # Store simulation data in a dict
    sim['ntot'] = sim_data[:, 6]
    sim['mtot'] = sim_data[:, 0]
    sim['qdmtot'] = sim_data[:, 1:3]
    sim['Etot'] = sim_data[:, 3:6]
    sim['nsect'] = nsect
    sim['tsm_order'] = tsm_order

    return sim, commonlen


def plot(ref, sim, title=''):
    """Plot sim against ref"""

    arr = np.arange(6).reshape(2, 3)  # array to locate subplots
    fig, axarr = plt.subplots(2, 3, sharex=True)
    fig.suptitle(title, fontsize=14)
    flatiter = arr.flat  # flat iterator
    coor = flatiter.coords  # get coordinates of first element
    for i in flatiter:
        if i < len(varplotnames):
            var = varplotnames[i]
            axarr[coor].plot(ref['time'], ref[var], marker='o', mfc='None',
                             linestyle='')
            axarr[coor].plot(ref['time'], sim[var])
            axarr[coor].set_title(var)

        coor = flatiter.coords  # get next element's coordinates


def kplot(kref, var, ite, time, ymax, ite_skip=1):
    """Use plot_sol methods to plot size distribution"""

    if var == 'uv':
        sol = plot_sol.Solution(varname='u', probename='probe_0',
                                ite_str=ite*ite_skip)
        sol_v = plot_sol.Solution(varname='u', probename='probe_0',
                                  ite_str=ite*ite_skip)
        sol.data.get_data()
        probe = sol.data.probe  # Reference to probe object
        probe.show = False
        sol_v.data.get_data()
        probe_v = sol_v.data.probe  # Reference to probe object
    else:
        sol = plot_sol.Solution(varname=var, probename='probe_0',
                                ite_str=ite*ite_skip)
        sol.data.get_data()
        probe = sol.data.probe  # Reference to probe object
        probe.show = False
    plt.close(probe.fig)  # Close useless figure
    probe.kplot()
    #title = "{}: size distribution for iteration {}".format(var, ite)
    title = "t = {:.3f}".format(time)
    probe.kfig.suptitle(title, fontsize=14)
    if var == 'uv':
        probe.kax.plot(probe.S, kref['u'][:, ite], marker='o', markersize=10,
                       mfc='None', linestyle='')

        data_v = probe_v.kvardict['v']
        probe.kax.plot(probe.S, data_v, '-', lw=1, mfc='k', markevery=1)
        probe.kax.plot(probe.S, kref['v'][:, ite], marker='o', markersize=10,
                       mfc='None', linestyle='')
        probe.kax.set_xlabel(r"Size $[\mu m^2]$")
    else:
        probe.kax.plot(probe.S, kref[var][:, ite], marker='o', markersize=10,
                       mfc='None', linestyle='')
    if ite == 0:
        ymax = probe.kax.get_ylim()[1]
        probe.kax.set_ylim((0., ymax))
    else:
        probe.kax.set_ylim((0., ymax))
    probe.kax.set_ylabel('')

    figname = "{}/{}_{}.{}".format(OUTDIR, var, ite, FIGFORM)
    probe.kfig.subplots_adjust(left=0.16, right=0.9, top=0.9, bottom=0.17)
    probe.kfig.set_size_inches(5, 4)
    probe.kfig.savefig(figname, format=FIGFORM, dpi=75)

    return ymax


def absolute_error(ref, sim):
    """Compute absolute error between ref and sim"""
    error = {}
    for var in scalars:
        error[var] = dproc.inf_err(sim[var], ref[var], norm=sim[var][0])

    norm_qdm = (sim['qdmtot'][0, 0]**2 + sim['qdmtot'][0, 1]**2)**0.5
    norm_E = norm_qdm**2/sim['mtot'][0]
    error['qdmtot'] = (dproc.inf_err(sim['qdmtot'][:, 0], ref['qdmtot'][:, 0],
                                     norm=norm_qdm)**2 +
                       dproc.inf_err(sim['qdmtot'][:, 1], ref['qdmtot'][:, 1],
                                     norm=norm_qdm)**2)**0.5

    err_E11 = dproc.inf_err(sim['Etot'][:, 0], ref['Etot'][:, 0], norm=norm_E)
    err_E12 = dproc.inf_err(sim['Etot'][:, 1], ref['Etot'][:, 1], norm=norm_E)
    err_E22 = dproc.inf_err(sim['Etot'][:, 2], ref['Etot'][:, 2], norm=norm_E)
    error['Etot'] = 0.5 * (err_E11 + err_E22 + ((err_E11 + err_E22)**2 +
                           4.*err_E12**2)**0.5)

    return error


def normalized_error(data1, data2, norm):
    return abs((data1 - data2)/norm)


def absolute_error_time(ref, sim):
    """Compute absolute error between ref and sim"""
    # Check time
    time_error = dproc.inf_err(ref['time'], sim['time'], norm=ref['time'][-1])
    if time_error > 1.e-8:
        print("WARNING! Time error: ", time_error)
    error = {}
    for var in scalars:
        error[var] = normalized_error(ref[var], sim[var], sim[var][0])

    norm_qdm = (sim['qdmtot'][0, 0]**2 + sim['qdmtot'][0, 1]**2)**0.5
    norm_E = norm_qdm**2/sim['mtot'][0]
    error['qdmtot'] = (normalized_error(ref['qdmtot'][:, 0],
                                        sim['qdmtot'][:, 0], norm_qdm)**2 +
                       normalized_error(ref['qdmtot'][:, 1],
                                        sim['qdmtot'][:, 1], norm_qdm)**2)**0.5

    err_E11 = normalized_error(sim['Etot'][:, 0], ref['Etot'][:, 0], norm_E)
    err_E12 = normalized_error(sim['Etot'][:, 1], ref['Etot'][:, 1], norm_E)
    err_E22 = normalized_error(sim['Etot'][:, 2], ref['Etot'][:, 2], norm_E)
    error['Etot'] = 0.5 * (err_E11 + err_E22 + ((err_E11 + err_E22)**2 +
                           4.*err_E12**2)**0.5)
    error['time'] = sim['time']
    return error


def compute(verbose=False):

    ref = load_ref()
#    ref_FL = load_ref(suffix='TSM_Ns0010_Nt0050')
    sim, commonlen = load_sim_h5(ref)

    for var in scalars + others:
        ref[var] = ref[var][:commonlen]
#        ref_FL[var] = ref_FL[var][:commonlen]

    do_plot = False and verbose
    if do_plot:
        from cycler import cycler
        plt.rc('axes', prop_cycle=(cycler('color', ['r', 'g', 'b', 'k']) +
                                   cycler('linestyle', ['-', '--', ':', '-.'])))

        plt.close('all')
        f = dproc.open_solution()
        ite_list = dproc.get_ite_list(f)

        dproc.create_dir(OUTDIR, erase=True)
        varlist = 'nk', 'rho', 'uv'
        ymax = dict(list(zip(varlist, np.zeros(len(varlist)))))
        for ite in ite_list:
            time = dproc.get_time(f, ite)
            # ----------------------
            # Plot size distribution
            # ----------------------
            # load analytical solution
            kref_ana = load_kref(prefix='analytique', verbose=verbose)
            # load numerical solution
            for var in 'nk', 'rho', 'uv':
                ymax[var] = kplot(kref_ana, var, ite, time, ymax[var],
                                  ite_skip=sim['ite_skip'])
        f.close()

    do_error_time = False
    if do_error_time:
        rundir_list = "evapdrag0D_n003_post", \
                      "evapdrag0D_n010_post", \
                      "evapdrag0D_n100_post", \
                      "evapdrag0D_osm"
        for var in varplotnames:
            plt.close('all')
            fig = plt.figure()
            ax = fig.add_subplot(111)
            for index, rundir in enumerate(rundir_list):
                os.chdir("../"+rundir)
                ref = load_ref()
                sim, commonlen = load_sim_h5(ref)
                for k in ref.keys():
                    ref[k] = ref[k][:commonlen]
                error_time = absolute_error_time(ref, sim)
                title = "{} normalized error".format(vardict[var])
                fig.suptitle(title, fontsize=16)
                if sim['tsm_order'] == 1:
                    method = "OSM"
                else:
                    method = "TSM"
                legend = "{} - {} sect.".format(method, sim['nsect'])
                ax.plot(error_time['time'][1:], error_time[var][1:],
                        lw=2, label=legend)
                ax.set_xlabel("t [s]", fontsize=16)
                ax.set_yscale("log")
                ax.legend(loc='lower right', frameon=False, fontsize=10)
                ax.set_xlim((0., 0.08))

                figname = "../evapdrag0D_n010/error_time_{}_{}.pdf". \
                    format(index, var)
                fig.set_size_inches(5, 4)
                fig.savefig(figname, format='pdf')

        # -----------------------
        # Plot temporal evolution
        # -----------------------
        #plot(ref, sim, title='sim vs ref')

        #plt.show()

    # Exit if time error is too high
    time_error = dproc.inf_err(sim['time'], ref['time'])
    if time_error > 1e-9:
        msg = "Time error is: {}".format(time_error)
        sys.exit(msg)

    error = absolute_error(ref, sim)
#    error_FL = absolute_error(ref_FL, sim)

    if verbose:
        for k, v in error.items():
            print("Error({}) = {}".format(k, v))
        print(error)
#        for k, v in error_FL.iteritems():
#            print "Error_FL({}) = {}".format(k, v)

    return error


def get_error_status(tol, tol_type, verbose=False):
    """Compute error and return a status and warning message"""
    error = compute(verbose=verbose)
    return dproc.check(error, tol, 'lower', verbose=verbose)
