program test
use TSM_aff_evap_drag

implicit none

double precision :: rpar(6)

double precision :: eps

!integer, parameter :: Nsec = 3, Nt = 15, Nvit = 2, Nene = 3
integer, parameter :: Nsec = 10, Nt = 50, Nvit = 2, Nene = 3
!integer, parameter :: Nsec = 100, Nt = 500, Nvit = 2, Nene = 3
integer :: k,i,j,Nmax
double precision :: t, dt, tmax, ug(Nvit), normug
double precision :: rmax, rhol
double precision :: dS,S(0:Nsec),Sp(0:Nsec)

double precision :: Sinta(Nsec),Sintb(Nsec)
double precision :: Sm52, Sm72

double precision :: nk(nsec),mk(nsec),uk(Nvit,nsec),sigmak(Nene,nsec)
double precision :: nkp(nsec),mkp(nsec),ukp(Nvit,nsec),sigmakp(Nene,nsec)

double precision :: n_an_tot(0:Nt), m_an_tot(0:Nt), qdm_an_tot(0:Nt,Nvit), e_an_tot(0:Nt,Nene)
double precision :: u_an_moy(0:Nt,Nvit), sig_an_moy(0:Nt,Nene)

double precision :: uk_an(0:Nt,Nvit,nsec), sigk_an(0:Nt,Nene,nsec)
double precision :: nk_an(0:Nt,nsec),mk_an(0:Nt,nsec),qdmk_an(0:Nt,Nvit,nsec),ek_an(0:Nt,Nene,nsec)

double precision :: nk_num(0:Nt,nsec),mk_num(0:Nt,nsec),qdmk_num(0:Nt,Nvit,nsec),ek_num(0:Nt,Nene,nsec)
double precision :: uk_num(0:Nt,Nvit,nsec), sigk_num(0:Nt,Nene,nsec)

double precision :: n_num_tot(0:Nt), m_num_tot(0:Nt), qdm_num_tot(0:Nt,Nvit), e_num_tot(0:Nt,Nene)
double precision :: u_num_moy(0:Nt,Nvit), sig_num_moy(0:Nt,Nene)
double precision :: erran(0:Nt), erram(0:Nt), errau(0:Nt,Nvit), erraq(0:Nt,Nvit), erras(0:Nt,Nene), errae(0:Nt,Nene)
double precision :: errn(0:Nt), errm(0:Nt), erru(0:Nt,Nvit), errq(0:Nt,Nvit), errs(0:Nt,Nene), erre(0:Nt,Nene), a, b, c

character :: nomt*4,nomsec*4
character nb_tag1*1,nb_tag2*2,nb_tag3*3

if(Nt.le.9)   then
   write (nb_tag1,'(I1)') Nt
   nomt = '000'//nb_tag1
else if (Nt.le.99) then
   write (nb_tag2,'(I2)') Nt
   nomt = '00'//nb_tag2
else if (Nt.le.999) then
   write (nb_tag3,'(I3)') Nt
   nomt = '0'//nb_tag3
else if (Nt.le.9999) then
   write (nomt,'(I4)') Nt
else
   write(6,*) 'attention, le nombre de pas de temps de doit pas depasser 9999',Nt
   stop
end if

if(Nsec.le.9)   then
   write (nb_tag1,'(I1)') Nsec
   nomsec = '000'//nb_tag1
else if (Nsec.le.99) then
   write (nb_tag2,'(I2)') Nsec
   nomsec = '00'//nb_tag2
else if (Nsec.le.999) then
   write (nb_tag3,'(I3)') Nsec
   nomsec = '0'//nb_tag3
else if (Nsec.le.9999) then
   write (nomsec,'(I4)') Nsec
else
   write(6,*) 'attention, le nombre de sections de doit pas depasser 9999',Nsec
   stop
end if

ug(1) = 4.d0                  ! vitesse direction 1
ug(2) = 2.d0                  ! vitesse direction 2
normug = sqrt(ug(1)**2+ug(2)**2) ! norme de ug
rhol = 634.239d0              ! densite du liquide (kg/m**3)
alpha_drag = .9680385e-6      ! alpha (m^2/s)
Rs = 1.99e-7                  ! coefficient d'evaporation Rs (m**2/s)
method_order = 2              ! 1: one-moment reconstruction
                              ! 2: two-moment reconstruction

smax = 11.310d3
ratio_mn = rhol/(6.d0*sqrt(pi))
tmax = smax/(Rs*1.d12)
dt = tmax/dble(Nt)
dS = smax/dble(Nsec)
do k = 0, Nsec
   S(k) = dble(k)*dS
end do
print*,'CFL evap',Rs*1.d12*dt/dS
print*,'alpha_drag/Rs',alpha_drag/Rs 

rpar(1) = 0.d0                  ! temps t
rpar(2) = Rs*1.d12              ! Rs - changement d'unite : m**2/s -> micron**2/s
rpar(3) = alpha_drag/Rs         ! alpha_drag/Rs -> sans unite
rpar(4) = ug(1)                  
rpar(5) = ug(2)                   

eps = 1.d-6                     ! parametre de convergence pour IntRomberg

nk_an   = 0.d0
mk_an   = 0.d0
qdmk_an = 0.d0
uk_an   = 0.d0
ek_an   = 0.d0
sigk_an = 0.d0
u_an_moy(:,1)   = ug(1)
u_an_moy(:,2)   = ug(2)
sig_an_moy = 0.d0

!Initialisation de nk,mk,uk,sigmak
call init_0d_ag2d(Nsec,S,eps,rpar,nk_an(0,:),mk_an(0,:),qdmk_an(0,:,:),&
				uk_an(0,:,:),ek_an(0,:,:),sigk_an(0,:,:))
n_an_tot(0)   = sum(nk_an(0,:))
m_an_tot(0)   = sum(mk_an(0,:))
do j = 1, Nvit
   qdm_an_tot(0,j) = sum(qdmk_an(0,j,:))
end do
do j = 1, Nene
   e_an_tot(0,j)   = sum(ek_an(0,j,:))
end do
if(m_an_tot(0)>0.d0) then
   u_an_moy(0,:)   = qdm_an_tot(0,:)/m_an_tot(0)
   sig_an_moy(0,1) = e_an_tot(0,1)/m_an_tot(0)-u_an_moy(0,1)**2
   sig_an_moy(0,2) = e_an_tot(0,2)/m_an_tot(0)-u_an_moy(0,1)*u_an_moy(0,2)
   sig_an_moy(0,3) = e_an_tot(0,3)/m_an_tot(0)-u_an_moy(0,2)**2
end if

!Ecriture de la solution analytique
do i=1,Nt-1                    ! Boucle temporelle
   t = dble(i)*dt
   rpar(1) = t
   !write(*,*) 'pas de temps analytique', i, '/', Nt

   Nmax = nbre_sections_non_vides(smax,Nsec,rpar)

   Sp(0:Nmax-1) = S(0:Nmax-1)
   if (Nmax.ge.1) then
      if (abs(S(Nsec)-rpar(1)*rpar(2)-S(Nmax-1)).lt.dS*1.d-5) then
         Nmax = Nmax - 1
      else
         Sp(Nmax) = S(Nsec)-rpar(1)*rpar(2)
      end if
   end if
   call init_0d_ag2d(Nmax,Sp(0:Nmax),eps,rpar,nk_an(i,1:Nmax),mk_an(i,1:Nmax),&
				qdmk_an(i,:,1:Nmax),uk_an(i,:,1:Nmax),ek_an(i,:,1:Nmax),sigk_an(i,:,1:Nmax))


   !masse, qdm et energie totales au pas de temps i
   n_an_tot(i)   = sum(nk_an(i,:))
   m_an_tot(i)   = sum(mk_an(i,:))
   do j = 1, Nvit
      qdm_an_tot(i,j) = sum(qdmk_an(i,j,:))
   end do
   do j = 1, Nene
      e_an_tot(i,j)   = sum(ek_an(i,j,:))
   end do
!   if(m_an_tot(i)>1.d-4*m_an_tot(0)) then
   if(m_an_tot(i)>0.d0) then
      u_an_moy(i,:)   = qdm_an_tot(i,:)/m_an_tot(i)
!      sig_an_moy(i,1) = e_an_tot(i,1)/m_an_tot(i)-u_an_moy(i,1)**2
!      sig_an_moy(i,2) = e_an_tot(i,2)/m_an_tot(i)-u_an_moy(i,1)*u_an_moy(i,2)
!      sig_an_moy(i,3) = e_an_tot(i,3)/m_an_tot(i)-u_an_moy(i,2)**2
      sig_an_moy(i,1) = (e_an_tot(i,1)*m_an_tot(i)-qdm_an_tot(i,1)**2)/m_an_tot(i)**2
      sig_an_moy(i,2) = (e_an_tot(i,2)*m_an_tot(i)-qdm_an_tot(i,1)*qdm_an_tot(i,2))/m_an_tot(i)**2
      sig_an_moy(i,3) = (e_an_tot(i,3)*m_an_tot(i)-qdm_an_tot(i,2)**2)/m_an_tot(i)**2
   end if

enddo

open(75,file='Result/t_ntot_analytique.dat',form='formatted')
open(70,file='Result/t_mtot_analytique.dat',form='formatted')
open(71,file='Result/t_qdmtot_analytique.dat',form='formatted')
open(72,file='Result/t_etot_analytique.dat',form='formatted')
open(73,file='Result/t_umoy_analytique.dat',form='formatted')
open(74,file='Result/t_sigmoy_analytique.dat',form='formatted')
do i = 0, Nt-2
   write(75,100) dble(i)*dt, n_an_tot(i)
   write(70,100) dble(i)*dt, m_an_tot(i)
   write(71,100) dble(i)*dt, qdm_an_tot(i,:)
   write(72,100) dble(i)*dt, e_an_tot(i,:)
   write(73,100) dble(i)*dt, u_an_moy(i,:)
   write(74,100) dble(i)*dt, sig_an_moy(i,:)
end do
close(70)
close(71)
close(72)
close(73)
close(74)
close(75)

open(71,file='Result/S_mk_analytique_Ns'//nomsec//'.dat',form='formatted')
open(72,file='Result/S_uk1_analytique_Ns'//nomsec//'.dat',form='formatted')
open(73,file='Result/S_uk2_analytique_Ns'//nomsec//'.dat',form='formatted')
open(74,file='Result/S_sigmak11_analytique_Ns'//nomsec//'.dat',form='formatted')
open(75,file='Result/S_sigmak12_analytique_Ns'//nomsec//'.dat',form='formatted')
open(76,file='Result/S_sigmak22_analytique_Ns'//nomsec//'.dat',form='formatted')
open(77,file='Result/S_nk_analytique_Ns'//nomsec//'.dat',form='formatted')

! Ecrire la masse, la qdm et l'energie a chaque pas de temps
do k=1,Nsec
   write(71,100) S(k-1), (mk_an(i,k),i=0,Nt+1)
   write(71,100) S(k),   (mk_an(i,k),i=0,Nt+1)
   write(72,100) S(k-1), (uk_an(i,1,k),i=0,Nt+1)
   write(72,100) S(k),   (uk_an(i,1,k),i=0,Nt+1)
   write(73,100) S(k-1), (uk_an(i,2,k),i=0,Nt+1)
   write(73,100) S(k),   (uk_an(i,2,k),i=0,Nt+1)
   write(74,100) S(k-1), (sigk_an(i,1,k),i=0,Nt+1)
   write(74,100) S(k),   (sigk_an(i,1,k),i=0,Nt+1)
   write(75,100) S(k-1), (sigk_an(i,2,k),i=0,Nt+1)
   write(75,100) S(k),   (sigk_an(i,2,k),i=0,Nt+1)
   write(76,100) S(k-1), (sigk_an(i,3,k),i=0,Nt+1)
   write(76,100) S(k),   (sigk_an(i,3,k),i=0,Nt+1)
   write(77,100) S(k-1), (nk_an(i,k),i=0,Nt+1)
   write(77,100) S(k),   (nk_an(i,k),i=0,Nt+1)
enddo

write(71,100) S(nsec), (0.d0,i=0,Nt+1)
write(72,100) S(nsec), (0.d0,i=0,Nt+1)
write(73,100) S(nsec), (0.d0,i=0,Nt+1)

close(71)
close(72)
close(73)
close(74)
close(75)
close(76)
close(77)

!simulation avec TSM
! initialisation
do k = 1, Nsec
   Sm52=(S(k)**2.5d0-S(k-1)**2.5d0)/2.5d0
   Sm72=(S(k)**3.5d0-S(k-1)**3.5d0)/3.5d0
   Sinta(k) = S(k)*Sm52-Sm72
   Sintb(k) = Sm72-S(k-1)*Sm52
end do
t=0.d0
sig_num_moy = 0.d0
u_num_moy(:,1)   = ug(1)
u_num_moy(:,2)   = ug(2)
nk_num(0,:) = nk_an(0,:)
mk_num(0,:) = mk_an(0,:)
qdmk_num(0,:,:) = qdmk_an(0,:,:)
uk_num(0,:,:)   = uk_an(0,:,:)
ek_num(0,:,:)   = ek_an(0,:,:)
sigk_num(0,:,:) = sigk_an(0,:,:)
m_num_tot(0)     = m_an_tot(0)
n_num_tot(0)     = n_an_tot(0)
u_num_moy(0,:)   = u_an_moy(0,:)
qdm_num_tot(0,:) = qdm_an_tot(0,:)
e_num_tot(0,:)   = e_an_tot(0,:)
sig_num_moy(0,:) = sig_an_moy(0,:)
nk = nk_num(0,:)
mk = mk_num(0,:)
uk = uk_num(0,:,:)
sigmak = sigk_num(0,:,:)

! evolution avec QKS
do i=1,Nt
   !write(*,*) 'pas de temps numerique', i, '/', Nt
   t=dble(i)*dt
   call evap_drag_aff(Nvit,Nene,nsec,S,Sinta,Sintb,dt,ug,nk,mk,uk,sigmak,nkp,mkp,ukp,sigmakp)
   nk = nkp
   mk = mkp
   uk = ukp
   sigmak = sigmakp
   !Ecriture des masse, qdm et energie section par section
   nk_num(i,:) = nk
   mk_num(i,:) = mk
   uk_num(i,:,:) = uk
   sigk_num(i,:,:) = sigmak
   do k=1,Nsec
      qdmk_num(i,:,k) = mk(k)*uk(:,k)
      ek_num(i,1,k) = mk(k)*(uk(1,k)**2.d0+sigmak(1,k))
      ek_num(i,2,k) = mk(k)*(uk(1,k)*uk(2,k)+sigmak(2,k))
      ek_num(i,3,k) = mk(k)*(uk(2,k)**2.d0+sigmak(3,k))
   enddo

   !Calcul des masse, qdm et energies totales
   n_num_tot(i) = sum(nk_num(i,:))
   m_num_tot(i) = sum(mk_num(i,:))
   do j = 1, Nvit
      qdm_num_tot(i,j) = sum(qdmk_num(i,j,:))
   end do
   do j = 1, Nene
      e_num_tot(i,j)   = sum(ek_num(i,j,:))
   end do
   !Calcul de u et sigma (section par section puis valeurs moyennes)
   if(m_num_tot(i)>0.d0) then
      u_num_moy(i,:)   = qdm_num_tot(i,:)/m_num_tot(i)
      sig_num_moy(i,1) = e_num_tot(i,1)/m_num_tot(i)-u_num_moy(i,1)**2
      sig_num_moy(i,2) = e_num_tot(i,2)/m_num_tot(i)-u_num_moy(i,1)*u_num_moy(i,2)
      sig_num_moy(i,3) = e_num_tot(i,3)/m_num_tot(i)-u_num_moy(i,2)**2
   end if
enddo

open(71,file='Result/S_mk_num_Ns'//nomsec//'.dat',form='formatted')
open(72,file='Result/S_uk1_num_Ns'//nomsec//'.dat',form='formatted')
open(73,file='Result/S_uk2_num_Ns'//nomsec//'.dat',form='formatted')
open(74,file='Result/S_sigmak11_num_Ns'//nomsec//'.dat',form='formatted')
open(75,file='Result/S_sigmak12_num_Ns'//nomsec//'.dat',form='formatted')
open(76,file='Result/S_sigmak22_num_Ns'//nomsec//'.dat',form='formatted')

! Ecrire la masse, la qdm et l'energie a chaque pas de temps
do k=1,Nsec
   write(71,100) S(k-1), (mk_num(i,k),i=0,Nt+1)
   write(71,100) S(k),   (mk_num(i,k),i=0,Nt+1)
   write(72,100) S(k-1), (uk_num(i,1,k),i=0,Nt+1)
   write(72,100) S(k),   (uk_num(i,1,k),i=0,Nt+1)
   write(73,100) S(k-1), (uk_num(i,2,k),i=0,Nt+1)
   write(73,100) S(k),   (uk_num(i,2,k),i=0,Nt+1)
   write(74,100) S(k-1), (sigk_num(i,1,k),i=0,Nt+1)
   write(74,100) S(k),   (sigk_num(i,1,k),i=0,Nt+1)
   write(75,100) S(k-1), (sigk_num(i,2,k),i=0,Nt+1)
   write(75,100) S(k),   (sigk_num(i,2,k),i=0,Nt+1)
   write(76,100) S(k-1), (sigk_num(i,3,k),i=0,Nt+1)
   write(76,100) S(k),   (sigk_num(i,3,k),i=0,Nt+1)
enddo

write(71,100) S(nsec), (0.d0,i=0,Nt+1)
write(72,100) S(nsec), (0.d0,i=0,Nt+1)
write(73,100) S(nsec), (0.d0,i=0,Nt+1)

close(71)
close(72)
close(73)
close(74)
close(75)
close(76)

! erreurs absolues normalisees
erran = 0.d0
erram = 0.d0
erraq = 0.d0
errau = 0.d0
errae = 0.d0
erras = 0.d0
do i = 0, Nt-1
      erran(i) = (n_num_tot(i)-n_an_tot(i))/n_an_tot(0)
      erram(i) = (m_num_tot(i)-m_an_tot(i))/m_an_tot(0)
      do k = 1, 2
         erraq(i,k) = (qdm_num_tot(i,k)-qdm_an_tot(i,k))/(normug*m_an_tot(0))
         errau(i,k) = (u_num_moy(i,k)-u_an_moy(i,k))/normug
      end do
      do k = 1, 3
         errae(i,k) = (e_num_tot(i,k)-e_an_tot(i,k))/(normug**2*m_an_tot(0))
         erras(i,k) = (sig_num_moy(i,k)-sig_an_moy(i,k))/normug**2
      end do
end do
where(abs(erran)<1.d-100) erran = 0.d0
where(abs(erram)<1.d-100) erram = 0.d0
where(abs(erraq)<1.d-100) erraq = 0.d0
where(abs(errau)<1.d-100) errau = 0.d0
where(abs(errae)<1.d-100) errae = 0.d0
where(abs(erras)<1.d-100) erras = 0.d0
! erreurs relatives
errn = 0.d0
errm = 0.d0
errq = 0.d0
erru = 0.d0
erre = 0.d0
errs = 0.d0
do i = 0, Nt-1
   if(m_an_tot(i)>1.d-3) then
      errn(i) = (n_num_tot(i)-n_an_tot(i))/n_an_tot(i)
      errm(i) = (m_num_tot(i)-m_an_tot(i))/m_an_tot(i)
      do k = 1, 2
         errq(i,k) = (qdm_num_tot(i,k)-qdm_an_tot(i,k))/qdm_an_tot(i,k)
         erru(i,k) = (u_num_moy(i,k)-u_an_moy(i,k))/u_an_moy(i,k)
      end do
      do k = 1, 3
         erre(i,k) = (e_num_tot(i,k)-e_an_tot(i,k))/e_an_tot(i,k)
         errs(i,k) = (sig_num_moy(i,k)-sig_an_moy(i,k))/sig_an_moy(i,k)
      end do
   end if
end do
where(abs(errn)<1.d-100) errn = 0.d0
where(abs(errm)<1.d-100) errm = 0.d0
where(abs(errq)<1.d-100) errq = 0.d0
where(abs(erru)<1.d-100) erru = 0.d0
where(abs(erre)<1.d-100) erre = 0.d0
where(abs(errs)<1.d-100) errs = 0.d0
open(75,file='Result/t_ntot_TSM_Ns'//nomsec//'_Nt'//nomt//'.dat',form='formatted')
open(70,file='Result/t_mtot_TSM_Ns'//nomsec//'_Nt'//nomt//'.dat',form='formatted')
open(71,file='Result/t_qdmtot_TSM_Ns'//nomsec//'_Nt'//nomt//'.dat',form='formatted')
open(72,file='Result/t_etot_TSM_Ns'//nomsec//'_Nt'//nomt//'.dat',form='formatted')
open(73,file='Result/t_umoy_TSM_Ns'//nomsec//'_Nt'//nomt//'.dat',form='formatted')
open(74,file='Result/t_sigmoy_TSM_Ns'//nomsec//'_Nt'//nomt//'.dat',form='formatted')
do i = 0, Nt-1
   write(75,100) dble(i)*dt, n_num_tot(i),erran(i),errn(i)
   write(70,100) dble(i)*dt, m_num_tot(i),erram(i),errm(i)
   write(71,100) dble(i)*dt, qdm_num_tot(i,:),erraq(i,:),errq(i,:)
   write(72,100) dble(i)*dt, e_num_tot(i,:),errae(i,:),erre(i,:)
   write(73,100) dble(i)*dt, u_num_moy(i,:),errau(i,:),erru(i,:)
   write(74,100) dble(i)*dt, sig_num_moy(i,:),erras(i,:),errs(i,:)
end do
close(70)
close(71)
close(72)
close(73)
close(74)
close(75)
print*,'absolu (normalise)'
print*,'erreur ntot',maxval((abs(erran(:))))
print*,'erreur mtot',maxval((abs(erram(:))))
print*,'erreur qdm ',(maxval(abs(erraq(:,k))),k=1,2),&
	sqrt(maxval(abs(erraq(:,1)))**2+maxval(abs(erraq(:,2)))**2)
print*,'erreur umoy',(maxval(abs(errau(:,k))),k=1,2)
a = maxval(abs(errae(:,1)))
b = maxval(abs(errae(:,2)))
c = maxval(abs(errae(:,3)))
print*,'erreur ener',(maxval(abs(errae(:,k))),k=1,3),0.5d0*(a+c+sqrt((a+c)**2+4.d0*b**2))
print*,'erreur sigm',(maxval(abs(erras(:,k))),k=1,3)
Nmax = int(Nt/2)
print*,'relatif, Nmax=',Nmax,dble(Nmax)*dt,m_an_tot(Nmax)/m_an_tot(0)
print*,'erreur ntot',maxval((abs(errn(0:Nmax))))
print*,'erreur mtot',maxval((abs(errm(0:Nmax))))
print*,'erreur qdm ',(maxval(abs(errq(0:Nmax,k))),k=1,2)
print*,'erreur umoy',(maxval(abs(erru(0:Nmax,k))),k=1,2)
print*,'erreur ener',(maxval(abs(erre(0:Nmax,k))),k=1,3)
print*,'erreur sigm',(maxval(abs(errs(0:Nmax,k))),k=1,3)


100 format (600(1x,1pe22.15)) 

contains
function nbre_sections_non_vides(Slim,Nsec,rpar)
   double precision, intent(in) :: rpar(*),Slim
   integer, intent(in) :: Nsec
   integer :: nbre_sections_non_vides

   nbre_sections_non_vides = Nsec-floor(rpar(1)*rpar(2)*Nsec/Slim)
   return
end function 

end
