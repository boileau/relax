"""
Evaporation and drag case: extension of evapdrag0D.py by F. Laurent
To postprocess, run:
./plot_sol.py --sol solution.h5 --var rho --probe probe_0 --ascii
"""

casetype = "evap&drag0D_FL"  # Case type
scheme = 'relax-ag-o1'   # Scheme name
CFL = 0.5               # CFL number
tf = 0.05               # simulation final time
maxite = 0              # simulation final time (O for time-limited)
Lx = 0.05               # Domain length
N = 1                   # Number of points
gamma = 3.              # polytropic exponent

nsect = 100             # number of sections
Smax = 11310.           # Max size [micro-m^2]
Rs = 1.99e-7            # Evaporation coefficient [m^2.s^-1]
# CFL condition for evaporation:
CFL_evap = 0.2*2  # (a factor 2 is used because of the strang splitting)

dt_store = 0.           # Storage time interval
dt_plot = 0.0           # Plotting time interval
is2D = False            # 2D test case
isSecST = False         # ST for each system?
isAllST = True          # ST coupling all systems?
miniplot = True
# Add probes:
probe = []
probe.append({'position': 0., 'comment': 'First point'})

# Error tolerance:
err_tol = {'mtot': 2.022910921e-07,
           'qdmtot': 0.00010421544607,
           'Etot': 0.000199015314247,
           'ntot': 1.285403685e-05}

