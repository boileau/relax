# -*- coding: utf-8 -*-
"""
Compute L2 error between simulation and analytical solution.
Warning: exclude expansion zone.
"""

import numpy as np
from Relax import data_process as dproc


# Default tolerance
tol = {'p_11': 9.8148926596929886,
       'p_12': 0.46300203466339879,
       'p_22': 1.6499507060086405,
       'u': 6.6229326137800992,
       'rho': 2.7511216042372042,
       'v': 1.1904471544346729}

varnames = 'rho', 'u', 'v', 'p_11', 'p_12', 'p_22'


def load_ref(filename):
    """Return a dict of reference data"""
    ref = {}
    path = "../Ref/" + filename
    ref_data = np.loadtxt(path)

    ref['x'] = ref_data[:, 0]/10.
    for i in range(len(varnames)):
        var = varnames[i]
        ref[var] = ref_data[:, i+1]

    return ref


def get_index(x_ref, x_sim):
    """return index in ref at x_sim position"""
    if x_sim <= x_ref[0]:  # Far-left region
        j = 0
    else:
        j = 1
        while x_ref[j] < x_sim:
            j += 1
            if j == len(x_ref):  # Far-right region
                j -= 1
                break
    return j


def build_err_ref(ref, sim):
    """
    return a dict of reference solution at simulation coordinates position.
    (Exclude expansion zone)
    """
    ref_err = {}

    # Remove expansion zone
    x_ref = np.concatenate((ref['x'][:2], ref['x'][-10:]))
    x_expans_start = ref['x'][1]
    x_expans_end = ref['x'][-10]
    for var in ref.keys():
        ref_err[var] = np.copy(sim[var])  # initialize ref_err
        if var != 'x':
            # Remove expansion zone
            filtered_ref = np.concatenate((ref[var][:2], ref[var][-10:]))
            for i in range(len(sim['x'])):
                x_sim = sim['x'][i]
                # Attribute reference value at local sim['x'][i] position
                if x_sim < x_expans_start or x_sim > x_expans_end:
                    ref_err[var][i] = filtered_ref[get_index(x_ref, x_sim)]

    return ref_err


def compute(sol_path='solution.h5', verbose=True):

    error = {}
    # Load and plot last solution
    ref = load_ref('analytical_solution.dat')
    sim = dproc.load_sim()
    sim['x'] -= 0.5  # Shift x sim data
    ref_err = build_err_ref(ref, sim)

    for var in varnames:
        error[var] = dproc.L2_err(sim[var], ref_err[var])

    if verbose:
        do_plot = False
        if do_plot:
            ref['t'] = sim['t']
            dproc.plot_ref_sim(sim, ref, varnames=varnames)
        for k, v in error.items():
            print("Error({}) = {}".format(k, v))

    return error


def get_error_status(tol, tol_type, verbose=True):
    """Compute error and return status"""
    error = compute(verbose=verbose)
    return dproc.check(error, tol, tol_type, verbose=verbose)
