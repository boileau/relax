"""
SOD test case from Berthon's test case
"""

scheme = 'relax-ag-o1'    # Scheme name
casetype = "SOD-AG"       # Case type
epsi_e = 1.0e-10          # Energy threshold for PGD/Gas dynamics
gamma = 3.0               # Polytropic coefficient
CFL = 0.5                 # CFL number
tf = 0.1                  # simulation final time
maxite = 0                # maximum number of iterations
Lx = 1.0                  # Domain length
N = 1000                  # Number of points
nsect = 0                 # number of sections
dt_store = 0.1            # Storage time interval
dt_plot = 0.0             # Plotting time interval
miniplot = True           # PLot first and last solution ?

# Error tolerance:
err_tol = {'p_11': 0.015649795933767544,
           'p_12': 0.012422401721777679,
           'p_22': 0.012861503483354544,
           'u': 0.036254455295485495,
           'rho': 0.022153926341371713,
           'v': 0.037290360380881994}
