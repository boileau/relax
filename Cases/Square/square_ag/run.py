"""
SOD test case from Berthon's test case
"""

scheme = 'relax-ag-o1'    # Scheme name
casetype = "square"       # Case type
epsi_e = 1.0e-10          # Energy threshold for PGD/Gas dynamics
gamma = 3.0               # Polytropic coefficient
CFL = 0.5                 # CFL number
tf = 1.0                  # simulation final time
maxite = 0                # maximum number of iterations
Lx = 1.0                  # Domain length
N = 100                   # Number of points
nsect = 0                 # number of sections
dt_store = 0.1            # Storage time interval
dt_plot = 0.0             # Plotting time interval
miniplot = True           # PLot first and last solution ?
