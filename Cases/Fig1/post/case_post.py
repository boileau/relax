# -*- coding: utf-8 -*-
"""
Compute relative error
Reference code is in: Cases/AG_TSM/Code_JL
"""

import numpy as np
import matplotlib.pyplot as plt
from Relax import data_process as dproc

# Default tolerance
tol = {'u': 0.22958505632630541, 'rho': 0.87233060992846712}


def bouchut_u(x):
    """solution Bouchut's Pb1 for u at (x, t=0.5)"""
    if x <= -0.5:
        u = - 0.5
    elif x > -0.5 and x <= 0.2:
        u = 0.4
    elif x > 0.2 and x <= 0.6:
        u = 0.8 - 2.*x
    elif x > 0.6:
        u = -0.4
    return u


def bouchut_rho(x):
    """solution Bouchut's Pb1 for rho at (x, t=0.5)"""
    if x > -0.75 and x <= - 0.3:
        rho = 0.
    elif x > 0.2 and x < 0.6:
        rho = 1.
    else:
        rho = 0.5
    return rho


def compute(sol_path, verbose=True):

    ref = {}
    sim = {}
    error = {}

    with dproc.open_h5(sol_path) as f:  # Open solution.h5
        npts = dproc.get_param(f, 'N')  # get grid point number
        t, V = dproc.load_snapshot(f)  # Load last solution
    sim['rho'] = V[0]
    sim['u'] = V[1]

    # Compute reference data
    ref['x'] = np.linspace(-1.0, 1.0, num=npts)
    ref['rho'] = np.array([bouchut_rho(x) for x in ref['x']])
    ref['u'] = np.array([bouchut_u(x) for x in ref['x']])

    varnames = 'rho', 'u'
    for var in varnames:
        error[var] = dproc.L2_err(sim[var], ref[var], norm=ref[var].max())

    # Finally, let's plot the solutions
    if verbose:
        plt.close('all')
        fig, axarr = plt.subplots(len(ref)-1, sharex=True)

        axarr[0].plot(ref['x'], ref['u'], linewidth=1.5, color='b')
        axarr[0].plot(ref['x'], sim['u'], linewidth=1.5, color='k')
        axarr[0].set_ylabel('velocity')
        axarr[1].plot(ref['x'], ref['rho'], linewidth=1.5, color='b')
        axarr[1].plot(ref['x'], sim['rho'], linewidth=1.5, color='k')
        axarr[1].set_ylabel('rho')

        plt.show()

    if verbose:
        for k, v in error.items():
            print("Error({}) = {}".format(k, v))
            print(error)

    return t, error


def get_error_status(tol, tol_type, verbose=False, sol_path="solution.h5"):
    """Compute error and return a status and warning message"""

    status = "OK"
    msg = ""
    t, error = compute(sol_path, verbose=verbose)

    if t - 0.5 < 1e-3:  # Check simulation time
        status, msg = dproc.check(error, tol, 'lower', verbose=verbose)
    else:
        print("Simulation time must be close to 0.5")
        status = "KO"

    if verbose:
        print(status)

    return status, msg
