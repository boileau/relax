"""
1D test case corresponding to Fig. 1 of Boileau et al. SISC2015 paper
:cite:`Boileau:2015`.
"""

scheme = 'Bouchut-o2'   # Scheme name
casetype = "Bouchut"    # Case type
CFL = 0.5               # CFL number
tf = 0.5                # simulation final time
maxite = 0              # maximum number of iterations
Lx = 2.0                # Domain length
N = 80                  # Number of points
nsect = 0               # number of sections
dt_store = 0.0          # Storage time interval
dt_plot = 0.0           # Plotting time interval
miniplot = True         # PLot first and last solution ?

# Add probes:
probe = []
probe.append({'position': 0.1, 'comment': 'First point'})
probe.append({'position': (0.5), 'comment': 'Second point'})

# Error tolerance
#err_tol = {'u': 0.013179463797643447, 'rho': 0.056331751040856456}
err_tol = {'u': 0.013179463797643429, 'rho': 0.056331751040856422}
