"""A setuptools based setup module.

See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
from distutils import log
# To use a consistent encoding
from codecs import open
import imp
from os import path
from setuptools import setup, find_packages
from distutils.command.build_py import build_py
import subprocess

PACKAGE_NAME = 'Relax'


class BuildF2py(build_py):
    """Custom build command for f2py"""

    def run(self):
        build_py.run(self)
        self.announce("Compiling with f2py", level=log.INFO)

        command = ['make', '-C', PACKAGE_NAME]
        subprocess.check_call(command)

        # Get suffix of python module file name
        libsuffix = imp.get_suffixes()[0][0]
        target_file = path.join(PACKAGE_NAME, 'libfortran' + libsuffix)
        dest_dir = path.join(self.build_lib, PACKAGE_NAME)
        self.copy_file(target_file, dest_dir)


CLASSIFIERS = [
    "Development Status :: 3 - Alpha",
    "Intended Audience :: Science/Research",
    "Intended Audience :: Developers",
    "License :: OSI Approved :: BSD License",
    "Programming Language :: Fortran",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3.6",
    "Topic :: Software Development",
    "Topic :: Scientific/Engineering",
    "Operating System :: Microsoft :: Windows",
    "Operating System :: POSIX",
    "Operating System :: Unix",
    "Operating System :: MacOS"]

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, './README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name=PACKAGE_NAME,

    # Versions should comply with PEP440.  For a discussion on single-sourcing
    # the version across setup.py and the project code, see
    # https://packaging.python.org/en/latest/single_source_version.html
    version='2.0.1',

    description='A Python/Fortran Finite-volume code',
    long_description='Python-Fortran 1D/2D relaxation code for Eulerian spray'
                     'dynamics',

    # The project's main homepage.
    url='https://gitlab.centralesupelec.fr/mathsem2c/relax',

    # Author details
    author='Matthieu Boileau',
    author_email='matthieu.boileau@math.unistra.fr',

    # Choose your license
    license='BSD',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=CLASSIFIERS,

    # What does your project relate to?
    keywords='spray f2py relaxation',

    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    packages=find_packages(include=[PACKAGE_NAME]),

    # Alternatively, if you want to distribute just a my_module.py, uncomment
    # this:
    #   py_modules=["my_module"],

    # List run-time dependencies here.  These will be installed by pip when
    # your project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/requirements.html
    install_requires=['blessings',
                      'configparser',
                      'cycler',
                      'h5py',
                      'pandas',
                      'progressbar33',
                      'tabulate',
                      'matplotlib',
                      'charset_normalizer',  # To help f2py
                      'scipy',
                      ],

    # List additional groups of dependencies here (e.g. development
    # dependencies). You can install these using the following syntax,
    # for example:
    # $ pip install -e .[dev,test]
    extras_require={
    },

    # If there are data files included in your packages that need to be
    # installed, specify them here.  If using Python 2.6 or less, then these
    # have to be included in MANIFEST.in as well.
    package_data={
#        'sample': ['package_data.dat'],
    },

    # Although 'package_data' is the preferred approach, in some case you may
    # need to place data files outside of your packages. See:
    # http://docs.python.org/3.4/distutils/setupscript.html#installing-additional-files # noqa
    # In this case, 'data_file' will be installed into '<sys.prefix>/my_data'
#    data_files=[('Cases', ['Cases']), ('README.md', ['README.md'])],

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the target platform.
    entry_points={
        'console_scripts': [
          'relax=Relax.relax:main',
        ],
    },
    cmdclass={
        'build_py': BuildF2py,
    },
)
