a1=load("cas_test1_100.dat");
a2=load("cas_test1_300.dat" );
a3=load("cas_test1_1000.dat");

a= zeros(100,1);

name=["Density";"Velocity";"P_{1,1}";"P_{1,2}";"P_{2,2}"];
celld=cellstr(name)
Nn=[2,3,8,9,10];
for i=1:5
n=Nn(i);
a(1:50)=a1(1,n);
a(50:100)=a1(100,n);
plot(a1(:,1),a1(:,n),"linewidth",7,a2(:,1),a2(:,n),"linewidth",7,a3(:,1),a3(:,n),"linewidth",7,a1(:,1),a,"--","linewidth",7);
xlabel("x","fontsize",14);
ylabel(celld{i},"fontsize",14);
h=legend("200","300","1000","initial solution");
set(h,"fontsize",16)
set(gca,"fontsize",20)
fichiername=strcat(celld{i},"_Berthon.png");
print(fichiername,"-dpng");
end
