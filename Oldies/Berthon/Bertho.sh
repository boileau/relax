#!/bin/bash

paramlist=`ls cas_test*.py |cut -d "." -f1`

for param in $paramlist
do
  echo
  echo "------------------"
  echo "Run code for case: $param"
  echo "------------------"
  echo
  echo "Remove old data file ./Post/sol/$param.dat"
  rm -f ./Post/sol/$param.dat
  ../../main.py $param
  mv last.dat ./Post/sol/$param.dat
done
