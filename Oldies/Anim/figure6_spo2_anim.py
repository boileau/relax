pi=3.141593

gamma    = 1.4                  # coefficient polytropique
scheme   = 'Relaxationsp_o2'       # nom du schema
epsi_e=1.0e-10                   # epsillon d'energie limite avec / sans pression (pour le schema hybride uniquement)
CFL = 1.0                       # nombre de CFL
tf  = 1.1                      # temps final de la simulation
precision_machine=1.0e-16       # valeurs utilisee pour le filtrage (valeur en dessous de laquelle les arrondis ne sont plus negligeables)
Lx  = 1.0                       # Longueur du domaine
N   = 200                       # Nb de point de discretisation
cas_test = 13                   # Numero du cas
isanim = True                  # Utile pour l'enregistrement de videos
dt_anim = 1.5e-2
st=13/(8*pi)                    # Nbr de Stokes, utile si il y a une source
