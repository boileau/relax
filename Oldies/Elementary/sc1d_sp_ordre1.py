pi=3.141593

gamma    = 1.4                  # coefficient polytropique
scheme   = 'Relaxationsp'       # nom du schema
epsi_e=1.0e-10                   # epsillon d'energie limite avec / sans pression (pour le schema hybride uniquement)
CFL = 0.5                       # nombre de CFL
tf  = 0.2                   # temps final de la simulation
precision_machine=0.0 #1.0e-16       # valeurs utilisee pour le filtrage (valeur en dessous de laquelle les arrondis ne sont plus negligeables)
Lx  = 1.0                       # Longueur du domaine
N   = 100                       # Nb de point de discretisation
cas_test = 104                   # Numero du cas
isanim = False                  # Utile pour l'enregistrement de videos
st=13/(8*pi)                    # Nbr de Stokes, utile si il y a une source