pi=3.141593

gamma    = 1.4                  # coefficient polytropique
scheme   = 'Relaxationsp_o2'       # nom du schema
epsi_e=1.0e-5                   # epsilon d'energie limite avec / sans pression
CFL = 0.5                       # nombre de CFL
tf  = 1.                        # temps final de la simulation
precision_machine=1.0e-16       # valeurs utilisee pour le filtrage (valeur en dessous de laquelle les arrondis ne sont plus negligeables)
Lx  = 1.0                       # Longueur du domaine
N   = 80                       # Nb de point de discretisation
cas_test = 103                    # Numero du cas
isanim = False                  # Utile pour l'enregistrement de videos
st=10.0  #st=13/(8*pi)                    # Nbr de Stokes, utile si il y a une source
