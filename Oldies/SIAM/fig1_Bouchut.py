pi=3.141593

gamma    = 1.4                  # coefficient polytropique, inutile ici
scheme   = 'Bouchut_o2'       # nom du schema
epsi_e=1.0e-10                   # epsilon d'energie limite avec / sans pression, inutile ici
CFL = 0.5 #5./3.*0.5                       # nombre de CFL
tf  = 0.5                        # temps final de la simulation
precision_machine=1.0e-16       # valeurs utilisee pour le filtrage (valeur en dessous de laquelle les arrondis ne sont plus negligeables)
Lx  = 2.0                       # Longueur du domaine
N   = 80                       # Nb de point de discretisation
cas_test = 2                    # Numero du cas
isanim = False                  # Utile pour l'enregistrement de videos
st=13/(8*pi)                    # Nbr de Stokes, utile si il y a une source
