#! /usr/bin/env python
# Se lance avec le numero de la figure en argument faisant reference au fichier parametres (fig4c.py par ex.)

import os
import shutil
import commands
import subprocess                 # For issuing commands to the OS.
import StringIO
import time
import sys
from numpy import *
from math import *
from matplotlib import rc, ticker
from matplotlib.pyplot import figure, plot, draw, subplot, show, title, xlabel, ylabel, legend, grid, savefig, xlim, ylim, xticks, yticks, hold, close, axis, axes, annotate, ion, ioff, imshow, contourf, contour, colorbar, quiver
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from matplotlib.colors import LinearSegmentedColormap, Normalize, LogNorm
import matplotlib.cm as cm
import numpy as np

from loadfile import *

    
#------------------------------------
# Define paths and generic variables
#------------------------------------
# WARNING: does not work if pathes contain characters such as '(', ')',....
pwd      = commands.getoutput('pwd')
localpath= './'
path     = str(pwd+'/'+localpath)
filepref = 'solution'
#timepre  = ''
timesuf  = '.dat'
FigDir='fig'
OutDir=commands.getoutput('pwd')
os.chdir(str(OutDir))
os.system(str('rm -r '+FigDir))
os.system(str('mkdir '+FigDir))

#----------------------------------
# Define Default Figure Parameters
#----------------------------------
#rc('text', usetex=True)
rc('font',family='sans-serif', style='normal')  # 5 values: 'serif' (Times), 'sans-serif' (Helvetica), 'cursive' (Zapf-Chancery), 'fantasy' (Western), and 'monospace' (Courier)
rc('legend', fancybox=True, numpoints=1, fontsize='medium')
rc('figure', figsize=(14.0,10.0))       # predefined figure size, (use "figure(figsize=(8,6))" otherwise) 
rc('figure.subplot', left=0.0714, right=0.9286, top=0.9, bottom=0.1)  # define the figure workspace
rc('lines', antialiased=True, markersize=6.5)   # 'ms' can be also used instead of 'markersize'
rc('axes',  labelsize=13)           # predefined label font size (use "xlabels(fontsize=10)" otherwise)
rc('xtick', labelsize=12)           # predefined fontsize for x-axis ticks marks (use "xticks(size=10)" otherwise)
rc('ytick', labelsize=12)           # predefined fontsize for y-axis ticks marks (use "yticks(size=10)" otherwise)
rc('xtick.major', pad=10)
rc('ytick.major', pad=6)
rc('savefig', dpi=150)              # predefined dpi for saved figures (default value is set to 100dpi)
FigForm = "pdf"                     # format of saved figures
cbfraction = 0.15 # default: 0.15
cbshrink   = 0.8
cbformat = '%.2f'
cborientation='vertical'
cbticks=None
iquiver=False
colorlevels='auto' 
isavefig=True

FigName=str(sys.argv[1])
print FigName

import declare_fig

ParamFile=declare_fig.FigParam(FigName)

module = __import__(ParamFile, globals(), locals(), ['*'])
for k in dir(module):
    locals()[k] = getattr(module, k)

DataFile=SolPrefix+FigName+".dat"    # Name of sol file in ./sol/directory
    

#----------------------------------
# Generates the list of files to analyze
#----------------------------------
TimeList=[]


if (isanim):
    path     = path+SolPrefix+'/'
    List     = commands.getoutput(str('find '+path+filepref+'*'))
    LineList = List.splitlines()
    
    for line in LineList:
        # find the time associated to the current file
        indBeg    = line.rfind(filepref)+len(filepref)
        indEnd    = line.rfind(timesuf)
        Ite       = line[indBeg:indEnd]

        # fill a dictionary and a time vector
        TimeList.append(int(Ite))
else:
    filepref  = DataFile
    List      = commands.getoutput(str('find '+path+filepref))
    LineList  = List.splitlines()
    TimeList.append(0)

Anim = "solution"
 

#------------------
# Main Procedure
#------------------

ion()
AnimCpt=0
for ifile in TimeList:

  fig = figure()
# read data
  line=LineList[ifile]
  dataf = loadfile(str(line),comments=True,convert=False)
  data=dataf[0]
  header=dataf[1]

# Get time of current solution
  time0=header.find('# Time t =') + 11
  time1=header.find('#',time0)
  Time=float(header[time0:time1])
  print "Time t = %.4f" % Time

# Reorder data
# For square domain only
  nx=int(sqrt(data.shape[0]))
  rho     = zeros((nx, nx))
  u       = zeros((nx, nx))
  v       = zeros((nx, nx))
  e       = zeros((nx, nx))
  p       = zeros((nx, nx))
  energie = zeros((nx, nx))

# Reorder data in (x,y) matrix
  for j in xrange(nx):
    rho[0:nx,j]     = data[j*nx:(j+1)*nx,2]
    u[0:nx,j]       = data[j*nx:(j+1)*nx,3]
    v[0:nx,j]       = data[j*nx:(j+1)*nx,4]
    e[0:nx,j]       = data[j*nx:(j+1)*nx,5] 
    p[0:nx,j]       = data[j*nx:(j+1)*nx,6]
#    energie[0:nx,j] = data[j*nx:(j+1)*nx,7]

  x = data[:nx,1]

  if (plot_type=="multi"):

      subplt = fig.add_subplot(231,aspect='equal')
      contourf(x,x,rho,256)
      subplt.autoscale_view(True,True,True)
      axis([0,1,0,1])
      xlabel(r'$x$', labelpad=6)
      ylabel(r'$y$', labelpad=12)
      xticks( arange(0, 1, 0.2),'')
      title('Density',fontsize=12)
      colorbar(fraction=cbfraction,
                 shrink=cbshrink,
                 format=cbformat)
      
      subplt = fig.add_subplot(232,aspect='equal')
      contourf(x,x,u,256)
      subplt.autoscale_view(True,True,True)
      axis([0,1,0,1])
      xticks( arange(0, 1, 0.2),'')
      yticks( arange(0, 1, 0.2),'')
      title('Horizontal velocity',fontsize=12)
      colorbar(fraction=cbfraction,
                 shrink=cbshrink,
                 format=cbformat)
    
      subplt = fig.add_subplot(233,aspect='equal')
      contourf(x,x,v,256)
      subplt.autoscale_view(True,True,True)
      axis([0,1,0,1])
      xticks( arange(0, 1, 0.2),'')
      yticks( arange(0, 1, 0.2),'')
      title('Vertical velocity',fontsize=12)
      colorbar(fraction=cbfraction,
                 shrink=cbshrink,
                 format=cbformat)  
      
      subplt = fig.add_subplot(234,aspect='equal')
      contourf(x,x,e,256)
      subplt.autoscale_view(True,True,True)
      axis([0,1,0,1])
      xlabel(r'$x$', labelpad=6)
      ylabel(r'$y$', labelpad=12)
      title('Internal energy',fontsize=12)
      colorbar(fraction=cbfraction,
                 shrink=cbshrink,
                 format=cbformat)
      
      subplt = fig.add_subplot(235,aspect='equal')
      contourf(x,x,p,256)
      subplt.autoscale_view(True,True,True)
      axis([0,1,0,1])
      xlabel(r'$x$', labelpad=6)
      yticks( arange(0, 1, 0.2),'')
      title('Pressure',fontsize=12)
      colorbar(fraction=cbfraction,
                 shrink=cbshrink,
                 format=cbformat)
    
      subplt = fig.add_subplot(236,aspect='equal')
      contourf(x,x,energie,256)
      subplt.autoscale_view(True,True,True)
      axis([0,1,0,1])
      xlabel(r'$x$', labelpad=6)
      yticks( arange(0, 1, 0.2),'')
      title('Sheme type',fontsize=12)
      colorbar(fraction=cbfraction,
                 shrink=cbshrink,
                 format=cbformat)
    #  draw()
     # close()
      #time.sleep(1)
      AnimCpt=AnimCpt+1
      AnimInd="%03d" % (AnimCpt)
      AnimName="%s/%s/%s_%s.%s" % (OutDir,FigDir,Anim,AnimInd,FigForm)
      print str('... saving "'+AnimName+'" ...')

      fig.suptitle(str('Time $t =$ %.4f' % Time), fontsize=14)
      savefig(AnimName, format=FigForm)

  else:
    subplt = fig.add_subplot(111,aspect='equal')
    if (autoscale==False):
        contour(x,x,rho,nisolevels,colors='k',linewidths=0.4,levels=isolevels)
        contourf(x,x,rho,256,cmap = cm.Greys,levels=colorlevels,extend='max')

# For Fig6 p only:
#        p = ma.masked_where(p<= 0, p)
#        contour(x,x,p,nisolevels,colors='k',linewidths=0.4,levels=isolevels,locator=ticker.LogLocator())
#        cf=contourf(x,x,p,cmap=cm.Greys,levels=colorlevels,locator=ticker.LogLocator())

#        cf=contourf(x,x,np.log(p),256,cmap = cm.Greys,extend='both')
#        contour(x,x,e,nisolevels,colors='k',linestyles='solid',linewidths=0.25,levels=[0])
#        if (colorlevels!='auto'):
#            cf=contourf(x,x,e,256,cmap = cm.Greys,levels=colorlevels,extend='both')
#        else:
#        cf=contourf(x,x,e,256,cmap = cm.Greys,vmin=vmin,vmax=vmax,extend='both')#,antialiased=True)
#        for c in cf.collections:
#            c.set_linewidth( 0.1 )
#        contourf(x,x,p,256,cmap = cm.YlOrRd,levels=colorlevels)
    else:
        contour(x,x,rho,16,colors='k',linewidths=0.4)
        contourf(x,x,rho,256,cmap = cm.Greys)
            
    axis([0,1,0,1])
    xlabel(r'$x$', labelpad=6)
    ylabel(r'$y$', labelpad=12)

    if  (cborientation!='None'):  
        colorbar(fraction=cbfraction,
               shrink=cbshrink,
               format=cbformat,
               orientation=cborientation,
               ticks=cbticks)
               
        
    if (iquiver==True):
        quivskip=9
        quiver(x[::quivskip],x[::quivskip],u[::quivskip,::quivskip],v[::quivskip,::quivskip])
        
    if (isanim):
        AnimCpt=AnimCpt+1
        AnimInd="%03d" % (AnimCpt)
        AnimName="%s/%s/%s_%s.%s" % (OutDir,FigDir,Anim,AnimInd,FigForm)
    else:
        AnimName="%s/%s/%s.%s" % (OutDir,FigDir,FigName,FigForm)

    if (isavefig):
        print str('... saving "'+AnimName+'" ...')
        savefig(AnimName, format=FigForm)

      
if not (isanim):
  draw()
  show()
  ion()

#-----------------
# Build Animation
#-----------------
if (isanim):
    print """\n... Building Animation ...\n"""
    print """... searching 'mencoder' application ..."""
    not_found_msg = """
    >>> ERROR !!! 
    >>> The mencoder command was not found.
    >>> (mencoder is used by this script to make an avi file)
    >>> STOP !!!
    """
    
    try:
        subprocess.check_call(['mencoder'])
    except subprocess.CalledProcessError:
        print "... 'mencoder' command was found ..."
        pass # mencoder is found, but returns non-zero exit as expected
             # This is a quick and dirty check; it leaves some spurious output
             # for the user to puzzle over.
    except OSError:
        print not_found_msg
        sys.exit("quitting\n")
    
    AnimFile = '../'+FigDir+'.avi'
    os.chdir(str(OutDir))
#    os.system(str('rm -r '+AnimDir))
#    os.system(str('mkdir '+AnimDir))
    command = ('mencoder',
               str('mf://'+OutDir+'/'+FigDir+'/'+Anim+'*'),
               '-mf',
               'type=png:fps=15',
               '-ovc',
               'lavc',
               '-lavcopts',
               'vcodec=mpeg4',
               '-oac',
               'copy',
               '-o',
               AnimFile) 
    
    print "\n\nabout to execute:\n%s\n\n" % ' '.join(command)
    subprocess.check_call(command)
