import os
import commands

from matplotlib import rc
from matplotlib.pyplot import figure, plot, draw, subplot, show, title, xlabel, ylabel, legend, grid, savefig, xlim, ylim, xticks, yticks, hold, close, axis, axes, annotate, ion, ioff
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

#------------------------------------
# Define paths and generic variables
#------------------------------------
# WARNING: does not work if pathes contain characters such as '(', ')',....
pwd      = commands.getoutput('pwd')
localpath= '../'
path     = str(pwd+'/'+localpath)
filepref = 'solution'
#timepre  = ''
timesuf  = '.dat'

FigDir='fig'
OutDir=commands.getoutput('pwd')
os.chdir(str(OutDir))
os.system(str('rm -r '+FigDir))
os.system(str('mkdir '+FigDir))

#----------------------------------
# Define Default Figure Parameters
#----------------------------------
#rc('text', usetex=True)
rc('font',family='sans-serif', style='oblique')  # 5 values: 'serif' (Times), 'sans-serif' (Helvetica), 'cursive' (Zapf-Chancery), 'fantasy' (Western), and 'monospace' (Courier)
rc('legend', fancybox=True, numpoints=1, fontsize='medium')
rc('figure', figsize=(8,5.5))       # predefined figure size, (use "figure(figsize=(8,6))" otherwise) 
rc('figure.subplot', left=0.12, right=0.96, top=0.96, bottom=0.12)  # define the figure workspace
rc('lines', antialiased=True, markersize=6.5)   # 'ms' can be also used instead of 'markersize'
rc('axes',  labelsize=13)           # predefined label font size (use "xlabels(fontsize=10)" otherwise)
rc('xtick', labelsize=12)           # predefined fontsize for x-axis ticks marks (use "xticks(size=10)" otherwise)
rc('ytick', labelsize=12)           # predefined fontsize for y-axis ticks marks (use "yticks(size=10)" otherwise)
rc('xtick.major', pad=10)
rc('ytick.major', pad=6)
rc('savefig', dpi=150)               # predefined dpi for saved figures (default value is set to 100dpi)
FigForm = "png"                     # format of saved figures

