Les fichiers output du code "solution_*.dat" ou "last.dat" doivent etre
places dans ./sol et renommes en "fig_toto.dat" avec "toto" representant les
differentes figures.

>>>                    <<<
>>> Post-traitement 1D <<<
>>>                    <<<

Le but est de produire des figures 1D avec Grace (necessite d'avoir le
logiciel Grace installe sur la machine. Paquets d'installation disponibles sur
Linux et sur Mac via MacPort. http://plasma-gate.weizmann.ac.il/Grace/).

>>> Repertoire concerne : "./XMG"

>>> Utilisation

1. Copier les fichiers solution 1D de "./sol/" dans "./XMG/DAT/"
2. Lire la doc du fichier "./XMG/readme.txt". Dans "./XMG", lancer :
./xmg_var fig_toto
ou bien
./post_xmg
pour un post-traitement en serie.

>>> Post-traitement automatique du cas SIAM
Lancer simplement :
./2Dpost.sh


>>>                    <<<
>>> Post-traitement 2D <<<
>>>                    <<<

Le but est de produire des figures 2D avec Matplotlib.

>>> Fichiers concernes :
post2dp.py     -> script de post-traitement
declare_fig.py -> indique quelle figure recoit quel fichier parametre
fig*_par.py    -> jeu de parametres commun a plusieurs figures

>>> Pour generer une figure existante "fig_toto"
./post2Dp.py fig_toto
le fichier image ("fig_toto.png" ou "fig_toto.pdf") est ecrit dans ./2D

>>> Pour generer une nouvelle figure "fig_toto"
A part cas tres particuliers, on ne modifie que le fichier parametre et
non le script de post-traitement directement.
Procedure :
1. Soit on utilise un fichier parametre "fig*_par.py" existant, soit on en
cree un nouveau
2. On ajoute le nom de la figure "fig_toto" dans declare_fig.py a la ligne
correspondant au fichier fig*_par.
3. On lance :
./post2Dp.py fig_toto

Note :
"fig4vec_par.py" permet de tracer un champ de vecteur pour la representation des
tourbillons de Taylor-Green.

>>> Post-traitement automatique du cas SIAM
Lancer simplement :
./2Dpost.sh
