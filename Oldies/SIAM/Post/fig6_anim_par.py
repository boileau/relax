from matplotlib import rc
from numpy import arange
import commands
import os


isanim=True
plot_type="single"

rc('figure', figsize=(4.0,3.0))       # predefined figure size, (use "figure(figsize=(8,6))" otherwise) 
rc('figure.subplot', left=0.18, right=0.9, top=0.95, bottom=0.15)  # define the figure workspace
rc('axes',  labelsize=12)           # predefined label font size (use "xlabels(fontsize=10)" otherwise)
rc('xtick', labelsize=10)           # predefined fontsize for x-axis ticks marks (use "xticks(size=10)" otherwise)
rc('ytick', labelsize=10)           # predefined fontsize for y-axis ticks marks (use "yticks(size=10)" otherwise)

FigForm = "png"                     # format of saved figures

autoscale=False
# Useful if autoscale=False
vmax=1.5
nisolevels=8.
isolevels=arange(1./8.,vmax,vmax/nisolevels)
vmax=0.26
colorlevels=arange(1e-5,vmax,vmax/256.)   
#

# Go to output directory:
os.chdir(str('./'))

FigName = 'fig6_anim'
 
# Figures subdirectory in Output directory:
FigDir='Anim/'+FigName
# Get Output directory:
OutDir=commands.getoutput('pwd')

# Where solutions are stored:
SolPrefix="sol/"+FigName
    
