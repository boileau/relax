#! /usr/bin/env python

import os
import shutil
import commands
import subprocess                 # For issuing commands to the OS.
import StringIO
import time
import sys
import string as s
from numpy import *
from math import *
from matplotlib import rc
from matplotlib.pyplot import figure, plot, draw, subplot, show, title, xlabel, ylabel, legend, grid, savefig, xlim, ylim, xticks, yticks, hold, close, axis, axes, annotate, ion, ioff, imshow, contourf, contour
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

from loadfile import *


#------------------------------------
# Define paths and generic variables
#------------------------------------
# WARNING: does not work if pathes contain characters such as '(', ')',....
pwd      = commands.getoutput('pwd')
localpath= '../'
path     = str(pwd+'/'+localpath)
filepref = 'solution'
#timepre  = ''
timesuf  = '.dat'
FigDir='fig'
OutDir=commands.getoutput('pwd')
os.chdir(str(OutDir))
os.system(str('rm -r '+FigDir))
os.system(str('mkdir '+FigDir))

#----------------------------------
# Define Default Figure Parameters
#----------------------------------
#rc('text', usetex=True)
rc('font',family='sans-serif', style='normal')  # 5 values: 'serif' (Times), 'sans-serif' (Helvetica), 'cursive' (Zapf-Chancery), 'fantasy' (Western), and 'monospace' (Courier)
rc('legend', fancybox=True, numpoints=1, fontsize='medium')
rc('figure', figsize=(14.0,10.0))       # predefined figure size, (use "figure(figsize=(8,6))" otherwise) 
rc('figure.subplot', left=0.0714, right=0.9286, top=0.9, bottom=0.1)  # define the figure workspace
rc('lines', antialiased=True, markersize=6.5, color='black')   # 'ms' can be also used instead of 'markersize'
rc('axes',  labelsize=13)           # predefined label font size (use "xlabels(fontsize=10)" otherwise)
rc('xtick', labelsize=12)           # predefined fontsize for x-axis ticks marks (use "xticks(size=10)" otherwise)
rc('ytick', labelsize=12)           # predefined fontsize for y-axis ticks marks (use "yticks(size=10)" otherwise)
rc('xtick.major', pad=10)
rc('ytick.major', pad=6)
rc('savefig', dpi=150)              # predefined dpi for saved figures (default value is set to 100dpi)
FigForm = "png"                     # format of saved figures

#----------------------------------
# Generates the list of files to analyze
#----------------------------------
List      = commands.getoutput(str('find '+path+filepref+'*'))
LineList  = List.splitlines()

if len(sys.argv) == 1:
# 1D data with animation
  is2D       = False
  SingleSnap = False
else:
  if sys.argv[1] == '2D':
# 2D data
    print "Converting 2D to 1D"
    is2D     = True
    if len(sys.argv) == 3:
# 2D data with single plot
      print "Anim"
      TimeShow   = sys.argv[2]
      SingleSnap = True
    else:
# 2D data with animation
      print "Anim"
      SingleSnap = False
  else:
# 1D data with single plot
    is2D = False
    TimeShow   = sys.argv[1]
    SingleSnap = True

TimeList=[]
for line in LineList:
    # find the time associated to the current file
    indBeg    = line.rfind(filepref)+len(filepref)
    indEnd    = line.rfind(timesuf)
    Ite       = line[indBeg:indEnd]

    # fill a dictionary and a time vector
    TimeList.append(int(Ite))

if SingleSnap is True:
  TimeList=[int(TimeShow)]

Anim = "solution"

#------------------
# Main Procedure
#------------------

ion()
AnimCpt=0
for ifile in TimeList:

  fig = figure()
# read data
  line=LineList[ifile]
  dataf = loadfile(str(line),comments=True,convert=False)
  data=dataf[0]
  header=dataf[1]

# Get time of current solution
  time0=header.find('# Time t =') + 11
  time1=header.find('#',time0)
  Time=float(header[time0:time1])
  print "Time t = %.4f" % Time

# Reorder data
  if (is2D):
# For square domain only
    nx=int(sqrt(data.shape[0]))
    x = data[:nx,1]

    rho     = zeros(nx)
    u       = zeros(nx)
    v       = zeros(nx)
    e       = zeros(nx)
    p       = zeros(nx)
    energie = zeros(nx)

    rho     = data[:nx*nx:nx,2]
    u       = data[:nx*nx:nx,3]
    v       = data[:nx*nx:nx,4]
    e       = data[:nx*nx:nx,5]
    p       = data[:nx*nx:nx,6]
    energie = data[:nx*nx:nx,7]

  else:
    x       = data[:,0]
    rho     = data[:,1]
    u       = data[:,2]
    v       = data[:,3]
    e       = data[:,4]
    p       = data[:,5]
    energie = data[:,6]

  subplt = fig.add_subplot(231)
  plot(x,rho,'.',lw=1, mfc='k', markevery=1)
  axis([0,1,0,1.1])
  subplt.autoscale_view(True,False,True)
  xticks( arange(0, 1, 0.2),'')
  title('Density',fontsize=12)
  
  subplt = fig.add_subplot(232)
  plot(x,u,'.',lw=1, mfc='k', markevery=1)
  axis([0,1,0,1.3])
  subplt.autoscale_view(True,False,True)
  xticks( arange(0, 1, 0.2),'')
  title('Horizontal velocity',fontsize=12)
  
  subplt = fig.add_subplot(233)
  plot(x,v,'.',lw=1, mfc='k', markevery=1)
  axis([0,1,0,1])
  subplt.autoscale_view(True,False,True)
  xticks( arange(0, 1, 0.2),'')
  title('Vertical velocity',fontsize=12)
  
  subplt = fig.add_subplot(234)
  plot(x,e,'.',lw=1, mfc='k', markevery=1)
  axis([0,1,0,3])
  subplt.autoscale_view(True,False,True)
  xlabel(r'$x$', labelpad=6)
  title('Internal energy',fontsize=12)
  
  subplt = fig.add_subplot(235)
  plot(x,p,'.',lw=3, mfc='k', markevery=1)
  axis([0,1,0,1.2])
  subplt.autoscale_view(True,False,True)
  xlabel(r'$x$', labelpad=6)
  title('Pressure',fontsize=12)

  subplt = fig.add_subplot(236)
  plot(x,energie,'.',lw=3, mfc='k', markevery=1)
  axis([0,1,-0.1,1.1])
  subplt.autoscale_view(True,False,True)
  xlabel(r'$x$', labelpad=6)
  title('Sheme type',fontsize=12)

  AnimCpt=AnimCpt+1
  AnimInd="%03d" % (AnimCpt)
  AnimName="%s/%s/%s_%s.%s" % (OutDir,FigDir,Anim,AnimInd,FigForm)
  print str('... saving "'+AnimName+'" ...')

  fig.suptitle(str('Time $t =$ %.4f' % Time), fontsize=14)
  savefig(AnimName, format=FigForm)

  draw()
  #time.sleep(1)

if (SingleSnap):
  show()
  exit()

#-----------------
# Build Animation
#-----------------
print """\n... Building Animation ...\n"""
print """... searching 'mencoder' application ..."""
not_found_msg = """
>>> ERROR !!! 
>>> The mencoder command was not found.
>>> (mencoder is used by this script to make an avi file)
>>> STOP !!!
"""

try:
    subprocess.check_call(['mencoder'])
except subprocess.CalledProcessError:
    print "... 'mencoder' command was found ..."
    pass # mencoder is found, but returns non-zero exit as expected
         # This is a quick and dirty check; it leaves some spurious output
         # for the user to puzzle over.
except OSError:
    print not_found_msg
    sys.exit("quitting\n")

AnimDir='anim'
os.chdir(str(OutDir))
os.system(str('rm -r '+AnimDir))
os.system(str('mkdir '+AnimDir))
command = ('mencoder',
           str('mf://'+OutDir+'/'+FigDir+'/'+Anim+'*'),
           '-mf',
           'type=png:fps=2',
           '-ovc',
           'lavc',
           '-lavcopts',
           'vcodec=mpeg4',
           '-oac',
           'copy',
           '-o',
           str('./'+AnimDir+'/'+Anim+'.avi'))

print "\n\nabout to execute:\n%s\n\n" % ' '.join(command)
subprocess.check_call(command)