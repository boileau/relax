# Module to find parameter file for each type of figure

OutPrefix="Post/sol/"

def FigParam(FigName):

# Dico: key is the name of the .py parameter file, value is the list of corresponding figures
    dico_fig={\
    'fig4_par':['fig4b_o2', 'fig4b', 'fig4c_o2'],\
    'fig4vec_par':['fig4a'],\
    'fig5_par':["fig5a_o2","fig5a","fig5b_o2","fig5b","fig5c_o2","fig5c"],\
    'fig6_par':["fig6a1_o2","fig6a_o2","fig6a3_o2","fig6b1_o2","fig6b2_o2","fig6b3_o2","fig6b_o2","fig6c_o2","fig6d_o2"],\
    'fig6p_par':["fig6p1_o2","fig6p2_o2","fig6p3_o2"],\
    'fig6vec_par':["fig6_init"],\
    'fig6_anim_par':["fig6_anim"],\
    'fig6_hybride_anim_par':["fig6_hybride_anim"]\
    }

    ParamFile=""
    
    for k in dico_fig:
        if matchFig(FigName,dico_fig[k]):
            ParamFile=k

    if (ParamFile==""):
        print "Unknown figure name "+FigName+". Edit declare_fig.py."
        exit()     
    
    return ParamFile
    
def matchFig(FigName,FigParam):
    return [s for s in FigParam if FigName in s]