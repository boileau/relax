#! /usr/bin/env python

import os
import shutil
import commands
import subprocess                 # For issuing commands to the OS.
import StringIO
import time
import sys
from numpy import *
from math import *
from matplotlib import rc
from matplotlib.pyplot import figure, plot, draw, subplot, show, title, xlabel, ylabel, legend, grid, savefig, xlim, ylim, xticks, yticks, hold, close, axis, axes, annotate, ion, ioff, imshow, contourf, contour, colorbar
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
import matplotlib.cm as cm

from loadfile import *


#------------------------------------
# Define paths and generic variables
#------------------------------------
# WARNING: does not work if pathes contain characters such as '(', ')',....
pwd      = commands.getoutput('pwd')
localpath= '../'
path     = str(pwd+'/'+localpath)
filepref = 'solution'
#timepre  = ''
timesuf  = '.dat'
FigDir='fig'
OutDir=commands.getoutput('pwd')
os.chdir(str(OutDir))
os.system(str('rm -r '+FigDir))
os.system(str('mkdir '+FigDir))

#----------------------------------
# Define Default Figure Parameters
#----------------------------------
#rc('text', usetex=True)
rc('font',family='sans-serif', style='normal')  # 5 values: 'serif' (Times), 'sans-serif' (Helvetica), 'cursive' (Zapf-Chancery), 'fantasy' (Western), and 'monospace' (Courier)
rc('legend', fancybox=True, numpoints=1, fontsize='medium')
rc('figure', figsize=(14.0,10.0))       # predefined figure size, (use "figure(figsize=(8,6))" otherwise) 
rc('figure.subplot', left=0.0714, right=0.9286, top=0.9, bottom=0.1)  # define the figure workspace
rc('lines', antialiased=True, markersize=6.5)   # 'ms' can be also used instead of 'markersize'
rc('axes',  labelsize=13)           # predefined label font size (use "xlabels(fontsize=10)" otherwise)
rc('xtick', labelsize=12)           # predefined fontsize for x-axis ticks marks (use "xticks(size=10)" otherwise)
rc('ytick', labelsize=12)           # predefined fontsize for y-axis ticks marks (use "yticks(size=10)" otherwise)
rc('xtick.major', pad=10)
rc('ytick.major', pad=6)
rc('savefig', dpi=150)              # predefined dpi for saved figures (default value is set to 100dpi)
FigForm = "png"                     # format of saved figures
cbfraction = 0.15 # default: 0.15
cbshrink   = 0.8
cbformat = '%.2f'


#----------------------------------
# Generates the list of files to analyze
#----------------------------------
List      = commands.getoutput(str('find '+path+filepref+'*'))
LineList  = List.splitlines()

if len(sys.argv) > 1:
  plot_type=sys.argv[1]

  if len(sys.argv) > 2:
      TimeShow=sys.argv[2]
      SingleSnap=True
      print "SingleSnap"
  else:
      SingleSnap=False
      plot_type="multi"
else:
    plot_type="multi"

if (plot_type=="single"):
    rc('figure', figsize=(4.0,3.0))       # predefined figure size, (use "figure(figsize=(8,6))" otherwise) 
    rc('figure.subplot', left=0.18, right=0.9, top=0.95, bottom=0.15)  # define the figure workspace
  
TimeList=[]
for line in LineList:
    # find the time associated to the current file
    indBeg    = line.rfind(filepref)+len(filepref)
    indEnd    = line.rfind(timesuf)
    Ite       = line[indBeg:indEnd]

    # fill a dictionary and a time vector
    TimeList.append(int(Ite))

if SingleSnap:
  TimeList=[int(TimeShow)]

Anim = "solution"


#------------------
# Main Procedure
#------------------

ion()
AnimCpt=0
for ifile in TimeList:

  fig = figure()
# read data
  line=LineList[ifile]
  dataf = loadfile(str(line),comments=True,convert=False)
  data=dataf[0]
  header=dataf[1]

# Get time of current solution
  time0=header.find('# Time t =') + 11
  time1=header.find('#',time0)
  Time=float(header[time0:time1])
  print "Time t = %.4f" % Time

# Reorder data
# For square domain only
  nx=int(sqrt(data.shape[0]))
  rho     = zeros((nx, nx))
  u       = zeros((nx, nx))
  v       = zeros((nx, nx))
  e       = zeros((nx, nx))
  p       = zeros((nx, nx))
  energie = zeros((nx, nx))

# Reorder data in (x,y) matrix
  for j in xrange(nx):
    rho[0:nx,j]     = data[j*nx:(j+1)*nx,2]
    u[0:nx,j]       = data[j*nx:(j+1)*nx,3]
    v[0:nx,j]       = data[j*nx:(j+1)*nx,4]
    e[0:nx,j]       = data[j*nx:(j+1)*nx,5] 
    p[0:nx,j]       = data[j*nx:(j+1)*nx,6]
#    energie[0:nx,j] = data[j*nx:(j+1)*nx,7]

  x = data[:nx,1]

  if (plot_type=="multi"):

      subplt = fig.add_subplot(231,aspect='equal')
      contourf(x,x,rho,256)
      subplt.autoscale_view(True,True,True)
      axis([0,1,0,1])
      xlabel(r'$x$', labelpad=6)
      ylabel(r'$y$', labelpad=12)
      xticks( arange(0, 1, 0.2),'')
      title('Density',fontsize=12)
      colorbar(fraction=cbfraction,
                 shrink=cbshrink,
                 format=cbformat)
      
      subplt = fig.add_subplot(232,aspect='equal')
      contourf(x,x,u,256)
      subplt.autoscale_view(True,True,True)
      axis([0,1,0,1])
      xticks( arange(0, 1, 0.2),'')
      yticks( arange(0, 1, 0.2),'')
      title('Horizontal velocity',fontsize=12)
      colorbar(fraction=cbfraction,
                 shrink=cbshrink,
                 format=cbformat)
    
      subplt = fig.add_subplot(233,aspect='equal')
      contourf(x,x,v,256)
      subplt.autoscale_view(True,True,True)
      axis([0,1,0,1])
      xticks( arange(0, 1, 0.2),'')
      yticks( arange(0, 1, 0.2),'')
      title('Vertical velocity',fontsize=12)
      colorbar(fraction=cbfraction,
                 shrink=cbshrink,
                 format=cbformat)  
      
      subplt = fig.add_subplot(234,aspect='equal')
      contourf(x,x,e,256)
      subplt.autoscale_view(True,True,True)
      axis([0,1,0,1])
      xlabel(r'$x$', labelpad=6)
      ylabel(r'$y$', labelpad=12)
      title('Internal energy',fontsize=12)
      colorbar(fraction=cbfraction,
                 shrink=cbshrink,
                 format=cbformat)
      
      subplt = fig.add_subplot(235,aspect='equal')
      contourf(x,x,p,256)
      subplt.autoscale_view(True,True,True)
      axis([0,1,0,1])
      xlabel(r'$x$', labelpad=6)
      yticks( arange(0, 1, 0.2),'')
      title('Pressure',fontsize=12)
      colorbar(fraction=cbfraction,
                 shrink=cbshrink,
                 format=cbformat)
    
      subplt = fig.add_subplot(236,aspect='equal')
      contourf(x,x,energie,256)
      subplt.autoscale_view(True,True,True)
      axis([0,1,0,1])
      xlabel(r'$x$', labelpad=6)
      yticks( arange(0, 1, 0.2),'')
      title('Sheme type',fontsize=12)
      colorbar(fraction=cbfraction,
                 shrink=cbshrink,
                 format=cbformat)
    #  draw()
     # close()
      #time.sleep(1)
  else:
#      subplt = fig.add_subplot(231,aspect='equal')
    #  contourf(x,x,rho,256)
      subplt = fig.add_subplot(111,aspect='equal')
      contour(x,x,rho,16,colors='k',linewidths=0.4)
#      contourf(x,x,rho,256,cmap = cm.Greys)
      subplt.autoscale_view(True,True,True)
      axis([0,1,0,1])
      xlabel(r'$x$', labelpad=6)
      ylabel(r'$y$', labelpad=12)
#      xticks( arange(0, 1, 0.2),'')
 #     yticks( arange(0, 1, 0.2),'')
    #  title('Density',fontsize=12)
      colorbar(fraction=cbfraction,
                 shrink=cbshrink,
                 format=cbformat)
                 
  AnimCpt=AnimCpt+1
  AnimInd="%03d" % (AnimCpt)
  AnimName="%s/%s/%s_%s.%s" % (OutDir,FigDir,Anim,AnimInd,FigForm)
  print str('... saving "'+AnimName+'" ...')

#  fig.suptitle(str('Time $t =$ %.4f' % Time), fontsize=14)
  savefig(AnimName, format=FigForm)
      
if (SingleSnap):
  draw()
  show()
#  exit()

#-----------------
# Build Animation
#-----------------
if (SingleSnap==False):
    print """\n... Building Animation ...\n"""
    print """... searching 'mencoder' application ..."""
    not_found_msg = """
    >>> ERROR !!! 
    >>> The mencoder command was not found.
    >>> (mencoder is used by this script to make an avi file)
    >>> STOP !!!
    """
    
    try:
        subprocess.check_call(['mencoder'])
    except subprocess.CalledProcessError:
        print "... 'mencoder' command was found ..."
        pass # mencoder is found, but returns non-zero exit as expected
             # This is a quick and dirty check; it leaves some spurious output
             # for the user to puzzle over.
    except OSError:
        print not_found_msg
        sys.exit("quitting\n")
    
    AnimDir='anim'
    os.chdir(str(OutDir))
    os.system(str('rm -r '+AnimDir))
    os.system(str('mkdir '+AnimDir))
    command = ('mencoder',
               str('mf://'+OutDir+'/'+FigDir+'/'+Anim+'*'),
               '-mf',
               'type=png:fps=2',
               '-ovc',
               'lavc',
               '-lavcopts',
               'vcodec=mpeg4',
               '-oac',
               'copy',
               '-o',
               str('./'+AnimDir+'/'+Anim+'.avi'))
    
    print "\n\nabout to execute:\n%s\n\n" % ' '.join(command)
    subprocess.check_call(command)