from matplotlib import rc
from numpy import arange, linspace
import numpy as np
import commands
import os

def log_levels(vmin,vmax,nisolevels):
    lvmin = np.log10(vmin)
    lvmax = np.log10(vmax)
    levels_exp = np.arange(lvmin,lvmax,abs(lvmax-lvmin)/nisolevels)
    levels = np.power(10, levels_exp)
    return levels

isanim=False
plot_type="single"

rc('figure', figsize=(4.0,3.0))       # predefined figure size, (use "figure(figsize=(8,6))" otherwise) 
rc('figure.subplot', left=0.18, right=0.9, top=0.95, bottom=0.15)  # define the figure workspace
rc('axes',  labelsize=12)           # predefined label font size (use "xlabels(fontsize=10)" otherwise)
rc('xtick', labelsize=10)           # predefined fontsize for x-axis ticks marks (use "xticks(size=10)" otherwise)
rc('ytick', labelsize=10)           # predefined fontsize for y-axis ticks marks (use "yticks(size=10)" otherwise)

autoscale=False
# Useful if autoscale=False

nisolevels=16.

figp='p1'
if (figp=='p1'):
    vmin=1.e-6
    vmax=1.e-4
    cbformat = '%.2e'
    nisolevels=8.
elif (figp=='p2'):
    vmin=0.001
    vmax=0.1
    cbformat = '%.2e'
    nisolevels=8.
elif (figp=='p3'):
    vmin=0.006 
    vmax=0.5
    cbformat = '%.2e'
    nisolevels=8.

isolevels   = log_levels(vmin,vmax,nisolevels)
colorlevels = log_levels(vmin,vmax,256)   

cbfraction = 0.15 # default: 0.15
cbshrink   = 0.9
cborientation='vertical'
#cborientation='horizontal'

rc('savefig', dpi=300)              # predefined dpi for saved figures (default value is set to 100dpi)
FigForm = "png"                     # format of saved figures
isavefig=True

# Go to output directory:
os.chdir(str('./'))
# Figures subdirectory in Output directory:
FigDir='2D'
# Get Output directory:
OutDir=commands.getoutput('pwd')

# Where solutions are stored:
SolPrefix="sol/"

