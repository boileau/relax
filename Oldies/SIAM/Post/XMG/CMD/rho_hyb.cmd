HARDCOPY DEVICE "EPS"
DEVICE "EPS" OP "bbox:page"

define clipping
clipping=1e-15

READ BLOCK "./DAT/test_interaction_choc_deltachoc/Relaxation_hybride_ordre2_SOD_m_t0p1.dat"
BLOCK XY "1:2"
BLOCK XY "1:2"
BLOCK XY "1:4"
RESTRICT(S0, S2.y > clipping)
RESTRICT(S1, S2.y <= clipping)
kill S2

READ BLOCK "./DAT/test_interaction_choc_deltachoc/Relaxation_hybride_ordre2_SOD_m_t0p5.dat"
BLOCK XY "1:2"
BLOCK XY "1:2"
BLOCK XY "1:4"
RESTRICT(S2, S4.y > clipping)
RESTRICT(S3, S4.y <= clipping)
kill S4

READ BLOCK "./DAT/test_interaction_choc_deltachoc/Relaxation_hybride_ordre2_initilal.dat"
BLOCK XY "1:2"
