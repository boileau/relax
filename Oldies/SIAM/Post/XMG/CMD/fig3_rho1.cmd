HARDCOPY DEVICE "EPS"
DEVICE "EPS" OP "bbox:page"

READ BLOCK "./DAT/fig3_init.ref"
BLOCK XY "1:2"

READ BLOCK "./DAT/fig3_o2_t01.dat"
BLOCK XY "1:2"
BLOCK XY "1:2"
BLOCK XY "1:7"
RESTRICT(S1, S3.y > 0)
RESTRICT(S2, S3.y <= 0)
kill S3

READ BLOCK "./DAT/fig3_o2_t02.dat"
BLOCK XY "1:2"
BLOCK XY "1:2"
BLOCK XY "1:7"
RESTRICT(S3, S5.y > 0)
RESTRICT(S4, S5.y <= 0)
kill S5

