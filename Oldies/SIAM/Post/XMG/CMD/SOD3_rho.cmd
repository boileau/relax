HARDCOPY DEVICE "EPS"
DEVICE "EPS" OP "bbox:page"

define seuil
seuil=0.0

READ BLOCK "./DAT/fig2_o1.dat"
BLOCK XY "1:2"
BLOCK XY "1:2"
BLOCK XY "1:7"
RESTRICT(S0, S2.y > seuil)
RESTRICT(S1, S2.y <= seuil)
kill S2

READ BLOCK "./DAT/fig2_o2.dat"
BLOCK XY "1:2"
BLOCK XY "1:2"
BLOCK XY "1:7"
RESTRICT(S2, S4.y > seuil)
RESTRICT(S3, S4.y <= seuil)
kill S4

READ BLOCK "./DAT/fig2_exact.ref"
BLOCK XY "1:2"

