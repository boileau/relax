from matplotlib import rc
from numpy import arange, linspace
import commands
import os


isanim=False
plot_type="single"

rc('figure', figsize=(4.0,3.0))       # predefined figure size, (use "figure(figsize=(8,6))" otherwise) 
rc('figure.subplot', left=0.18, right=0.9, top=0.95, bottom=0.15)  # define the figure workspace
rc('axes',  labelsize=12)           # predefined label font size (use "xlabels(fontsize=10)" otherwise)
rc('xtick', labelsize=10)           # predefined fontsize for x-axis ticks marks (use "xticks(size=10)" otherwise)
rc('ytick', labelsize=10)           # predefined fontsize for y-axis ticks marks (use "yticks(size=10)" otherwise)

autoscale=False
# Useful if autoscale=False
vmax=1.5
nisolevels=16.
isolevels=arange(1./8.,vmax,vmax/nisolevels)
#vmax=2.
colorlevels=arange(0.,vmax,vmax/256.)   
#
# Colorbar:
cbfraction = 0.15 # default: 0.15
cbshrink   = 0.8
cbformat = '%.2f'
cbticks=linspace(0.,1.5,7,endpoint=True)
cborientation='horizontal'

rc('savefig', dpi=300)              # predefined dpi for saved figures (default value is set to 100dpi)
FigForm = "png"                     # format of saved figures
isavefig=True

# Go to output directory:
os.chdir(str('./'))
# Figures subdirectory in Output directory:
FigDir='2D'
# Get Output directory:
OutDir=commands.getoutput('pwd')

# Where solutions are stored:
SolPrefix="sol/"
