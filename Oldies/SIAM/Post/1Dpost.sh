#!/bin/bash

echo ">>> Go to 1D postproc directory XMG"
cd XMG

echo ">>> Clean old data files"
rm -f ./DAT/*.dat

echo ">>> Copy new data files from sol dir"
cp ../sol/fig1_*.dat ./DAT
cp ../sol/fig2_*.dat ./DAT
cp ../sol/fig3_*.dat ./DAT

echo ">>> Run xmgrace postproc"
./post_xmg
echo
echo ">>> 1D figures are stored in ./1D <<<"


