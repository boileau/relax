#!/bin/bash

echo ">>> Clean old 2D images"
rm -f ./2D/fig*

figlist="\
fig4b_o2 \
fig4c_o2 \
fig5a_o2 \
fig5b_o2 \
fig5c_o2 \
fig6a_o2 \
fig6b_o2 \
fig6c_o2 \
fig6d_o2 \
"

for fig in $figlist
do
  echo 
  echo ">>> Create image $fig"
  ./post2Dp.py $fig
done

echo ">>> 2D figures are stored in ./2D <<<"

