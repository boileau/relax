To run the test cases of the SIAM SISC 2013 paper, simply type:
./siam.sh

To post-process the data and print 1D and 2D figures:
cd Post
./1Dpost.sh
./2Dpost.sh

