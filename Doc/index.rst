.. Relax documentation master file, created by
   sphinx-quickstart on Tue Apr  7 12:47:55 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==========================================
Relax: a Python/Fortran Finite-volume code
==========================================

:Author: Matthieu Boileau
:Copyright: This document is property of EM2C laboratory.

Contents:

.. toctree::
   :maxdepth: 2

   installation
   execution
   test_cases
   postprocessing
   modules
   references

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
