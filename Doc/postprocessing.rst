Plotting the results
====================

Introduction
------------

`Matplotlib`_ is used to plot 1D or 2D results.

.. _Matplotlib: http://matplotlib.org/

At runtime
----------

The solution plotting is controlled by the main program ``relax.py`` using the
:py:mod:`data_plot` module following this rules:

* Initial and final solutions are always plotted except if ``miniplot = False`` in the input file.
* Plotting of intermediate time steps is handled by the ``dt_plot`` parameter in the input file (``dt_plot = 0.0`` means that no intermediate time step is plotted)


After simulation
----------------

The :py:mod:`plot_sol` module is called on the result ``.h5`` file using the
following syntax::

    ./plot_sol.py --sol "solution.h5"" --var rho

where the density ``rho`` is here the data to plot.