Test Cases
==========

Directory organization
----------------------

Test cases are grouped in ``./Cases/`` directory where one can find a set of ``Case_name`` directories
For example for ``Fig4`` directory::

    Fig4
    ├── case.ini
    ├── fig4b_o1
    │   ├── Ref
    │   │   └── solution_ref.h5
    │   └── run.py
    ├── fig4b_o2
    │   ├── Ref
    │   │   └── solution_ref.h5
    │   └── run.py
    └── fig4c_o2
        ├── Ref
        │   └── solution_ref.h5
        └── run.py

- ``fig4b_*`` directories are :ref:`run directories <run-directory>`.
- Each of them contains a ``Ref/solution_ref.h5`` which the reference solution file for calculating error in automatic test cases **(optional)**.
- The ``case.ini`` file describes how the test case must be run by the `auto_cases.py` script:

.. literalinclude:: ../Cases/Fig4/case.ini


.. _auto_cases.py:

Automatic test cases
--------------------

The ``Cases/auto_cases.py`` script launches a test case suite from a selection of test cases of ``Cases/`` directory::

    ./auto_cases.py -h
    usage: auto_cases.py [-h] [--case Case [Case ...]] [--plot]

    Run automatic test cases

    optional arguments:
      -h, --help            show this help message and exit
      --case Case [Case ...]
                            list of test cases
      --plot                Plot figures

For example for ``Fig4``, run::

    ./auto_cases.py --case Fig4

For all 1D cases, run::

    ./auto_cases.py --case 1D

From SISC2015 article
---------------------

The test cases are described in :cite:`Boileau:2015`.

From ICMF2016 article
---------------------

The test cases are described in :cite:`Boileau:2016`.
