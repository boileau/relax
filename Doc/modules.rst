Source code structure
=====================


.. contents:: Modules and content


Main program: ``relax.py``
--------------------------

The main program uses different modules to initialize, store, plot and iterate the solution.

.. literalinclude:: ../Relax/relax.py

General class instanciation graph
---------------------------------

.. graphviz::

    digraph example{
    Relax [label="Relax", href="modules.html#relax.Relax"];
    Simulation [label="Simulation", href="modules.html#simulation.Simulation"];
    Store [label="Store", href="modules.html#data_store.Store"];
    Plot [label="Plot", href="modules.html#data_plot.Plot"];
    MonoDisperse [label="MonoDisperse", href="modules.html#scheme.MonoDisperse"];
    PolyDisperse [label="PolyDisperse", href="modules.html#scheme.PolyDisperse"];
    Section [label="Section", href="modules.html#scheme.Section"];
    Bouchut [label="Bouchut", href="modules.html#scheme.Bouchut"];
    Relaxation [label="Relaxation", href="modules.html#scheme.Relaxation"];
    Relaxation_o2 [label="Relaxation_o2", href="modules.html#scheme.Relaxation_o2"];
    WithDrag [label="WithDrag", href="modules.html#scheme.WithDrag"];
    NoST [label="NoST", href="modules.html#scheme.NoST"];
    SizeTransport [label="SizeTransport", href="modules.html#scheme.SizeTransport"];

    Relax -> Simulation;
    Simulation -> MonoDisperse;
    Simulation -> PolyDisperse;
    Simulation -> Store;
    MonoDisperse ->  Section;
    PolyDisperse ->  Section;
    PolyDisperse ->  SizeTransport;
    PolyDisperse -> Plot;
    Section ->  Bouchut;
    Section ->  Relaxation;
    Section ->  Relaxation_o2;
    Section ->  WithDrag;
    Section ->  NoST;
    Section ->  Plot;
    }


Scheme module: ``scheme.py``
----------------------------

Description
~~~~~~~~~~~

.. automodule:: scheme

.. currentmodule:: scheme

.. autosummary::

    MonoDisperse
    PolyDisperse
    Section
    Transport
    Bouchut
    Relaxation
    Relaxation_o2
    SectionSourceTerm
    WithDrag
    SizeTransport

MonoDisperse and PolyDisperse classes
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Inheritance diagram**

.. inheritance-diagram:: MonoDisperse PolyDisperse

- According to the input parameter, either the :py:class:`MonoDisperse` or :py:class:`PolyDisperse` class is instanciated by :py:class:`Simulation`
- :py:class:`PolyDisperse` inherits from :py:class:`MonoDisperse`


.. autoclass:: MonoDisperse
    :members:

.. autoclass:: PolyDisperse
    :show-inheritance:
    :members:

Section class
^^^^^^^^^^^^^

This class is instanciated:

- once by :py:class:`MonoDisperse`
- as many times as n-sections by :py:class:`PolyDisperse`.

.. autoclass:: Section
    :members:


Transport class and inherited
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Inheritance diagram**

.. inheritance-diagram:: Bouchut Relaxation_o2 Relaxation 

Both :py:class:`Bouchut` and :py:class:`Relaxation` classes inherit from
master class :py:class:`Transport`.

.. autoclass:: Transport
    :members:

.. autoclass:: Bouchut
    :show-inheritance:
    :members:

.. autoclass:: Relaxation
    :show-inheritance:
    :members:

.. autoclass:: Relaxation_o2
    :show-inheritance:
    :members:


Single section source terms
^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Inheritance diagram**

.. inheritance-diagram:: WithDrag No_ST

Both :py:class:`WithDrag` and :py:class:`No_ST` classes inherit from
master class :py:class:`SectionSourceTerm`.

.. autoclass:: SectionSourceTerm
    :members:

.. autoclass:: WithDrag
    :show-inheritance:
    :members:

.. autoclass:: No_ST
    :show-inheritance:
    :members:

Transport in the phase space
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: SizeTransport
    :members:

Defining test cases: ``test_case.py``
-------------------------------------

.. automodule:: test_case
    :members:

Storing results: ``data_store.py``
----------------------------------

.. automodule:: data_store
    :members:

Plotting solution during execution: ``data_plot.py``
----------------------------------------------------

.. automodule:: data_plot
    :members:

**Inheritance diagram**

.. inheritance-diagram:: Plot1D Plot2D


Plotting results after execution: ``plot_sol.py``
-------------------------------------------------

.. automodule:: plot_sol

Content of ``plot_sol`` module
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. autosummary::

    plot_sol.Solution
    plot_sol.Data
    plot_sol.MonoDisperse
    plot_sol.PolyDisperse
    plot_sol.Probe
    plot_sol.PolyProbe
    plot_sol.Section

Solution class
~~~~~~~~~~~~~~

.. autoclass:: plot_sol.Solution
    :members:

Data class and inherited
~~~~~~~~~~~~~~~~~~~~~~~~

**Inheritance diagram**

.. inheritance-diagram:: plot_sol.MonoDisperse plot_sol.PolyDisperse

.. autoclass:: plot_sol.Data
    :members:

.. autoclass:: plot_sol.MonoDisperse
    :show-inheritance:
    :members:

.. autoclass:: plot_sol.PolyDisperse 
    :show-inheritance:
    :members:

Probe class and inherited
~~~~~~~~~~~~~~~~~~~~~~~~~

**Inheritance diagram**

.. inheritance-diagram:: plot_sol.PolyProbe

.. autoclass:: plot_sol.Probe
    :members:

.. autoclass:: plot_sol.PolyProbe
    :show-inheritance:
    :members:

Section class
~~~~~~~~~~~~~

.. autoclass:: plot_sol.Section
    :members:

