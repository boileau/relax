Running the code
----------------

Short help
~~~~~~~~~~
A short help is given by ``relax.py -h`` command::

     relax.py -h
     usage: relax.py [-h] [--batch] [--noplot] [RunDir]
     
     Run 1D/2D simulation
     
     positional arguments:
       RunDir      Directory of input file run.py
     
     optional arguments:
       -h, --help  show this help message and exit
       --batch     Run in batch (no plotting)
       --noplot    Display parameters but no plot

.. _run-directory:

Run directory
~~~~~~~~~~~~~

Examples are in::

    ./Cases/

A typical run directory is ``"Cases/Fig4/fig4b_o2/"``, which contains an input file called ``run.py``:

.. literalinclude:: ../Cases/Fig4/fig4b_o2/run.py


Here, the ``casetype`` keyword ``"Figure4"`` refers to the corresponding ``if`` statement in
``"test_case.py"``::

    [...]
    elif sim.casetype == "Figure4":
        # figure4 of SISC paper
        sim.is2D = True
        init2D(section)
        sincardinal(section.V,
                    sim.Xgrid,
                    centerx=0.625,
                    centery=0.625,
                    radius=0.125)

        sim.mu = 0.
        sim.mu = taylor(sim.Xgrid)
        sim.mu[2:5, :, :] = 0.
        sim.BCx = 'periodic'
        sim.BCy = 'periodic'
    [...]


Execute the python script
~~~~~~~~~~~~~~~~~~~~~~~~~

From the run directory, the syntax is::

    relax.py

From any directory, the syntax is::

    relax.py Cases/Fig4/fig4b_o2

The execution:

#. prints a summary of the simulation parameters in a form of a table.
#. opens a matplot figure to display initial solution - stores initial solution in ``solution.h5``
#. iterates over time until final time or maximum iteration number is reached - stores intermediate snapshots in ``solution.h5``
#. plots final solution - stores final solution in ``solution.h5``


