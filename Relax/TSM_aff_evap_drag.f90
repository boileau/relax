module TSM_aff_evap_drag
!$ use OMP_LIB

    implicit none

    double precision, parameter :: pi = 3.14159265358979324d0
    double precision, private, parameter :: u0 = 3.d0
    double precision, private, parameter :: epsi = 1.d-12
    double precision :: alpha_drag  ! drag coefficient in m^2/s
    double precision :: Rs  ! evaporation rate in m^2/s
    double precision :: ratio_mn
    logical :: novoid = .False.
    double precision :: Smax  ! Maximum section size [micro-m^2]
    integer :: method_order  ! 1: one-moment reconstruction
                             ! 2: two-moment reconstruction

contains

!-----------------
! For 0D test case
!-----------------

function phi(S)
    double precision, intent(in) :: S
    double precision :: phi

    if (S > 0.d0 .and. S < Smax) then
        phi = (1.d0/9.105e-09)*(1.d0/Smax)*(1.d0 + 8.d0*(S/Smax)) * &
        (1.d0 - (S/Smax))**2 * &
        dexp(0.001d0*(1.d0 - 1.d0/(1.d0 - (S/Smax))**2))
    else
        phi = 0.d0
    endif

    return
end function

function u_mean_without_ug(S)
    double precision, intent(in) :: S
    double precision :: u_mean_without_ug

    if (S > 0.d0 .and. S < Smax) then
    ! 2nd order polynome: large droplets are faster than small ones because
    ! they are less slowed down by the gas
        u_mean_without_ug = (S/Smax)*(2.d0 - (S/Smax))
    else
        u_mean_without_ug = 0.d0
    endif

   return
    return
end function

function sigma(S)
    double precision, intent(in) :: S
    double precision :: sigma

    if (S > 0.d0 .and. S < Smax) then
    ! 2nd order polynome: large droplets are faster than small ones because
    ! they are less slowed down by the gas
        sigma = (S/Smax)*(2.d0 - S/Smax)
    else
        sigma = 0.d0
    endif

    return
end function

function get_nk(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: get_nk
    double precision :: t,Rs

    t = rpar(1)
    Rs = rpar(2)
    get_nk = phi(S + Rs*t)

    return
end function

function get_mk(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: get_mk

    get_mk = S**1.5d0 * get_nk(S, rpar)

    return
end function

function get_mkuk(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: ug
    double precision :: get_mkuk

    ug = rpar(4)
    get_mkuk = S**1.5d0 * phi(S)*(ug + u_mean_without_ug(S))

    return
end function

function get_energy(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: ug
    double precision :: get_energy

    ug = rpar(4)
    get_energy = S**1.5d0 * phi(S)*((ug + u_mean_without_ug(S))**2 + sigma(S))

    return
end function

function get_mkuk1(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: t, Rs, ug, coef
    double precision :: get_mkuk1

    t = rpar(1)
    Rs = rpar(2)
    coef = rpar(3)
    ug = rpar(4)
    if (S == 0) then
        get_mkuk1 = 0.d0
    else
        get_mkuk1 = get_mk(S, rpar)*(ug + (1.d0 + Rs*t/S)**(-coef) * &
                    u_mean_without_ug(S + Rs*t))
    endif

    return
end function

function get_mkuk2(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: t, Rs, ug, coef
    double precision :: get_mkuk2

    t = rpar(1)
    Rs = rpar(2)
    coef = rpar(3)
    ug = rpar(5)
    if (S == 0) then
        get_mkuk2 = 0.d0
    else
        get_mkuk2 = get_mk(S, rpar)*(ug + 2.d0*(1.d0 + Rs*t/S)**(-coef) * &
                    u_mean_without_ug(S + Rs*t))
    endif

    return
end function

function get_energy11(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: t, Rs, ug, coef
    double precision :: get_energy11

    t = rpar(1)
    Rs = rpar(2)
    coef = rpar(3)
    ug = rpar(4)
    if (S == 0) then
        get_energy11 = 0.d0
    else
        get_energy11 = get_mk(S, rpar)*((ug + (1.d0 + Rs*t/S)**(-coef) * &
                       u_mean_without_ug(S + Rs*t))**2 + &
                       (1.d0 + Rs*t/S)**(-2.d0*coef) * (sigma(S + Rs*t)))
    endif

    return
end function

function get_energy12(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: t, Rs, ug1, ug2, coef
    double precision :: get_energy12

    t = rpar(1)
    Rs = rpar(2)
    coef = rpar(3)
    ug1 = rpar(4)
    ug2 = rpar(5)
    if (S == 0) then
        get_energy12 = 0.d0
    else
        get_energy12 = get_mk(S, rpar)*(ug2 + 2.d0*(1.d0 + Rs*t/S)**(-coef) * &
                       u_mean_without_ug(S + Rs*t)) * &
                       (ug1 + (1.d0 + Rs*t/S)**(-coef) * u_mean_without_ug(S + Rs*t))
    endif

    return
end function

function get_energy22(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: t, Rs, ug, coef
    double precision :: get_energy22

    t = rpar(1)
    Rs = rpar(2)
    coef = rpar(3)
    ug = rpar(5)
    if (S == 0) then
        get_energy22 = 0.d0
    else
        get_energy22 = get_mk(S, rpar)*((ug + 2.d0*(1.d0 + Rs*t/S)**(-coef) * &
                       u_mean_without_ug(S + Rs*t))**2 + &
                       2.d0*(1.d0 + Rs*t/S)**(-2.d0*coef) * (sigma(S + Rs*t)))
    endif

    return
end function


subroutine IntRomberg(fn, a, b, eps, area, rpar)
    ! Integration using Romberg's method
    ! Dominique Lefebvre, february 2007
    ! Inspired by A.Garcia's code in Numerical Methods for Physics
    !
    ! Modified on 30/08/09 to correct an error on precision estimation
    ! identified by Maelle Nodet (Universite de Grenoble)
    !
    ! a = inf integration limit
    ! b = sup integration limit
    ! eps = requested precision
    ! area = computed area
    double precision, intent(in) :: a, b, eps, rpar(*)
    double precision, intent(out) :: area
    integer, parameter :: Nmax = 25
    double precision :: h, xi, delta, sum, c, fotom, t(1000)
    integer :: pieces, i, nx(Nmax+1), nt, ii, n, nn, l, ntra, k, m, j, l2
    LOGICAL :: ended

    interface
        double precision function fn(S, rpar)
            double precision, intent(in) :: S, rpar(*)
        end function fn
    end interface

! Computing 1rst term
    h = b - a
    pieces = 1
    nx(1) = 1
    delta = h/pieces
    c = (fn(a, rpar) + fn(b, rpar))/2.d0
    sum = c
    t(1) = delta*c
    n = 1
    nn = 2

    ended = .false.
    do while (.not. ended .and. n < Nmax)
        n = n + 1
        fotom = 4.d0
        nx(n) = nn
        pieces = pieces*2
        l = pieces - 1
        l2 = (l + 1)/2
        delta = h/pieces

! Evaluation using trapezes method
        do ii = 1, l2
            i = ii*2 - 1
            xi = a + delta*i
            sum = sum + fn(xi, rpar)
        enddo
        t(nn) = sum*delta
        ntra = nx(n-1)
        k = n - 1

! Computing column n of table
        do m = 1, k
            j = nn + m
            nt = nx(n-1) + m - 1
            t(j) = (fotom * t(j-1) - t(nt))/(fotom - 1)
            fotom = fotom * 4
        enddo

! Estimation of precision
        if (n < 5) then
            nn = j + 1
        else if (t(nn+1) == 0.0d0) then
            nn = j + 1
        else
            if (abs(t(ntra+1) - t(nn+1)) <= abs(t(nn+1)*eps)) then
                ended = .true.
            elseif (abs(t(nn-1) - t(j)) <= abs(t(j)*eps)) then
                ended = .true.
            else
                nn = j+1
            endif
        endif
    enddo

! Returning final area value
    area = t(j)
    if (nn >= 1000) then
        write(*,*) 'nn >= 1000 in IntRomberg. n =', nn
    endif
    if (n >= Nmax .and. (fn(a, rpar) > 1.d-20 .and. fn(b, rpar) > 1.d-20)) then
        write(*,*) 'n=Nmax in IntRomberg', a, b, area, fn(a, rpar), fn(b, rpar)
    endif

    return
end subroutine IntRomberg


subroutine init_0d(Nsec, S, eps, rpar, nk, mk, uk, sigmak)
    integer,intent(in) :: Nsec
    double precision,intent(in) :: eps, rpar(4)
    double precision,intent(in) :: S(0:Nsec)
    double precision,intent(out) :: nk(Nsec), mk(Nsec), uk(Nsec), sigmak(Nsec)
    integer :: k
    double precision :: area0, area1, area2 ,area3

    do k = 0, Nsec-1
        call IntRomberg(get_nk, S(k), S(k+1), eps, area0, rpar)
        call IntRomberg(get_mk, S(k), S(k+1), eps, area1, rpar)
        call IntRomberg(get_mkuk, S(k), S(k+1), eps, area2, rpar)
        call IntRomberg(get_energy, S(k), S(k+1), eps, area3, rpar)
        nk(k+1) = 1.d-18*area0
        mk(k+1) = 1.d-18*ratio_mn*area1
        uk(k+1) = area2/area1
        sigmak(k+1) = area3/area1 - (area2/area1)**2
    enddo

end subroutine init_0d

subroutine init_0d_ag2d(Nsec, S, eps, rpar, nk, mk, qdm, uk, &
                        ene, sigmak)
    integer,intent(in) :: Nsec
    double precision,intent(in) :: eps, rpar(6)
    double precision,intent(in) :: S(0:Nsec)
    double precision,intent(out):: nk(Nsec), mk(Nsec), qdm(2,Nsec)
    double precision,intent(out):: uk(2,Nsec), ene(3,Nsec), sigmak(3,Nsec)
    integer :: k
    double precision :: area0, area1, area21, area22, area31, area32, area33

    do k = 0, Nsec-1
        call IntRomberg(get_nk, S(k), S(k+1), eps, area0, rpar)
        call IntRomberg(get_mk, S(k), S(k+1), eps, area1, rpar)
        call IntRomberg(get_mkuk1, S(k), S(k+1), eps, area21, rpar)
        call IntRomberg(get_mkuk2, S(k), S(k+1), eps, area22, rpar)
        call IntRomberg(get_energy11, S(k), S(k+1), eps, area31, rpar)
        call IntRomberg(get_energy12, S(k), S(k+1), eps, area32, rpar)
        call IntRomberg(get_energy22, S(k), S(k+1), eps, area33, rpar)
        nk(k+1) = 1.d-18*area0
        mk(k+1) = 1.d-18*ratio_mn*area1
        qdm(1,k+1)= (1.d-18*ratio_mn)*area21
        qdm(2,k+1)= (1.d-18*ratio_mn)*area22
        uk(1,k+1) = area21/area1
        uk(2,k+1) = area22/area1
        ene(1,k+1) = (1.d-18*ratio_mn)*area31
        ene(2,k+1) = (1.d-18*ratio_mn)*area32
        ene(3,k+1) = (1.d-18*ratio_mn)*area33
        sigmak(1,k+1) = area31/area1 - (area21/area1)**2
        sigmak(2,k+1) = area32/area1 - (area21/area1)*(area22/area1)
        sigmak(3,k+1) = area33/area1 - (area22/area1)**2
    enddo

end subroutine init_0d_ag2d


!-----------------
! Pure transport 1D test case
!-----------------

function coef_t1D_1(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: coef_t1D_1
    double precision :: t, sigmax, den

    t  = rpar(1)
    sigmax = rpar(6)
    den = sigmax + sigma(S)*t**2
    coef_t1D_1 = sigmax*sigma(S)/den

    return
end function

function coef_t1D_2(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: coef_t1D_2
    double precision :: t, x, x0, sigmax, den

    t = rpar(1)
    x = rpar(4)
    x0 = rpar(5)
    sigmax = rpar(6)
    den = sigmax + sigma(S)*t**2
    coef_t1D_2 = (sigmax*u0*u_mean_without_ug(S) + (x - x0)*sigma(S)*t)/den

    return
end function

function get_nk_t1D(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: get_nk_t1D
    double precision :: t, x, x0, sigmax, den

    t = rpar(1)
    x = rpar(4)
    x0 = rpar(5)
    sigmax  = rpar(6)
    den = sigmax + sigma(S)*t**2
    if (novoid) then
        get_nk_t1D = phi(S)/sqrt(2*pi*den)* &
                     (exp(-(x - x0)**2/(2.d0*sigmax) - (u0*u_mean_without_ug(S))**2/ &
                     (2.d0*sigma(S)) + &
                     coef_t1D_2(S,rpar)**2/(2.d0*coef_t1D_1(S,rpar))) + 1.d-7)
    else
        get_nk_t1D = phi(S)/sqrt(2*pi*den) * &
                     exp(-(x - x0)**2/(2.d0*sigmax) - (u0*u_mean_without_ug(S))**2/ &
                     (2.d0*sigma(S)) + &
                     coef_t1D_2(S, rpar)**2/(2.d0*coef_t1D_1(S, rpar)))
    endif

    return
end function

function get_mk_t1D(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: get_mk_t1D

    get_mk_t1D = S**1.5d0*get_nk_t1D(S, rpar)

    return
end function

function get_mkuk_t1D(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: get_mkuk_t1D

    get_mkuk_t1D = get_mk_t1D(S, rpar)*coef_t1D_2(S, rpar)

    return
end function

function get_energy_t1D(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: get_energy_t1D

    get_energy_t1D = get_mk_t1D(S, rpar)*(coef_t1D_2(S, rpar)**2 + &
                     coef_t1D_1(S, rpar))

    return
end function

subroutine moments_t1d(Nsec, S, rpar, nk, mk, qdm, uk, ene, sigmak)
    integer,intent(in) :: Nsec
    double precision, intent(in)  :: rpar(6)
    double precision, intent(in)  :: S(0:Nsec)
    double precision, intent(out) :: nk(Nsec), mk(Nsec), qdm(Nsec), uk(Nsec)
    double precision, intent(out) :: ene(Nsec),sigmak(Nsec)
    double precision :: area0, area1, area21, area31
    integer :: k

    do k = 0, Nsec-1
        call Gauss_50(get_nk_t1D,     S(k), S(k+1), area0,  rpar)
        call Gauss_50(get_mk_t1D,     S(k), S(k+1), area1,  rpar)
        call Gauss_50(get_mkuk_t1D,   S(k), S(k+1), area21, rpar)
        call Gauss_50(get_energy_t1D, S(k), S(k+1), area31, rpar)
        nk(k+1) = 1.d-18*area0
        mk(k+1) = 1.d-18*ratio_mn*area1
        qdm(k+1)= (1.d-18*ratio_mn)*area21
        uk(k+1) = area21/area1
        ene(k+1) = (1.d-18*ratio_mn)*area31
        sigmak(k+1) = area31/area1 - (area21/area1)**2.d0
    enddo

end subroutine moments_t1d

!-----------------
! New 1D test case
!-----------------

function coef_1D_1(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: coef_1D_1
    double precision :: t, Rs, alphadRs, sigmax, den, val

    t = rpar(1)
    Rs = rpar(2)
    alphadRs = rpar(3)
    sigmax = rpar(6)
    if (S == 0.d0) then
       coef_1D_1 = 0.d0
    else
       val = (S + Rs*t - S*(S/(S + Rs*t))**alphadRs)/((1.d0 + alphadRs)*Rs)
       den = sigmax + sigma(S + Rs*t)*val**2
       coef_1D_1 = sigmax*sigma(S + Rs*t)/(den*(1.d0 + Rs*t/S)**(2.d0*alphadRs))
    endif

    return
end function

function coef_1D_2(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: coef_1D_2
    double precision :: t, Rs, alphadRs, x, x0, sigmax, den, val

    t = rpar(1)
    Rs = rpar(2)
    alphadRs = rpar(3)
    x = rpar(4)
    x0 = rpar(5)
    sigmax = rpar(6)
    if (S == 0.d0) then
       coef_1D_2 = 0.d0
    else
       val = (S + Rs*t - S*(S/(S + Rs*t))**alphadRs)/((1.d0 + alphadRs)*Rs)
       den = sigmax + sigma(S + Rs*t)*val**2
       coef_1D_2 = (sigmax*u0*u_mean_without_ug(S + Rs*t)+(x - x0)*sigma(S + Rs*t)*val)/ &
                   (den*(1.d0 + Rs*t/S)**(alphadRs))
    endif

    return
end function

function get_nk_1D(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: get_nk_1D
    double precision :: t, Rs, alphadRs, x, x0, sigmax, den, val

    t = rpar(1)
    Rs = rpar(2)
    alphadRs = rpar(3)
    x = rpar(4)
    x0 = rpar(5)
    sigmax = rpar(6)
    if ((S == 0.d0) .or. (t == 0.d0)) then
        get_nk_1D = phi(S + Rs*t)/sqrt(2*pi*sigmax)*exp(-(x - x0)**2/(2.d0*sigmax))
    else
        val = (S + Rs*t - S*(S/(S + Rs*t))**alphadRs) / ((1.d0 + alphadRs)*Rs)
        den = sigmax + sigma(S + Rs*t)*val**2
        get_nk_1D = phi(S + Rs*t)/sqrt(2*pi*den) * &
                    exp(-(x - x0)**2/(2.d0*sigmax) - &
                    (u0*u_mean_without_ug(S + Rs*t))**2/(2.d0*sigma(S + Rs*t)) + &
                    coef_1D_2(S, rpar)**2/(2.d0*coef_1D_1(S, rpar)))
    endif

    return
end function

function get_mk_1D(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: get_mk_1D

    get_mk_1D = S**1.5d0*get_nk_1D(S, rpar)

    return
end function

function get_mkuk_1D(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: get_mkuk_1D

    get_mkuk_1D = get_mk_1D(S, rpar)*coef_1D_2(S, rpar)

    return
end function

function get_energy_1D(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: get_energy_1D

    get_energy_1D = get_mk_1D(S, rpar)*(coef_1D_2(S, rpar)**2+coef_1D_1(S, rpar))

    return
end function

subroutine moments_1d(Nsec, S, rpar, nk, mk, qdm, uk, ene, sigmak)
    integer,intent(in) :: Nsec
    double precision,intent(in) :: rpar(6)
    double precision,intent(in) :: S(0:Nsec)
    double precision,intent(out):: nk(Nsec),mk(Nsec),qdm(Nsec),uk(Nsec),ene(Nsec),sigmak(Nsec)
    integer :: k
    double precision :: area0, area1, area21, area31

    do k = 0, Nsec-1
        call Gauss_50(get_nk_1D,     S(k), S(k+1), area0,  rpar)
        call Gauss_50(get_mk_1D,     S(k), S(k+1), area1,  rpar)
        call Gauss_50(get_mkuk_1D,   S(k), S(k+1), area21, rpar)
        call Gauss_50(get_energy_1D, S(k), S(k+1), area31, rpar)
        nk(k+1) = 1.d-18*area0
        mk(k+1) = 1.d-18*ratio_mn*area1
        qdm(k+1)= (1.d-18*ratio_mn)*area21
        uk(k+1) = area21/area1
        ene(k+1) = (1.d-18*ratio_mn)*area31
        sigmak(k+1) = area31/area1 - (area21/area1)**2
    enddo

end subroutine moments_1d

!-----------------
! For 1D test case
!-----------------

function phi_1d(S)
    double precision, intent(in) :: S
    double precision :: phi_1d

    if (S > 0.d0 .and. S < Smax) then
        phi_1d = (1.d0 + 8.d0*(S/Smax))*(1.d0 - (S/Smax))**2* &
                 dexp(0.001d0*(1.d0 - 1.d0/(1.d0 - (S/Smax))**2))
    else
        phi_1d = 0.d0
    endif

    return
end function

function sigma_1d(S)
    double precision, intent(in) :: S
    double precision :: sigma_1d

    if (S > 0.d0 .and. S < Smax) then
        ! 2nd order polynomial
        ! Large droplets are faster because they are less slowed down
        sigma_1d = (S/Smax)*(2.d0 - S/Smax)
    else
        sigma_1d = 0.d0
    endif

    return
end function

function u_mean_without_x(S, xmax)
    double precision, intent(in) :: S, xmax
    double precision :: u_mean_without_x

    if (S > 0.d0 .and. S < Smax) then
       ! 2nd order polynomial
       ! Large droplets are faster because they are less slowed down
       u_mean_without_x = S/Smax*(2.d0 - (S/Smax))
    else
       u_mean_without_x = 0.d0
    endif
    u_mean_without_x = u_mean_without_x*3.d0/xmax

    return
end function

function coef1(t, S, alpha_drag, Rs)
    double precision, intent(in) :: t, S, alpha_drag, Rs
    double precision :: coef1
    coef1 = (S + Rs*t)**(alpha_drag/Rs)*phi_1d(S + Rs*t)
end function

function coef2(t, S, alpha_drag, Rs, xmax)
    double precision, intent(in) :: t, S, alpha_drag, Rs, xmax
    double precision :: coef2
    coef2 = (S + Rs*t)**(alpha_drag/Rs) &
      - u_mean_without_x(S + Rs*t, xmax)/(alpha_drag + Rs) &
       *(S**(alpha_drag/Rs + 1.d0) - (S + Rs*t)**(alpha_drag/Rs + 1.d0))
end function

function get_nk_t(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: t, x, alpha_drag, Rs, xmax
    double precision :: get_nk_t

    t = rpar(1)
    Rs = rpar(2)
    alpha_drag = rpar(3)
    x = rpar(4)
    xmax = rpar(5)
    get_nk_t = coef1(t, S, alpha_drag, Rs)/ &
               coef2(t, S, alpha_drag, Rs, xmax)

    return
end function

function get_mk_t(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: get_mk_t

    get_mk_t = S**1.5d0*get_nk_t(S, rpar)

    return
end function

function get_mkuk_t(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: t, x, alpha_drag, Rs, xmax
    double precision :: get_mkuk_t

    t = rpar(1)
    Rs = rpar(2)
    alpha_drag = rpar(3)
    x = rpar(4)
    xmax = rpar(5)

    get_mkuk_t = S**1.5d0*coef1(t, S, alpha_drag, Rs)/ &
                 coef2(t, S, alpha_drag, Rs, xmax)**2 &
                 *x*u_mean_without_x(S + Rs*t, xmax)*S**(alpha_drag/Rs)

    return
end function

function get_energy_t(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: t, x, alpha_drag, Rs, xmax
    double precision :: get_energy_t

    t = rpar(1)
    Rs = rpar(2)
    alpha_drag = rpar(3)
    x = rpar(4)
    xmax = rpar(5)

    get_energy_t = S**1.5d0*coef1(t, S, alpha_drag, Rs)/ &
                            coef2(t, S, alpha_drag, Rs, xmax)**3* &
                            (sigma_1d(S + Rs*t) + &
                            (x*u_mean_without_x(S + Rs*t, xmax))**2)* &
                             S**(2.d0*alpha_drag/Rs)

    return
end function


subroutine Gauss_50(func, a, b, integ, rpar)
    ! Compute integral of func, x in [a, b] using a 24-point Gauss quadrature

    double precision, intent(in) :: a, b, rpar(*)
    integer, parameter :: order=50
    double precision, intent(out) :: integ
    double precision :: xi
    double precision :: weig(order)
    double precision :: absc(order)
    integer :: i

    interface
        double precision function func(S, rpar)
            double precision, intent(in) :: S, rpar(*)
        end function func
    end interface

    weig(1:order)    = (/ 1.4543112765773515d-3,   3.3798995978727396d-3,   5.2952741918255233d-3,&
    7.1904113807281398d-3,   9.0577803567421060d-3,   1.0890121585061753d-2,   1.2680336785006062d-2,   1.4421496790267552d-2,&
    1.6106864111788976d-2,   1.7729917807573041d-2,   1.9284378306293828d-2,   2.0764231545073817d-2,   2.2163752169401633d-2,&
    2.3477525651974220d-2,   2.4700469224733193d-2,   2.5827851534790579d-2,   2.6855310944498136d-2,   2.7778872403106270d-2,&
    2.8594962823864197d-2,   2.9300424906611226d-2,   2.9892529352132730d-2,   3.0368985420885106d-2,   3.0727949795158378d-2,&
    3.0968033710341607d-2,   3.1088308327673626d-2,   3.1088308327673626d-2,   3.0968033710341607d-2,   3.0727949795158378d-2,&
    3.0368985420885106d-2,   2.9892529352132730d-2,   2.9300424906611226d-2,   2.8594962823864197d-2,   2.7778872403106270d-2,&
    2.6855310944498136d-2,   2.5827851534790579d-2,   2.4700469224733193d-2,   2.3477525651974220d-2,   2.2163752169401633d-2,&
    2.0764231545073817d-2,   1.9284378306293828d-2,   1.7729917807573041d-2,   1.6106864111788976d-2,   1.4421496790267552d-2,&
    1.2680336785006062d-2,   1.0890121585061753d-2,   9.0577803567421060d-3,   7.1904113807281398d-3,   5.2952741918255233d-3,&
    3.3798995978727396d-3,   1.4543112765773515d-3 /)

    absc(1:order)   = (/ 5.6679778996449048d-4,   2.9840152839546441d-3,   7.3229579759970798d-3,&
    1.3567807446653979d-2,   2.1694522378596037d-2,   3.1671690527561025d-2,   4.3460721672104075d-2,   5.7016010238193471d-2,&
    7.2285115285026957d-2,   8.9208964570332006d-2,   0.10772208354980040d0,   0.12775284888696575d0,   0.14922376564658890d0,&
    0.17205176715728032d0,   0.19614853640752489d0,   0.22142084774267495d0,   0.24777092754626789d0,   0.27509683251298062d0,&
    0.30329284405121743d0,   0.33224987729028133d0,   0.36185590311023397d0,   0.39199638156197913d0,   0.42255470500092707d0,&
    0.45341264921995694d0,   0.48445083083640555d0,   0.51554916916359439d0,   0.54658735078004306d0,   0.57744529499907293d0,&
    0.60800361843802087d0,   0.63814409688976603d0,   0.66775012270971867d0,   0.69670715594878252d0,   0.72490316748701944d0,&
    0.75222907245373216d0,   0.77857915225732510d0,   0.80385146359247517d0,   0.82794823284271968d0,   0.85077623435341110d0,&
    0.87224715111303430d0,   0.89227791645019960d0,   0.91079103542966799d0,   0.92771488471497299d0,   0.94298398976180653d0,&
    0.95653927832789587d0,   0.96832830947243898d0,   0.97830547762140396d0,   0.98643219255334602d0,   0.99267704202400298d0,&
    0.99701598471604536d0,   0.99943320221003551d0  /)

    integ = 0.d0
    do i = 1, order
        xi = a + (b - a)*absc(i)
        integ = integ + weig(i)*func(xi, rpar)
    enddo
    integ = integ*(b - a)

end subroutine


subroutine init_1d(Nsec, S, rpar, nk, mk, qdmk, uk, ek, sigmak)
    integer,intent(in) :: Nsec
    double precision,intent(in) :: rpar(6)
    double precision,intent(in) :: S(0:Nsec)
    double precision,intent(out) :: nk(Nsec), mk(Nsec), qdmk(Nsec), uk(Nsec)
    double precision,intent(out) :: ek(Nsec), sigmak(Nsec)
    integer :: k
    double precision :: area0, area1, area2 ,area3

    do k = 0, Nsec-1
        call Gauss_50(get_nk_t, S(k), S(k+1), area0, rpar)
        call Gauss_50(get_mk_t, S(k), S(k+1), area1, rpar)
        call Gauss_50(get_mkuk_t, S(k), S(k+1), area2, rpar)
        call Gauss_50(get_energy_t, S(k), S(k+1), area3, rpar)

        nk(k+1) = 1.d-18*area0 ! Unity conversion
        mk(k+1) = 1.d-18*ratio_mn*area1
        qdmk(k+1)= (1.d-18*ratio_mn)*area2
        uk(k+1) = area2/area1
        ek(k+1) = (1.d-18*ratio_mn)*area3
        sigmak(k+1) = area3/area1 - (area2/area1)**2
    enddo

end subroutine init_1d

!------------------
! 1D transport only
!------------------

function get_nk_tt(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: t, x, xmax
    double precision :: get_nk_tt

    t = rpar(1)
    x = rpar(4)
    xmax = rpar(5)
    get_nk_tt = phi(S)/(1.d0+t*u_mean_without_x(S, xmax))

    return
end function

function get_mk_tt(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: Rs, t
    double precision :: get_mk_tt

    get_mk_tt = S**1.5d0*get_nk_tt(S, rpar)

    return
end function

function get_mkuk_tt(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: t, x, xmax
    double precision :: get_mkuk_tt

    t = rpar(1)
    x = rpar(4)
    xmax = rpar(5)

    get_mkuk_tt = get_mk_tt(S, rpar)/(1.d0+t*u_mean_without_x(S, xmax))&
        *x*u_mean_without_x(S, xmax)

    return
end function

function get_energy_tt(S, rpar)
    double precision, intent(in) :: S, rpar(*)
    double precision :: t, x, xmax
    double precision :: get_energy_tt

    t = rpar(1)
    x = rpar(4)
    xmax = rpar(5)

    get_energy_tt = get_mk_tt(S, rpar)/(1.d0+t*u_mean_without_x(S, xmax))**2 &
                    *(sigma(S) + (x*u_mean_without_x(S, xmax))**2)

    return
end function

subroutine init_t1d(Nsec, S, rpar, nk, mk, qdm, uk, ek, sigmak)
    integer, intent(in) :: Nsec
    double precision, intent(in) :: rpar(6)
    double precision, intent(in) :: S(0:Nsec)
    double precision, intent(out) :: nk(Nsec), mk(Nsec), qdm(Nsec), uk(Nsec)
    double precision, intent(out) :: ek(Nsec),sigmak(Nsec)
    integer :: k
    double precision :: area0, area1, area21 ,area31

    do k = 0, Nsec-1
        call Gauss_50(get_nk_tt, S(k), S(k+1), area0, rpar)
        call Gauss_50(get_mk_tt, S(k), S(k+1), area1, rpar)
        call Gauss_50(get_mkuk_tt, S(k), S(k+1), area21, rpar)
        call Gauss_50(get_energy_tt, S(k), S(k+1), area31, rpar)

        ! Unity conversion
        nk(k+1) = 1.d-18*area0
        mk(k+1) = 1.d-18*ratio_mn*area1
        qdm(k+1)= (1.d-18*ratio_mn)*area21
        uk(k+1) = area21/area1
        ek(k+1) = (1.d-18*ratio_mn)*area31
        sigmak(k+1) = area31/area1 - (area21/area1)**2
    enddo

end subroutine

subroutine check_S(nsec, S, nl, ml, nn)
! Check that we are in moment space and project small values

    integer, intent(in) :: nsec, nn
    double precision, intent(in) :: S(0:nsec)
    double precision, intent(inout) :: nl(nsec), ml(nsec)
    integer :: k
    double precision :: val

    do k = 1, nsec
        if (ml(k) < ratio_mn*S(k-1)**1.5d0*nl(k) .or. &
            ml(k) > ratio_mn*S(k)**1.5d0*nl(k)) then
           if (ml(k) > epsi) then
                write(*,*) 'Exiting moments space!'
                write(*,*) 'nn     =', nn
                write(*,*) 'k      =', k
                write(*,*) 'mk_min =', ratio_mn*S(k-1)**1.5d0*nl(k)
                write(*,*) 'mk_max =', ratio_mn*S(k)**1.5d0*nl(k)
                write(*,*) 'mk     =', ml(k)
                write(*,*) 'nk     =', nl(k)
                stop
           else
                write(*,*) 'Projection:'
                write(*,*) 'nn     =', nn
                write(*,*) 'k      =', k
                write(*,*) 'mk_min =', ratio_mn*S(k-1)**1.5d0*nl(k)
                write(*,*) 'mk_max =', ratio_mn*S(k)**1.5d0*nl(k)
                write(*,*) 'mk     =', ml(k)
                write(*,*) 'nk     =', nl(k)
           endif
        endif
    enddo
end subroutine

subroutine check_S_old(nsec, S, nl, ml, nn)
! Check that we are in moment space and project small values

    integer, intent(in) :: nsec, nn
    double precision, intent(in) :: S(0:nsec)
    double precision, intent(inout) :: nl(nsec), ml(nsec)
    double precision :: ss
    integer :: k

    do k = 1, nsec
        if (ml(k) < ratio_mn*S(k-1)**1.5d0*nl(k) .or. &
            ml(k) > ratio_mn*S(k)**1.5d0*nl(k)) then
            if (ml(k) > epsi) then
                write(*,*) 'Exiting moments space!'
                write(*,*) 'nn     =', nn
                write(*,*) 'k      =', k
                write(*,*) 'mk_min =', ratio_mn*S(k-1)**1.5d0*nl(k)
                write(*,*) 'mk_max =', ratio_mn*S(k)**1.5d0*nl(k)
                write(*,*) 'mk     =', ml(k)
                write(*,*) 'nk     =', nl(k)
                stop
            else if (ml(k) > 1.d-50) then
                if (ml(k) > 1.d-20) then
                    write(*,*) 'Projection', nn, k, &
                                ratio_mn*S(k-1)**1.5d0*nl(k), &
                                ml(k), ratio_mn*S(k)**1.5d0*nl(k)
                    stop
                endif
                ss = S(k)
                if (k == nsec) then
                    ss = 2.d0*S(k-1) - S(k-2)
                endif
                nl(k) = ml(k)*2.5d0*(ss - S(k-1)) / &
                        (ratio_mn*(ss**2.5d0 - S(k-1)**2.5d0))
                if (ml(k) < ratio_mn*S(k-1)**1.5d0*nl(k) .or. &
                    ml(k) > ratio_mn*S(k)**1.5d0*nl(k))then
                    write(*,*) 'Projection error:', S(k-1)**1.5d0, &
                                ml(k)/ratio_mn/nl(k), S(k)**1.5d0
                    write(*,*) k, S(k-1), S(k), nl(k), ml(k), &
                               2.5d0*(S(k) - S(k-1)) / &
                               (S(k)**2.5d0 - S(k-1)**2.5d0)
                    stop
                endif
            else
                nl(k) = 0.d0
                ml(k) = 0.d0
            endif
        endif
        if (isnan(nl(k))) then
            write(*,*) 'n = NAN, k=', k
            write(*,*) nl(k), ml(k), nn
            stop
        endif
    enddo

end subroutine


subroutine compute_gauss_k(N, V, f_get_nk, f_get_mk, f_get_mkuk, f_get_energy, &
                           rpar, X, S, Sp, k)

    implicit none

    integer, intent(in) :: N
    integer, intent(in) :: k
    double precision, dimension(0:6,0:N+1), intent(inout) :: V
    double precision, intent(inout) :: rpar(6)
    double precision, dimension(0:N+1), intent(in) :: X
    double precision, intent(in) :: S, Sp
    integer :: i
    double precision :: mk, nk
    double precision :: area0, area1, area21, area31

    interface
        double precision function f_get_nk(S, rpar)
            double precision, intent(in) :: S, rpar(*)
        end function f_get_nk
        double precision function f_get_mk(S, rpar)
            double precision, intent(in) :: S, rpar(*)
        end function f_get_mk
        double precision function f_get_mkuk(S, rpar)
            double precision, intent(in) :: S, rpar(*)
        end function f_get_mkuk
        double precision function f_get_energy(S, rpar)
            double precision, intent(in) :: S, rpar(*)
        end function f_get_energy
    end interface

    do i = 0, N+1
        rpar(4) = X(i)

        call Gauss_50(f_get_nk, S, Sp, area0, rpar)
        call Gauss_50(f_get_mk, S, Sp, area1, rpar)
        call Gauss_50(f_get_mkuk, S, Sp, area21, rpar)
        call Gauss_50(f_get_energy, S, Sp, area31, rpar)

        ! Unity conversion
        nk = 1.d-18 * area0
        mk = 1.d-18 * ratio_mn * area1
        V(0, i) = mk
        if (area1 > 0.d0) then
            V(1, i) = area21/area1
            V(3, i) = area31/area1 - (area21/area1)**2
            V(6, i) = nk/mk
        else
            V(1, i) = 0.d0
            V(3, i) = 0.d0
            V(6, i) = 0.d0
        endif
    enddo

end subroutine

subroutine compute_gauss_1d(N, Nsec, Vk, &
                            f_get_nk, f_get_mk, f_get_mkuk, f_get_energy, &
                            rpar, X, S, use_nmax)

    implicit none

    integer, intent(in) :: N, Nsec
    double precision, dimension(0:6,0:N+1,0:Nsec-1), intent(inout) :: Vk
    double precision, intent(inout) :: rpar(6)
    double precision, dimension(0:N+1), intent(in) :: X
    double precision, dimension(0:Nsec), intent(in) :: S
    logical, intent(in) :: use_nmax
    double precision, dimension(0:Nsec) :: Sc
    integer :: i, k, Nmax
    double precision :: mk, nk
    double precision :: area0, area1, area21, area31

    interface
        double precision function f_get_nk(S, rpar)
            double precision, intent(in) :: S, rpar(*)
        end function f_get_nk
        double precision function f_get_mk(S, rpar)
            double precision, intent(in) :: S, rpar(*)
        end function f_get_mk
        double precision function f_get_mkuk(S, rpar)
            double precision, intent(in) :: S, rpar(*)
        end function f_get_mkuk
        double precision function f_get_energy(S, rpar)
            double precision, intent(in) :: S, rpar(*)
        end function f_get_energy
    end interface


    do i = 0, N+1
        if (use_nmax) then
            Nmax = non_void_sections(Nsec, rpar)

            Sc(0:Nmax-1) = S(0:Nmax-1)
            if (Nmax >= 1) then
               if (abs(S(Nsec) - rpar(1)*rpar(2) - S(Nmax-1)) < S(1)/1.d10) then
                  Nmax = Nmax - 1
               else
                  Sc(Nmax) = S(Nsec) - rpar(1)*rpar(2)
               end if
            end if
        else
            Nmax = Nsec - 1
            Sc(:) = S(:)
        endif
        do k = 0, Nmax -1
            rpar(4) = X(i)

            call Gauss_50(f_get_nk, Sc(k), Sc(k+1), area0, rpar)
            call Gauss_50(f_get_mk, Sc(k), Sc(k+1), area1, rpar)
            call Gauss_50(f_get_mkuk, Sc(k), Sc(k+1), area21, rpar)
            call Gauss_50(f_get_energy, Sc(k), Sc(k+1), area31, rpar)

            ! Unity conversion
            nk = 1.d-18 * area0
            mk = 1.d-18 * ratio_mn * area1
            Vk(0, i, k) = mk
            if (area1 > 0.d0) then
                Vk(1, i, k) = area21/area1
                Vk(3, i, k) = area31/area1 - (area21/area1)**2
                Vk(6, i, k) = nk/mk
            else
                Vk(1, i, k) = 0.d0
                Vk(3, i, k) = 0.d0
                Vk(6, i, k) = 0.d0
            endif
        enddo
    enddo

end subroutine

function non_void_sections(Nsec, rpar)
   double precision, intent(in) :: rpar(*)
   integer, intent(in) :: Nsec
   integer :: non_void_sections

   non_void_sections = Nsec - floor(rpar(1)*rpar(2)*Nsec/Smax)
   return
end function


subroutine compute_evap_drag_ag_1d(Nvel, Nene, N, Nsect, Vk, &
                                   S, Sinta, Sintb, ug, dt)

    implicit none
    integer, intent(in) :: Nvel, Nene, N, Nsect
    double precision, dimension(0:6,0:N+1,Nsect), intent(inout) :: Vk
    double precision, intent(in) :: S(Nsect+1), Sinta(Nsect), Sintb(Nsect)
    double precision, dimension(Nvel,0:N+1), intent(in) :: ug
    double precision, intent(in) :: dt
    double precision, dimension(Nsect) :: nk
    double precision :: nkp(Nsect), mkp(Nsect)
    double precision :: ukp(Nvel,Nsect)
    double precision :: sigmakp(Nene,Nsect)
    integer :: i, k

    do i = 0, N+1
        nk(:) = Vk(0,i,:)*Vk(6,i,:)  ! Compute nk = mk*muk
        call evap_drag_aff(Nvel, Nene, Nsect, S, Sinta, Sintb, &
                           dt, ug(:,i), nk(:), &
                           Vk(0,i,:), Vk(1:Nvel,i,:), Vk(3:2+Nene,i,:), &
                           nkp, mkp, ukp, sigmakp)
        Vk(0,i,:) = mkp(:)
        Vk(1:Nvel,i,:) = ukp(1:Nvel,:)
        Vk(3:2+Nene,i,:) = sigmakp(1:Nene,:)
        do k = 1, Nsect
            if (mkp(k) /= 0.d0) then
                Vk(6,i,k) = nkp(k)/mkp(k)
            else
                Vk(6,i,k) = 0.d0  ! Handle void in section
            endif
        enddo
    enddo

end subroutine compute_evap_drag_ag_1d


subroutine compute_evap_drag_ag_2d(Nvel, Nene, N, Nsect, Vk, &
                                   S, Sinta, Sintb, ug, dt)

    implicit none
    integer, intent(in) :: Nvel, Nene, N, Nsect
    double precision, dimension(0:6,0:N+1,0:N+1,Nsect), intent(inout) :: Vk
    double precision, intent(in) :: S(Nsect+1), Sinta(Nsect), Sintb(Nsect)
    double precision, dimension(Nvel,0:N+1,0:N+1), intent(in) :: ug
    double precision, intent(in) :: dt
    integer :: j
    integer :: rang

    !$omp parallel
    !$omp do schedule(static)
    do j = 0, N+1
        call compute_evap_drag_ag_1d(Nvel, Nene, N, Nsect, Vk(:,:,j,:), &
                                     S, Sinta, Sintb, ug(:,:,j), dt)
    enddo
    !$omp end parallel
end subroutine compute_evap_drag_ag_2d

!------------------------!
!  Evaporation-drag step !
!------------------------!
subroutine evap_drag_aff(Nvel, Nene, nsec, S, Sinta, Sintb, &
                         dt, ug, nk, mk, uk, sigmak, nkp, mkp, &
                         ukp, sigmakp)
! un pas de temps d'evaporation avec l'algorithme du SIAP un peu ameliore
! entrees:
!    dt: pas de temps
!    S(k-1),S(k): bornes de la section k, k=1,..,nsec (variable globale)
!    ug : vitesse du gaz
!    nk(nsec): vecteur de moments d'ordre 0 dans les nsec sections a l'instant t
!    mk(nsec): vecteur de moments d'ordre 3/2 dans les nsec sections a l'instant t
!    uk(nsec): vecteur de vitesse dans les nsec sections a l'instant t
!    sigma(nsec): vecteur de moment d'ordre 2 dans les nsec a l'instant t
! sorties:
!    nkp(nsec): vecteur de moments d'ordre 0 dans les nsec sections a l'instant t+dt
!    mkp(nsec): vecteur de moments d'ordre 3/2 dans les nsec sections a l'instant t+dt
!    ukp(nsec): vecteur de vitesse dans les nsec sections a l'instant t+dt
!    sigmakp(nsec): vecteur de moments d'ordre 3/2 dans les nsec a l'instant t+dt
!
    integer, intent(in) :: Nvel, Nene, nsec  ! number of velocities, energies and sections
    double precision, intent(in) :: S(0:nsec)  ! bounds of sections (surfaces in micron^2)
    double precision, intent(in) :: Sinta(Nsec), Sintb(Nsec)  ! internal bounds for linear reconstruction
    double precision, intent(in) :: dt
    double precision, intent(in) :: ug(Nvel), nk(nsec), mk(nsec)
    double precision, intent(in) :: uk(Nvel,nsec), sigmak(Nene,nsec)
    double precision, intent(out) :: nkp(nsec), mkp(nsec)
    double precision, intent(out) :: ukp(Nvel,nsec), sigmakp(Nene,nsec)
    double precision :: Sa(nsec), Sb(nsec), alpha(nsec), beta(nsec)
    double precision :: mm(0:3), mmp(0:3), wei(2), absc(2)
    double precision :: dd, SS, m1, m2
    double precision :: u1(Nvel), u2(Nvel), v1(Nvel) ,v2(Nvel)
    double precision :: sigma1(Nene), sigma2(Nene)
    double precision :: delta, dtl, dtmax
    double precision :: gam, Ra, Rb
    double precision, parameter :: Xsmall = 1.d-30
    integer :: i, k, niter, ni, err


    ! discretization
    dtmax = dt + 1.d0
    do k = 1, nsec-1
        dtmax = min(dtmax, (S(k) - S(k-1))/(Rs*1.d12))
    enddo
    niter = int(dt/dtmax) + 1
    dtl = dt/dble(niter)
    if (niter > 1) then
        write(*,*) 'Under discretisation:', niter, dt, dtmax, dtl
    endif
    !write(*,*) 'CFL',(Rs*dtl/(S(k)-S(k-1))*1.d12,k=1,nsec-1)

    nkp = nk
    mkp = mk
    ukp = uk
    sigmakp = sigmak
    if (method_order == 2) then
        call check_S(nsec, S, nkp, mkp, -2)
    endif

    ! Time loop
    do ni = 1, niter

        ! reconstruction
        do k = 1, nsec
            if (method_order == 2) then  ! TSM case
                call reconstruct_affine(S(k-1), S(k), Sinta(k), Sintb(k), &
                                        nkp(k), mkp(k)/ratio_mn, Sa(k), &
                                        Sb(k), alpha(k), beta(k))
            else  ! OSM case
                Sa(k) = S(k-1)
                Sb(k) = S(k)
                alpha(k) = mk(k)/(ratio_mn*(S(k)**2.5d0 - S(k-1)**2.5d0)/2.5d0)
                beta(k) = alpha(k)
            endif
        enddo

        dd = Rs*dtl*1.d12  ! Warning: unity conversion
        mmp = 0.d0
        do k = 1, nsec-1
            ! integrals from S(k-1)+dd to S(k) of S**i*frecons(S)
            mm = 0.d0
            SS = S(k-1) + dd
            delta = 0.d0
            if (SS < Sa(k)) then
                SS = Sa(k)
            endif
            if (SS < Sb(k)) then
                delta = sqrt(Sb(k)) - sqrt(SS)
                gam = (beta(k) - alpha(k))/(Sb(k) - Sa(k))
                Rb = sqrt(Sb(k))
                do i = 0, 3
                    mm(i) = -gam*delta**(i + 4)/dble(i + 4) + &
                            3.d0*gam*Rb*delta**(i + 3)/dble(i + 3) - &
                           (2.d0*gam*Sb(k) + beta(k))*delta**(i + 2)/ &
                            dble(i + 2) + beta(k)*Rb*delta**(i + 1)/dble(i + 1)
                enddo
                mm = mm*2.d0
            endif
            ! quadrature and evolution of abscissa of quadrature
            if (mm(0) > Xsmall) then
                call quadrature_2nodes(0.d0, delta, mm, absc, wei, err)
                if (err /= 0) then
                    write(*,*) 'Quadrature error 1', k
                    write(*,*) 'alpha =', alpha(k)
                    write(*,*) 'beta =', beta(k)
                    write(*,*) 'Sa =', Sa(k)
                    write(*,*) 'Sb =', Sb(k)
                    write(*,*) 'delta =', delta
                    write(*,*) 'mm', mm
                    stop
                endif
                absc = (Rb - absc)**2
                m1 = wei(1)*(absc(1) - dd)**1.5d0 + wei(2)*(absc(2) - dd)**1.5d0
                if (m1 <= S(k-1)**1.5d0*mm(0) .or. &
                    m1 >= (S(k) - dd)**1.5d0*mm(0)) then
                    write(*,*) 'interm 1: exiting moment space', k
                    write(*,*) S(k-1)**1.5d0*mm(0), m1, (S(k) - dd)**1.5d0*mm(0)
                    if (m1 > 1.d-50) then
                        stop
                    endif
                endif

                v1 = ug + (ukp(:,k) - ug)*(1.d0 - dd/absc(1))**(alpha_drag/Rs)
                v2 = ug + (ukp(:,k) - ug)*(1.d0 - dd/absc(2))**(alpha_drag/Rs)

                u1 = wei(1)*(absc(1) - dd)**1.5d0*v1 + &
                     wei(2)*(absc(2) - dd)**1.5d0*v2

                sigma1(1) = wei(1)*(absc(1) - dd)**1.5d0*(v1(1)**2 + &
                            (1.d0 - dd/absc(1))**(2.d0*alpha_drag/Rs)*sigmakp(1,k)) + &
                            wei(2)*(absc(2) - dd)**1.5d0*(v2(1)**2 + &
                            (1.d0 - dd/absc(2))**(2.d0*alpha_drag/Rs)*sigmakp(1,k))
                if (Nene == 3) then
                    sigma1(2) = wei(1)*(absc(1) - dd)**1.5d0*(v1(1)*v1(2) + &
                                (1.d0 - dd/absc(1))**(2.d0*alpha_drag/Rs)*sigmakp(2,k)) + &
                                wei(2)*(absc(2) - dd)**1.5d0*(v2(1)*v2(2) + &
                                (1.d0 - dd/absc(2))**(2.d0*alpha_drag/Rs)*sigmakp(2,k))
                    sigma1(3) = wei(1)*(absc(1) - dd)**1.5d0*(v1(2)**2 + &
                                (1.d0 - dd/absc(1))**(2.d0*alpha_drag/Rs)*sigmakp(3,k)) + &
                                wei(2)*(absc(2) - dd)**1.5d0*(v2(2)**2 + &
                                (1.d0 - dd/absc(2))**(2.d0*alpha_drag/Rs)*sigmakp(3,k))
                endif

                u1 = u1/m1
                m1 = m1*ratio_mn
            else
                m1 = 0.d0
                u1 = ug
                sigma1 = 0.d0
                mm = 0.d0
            endif
            ! integrals from S(k) to S(k)+dd of S**i*frecons(S)
            mmp = 0.d0
            SS = S(k) + dd
            delta = 0.d0
            if (SS > Sb(k+1)) then
                SS = Sb(k+1)
            endif
            if (SS > Sa(k+1)) then
                delta = sqrt(SS) - sqrt(Sa(k+1))
                gam = (beta(k+1) - alpha(k+1))/(Sb(k+1) - Sa(k+1))
                Ra = sqrt(Sa(k+1))
                do i = 0, 3
                    mmp(i) = gam*delta**(i + 4)/dble(i + 4) + &
                             3.d0*gam*Ra*delta**(i + 3)/dble(i + 3) + &
                             (2.d0*gam*Sa(k+1) + alpha(k+1))*delta**(i + 2)/ &
                             dble(i + 2) + alpha(k+1)*Ra*delta**(i + 1)/ &
                             dble(i + 1)
                enddo
                mmp = mmp*2.d0
            endif
            ! quadrature and evolution of abscissa of quadrature
            if (mmp(0) > Xsmall) then
                call quadrature_2nodes(0.d0, delta, mmp, absc, wei, err)
                absc = (absc+Ra)**2
                m2 = wei(1)*(absc(1) - dd)**1.5d0 + &
                     wei(2)*(absc(2) - dd)**1.5d0

                v1 = ug + (ukp(:,k+1) - ug)*(1.d0 - dd/absc(1))**(alpha_drag/Rs)
                v2 = ug + (ukp(:,k+1) - ug)*(1.d0 - dd/absc(2))**(alpha_drag/Rs)

                u2 = wei(1)*(absc(1) - dd)**1.5d0*v1 + &
                     wei(2)*(absc(2) - dd)**1.5d0*v2
                sigma2(1) = wei(1)*(absc(1) - dd)**1.5d0*(v1(1)**2 + &
                            (1.d0 - dd/absc(1))**(2.d0*alpha_drag/Rs)*sigmakp(1,k+1)) + &
                            wei(2)*(absc(2) - dd)**1.5d0*(v2(1)**2 + &
                            (1.d0 - dd/absc(2))**(2.d0*alpha_drag/Rs)*sigmakp(1,k+1))
                if (Nene == 3) then
                    sigma2(2) = wei(1)*(absc(1) - dd)**1.5d0*(v1(1)*v1(2) + &
                                (1.d0 - dd/absc(1))**(2.d0*alpha_drag/Rs)*sigmakp(2,k+1)) + &
                                wei(2)*(absc(2) - dd)**1.5d0*(v2(1)*v2(2) + &
                                (1.d0-dd/absc(2))**(2.d0*alpha_drag/Rs)*sigmakp(2,k+1))
                    sigma2(3) = wei(1)*(absc(1) - dd)**1.5d0*(v1(2)**2 + &
                                (1.d0 - dd/absc(1))**(2.d0*alpha_drag/Rs)*sigmakp(3,k+1)) + &
                                wei(2)*(absc(2) - dd)**1.5d0*(v2(2)**2 + &
                                (1.d0 - dd/absc(2))**(2.d0*alpha_drag/Rs)*sigmakp(3,k+1))
                endif

                u2 = u2/m2
                m2 = m2*ratio_mn
            else
                m2 = 0.d0
                u2 = ug
                sigma2 = 0.d0
                mmp = 0.d0
            endif

            nkp(k) = mm(0)+ mmp(0)
            mkp(k) = m1 + m2

            if ((mkp(k) <= S(k-1)**1.5d0*nkp(k)*ratio_mn .or. &
                 mkp(k) >= S(k)**1.5d0*nkp(k)*ratio_mn) .and. &
                (mkp(k) + nkp(k) /= 0.d0)) then
                write(*,*) 'Final: exiting moment space', k
                write(*,*) S(k-1)**1.5d0*nkp(k)*ratio_mn,mkp(k), &
                           S(k)**1.5d0*nkp(k)*ratio_mn
                write(*,*) mm(0), mmp(0)
                write(*,*) m1, m2
                write(*,*) S(k-1)**1.5d0*ratio_mn, m1/mm(0), m2/mmp(0), &
                           S(k)**1.5d0*ratio_mn
                if (mkp(k) > 1.d-50) then
                    stop
                endif
            endif
            if (mkp(k) /= 0.d0) then
                ukp(:,k) = (m1*u1 + m2*u2)/(m1 + m2)
                !Est-ce que ca ne serait pas mieux d'ecrire sigmak = (E_to)/mkp(k) - ukp(k)**2 ? A essayer
                sigmakp(1,k) = ratio_mn*(sigma1(1) + sigma2(1))/(m1 + m2) - ukp(1,k)**2
                if (Nene == 3) then
                    sigmakp(2,k) = ratio_mn*(sigma1(2) + sigma2(2))/(m1 + m2) - ukp(1,k)*ukp(2,k)
                    sigmakp(3,k) = ratio_mn*(sigma1(3) + sigma2(3))/(m1 + m2) - ukp(2,k)**2
                endif
            else
                ukp(:,k) = ug
                sigmakp(:,k) = 0
            endif
        enddo

        ! Processing last section
        k = nsec
        mm = 0.d0
        delta = 0.d0
        SS = S(k-1) + dd
        if (SS < Sa(k)) then
            SS = Sa(k)
        endif
        if (SS < Sb(k)) then
            delta = sqrt(Sb(k)) - sqrt(SS)
            gam = (beta(k) - alpha(k))/(Sb(k) - Sa(k))
            Rb = sqrt(Sb(k))
            do i = 0, 3
                mm(i) = -gam*delta**(i + 4)/dble(i + 4) + &
                        3.d0*gam*Rb*delta**(i + 3)/dble(i + 3) - &
                       (2*gam*Sb(k) + beta(k))*delta**(i + 2)/dble(i + 2) + &
                        beta(k)*Rb*delta**(i + 1)/dble(i + 1)
            enddo
            mm = mm*2.d0
        endif
        nkp(k) = mm(0)

        ! quadrature and evolution of abscissa of quadrature
        if (mm(0) > Xsmall) then
            call quadrature_2nodes(0.d0,delta,mm,absc,wei,err)
            if (err /= 0) then
                write(*,*) 'Quadrature error 3', k
                write(*,*) 'alpha =', alpha(k)
                write(*,*) 'beta =', beta(k)
                write(*,*) 'Sa =', Sa(k)
                write(*,*) 'Sb =', Sb(k)
                write(*,*) 'delta =', delta
                write(*,*) 'mm', mm
                stop
            endif
            absc = (Rb - absc)**2
            mkp(k) = wei(1)*(absc(1) - dd)**1.5d0 + &
                     wei(2)*(absc(2) - dd)**1.5d0
            if (mkp(k) <= S(k-1)**1.5d0*mm(0) .or. &
                mkp(k) >= (S(k) - dd)**1.5d0*mm(0)) then
                write(*,*) 'interm 1f: exiting moment space', k
                write(*,*) S(k-1)**1.5d0*mm(0), mkp(k), (S(k) - dd)**1.5d0*mm(0)
                if (mkp(k) > 1.d-50) then
                    stop
                endif
            endif

            v1 = ug + (ukp(:,k) - ug)*(1.d0 - dd/absc(1))**(alpha_drag/Rs)
            v2 = ug + (ukp(:,k) - ug)*(1.d0 - dd/absc(2))**(alpha_drag/Rs)

            ukp(:,k) = wei(1)*(absc(1) - dd)**1.5d0*v1 + &
                       wei(2)*(absc(2) - dd)**1.5d0*v2
            sigmakp(1,k) = wei(1)*(absc(1) - dd)**1.5d0*(v1(1)**2 + &
                           (1.d0 - dd/absc(1))**(2.d0*alpha_drag/Rs)*sigmakp(1,k)) + &
                           wei(2)*(absc(2) - dd)**1.5d0*(v2(1)**2 + &
                           (1.d0 - dd/absc(2))**(2.d0*alpha_drag/Rs)*sigmakp(1,k))
            if (Nene == 3) then
                sigmakp(2,k) = wei(1)*(absc(1) - dd)**1.5d0*(v1(1)*v1(2) + &
                              (1.d0 - dd/absc(1))**(2.d0*alpha_drag/Rs)*sigmakp(2,k)) + &
                               wei(2)*(absc(2) - dd)**1.5d0*(v2(1)*v2(2) + &
                              (1.d0 - dd/absc(2))**(2.d0*alpha_drag/Rs)*sigmakp(2,k))
                sigmakp(3,k) = wei(1)*(absc(1) - dd)**1.5d0*(v1(2)**2 + &
                              (1.d0 - dd/absc(1))**(2.d0*alpha_drag/Rs)*sigmakp(3,k)) + &
                               wei(2)*(absc(2) - dd)**1.5d0*(v2(2)**2 + &
                              (1.d0 - dd/absc(2))**(2.d0*alpha_drag/Rs)*sigmakp(3,k))
            endif

            ukp(:,k) = ukp(:,k)/mkp(k)
            sigmakp(:,k) = sigmakp(:,k)/mkp(k)
            sigmakp(1,k) = sigmakp(1,k) - ukp(1,k)**2
            if (Nene == 3) then
                sigmakp(2,k) = sigmakp(2,k) - ukp(1,k)*ukp(2,k)
                sigmakp(3,k) = sigmakp(3,k) - ukp(2,k)**2
            endif
            mkp(k) = mkp(k)*ratio_mn
        else
            mkp(k) = 0.d0
            ukp(:,k) = ug
            sigmakp(:,k) = 0.d0
        endif

        do k = 1, nsec
            if (isnan(mkp(k))) then
                write(*,*) 'NaN for mk, k=', k
                stop
            endif
            if (any(isnan(ukp(:,k)))) then
                write(*,*) 'NaN for uk, k=', k
                stop
            endif
        enddo
        if (method_order == 2) then
            call check_S(nsec, S, nkp, mkp, -1)
        endif
    ! End of time loop
    enddo
    return

end subroutine


subroutine quadrature_2nodes(a, b, mom, absc, wei, err)

    double precision, intent(in) :: a, b, mom(4)
    double precision, intent(out) :: absc(2), wei(2)
    integer, intent(out) :: err
    double precision :: mm(3), moy, sigma2, q, e, x, val, abmoy
    double precision :: p1, p2, p3, c1, c2, c3
    double precision, parameter :: mommin=0.d0
    double precision, parameter :: psi=1.d-10

    err = 0
    wei = 0.5d0*mom(1)*(/1.d0, 1.d0/)
    absc = 0.5d0*(a + b)*(/1.d0, 1.d0/)

    if (mom(1) <= mommin) then
        return
    endif

    mm = mom(2:4)/mom(1)
    c1 = (mm(1) - a)/(b - a)
    c2 = (mm(2) - 2.d0*a*mm(1) + a**2)/(b - a)**2
    c3 = (mm(3) - 3.d0*a*mm(2) + 3.d0*a**2*mm(1) - a**3)/(b - a)**3

    p1 = c1
    p2 = (c2 - c1**2)/c1/(1.d0 - c1)
    p3 = (1.d0 - c1)*(c1*c3 - c2**2)/(c2 - c1**2)/(c1 - c2)

    if (p1 <= 0.D0 .or. p1 >= 1.d0 .or. &
        p2 <= 0.D0 .or. p2 >= 1.d0 .or. &
        p3 <= 0.D0 .or. p3 >= 1.d0) then
        write(*,*) 'canonical moments:', p1, p2, p3
        write(*,*) 'a, b', a, b
        write(*,*) 'moments on [ab]', mm
        write(*,*) 'moments on [01]', c1, c2, c3
        err = 1
    endif

    moy = mm(1)
    sigma2 = mm(2) - mm(1)**2
    if (sigma2 <= 0.d0) then
        sigma2 = 1.d-12
    endif
    q = mm(3) - 3.d0*mm(2)*mm(1) + 2.d0*mm(1)**3
    e = q/sigma2
    x = 0.5d0*e/dsqrt(e*e + 4.d0*sigma2)
    wei(1) = 0.5d0 + x
    wei(2) = 0.5d0 - x
    val = (e + dsqrt(e*e + 4.d0*sigma2))**2/4.d0
    absc(1) = moy - dsqrt(sigma2**2/val)
    absc(2) = moy + dsqrt(val)
    if (absc(1) < a) then
        if (wei(1)*(a - absc(1)) > epsi) then
            write(*,*) 'Changing absc1 a', absc(1), a, a - absc(1), wei(1)
        endif
        absc(1) = a
    endif
    if (absc(2) < a) then
        if (wei(2) > epsi) then
            write(*,*) 'Changing absc2 a + eps', absc(2), a + epsi
        endif
        absc(2) = a + epsi
    endif
    if (absc(2) > b) then
        if (wei(2)*(b - absc(2)) > epsi) then
            write(*,*) 'changing absc2 b', absc(2), b, b - absc(2), wei(2)
        endif
        absc(2) = b
    endif
    if (absc(1) > b) then
        if (wei(1) > epsi) then
            write(*,*) 'changing absc1 b - eps', absc(1), b - epsi
        endif
        absc(1) = b - epsi
    endif

    abmoy = 0.5d0*( a +b)

    if (dabs(1.d0 - wei(1) - wei(2)) > epsi .or. &
        dabs(mm(1) - wei(1)*absc(1) - wei(2)*absc(2)) > epsi*max(1.d0, mm(1)) .or. &
        dabs(mm(2) - wei(1)*absc(1)**2 - wei(2)*absc(2)**2) > epsi*max(1.d0, mm(2)) .or. &
        dabs(mm(3) - wei(1)*absc(1)**3 - wei(2)*absc(2)**3) > epsi*max(1.d0, mm(3))) then
        write(*,*) 'Error in quadrature_2nodes'
        write(*,*) 'm0', 1.d0, wei(1) + wei(2), 1.d0 - wei(1) - wei(2)
        write(*,*) 'm1', mm(1), wei(1)*absc(1) + wei(2)*absc(2), (mm(1) - wei(1)*absc(1) - wei(2)*absc(2))/mm(1)
        write(*,*) 'm2', mm(2), wei(1)*absc(1)**2 + wei(2)*absc(2)**2, &
                    (mm(2) - wei(1)*absc(1)**2 - wei(2)*absc(2)**2)/mm(2)
        write(*,*) 'm3', mm(3), wei(1)*absc(1)**3 + wei(2)*absc(2)**3, &
                    (mm(3) - wei(1)*absc(1)**3 - wei(2)*absc(2)**3)/mm(3)
        write(*,*) 'sigma', mm(2) - mm(1)**2
        write(*,*) 'a, b', a, b
        write(*,*) 'mc', p1, p2, p3
        err=2
    endif
    wei = wei*mom(1)

    return

end subroutine

subroutine reconstruct_affine(Skm1, Sk, Sintak, Sintbk, nk, mk, Sa, Sb, alpha, beta)
! Recontruction d'une fonction fa(S) à partir de ses moments d'ordre 0 (nk)
! et 3/2 (mk) sur la section [Skm1,Sk].
! fa(S) est une fonction affine sur [Sa,Sb] et nulle en dehors de l'intervalle
! et est caractérisee par: fa(Sa)=alpha et fa(Sb)=beta
! 3 cas seulement sont possibles:
! - [Sa,Sb]=[Skm1,Sk]   <=> mk/nk dans [Sintak,Sintbk]
! - Sa=Skm1 et beta=0   <=> mk/nk dans ]Skm1,Sintak[
! - Sb=Sk   et alpha=0  <=> mk/nk dans ]Sintbk,Sk[

    double precision, intent(in) :: Skm1, Sk, Sintak, Sintbk, nk, mk
    double precision, intent(out) :: Sa, Sb, alpha, beta
    double precision :: dS, det, Rkm1, Rk, Ra, Rb, r, rpar(2), valpoly
    double precision, parameter :: Racc = 1.d-12
    double precision, parameter :: coeff = 0.5d0*35.d0**(1.d0/3.d0)

    dS = Sk - Skm1
    det = 0.5d0*(Sintbk - Sintak)
    if (mk*dS**2 >= 2.d0*Sintak*nk .and. mk*dS**2 <= 2.d0*Sintbk*nk) then
        ! case [Sa, Sb] = [Skm1, Sk]
        Sa = Skm1
        Sb = Sk
        alpha = (Sintbk/dS*nk - 0.5d0*dS*mk)/det
        beta = (-Sintak/dS*nk + 0.5d0*dS*mk)/det
    else if (mk > Skm1**1.5d0*nk .and. mk*dS**2 < 2.d0*Sintak*nk) then
        ! case Sa = Skm1 et beta = 0
        Rkm1 = dsqrt(Skm1)
        Rk = dsqrt(Sk)
        beta = 0.d0
        Sa = Skm1
        rpar(1) = Rkm1
        rpar(2) = mk/nk
        if (Skm1 /= 0.d0) then
           r = rpar(2)**(1.d0/3.d0)
           call muller_modified(Poly1, r, min(coeff*r, Rk), Racc, rpar, Rb, valpoly)
        else
           Rb = (35.d0*rpar(2)/8.d0)**(1.d0/3.d0)
        endif
        Sb = Rb**2
        alpha = 2.d0*nk/(Sb - Skm1)
    elseif (mk < Sk**1.5d0*nk .and. mk*dS**2 > 2.d0*Sintbk*nk) then
        ! cas Sb = Sk   et alpha = 0
        Rkm1 = dsqrt(Skm1)
        Rk = dsqrt(Sk)
        alpha = 0.d0
        Sb = Sk
        rpar(1) = Rk
        rpar(2) = mk/nk
        r = rpar(2)**(1.d0/3.d0)
        call muller_modified(Poly3, Rkm1, r, Racc, rpar, Ra, valpoly)
        Sa = Ra**2
        beta = 2.d0*nk/(Sk - Sa)
    else
        if (mk > 1.d-20) then
            write(*,*) 'Exiting moments space!'
            write(*,*) 'nk           =', nk
            write(*,*) 'mk           =', mk
            write(*,*) '(mk/nk)**2/3 =', (mk/nk)**(2.d0/3.d0)
            write(*,*) 'Skm1         =', Skm1
            write(*,*) 'Sk           =', Sk
        endif
        if (mk > epsi) then
            write(*,*) 'Problem in reconstruction!'
            write(*,*) 'nk           =', nk
            write(*,*) 'mk           =', mk
            write(*,*) '(mk/nk)**2/3 =', (mk/nk)**(2.d0/3.d0)
            write(*,*) 'Skm1         =', Skm1
            write(*,*) 'Sk           =', Sk
            stop
        endif
        Sa = Skm1
        Sb = Sk
        alpha = 5.d0*mk/(2.d0*(Sk**2.5d0 - Skm1**2.5d0))
        beta = alpha
    endif
    return

end subroutine

! Polynome for case 1
function Poly1(X, rpar)
! polynome P
    double precision, intent(in) :: X, rpar(*)
    double precision :: Poly1
    double precision :: Rkm, Vmean

    Rkm = rpar(1)
    Vmean = rpar(2)
    Poly1 = (5.d0*Rkm**3 - 8.75*Vmean)*Rkm**2 + X*((10.d0*Rkm**3-17.5*Vmean)*Rkm + &
             X*((8.d0*Rkm**3 - 8.75*Vmean) + X*(6.d0*Rkm**2 + X*(4.d0*Rkm + 2.d0*X))))
    return
end function

! Polynome for case 2
function Poly3(X,rpar)
! polynome Q
   double precision, intent(in) :: X, rpar(*)
   double precision :: Poly3
   double precision :: Rk, Vmean

   Rk = rpar(1)
   Vmean = rpar(2)
   Poly3 = (5.d0*Rk**3 - 8.75*Vmean)*Rk**2 + X*((10.d0*Rk**3 - 17.5*Vmean)*Rk &
            + X*((8.d0*Rk**3 - 8.75*Vmean) + X*(6.d0*Rk**2 + X*(4.d0*Rk + 2.d0*X))))

   return
end function

function affine(S,rpar)
! reconstructed function fa(S)
    double precision, intent(in) :: S, rpar(*)
    double precision :: affine
    double precision :: Sa, Sb, alpha, beta, k, val

    k = rpar(1)
    Sa = rpar(2)
    Sb = rpar(3)
    alpha = rpar(4)
    beta = rpar(5)
    if(S >= Sa .and. S <= Sb) then
        val = (alpha*(Sb - S) + beta*(S - Sa))/(Sb - Sa)
    else
        val = 0.d0
    endif
    affine = val*S**k

    return
end function


subroutine muller_modified(func, x1, x2, xacc, rpar, root, valfunc)
! USES func
! Using modfied Muller’s method, return the root of a function func known to lie between x1 and x2.
! The root, returned as root, will be refined to an approximate accuracy xacc.
! valfunc if the falue of func at the root
   double precision, intent(in) :: x1, x2, xacc, rpar(*)
   double precision, external :: func
   double precision, intent(out) :: root, valfunc
   integer, parameter :: MAXIT = 100
   double precision, parameter :: UNUSED = -1.11e30
   double precision :: xa, xb, xc, xd, r1, r2, fa, fb, fc, fcb, fba, fcba, gamma
   integer ::j

   fa = func(x1, rpar)
   fb = func(x2, rpar)
   if (fa*fb < 0.d0) then
      if (fa < 0.d0) then
         xa = x1
         xb = x2
      else
         fc = fa
         xa = x2
         fa = fb
         xb = x1
         fb = fc
      endif
      root = UNUSED ! Any highly unlikely value, to simplify logic below.
      do j = 1, MAXIT
         xc = 0.5d0*(xa + xb)
         fc = func(xc, rpar) ! First of two function evaluations per iteration.
         fcb = (fc - fb)/(xc - xb)
         fba = (fb - fa)/(xb - xa)
         fcba = (fcb - fba)/(xc - xa)
         gamma = fcb + (xc - xb)*fcba
         if (gamma**2 < 4.d0*fc*fcba) then
            write(*,*) 'pb in searching root of f'
            stop
         endif
         r1 = xc -2.d0*fc/(gamma + dsqrt(gamma**2 - 4.d0*fc*fcba)) ! Updating formula 1.
         r2 = xc -2.d0*fc/(gamma - dsqrt(gamma**2 - 4.d0*fc*fcba)) ! Updating formula 2.
         if ((r1 >= xa .and. r1 <= xb) .or. (r1 >= xb .and. r1 <= xa)) then
            xd = r1
         elseif((r2 >= xa .and. r2 <= xb) .or. (r2 >= xb .and. r2 <= xa)) then
            xd = r2
         else
            write(*,*) 'pb in muller modified', xa, r1, r2, xb, r1 - xa, r2 - xa, xb - xa
            stop
         endif
         if (dabs(xd - root) <= xacc) then
             return
         else
             root = xd
             valfunc = func(root, rpar) ! Second of two function evaluations per iteration
             if (valfunc < 0.d0) then ! Bookkeeping to keep the root bracketed on next iteration.
                xa = root
                fa = valfunc
                if (fc > 0.d0) then
                   xb = xc
                   fb = fc
                endif
             elseif (valfunc > 0.d0) then
                 xb = root
                 fb = valfunc
                 if (fc < 0.d0)  then
                    xa = xc
                    fa = fc
                 endif
             else
                return
             endif
             if (dabs(xa - xb) <= xacc) then
                 return
             endif
         endif
      enddo
      write(*,*)  'muller_modified exceeds maximum iterations'
   elseif (fa == 0.d0) then
      root = x1
      valfunc = fa
   elseif (fb == 0.d0) then
      root  = x2
      valfunc = fb
   else
      write(*,*)  'root must be bracketed in muller_modified', fa, fb, x1, x2
      stop
   endif
   return
end subroutine

end module
