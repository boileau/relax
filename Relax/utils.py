# -*- coding: utf-8 -*-
"""
Some utilitary functions
"""

import argparse
from copy import deepcopy
from itertools import product
import imp
import re
import sys


def parsearg():
    """Parse relax.py argument to get input file name"""
    parser = argparse.ArgumentParser(description="Run 1D/2D simulation")
    parser.add_argument(dest='rundir', nargs='?', metavar="RunDir",
                        default='.', help="Directory of input file run.py")
    parser.add_argument('--batch', help="Run in batch (no plotting)",
                        action="store_true")
    parser.add_argument('--noplot', help="Display parameters but no plot",
                        action="store_true")
    args = parser.parse_args()

    rundir = args.rundir

    if args.batch:
        verbose = 0
    elif args.noplot:
        verbose = 1
    else:
        verbose = 2

    return {'rundir': rundir, 'verbose': verbose}


def parsefile(rundir):
    """Parse input file to get simulation parameters values"""
    # Module import_file by ubershmekel
    rundict = {}
    mod_name = "run"
    fhandle = None
    try:
        try:
            tup = imp.find_module(mod_name, [rundir])
        except ImportError as err:
            msg = "Input file {}/{}.py not found".format(rundir, mod_name)
            sys.exit(msg)
        param = imp.load_module(mod_name, *tup)
        # Clean all existing attribute from a previous import
        # (required for multiple instanciations in the same python shell)
        for name in dir(param):
            if not name.startswith('_'):
                delattr(param, name)
        # Import again
        param = imp.load_module(mod_name, *tup)
        # Update parameters with values of module attributes
        for attr in dir(param):
            # exclude builtin attributes and math import
            if not (re.match(r'__*', attr) or re.match(r'math', attr)):
                # Update rundict with module attributes
                rundict[attr] = getattr(param, attr)

        fhandle = tup[0]

    finally:
        if fhandle:
            fhandle.close()

    return rundict


def cart_product(basedict):
    """Return the names and Cartesian product of varying parameters"""
    # Build a dict of input parameters that are tuples
    var_param = {name: val for name, val in basedict.items()
                 if type(val) is tuple}
    names = list(var_param.keys())
    args = tuple(var_param.values())
    prod = [comb for comb in product(*args)]  # Get all the combinations

    return names, prod


def build_suite(basedict):
    """
    Return a list of run parameters dictionaries by interpretating input
    parameters that tuples.
    """
    # Create a set of parameters combination
    names, prod = cart_product(basedict)
    if names:  # some parameters are varying
        rundictlist = []
        for comb in prod:  # Loop on all combinations
            rundict = deepcopy(basedict)
            # parameters combination is written in solution file name
            if 'solfile' in list(rundict.keys()):
                solname = rundict['solfile'][:-3]
            else:
                solname = 'solution'
            # iter varying parameters to build a rundict
            for name, val in zip(names, comb):
                rundict[name] = val
                solname = solname + "_{}{}".format(name, val)
            rundict['solfile'] = solname + '.h5'
            rundictlist.append(rundict)
        return rundictlist
    else:  # No varying parameter
        return [basedict]
