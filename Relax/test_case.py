# -*- coding: utf-8 -*-
"""
This module is used to initialize a test cases according to the user input
parameters.
"""
from numpy import asfortranarray, cos, exp, floor, linspace, nan, pi, power, \
                  sin, sinc, sqrt, transpose, zeros, zeros_like
from Relax import libfortran as libfort
from Relax import nvar, small
import sys

modfort = libfort.tsm_aff_evap_drag


def init_solution(section):
    """ Initialize solution for given test case."""

    sim = section.sim
    if sim.casetype == "SOD":
        # SOD (fig. 2)
        sim.is2D = False
        init1D(section)
        initSOD(V=section.V,
                Xgrid=sim.Xgrid,
                middle=sim.Lx/2.0,
                rhoL=1.0,
                uL=0.0,
                p11L=1.0,
                p12L=0.0,
                p22L=0.0,
                rhoR=0.125,
                uR=0.0,
                p11R=0.1,
                p12R=0.0,
                p22R=0.0,
                gamma=sim.gamma)
        sim.BCx = 'closed'
        sim.BCy = 'non applicable'

    elif sim.casetype == "Bouchut":
        # figure1 (cas test de Bouchut)
        sim.is2D = False
        init1D(section)
        section.V[:, :] = 0.0e0
        for i in range(section.V.shape[1]):
            x = sim.Xgrid[i]
            section.V[0, i] = 0.5
            section.V[3:, i] = 0.0
            if (x < 0.5):
                section.V[1, i] = -0.5
            elif (x < 1.0):
                section.V[1, i] = 0.4
            elif (x < 1.8):
                section.V[1, i] = 0.4 - (x-1.0)
            else:
                section.V[1, i] = -0.4

        sim.BCx = 'closed'
        sim.BCy = 'Non applicable'

    elif sim.casetype == "SISC-fig2":
        # SOD (fig. 2)
        sim.is2D = False
        init1D(section)
        initSOD(V=section.V,
                Xgrid=sim.Xgrid,
                middle=sim.Lx/2.0,
                rhoL=1.0,
                uL=0.0,
                p11L=1.1,
                p12L=0.0,
                p22L=0.0,
                rhoR=0.125,
                uR=0.0,
                p11R=0.0,
                p12R=0.0,
                p22R=0.0,
                gamma=sim.gamma)
        sim.BCx = 'closed'
        sim.BCy = 'non applicable'

    elif sim.casetype == "SISC-fig3":
        # SOD (fig. 3)
        sim.is2D = False
        init1D(section)
        initSOD(V=section.V,
                Xgrid=sim.Xgrid,
                middle=sim.Lx/4.0,
                rhoL=1.0,
                uL=0.0,
                p11L=1.0,
                p12L=0.0,
                p22L=0.0,
                rhoR=0.125,
                uR=0.0,
                p11R=0.0,
                p12R=0.0,
                p22R=0.0,
                gamma=sim.gamma)
        addDirac(V=section.V, Xgrid=sim.Xgrid, init=65)
        sim.BCx = 'closed'
        sim.BCy = 'non applicable'

    elif sim.casetype == "SOD-AG":
        # SOD shock tube from Berthon 2006
        sim.is2D = False
        init1D(section)
        initSOD(V=section.V,
                Xgrid=sim.Xgrid,
                middle=sim.Lx/2.0,
                rhoL=1.0,
                uL=0.0,
                p11L=2.0,
                p12L=0.05,
                p22L=0.6,
                rhoR=0.125,
                uR=0.0,
                p11R=0.2,
                p12R=0.1,
                p22R=0.2,
                gamma=sim.gamma)
        sim.BCx = 'closed'
        sim.BCy = 'non applicable'

    elif sim.casetype == "SOD-AG-2D-x":
        # 2D SOD shock tube from Berthon 2006 in x-direction
        sim.is2D = True
        init2D(section)
        initSOD_2D(sim,
                   V=section.V,
                   Xgrid=sim.Xgrid,
                   middle=sim.Lx/2.0,
                   rhoL=1.0,
                   uL=0.0,
                   p11L=2.0,
                   p12L=0.05,
                   p22L=0.6,
                   rhoR=0.125,
                   uR=0.0,
                   p11R=0.2,
                   p12R=0.1,
                   p22R=0.2,
                   gamma=sim.gamma)
        sim.BCx = 'outflow'
        sim.BCy = 'periodic'

    elif sim.casetype == "SOD-AG-2D-y":
        # 2D SOD shock tube from Berthon 2006 in y-direction
        sim.is2D = True
        init2D(section)
        initSOD_2D(sim,
                   V=section.V,
                   Xgrid=sim.Xgrid,
                   middle=sim.Lx/2.0,
                   rhoL=1.0,
                   uL=0.0,
                   p11L=2.0,
                   p12L=0.05,
                   p22L=0.6,
                   rhoR=0.125,
                   uR=0.0,
                   p11R=0.2,
                   p12R=0.1,
                   p22R=0.2,
                   gamma=sim.gamma)
        libfort.transport.transpose_2d(section.V, section.is_energy)
        sim.BCx = 'periodic'
        sim.BCy = 'outflow'

    elif sim.casetype == "drag0D":
        # Constant solution with 0 velocity and with drag
        sim.is2D = False
        init1D(section)
        section.V[0, :] = 1.0
        # Relax towards axial velocity = 1.0
        sim.mu[0, :] = 1.0

        sim.BCx = 'periodic'
        sim.BCy = 'non applicable'

    elif sim.casetype == "dragandevap0D":
        # Constant solution with 0 velocity and with drag
        sim.is2D = False
        init1D(section)
        section.V[0, :] = 1.0
        sim.mu[0, :] = 1.0  # Relax towards axial velocity = 1.0

        sim.BCx = 'periodic'
        sim.BCy = 'non applicable'

    elif sim.casetype == "Figure4":
        # figure4 of SISC paper
        sim.is2D = True
        init2D(section)
        sincardinal(section.V,
                    sim.Xgrid,
                    centerx=0.625,
                    centery=0.625,
                    radius=0.125)

        sim.mu = 0.
        sim.mu = taylor(sim.Xgrid)
        sim.mu[2:5, :, :] = 0.
        sim.BCx = 'periodic'
        sim.BCy = 'periodic'

    elif sim.casetype == "Figure5":
        sim.is2D = True
        init2D(section)
        section.V[0] = 1.0
        sim.mu = 0.
        sim.mu = taylor(sim.Xgrid)
        sim.mu[3:5, :, :] = 0.
        sim.BCx = 'periodic'
        sim.BCy = 'periodic'

    elif sim.casetype == "Figure6":
        sim.is2D = True
        init2D(section)
        sincardinal(section.V, sim.Xgrid, centerx=0.625, centery=0.875,
                    radius=0.125)
        sincardinal(section.V, sim.Xgrid, centerx=0.625, centery=0.125,
                    radius=0.125)
        # Ensure symmetry
        section.V[0, :, :] = 0.5 * (section.V[0, :, :] + section.V[0, :, ::-1])

        sim.mu = 0.
        sim.mu = taylor(sim.Xgrid)
        sim.mu = energy_strip(sim, model='iso')

        sim.BCx = 'periodic'
        sim.BCy = 'periodic'

    elif sim.casetype == "Figure6-AG":
        sim.is2D = True
        init2D(section)
        sincardinal(section.V, sim.Xgrid, centerx=0.625, centery=0.875,
                    radius=0.125)
        sincardinal(section.V, sim.Xgrid, centerx=0.625, centery=0.125,
                    radius=0.125)

        sim.mu = 0.
        sim.mu = taylor(sim.Xgrid)
        energy_strip(sim, model='AG')

        # Ensure symmetry
        section.V[0, :, :] = 0.5 * (section.V[0, :, :] + section.V[0, :, ::-1])

        sim.BCx = 'periodic'
        sim.BCy = 'periodic'

    elif sim.casetype == "ICMF2016-2D":
        sim.is2D = True
        init2D(section)

        # Bind V to a slice of Vk for transport step
        section.V = sim.Vk[:, :, :, section.k]

        # initialize polydisperse distribution
        poly_distrib_2D_TG(section)

        sim.BCx = 'periodic'
        sim.BCy = 'periodic'

    elif sim.casetype == "crenel1D":
        sim.is2D = False
        init1D(section)
        rho_int = 1.0
        X = sim.Xgrid
        xmin = 0.25
        xmax = 0.75
        section.V[0, :] = 0.1
        section.V[1, :] = 1.

        mask = ((X[:] > xmin) * (X[:] <= xmax))
        section.V[0, mask] = rho_int
        sim.BCx = 'periodic'
        sim.BCy = 'periodic'

    elif sim.casetype == "gauss1D":
        sim.is2D = False
        init1D(section)
        X = sim.Xgrid
        x0 = 0.5
        sigma = 0.025
        amp = 1.0
        rho = amp*gaussian(X, x0, sigma)
        section.V[0, :] = rho
        section.V[1, :] = 1.
        section.V[6, :] = 1.

        sim.BCx = 'periodic'

    elif sim.casetype == "gauss1D_muk":
        sim.is2D = False
        init1D(section)
        X = sim.Xgrid
        x0 = 0.5
        sigma = 0.025
        amp = 1.0
        muk = amp*gaussian(X, x0, sigma)
        section.V[0, :] = 1.
        section.V[1, :] = 1.
        section.V[6, :] = muk

        sim.BCx = 'periodic'

    elif sim.casetype == "square":
        # Put a square of rho
        sim.is2D = True
        init2D(section)
        square(X=sim.Xgrid,
               V=section.V,
               x_center=0.5, y_center=0.5, vertex=0.5,
               rho_ext=0.1, u_ext=1., v_ext=1., p_ext=0.,
               rho_int=1.0, u_int=1., v_int=1., p_int=0.,
               gamma=sim.gamma)
        sim.BCx = 'periodic'
        sim.BCy = 'periodic'

    elif sim.casetype == "evap&drag0D":
        sim.is2D = False
        sim.BCx = 'periodic'
        init1D(section)
        poly_distrib_0D(section)  # initialize polydisperse distribution
        # Bind V to a slice of Vk for transport step
        section.V = sim.Vk[:, :, section.k]

    elif sim.casetype == "evap&drag0D_FL":
        sim.is2D = False
        sim.BCx = 'periodic'
        init1D(section)
        poly_distrib_0D_FL(section)  # initialize polydisperse distribution
        # Bind V to a slice of Vk for transport step
        section.V = sim.Vk[:, :, section.k]

    elif sim.casetype in ("evap&drag1D",
                          "evap&drag1D-novoid",
                          "evap&drag1D-homo",
                          "evap&drag1D-mono",
                          "evap&drag2D_x"):

        if '1D' in sim.casetype:
            sim.is2D = False
            init1D(section)
        else:
            sim.is2D = True
            sim.BCx = 'outflow'
            sim.BCy = 'periodic'
            init2D(section)

        sim.use_nmax = False
        if sim.casetype == "evap&drag1D-homo":
            sim.BCx = 'outflow'
            if sim.isAllST:
                case_type = 3
            else:
                case_type = 4
        else:
            if sim.isAllST:
                case_type = 1
                sim.use_nmax = True
            else:
                case_type = 2
        if sim.casetype == "evap&drag1D-mono":
            # init_polydisp(10)
            poly = False
        else:
            poly = True

        novoid = (sim.casetype == "evap&drag1D-novoid")
        if sim.is2D:
            # Bind V to a slice of Vk for transport step
            section.V = sim.Vk[:, :, :, section.k]
            # initialize polydisperse distribution
            poly_distrib_2D(section, case_type=case_type)
        else:
            section.V = sim.Vk[:, :, section.k]
            poly_distrib_1D(section, case_type=case_type, novoid=novoid,
                            poly=poly)

        if sim.compute_ref:
            sim.reference = ref_compute_gauss
            section.V_ref = sim.Vk_ref[:, :, section.k]

#    elif sim.casetype == "evap&drag2D_x":
#        sim.BCx = 'outflow'
#        sim.BCy = 'periodic'
#        init2D(section)
#        poly_distrib_2D(section)  # initialize polydisperse distribution
#        # Bind V to a slice of Vk for transport step
#        section.V = sim.Vk[:, :, :, section.k]

    else:
        exit("Unknown test case: " + sim.casetype)

    if sim.isSecST:
        if sim.is2D:
            sim.maxmu = abs(sim.mu[0:1, :, :]).max()
        else:
            sim.maxmu = abs(sim.mu[0:1, :]).max()

    else:
        sim.maxmu = small

    # update sim.paramdict
    sim.update('is2D', sim.is2D)
    sim.update('BCx', sim.BCx)
    sim.update('BCy', sim.BCy)

    sim.ref_time = -1.  # Initialize reference time


def init1D(section):
    """Initialize Fortran-ordered arrays for 1D cases"""

    sim = section.sim
    N = sim.N
    dx = sim.dx
    Lx = sim.Lx
    section.V = zeros((nvar, N+2), order='f')
    section.V2 = zeros_like(section.V)
    section.V_inter = zeros((2*nvar, N+2), order='f')
    section.F = zeros((nvar, N+2), order='f')
    section.is_energy = zeros(N+2, dtype='i', order='f')
    section.is_energy2 = zeros_like(section.is_energy)
    section.F[:, 0] = nan  # No need for flux in 0
    if sim.isSecST:
        sim.mu = zeros((3, N+2), order='f')

    sim.Xgrid = zeros(N+2, order='f')
    sim.Xgrid = linspace(sim.xmin-dx/2, sim.xmin+Lx+dx/2, N+2)


def init2D(section):
    """Initialize Fortran-ordered arrays for 2D cases"""

    sim = section.sim
    N = sim.N
    dx = sim.dx
    Lx = sim.Lx
    section.V = zeros((nvar, N+2, N+2), order='f')
    section.V2 = zeros_like(section.V)
    section.V_inter = zeros((2*nvar, N+2, N+2), order='f')
    section.F = zeros((nvar, N+2, N+2), order='f')
    section.is_energy = zeros((N+2, N+2), dtype='i', order='f')
    section.is_energy2 = zeros_like(section.is_energy)

    sim.Xgrid = zeros((2, N+2, N+2), order='f')
    # Set Y coordinate
    sim.Xgrid[1] = linspace(-dx/2, Lx+dx/2, N+2)
    # Set X coordinate
    sim.Xgrid[0] = transpose(sim.Xgrid[1])


def init_polydisp(sim):
    """
    Initialize arrays data for polydisperse cases
    """
    nsect = sim.nsect
    N = sim.N  # Number of grid points
    if sim.is2D:
        sim.Vk = zeros((nvar, N+2, N+2, nsect), order='f')
    else:
        sim.Vk = zeros((nvar, N+2, nsect), order='f')
        if sim.compute_ref:  # Compute reference solution
            sim.Vk_ref = zeros((nvar, N+2, nsect), order='f')
    sim.S = zeros(nsect+1, order='f')
    sim.Sinta = zeros(nsect, order='f')
    sim.Sintb = zeros_like(sim.Sinta)
    sim.dS = sim.Smax/nsect
    sim.S[nsect] = sim.Smax  # Store last section size
    sim.ratio_mn = sim.rhol/(6.0*sqrt(pi))

    sim.dt_evap = sim.CFL_evap*sim.Smax/(sim.Rs*1.e12*float(sim.nsect) +
                                         small)
    sim.dt_drag = 0.

    # Set parameters in Fortran module
    libfort.tsm_aff_evap_drag.alpha_drag = sim.alpha_drag
    libfort.tsm_aff_evap_drag.rs = sim.Rs
    libfort.tsm_aff_evap_drag.ratio_mn = sim.ratio_mn
    libfort.tsm_aff_evap_drag.smax = sim.Smax
    libfort.tsm_aff_evap_drag.method_order = sim.tsm_order


def compute_dt_drag(sim):
    ug = sim.ug
    ugmax = abs(ug).max()
    if ugmax > 0.:
        dt_drag = sim.CFL*sim.dx/ugmax
    else:
        dt_drag = 0.
    return dt_drag


def initSOD(V, Xgrid, middle, rhoL, uL, p11L, p12L, p22L,
            rhoR, uR, p11R, p12R, p22R, gamma):
    """Initialize SOD problem (shock tube)"""

    V[:, :] = 0.0e0
    V[0, :] = (Xgrid <= middle)*rhoL + (Xgrid > middle)*rhoR
    V[1, :] = (Xgrid <= middle)*uL + (Xgrid > middle)*uR
    V[3, :] = (Xgrid <= middle)*p11L/((gamma-1.0)*rhoL) +\
              (Xgrid > middle)*p11R/((gamma-1.0)*rhoR)
    V[4, :] = (Xgrid <= middle)*p12L/((gamma-1.0)*rhoL) +\
              (Xgrid > middle)*p12R/((gamma-1.0)*rhoR)
    V[5, :] = (Xgrid <= middle)*p22L/((gamma-1.0)*rhoL) +\
              (Xgrid > middle)*p22R/((gamma-1.0)*rhoR)
    V[0, 0] = rhoL
    V[0, -1] = rhoR
    V[1, 0] = uL
    V[1, -1] = uR
    V[3, 0] = p11L/((gamma-1.0)*rhoL)
    V[3, -1] = p11R/((gamma-1.0)*rhoR)
    V[4, 0] = p12L/((gamma-1.0)*rhoL)
    V[4, -1] = p12R/((gamma-1.0)*rhoR)
    V[5, 0] = p22L/((gamma-1.0)*rhoL)
    V[5, -1] = p22R/((gamma-1.0)*rhoR)


def initSOD_2D(sim, V, Xgrid, **args):
    """Initialize a 2D SOD problem (shock tube)"""
    for i in range(sim.N+2):
        initSOD(V[:, :, i], Xgrid[0, :, i], **args)


def addDirac(V, Xgrid, init):
    """Add a density Dirac to a given solution"""
    V[1, init] = -0.0625
    V[1, init+1] = -0.1875
    V[1, init+2] = -0.3125
    V[1, init+3] = -0.4375
    V[1, init+4] = -0.4375
    V[1, init+5] = -0.3125
    V[1, init+6] = -0.1875
    V[1, init+7] = -0.0625


def sincardinal(V, X, centerx, centery, radius):
    """Add a density cardinal sinus to a given solution"""
    N = V.shape[1] - 2
    for i in range(1, N+1):
        for j in range(1, N+1):
            dist = sqrt((X[0, i, j] - centerx)**2 + (X[1, i, j] - centery)**2)
            if dist < radius:
                V[0, i, j] = V[0, i, j] + sinc(dist/radius)

    return V


def gaussian(x, x0, sigma):
    """Gaussian function"""
    return exp(-power((x - x0)/(2.*sigma), 2.))


def taylor(X):
    """Set a Taylor-Green gaseous velocity field"""

    N = X.shape[1]-2
    mu = zeros((5, N+2, N+2), order='f')
    for i in range(1, N+1):
        for j in range(1, N+1):
            x = X[0, i, j] * 2.0e0 * pi
            y = X[1, i, j] * 2.0e0 * pi
            mu[0, i, j] = sin(x)*cos(y)
            mu[1, i, j] = -cos(x)*sin(y)
            mu[2, i, j] = 1.0
            mu[3, i, j] = 0.0
            mu[4, i, j] = 0.0
    # Symmetrize to avoid rounding errors
    mu[0, :, :] = 0.5*(mu[0, :, :] + mu[0, :, ::-1])
    mu[0, :, :] = 0.5*(mu[0, :, :] - mu[0, ::-1, :])
    mu[1, :, :] = 0.5*(mu[1, :, :] + mu[1, ::-1, :])
    mu[1, :, :] = 0.5*(mu[1, :, :] - mu[1, :, ::-1])
    return mu


def energy_strip(sim, model):
    """Define a strip of energy source term"""

    X = sim.Xgrid
    mu = sim.mu

    mu[2:5, :, :] = 0.  # Reset energy

    if model == 'iso':
        axis = 2
    elif model == 'AG':
        axis = 4
    else:
        msg = "Wrong model in energy_strip()"
        sys.exit(msg)

    N = sim.N
    epsilon_t_max = 0.5
    radius = 0.125
    for i in range(1, N+1):
        for j in range(1, N+1):
            distance = abs(X[1, i, j] - 0.5)
            if (distance < radius):
                mu[axis, i, j] = epsilon_t_max*sinc(distance/radius)
            else:
                mu[axis, i, j] = 0.
    # To ensure exact symmetry:
    mu[axis, :, :] = 0.5 * (mu[axis, :, :] + mu[axis, :, ::-1])

    return mu


def square(X, V, x_center, y_center, vertex,
           rho_ext, u_ext, v_ext, p_ext,
           rho_int, u_int, v_int, p_int,
           gamma):
    """Add constant value in a square area to a given field"""

    e_ext = p_ext/((gamma-1.0)*rho_ext)
    e_int = p_int/((gamma-1.0)*rho_int)

    V[0, :, :] = rho_ext
    V[1, :, :] = u_ext
    V[2, :, :] = v_ext
    V[3:, :, :] = e_ext

    xmin = x_center - vertex/2.
    xmax = x_center + vertex/2.
    ymin = y_center - vertex/2.
    ymax = y_center + vertex/2.

    mask = (((X[0, :, :] > xmin) * (X[0, :, :] <= xmax)) *
            ((X[1, :, :] > ymin) * (X[1, :, :] <= ymax)))

    V[0, mask] = rho_int
    V[1, mask] = u_int
    V[2, mask] = v_int
#    V[3:6,mask] = e_int


def init_sizes(section):
    """Initiliaze section sizes"""

    sim = section.sim
    k = section.k

    dS = sim.dS
    S = k*dS
    Sp = (k+1)*dS
    Sm52 = (Sp**2.5 - S**2.5)/2.5

    Sm72 = (Sp**3.5 - S**3.5)/3.5
    sim.Sinta[k] = Sp*Sm52 - Sm72
    sim.Sintb[k] = Sm72-S*Sm52

    sim.S[k] = S

    section.S = S
    section.Sp = Sp


def init_single_section(section):
    """Initiliaze section parameters"""

    sim = section.sim
    nsect = 10
    dS = sim.Smax/nsect
    k = section.k

    S = k*dS
    Sp = (k+1)*dS
    Sm52 = (Sp**2.5 - S**2.5)/2.5

    Sm72 = (Sp**3.5 - S**3.5)/3.5
    sim.Sinta[k] = Sp*Sm52 - Sm72
    sim.Sintb[k] = Sm72-S*Sm52

    sim.S[k] = S
    sim.S[nsect] = nsect*dS
    sim.dS = dS

    sim.ratio_mn = sim.rhol/(6.0*sqrt(pi))

    sim.dt_evap = sim.CFL_evap*sim.Smax/(sim.Rs*1.e12*float(sim.nsect) +
                                         small)

    # Set parameters in Fortran module
    libfort.tsm_aff_evap_drag.alpha_drag = sim.alpha_drag
    libfort.tsm_aff_evap_drag.rs = sim.Rs
    libfort.tsm_aff_evap_drag.ratio_mn = sim.ratio_mn
    libfort.tsm_aff_evap_drag.smax = sim.Smax
    libfort.tsm_aff_evap_drag.method_order = sim.tsm_order

    section.S = S
    section.Sp = Sp


def poly_distrib_0D(section):
    """Set a polydisperse distribution (See J. Lagarde's PdS2015)"""

    init_sizes(section)

    sim = section.sim
    sim.Nene = 1  # Number of energies components
    sim.ug = zeros((1, sim.N+2), order='f')  # Gas velocity
    ug = 4.0
    sim.ug[:] = ug

    rpar = zeros(4, order='f')
    rpar[0] = 0.0  # time t
    # Rs - unity conversion: m**2/s -> micron**2/s
    rpar[1] = sim.Rs*1e12
    # alpha_drag - unity conversion: m**2/s -> micron**2/s
    rpar[2] = sim.alpha_drag*1e12
    rpar[3] = ug

    # Define the Fortran subroutine for computing phase transport
    # Here: velocity and sigma are single-component
    IntRomberg = modfort.intromberg

    eps = 1e-6  # convergence parameter for IntRomberg
    args = section.S, section.Sp, eps, rpar
    area0 = IntRomberg(modfort.get_nk, *args)
    area1 = IntRomberg(modfort.get_mk, *args)
    area2 = IntRomberg(modfort.get_mkuk, *args)
    area3 = IntRomberg(modfort.get_energy, *args)

    k = section.k
    nk = 1.e-18 * area0
    mk = 1.e-18 * sim.ratio_mn * area1
    sim.Vk[0, :, k] = mk
    sim.Vk[1, :, k] = area2/area1
    sim.Vk[3, :, k] = area3/area1 - (area2/area1)**2
    sim.Vk[6, :, k] = nk/mk  # muk


def poly_distrib_0D_FL(section):
    """Set a polydisperse distribution (See Boileau et al. ICMF2016)"""

    init_sizes(section)

    sim = section.sim
    sim.Nene = 3  # Number of energies components
    sim.ug = zeros((2, sim.N+2), order='f')  # Gas velocity
    ugx = 4.0
    ugy = 2.0
    sim.ug[0, :] = ugx  # X-component of gas velocity
    sim.ug[1, :] = ugy  # Y-component of gas velocity

    rpar = zeros(6, order='f')
    rpar[0] = 0.0  # time t
    # Rs - unity conversion: m**2/s -> micron**2/s
    rpar[1] = sim.Rs*1e12
    # alpha_drag - unity conversion: m**2/s -> micron**2/s
    rpar[2] = sim.alpha_drag*1e12
    rpar[3] = ugx
    rpar[4] = ugy

    # Define the Fortran subroutine for computing phase transport
    # Here: 2-component velocity and 3-component sigma
    IntRomberg = modfort.intromberg

    eps = 1e-6  # convergence parameter for IntRomberg
    args = section.S, section.Sp, eps, rpar
    area0 = IntRomberg(modfort.get_nk, *args)
    area1 = IntRomberg(modfort.get_mk, *args)
    area21 = IntRomberg(modfort.get_mkuk1, *args)
    area22 = IntRomberg(modfort.get_mkuk2, *args)
    area31 = IntRomberg(modfort.get_energy11, *args)
    area32 = IntRomberg(modfort.get_energy12, *args)
    area33 = IntRomberg(modfort.get_energy22, *args)

    k = section.k
    nk = 1.e-18 * area0
    mk = 1.e-18 * sim.ratio_mn * area1
    sim.Vk[0, :, k] = mk
    sim.Vk[1, :, k] = area21/area1
    sim.Vk[2, :, k] = area22/area1
    sim.Vk[3, :, k] = area31/area1 - (area21/area1)**2
    sim.Vk[4, :, k] = area32/area1 - (area21/area1)*(area22/area1)
    sim.Vk[5, :, k] = area33/area1 - (area22/area1)**2
    sim.Vk[6, :, k] = nk/mk  # muk


def poly_init_gauss_1D(t, section, V):

    Gauss_50 = modfort.gauss_50
    sim = section.sim
    rpar = sim.ref['rpar']
    rpar[0] = t

    for i in range(0, sim.N+2):
        rpar[3] = sim.Xgrid[i]
        args = section.S, section.Sp, sim.ref['rpar']
        area0 = Gauss_50(sim.ref['get_nk'], *args)
        area1 = Gauss_50(sim.ref['get_mk'], *args)
        area21 = Gauss_50(sim.ref['get_mkuk'], *args)
        area31 = Gauss_50(sim.ref['get_energy_1d'], *args)

        nk = 1.e-18 * area0
        mk = 1.e-18 * sim.ratio_mn * area1
        V[0, i] = mk
        if area1 != 0:
            V[1, i] = area21/area1
            V[3, i] = area31/area1 - (area21/area1)**2
            V[6, i] = nk/mk  # muk
        else:
            V[1, i] = 0.
            V[3, i] = 0.
            V[6, i] = 0.


def ref_compute_gauss(sim, t, Vk, X):
    sim.ref['rpar'][0] = t
    fort_compute_gauss = libfort.tsm_aff_evap_drag.compute_gauss_1d
    fort_compute_gauss(Vk,
                       sim.ref['get_nk'],
                       sim.ref['get_mk'],
                       sim.ref['get_mkuk'],
                       sim.ref['get_energy_1d'],
                       sim.ref['rpar'],
                       X, sim.S, sim.use_nmax)


def poly_case_type(sim, case_type, rpar):
    """Return a ref dict according to case_type"""

    if case_type == 1:
        # Parameters of the Gaussian function
        x0 = 0.02
        sigmax = x0**2/20./2.
        rpar[1] = sim.Rs*1e12  # Rs - unity conversion: m**2/s -> micron**2/s
        rpar[2] = sim.alpha_drag/sim.Rs
        rpar[4] = x0
        rpar[5] = sigmax
        get_nk = modfort.get_nk_1d
        get_mk = modfort.get_mk_1d
        get_mkuk = modfort.get_mkuk_1d
        get_energy_1d = modfort.get_energy_1d

    elif case_type == 2:
        x0 = 0.02
        sigmax = x0**2/20./2.
        rpar[1] = 0.
        rpar[2] = 0.
        rpar[4] = x0
        rpar[5] = sigmax
        get_nk = modfort.get_nk_t1d
        get_mk = modfort.get_mk_t1d
        get_mkuk = modfort.get_mkuk_t1d
        get_energy_1d = modfort.get_energy_t1d

    elif case_type == 3:
        rpar[1] = sim.Rs*1e12  # Rs - unity conversion: m**2/s -> micron**2/s
        rpar[2] = sim.alpha_drag*1e12
        rpar[4] = -sim.xmin
        get_nk = modfort.get_nk_t
        get_mk = modfort.get_mk_t
        get_mkuk = modfort.get_mkuk_t
        get_energy_1d = modfort.get_energy_t

    elif case_type == 4:
        rpar[1] = 0.
        rpar[2] = 0.
        rpar[4] = -sim.xmin
        get_nk = modfort.get_nk_tt
        get_mk = modfort.get_mk_tt
        get_mkuk = modfort.get_mkuk_tt
        get_energy_1d = modfort.get_energy_tt

    else:
        sys.exit("Wrong case type in poly_distrib_1D:", case_type)

    return {'rpar': rpar,
            'get_nk': get_nk,
            'get_mk': get_mk,
            'get_mkuk': get_mkuk,
            'get_energy_1d': get_energy_1d
            }


def poly_distrib_1D(section, novoid=False, case_type=1, poly=True):
    """Set a polydisperse distribution (See Boileau et al. ICMF2016)"""

    if poly:
        init_sizes(section)
    else:
        init_single_section(section)
    modfort.novoid = novoid

    sim = section.sim
    sim.Nene = 1  # Number of energies components
    sim.ug = zeros((1, sim.N+2), order='f')  # Gas velocity
    sim.ug[0, :] = 0.  # X-component of gas velocity

    rpar = zeros(6, order='f')
    rpar[0] = sim.t0  # time t

    sim.ref = poly_case_type(sim, case_type, rpar)

    # Initialize with analytical solution
    if case_type == 1:
        if section.k == sim.nsect - 1:  # Only compute once
            ref_compute_gauss(sim, sim.t0, sim.Vk, sim.Xgrid)
    else:
        poly_init_gauss_1D(sim.t0, section, section.V)


def poly_distrib_2D(section, case_type=1):
    """Set a polydisperse distribution (See Boileau et al. ICMF2016)"""

    init_sizes(section)

    sim = section.sim
    sim.Nene = 3  # Number of energies components

    sim.ug = zeros((2, sim.N+2, sim.N+2), order='f')  # Gas velocity
    sim.ug[0, :] = 0.  # X-component of gas velocity
    sim.ug[1, :] = 0.  # Y-component of gas velocity

    rpar = zeros(6, order='f')
    rpar[0] = sim.t0  # time t

    sim.ref = poly_case_type(sim, case_type, rpar)

    # Initialize with analytical solution
    if case_type == 1:
        if section.k == sim.nsect - 1:  # Only compute once
            X_1D = asfortranarray(sim.Xgrid[0, :, 0])
            for j in range(0, sim.N+2):
                Vk_1D = asfortranarray(sim.Vk[:, :, j, :])
                ref_compute_gauss(sim, sim.t0, Vk_1D, X_1D)
                sim.Vk[:, :, j, :] = Vk_1D
    else:
        for j in range(0, sim.N+2):
            poly_init_gauss_1D(sim.t0, section, section.V[:, :, j, :])


def poly_distrib_2D_TG(section):
    """Initialize 2 cardinal sinus with 0-velocity, 0-energy"""

    init_sizes(section)

    sim = section.sim
    sim.Nene = 3  # Number of energies components

    sim.ug = zeros((2, sim.N+2, sim.N+2), order='f')  # Gas velocity
    sim.mu = taylor(sim.Xgrid)
    sim.mu[2] = 0.  # Set energy source term to zero
    sim.ug[:2] = sim.mu[:2]
    sim.dt_drag = compute_dt_drag(sim)

    rpar = zeros(2, order='f')
    rpar[0] = 0.0  # time t
    # Rs - unity conversion: m**2/s -> micron**2/s
    rpar[1] = sim.Rs*1e12

    # Define the Fortran subroutine for computing phase transport
    # Here: 2-component velocity and 3-component sigma
    IntRomberg = modfort.intromberg

    eps = 1e-6  # convergence parameter for IntRomberg
    args = section.S, section.Sp, eps, rpar
    area0 = IntRomberg(modfort.get_nk, *args)
    area1 = IntRomberg(modfort.get_mk, *args)

    nk = 1.e-18 * area0
    mk = 1.e-18 * sim.ratio_mn * area1

    section.V[0, :] = mk
    if mk != 0:
        section.V[6, :] = nk/mk  # muk
    else:
        section.V[6, :] = 0.
    section.V[1:6, :] = 0.  # Set velocities and energies to 0

    centerx = 0.625
    centery = 0.875
    radius = 0.125

    for i in range(0, sim.N+2):
        for j in range(0, sim.N+2):
            dist = sqrt((sim.Xgrid[0, i, j] - centerx)**2 +
                        (abs(sim.Xgrid[1, i, j] - 0.5) - (centery - 0.5))**2)
            if dist < radius:
                section.V[0, i, j] *= sinc(dist/radius)
            else:
                section.V[0, i, j] = 0.
                section.V[6, i, j] = 0.

    # Ensure symmetry according to x-axis
    section.V[:, :, :] = 0.5*(section.V[:, :, :] + section.V[:, :, ::-1])
    section.stokes = 0.5*(section.S + section.Sp)*sim.stokes/sim.Smax
