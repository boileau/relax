# -*- coding: utf-8 -*-
"""
Common methods for processing simulation data
"""

from collections import OrderedDict
import h5py
import math
import numpy as np
import matplotlib.pyplot as plt
import os
import shutil
from Relax import nvar
import sys

varnames = 'rho', 'u', 'v', 'e_11', 'e_12', 'e_22', 'p_11', 'p_12', 'p_22',\
           'color', 'nk'

zero_tol = dict(tup for tup in zip(varnames, [0.]*len(varnames)))


def build_vardict(V, n, gamma, color=None, varlist=varnames):
    """
    Return a dict of the variables from the solution array.
    n may be the number of space nodes or size nodes
    """

    rho, u, v, e_11, e_12, e_22, muk = V  # Reference variables
    ndim = len(V.shape) - 1

    nk = rho * muk  # Compute droplet density
    if ndim == 1:  # 1D array
        p_11 = np.zeros(n)
        p_12 = np.zeros_like(p_11)
        p_22 = np.zeros_like(p_11)
        # Compute pressures from energies
        p_11, p_12, p_22 = [rho*e*(gamma-1.0) for e in (e_11, e_12, e_22)]
        if color is None:
            color = np.zeros(n)

    else:  # 2D array
        if color is None:
            color = np.zeros((n, n))
        rho, u, v, e_11, e_12, e_22, color, nk = [var.transpose() for var in
                                                  (rho, u, v, e_11, e_12, e_22,
                                                  color, nk)]
        p_11 = np.zeros((n, n))
        p_12 = np.zeros_like(p_11)
        p_22 = np.zeros_like(p_11)
        # Compute pressures from energies
        p_11[:, :] = V[0]*V[3]*(gamma - 1.0)
        p_12[:, :] = V[0]*V[4]*(gamma - 1.0)
        p_22[:, :] = V[0]*V[5]*(gamma - 1.0)
        p_11, p_12, p_22 = [var.transpose() for var in (p_11, p_12, p_22)]

    # Build variables dictionary
    var = rho, u, v, e_11, e_12, e_22, p_11, p_12, p_22, color, nk
    vardict = dict(tup for tup in zip(varnames, var))  # build full dict

    # return an extract from full dict
    return OrderedDict((k, vardict[k]) for k in varlist)


def prim_to_cons(rho, u, v, e_11, e_12, e_22, mu):
    """Return conservative variables from primitive variables"""
    rhou = rho * u
    rhov = rho * v
    E_11 = rho * e_11 + rho * u * u
    E_12 = rho * e_12 + rho * u * v
    E_22 = rho * e_22 + rho * v * v
    nk = rho * mu
    E_kin_11 = rho * u * u
    E_kin_12 = rho * u * v
    E_kin_22 = rho * v * v
    return rho, rhou, rhov, E_11, E_12, E_22, nk, E_kin_11, E_kin_12, E_kin_22


def u_v_e_ii(rho, rhou, rhov, E_11, E_12, E_22):
    """Return velocities and internal energies"""
    if rho != 0.:
        u = rhou / rho
        v = rhov / rho
        e_11 = E_11 / rho - u * u
        e_12 = E_12 / rho - u * v
        e_22 = E_22 / rho - v * v
        varlist = u, v, e_11, e_12, e_22
    else:
        varlist = (0., 0., 0., 0., 0.)
    return np.array(varlist)


def divide_and_filter(a, b):
    with np.errstate(divide='ignore', invalid='ignore'):
        res = np.asarray(np.true_divide(a, b))
        if np.size(res) == 1:
            res = 0
        else:
            res[res == np.inf] = 0
        return np.nan_to_num(res)


def create_dir(directory, erase=False):
    if os.path.exists(directory):
        if erase:
            shutil.rmtree(directory)
            os.makedirs(directory)
    else:
        os.makedirs(directory)


def find_nearest(array, value):
    """Find index in array whose value is the nearest to 'value'"""
    index = np.searchsorted(array, value, side="left")
    if index == len(array) or \
       math.fabs(value - array[index-1]) < math.fabs(value - array[index]):
        return index - 1
    else:
        return index


def cons_to_prim(rho, rhou, rhov, E_11, E_12, E_22, nk,
                 E_kin_11, E_kin_12, E_kin_22):
    """Return velocities, pressures and mu"""

    u = divide_and_filter(rhou, rho)
    v = divide_and_filter(rhov, rho)
    e_11 = divide_and_filter((E_11 - E_kin_11), rho)
    e_12 = divide_and_filter((E_12 - E_kin_12), rho)
    e_22 = divide_and_filter((E_22 - E_kin_22), rho)
    mu = divide_and_filter(nk, rho)
    return np.array((rho, u, v, e_11, e_12, e_22, mu))


def sum_sections(Vk, output='concat'):
    """Sum over all sections of polydisperse solution"""
    # Unpack solution into sim.nvar=7 subarrays
    args = (subarray[0] for subarray in np.split(Vk, nvar))
    V_cons = prim_to_cons(*args)  # Convert to conservative variables
    ndim = len(Vk.shape) - 1
    V_cons_sum = np.sum(V_cons, axis=ndim)
    if output == 'cons':
        V_sum = V_cons_sum
    else:
        args = (subarray[0] for subarray in np.split(V_cons_sum, nvar + 3))
        V_prim_sum = cons_to_prim(*args)
        if output == 'concat':
            # Concatenate sum over sections for:
            # (rho rhou rhov E_11 E_12 E22 nk) + (u v e_11 e_12 e_22 mu)
            V_sum = np.concatenate((V_cons_sum[:7], V_prim_sum[1:]))
        elif output == 'prim':
            V_sum = V_prim_sum
        else:
            sys.exit("Wrong output in sum_sections")
    return V_sum


def inf_err(sim, ref, norm=1.0):
    """return normalized infinite-norm error between sim and ref"""
    return max(abs(sim - ref))/norm


def L2_err(sim, ref, norm=1.0):
    """return normalized infinite-norm error between sim and ref"""
    N = len(sim)
    return np.linalg.norm(sim - ref, ord=2)/np.sqrt(N)/norm


def open_solution(path='solution.h5'):
    """Return a h5 file object from solution path"""
    try:
        f = h5py.File(path, 'r')  # Open .h5 solution file
    except IOError:
        msg = "File not found: {}".format(path)
        sys.exit(msg)
    return f


class open_h5(object):
    """Context manager: open a h5 solution file"""

    def __init__(self, h5path='solution.h5'):
        self.path = h5path

    def __enter__(self):
        self.file = open_solution(path=self.path)
        return self.file

    def __exit__(self, type, value, traceback):
        self.file.close()


def get_param(f, param):
    """return parameter value"""
    return f['param'][param][()]


def get_last_ite(f):
    """return last iteration number"""
    return f['snapshot/lastite'][()]


def get_ite_list(f):
    """return a list of existing snapshots"""
    return f['snapshot/itelist'][()]


def print_ite_list(f):
    print("List of snapshots in {}:".format(f.filename))
    ite_list = get_ite_list(f)
    for ite in ite_list:
        time = get_time(f, ite)
        print("- ite {}: t = {}".format(ite, time))


def get_probe_list(f):
    """return a list of existing probes"""
    return [dset for dset in f['probes']]


def get_probe_info(f, probename):
    """return a tuple of probe coordinates and comment"""
    prefix = 'probes/{}'.format(probename)
    coor = f[prefix]['position/coor'][()]
    comment = f[prefix]['comment'][()]
    return coor, comment


def get_probe_mask(f, probename):
    """return the probe time array"""
    path = 'probes/{}/position/mask'.format(probename)
    return f[path][()]


def get_probe_data(f, probename, dataname):
    """return the probe time array"""
    path = 'probes/{}/{}'.format(probename, dataname)
    return f[path][()]


def get_probe_description(f, probename):
    """Return a string describing probe information"""
    coor, comment = get_probe_info(f, probename)
    probe_info = '{} at position [{}] ({})'.format(probename, coor, comment)
    return probe_info


def print_probe_list(f):
    print("List of probes to plot:")
    probenames = get_probe_list(f)
    for name in probenames:
        print("- " + get_probe_description(f, name))


def get_time_list(f):
    """return a list of time snapshots"""
    return f['snapshot/timelist'][()]


def get_time(f, nite):
    """return time corresponding to iteration number"""
    if nite not in get_ite_list(f):
        print_ite_list(f)
        msg = "Iteration {} not found".format(nite)
        sys.exit(msg)
    else:
        ite_str = 'ite_{}'.format(nite)
        return f['snapshot'][ite_str]['time'][()]


def get_ite(f, time, get_index=False):
    """return the closest iteration for given time"""
    last_ite = get_last_ite(f)
    last_time = get_time(f, last_ite)
    if time > last_time:
        msg = "{}: time t = {} is greater than last time t = {}" \
            .format(f.filename, time, last_time)
        sys.exit(msg)
    else:
        ite_list = get_ite_list(f)
        time_list = get_time_list(f)
        index = find_nearest(time_list, time)
        if get_index:
            return index, ite_list[index]
        else:
            return ite_list[index]


def load_snapshot(f, ite='last', k='sum'):
    """return last time and corresponding solution"""

    if ite == 'last':
        nite = get_last_ite(f)
    else:
        itelist = get_ite_list(f)
        itemax = itelist[-1]
        if ite > itemax:
            msg = "File {}: ite {} > itemax {}".format(f.filename, ite, itemax)
            sys.exit(msg)
        elif ite in itelist:
            nite = ite
        else:
            print_ite_list(f)
            msg = "File {}: ite number {} not present in ite list". \
                format(f.filename, ite)
            sys.exit(msg)

    t = get_time(f, nite)  # current time

    # Read ite solution and remove phantom nodes

    nsect = get_param(f, 'nsect')
    if nsect == 0:
        sol_path = 'solution'.format(nite)
    else:  # Add section number in the path for polydisperse system
        if k == 'sum':
            sol_path = 'sum'
        else:
            sol_path = 'sect_{}/solution'.format(k)

    ite_str = 'ite_{}'.format(nite)
    ndim = get_param(f, 'ndim')
    if ndim == 1:
        V = f['snapshot'][ite_str][sol_path][:, 1:-1]
    else:
        V = f['snapshot'][ite_str][sol_path][:, 1:-1, 1:-1]
    return t, V


def load_color(f, ite='last'):
    """return color array"""
    if ite == 'last':
        nite = f['snapshot/lastite'][()]
    else:
        nite = ite

    ite_str = 'ite_{}'.format(nite)
    # Read last ite solution and remove phantom nodes
    ndim = f['param']['ndim'][()]
    if ndim == 1:
        color = f['snapshot'][ite_str]['color'][1:-1]
    else:
        color = f['snapshot'][ite_str]['color'][1:-1, 1:-1]
    return color


def get_grid(f):
    """return grid coordinates. If case is 2D, return a 1D-slice"""
    ndim = get_param(f, 'ndim')
    if ndim == 1:  # 1D
        Xgrid = f['grid']['Xgrid'][1:-1]  # Remove phantom points
    else:  # 2D
        Xgrid = f['grid']['Xgrid'][0, 1:-1, 1]  # Remove phantom points
    return Xgrid


def get_sol_dict(f, ite='last', color=None):
    """load solution and return a dictionary of variables"""
    t, V = load_snapshot(f, ite=ite)
    N = get_param(f, 'N')
    gamma = get_param(f, 'gamma')
    sim = build_vardict(V, N, gamma, color=color, varlist=varnames)
    sim['t'] = t
    return sim


def load_sim(sol_path='solution.h5', ite='last'):
    """Return a sim dict from relax solution file"""
    sim = {}
    with open_h5(sol_path) as f:
        sim['x'] = get_grid(f)
        sim['solname'] = f.filename
        sim.update(get_sol_dict(f, ite=ite, color=load_color(f)))
    return sim


def plot_ref_sim(ref, *sim_data, **kwargs):
    """plot sim against ref"""

    params = {'legend.fontsize': 10}
    plt.rcParams.update(params)

    loc_varnames = kwargs.pop('varnames', varnames)
    title = kwargs.pop('title', '')
    figname = kwargs.pop('figname', '')
    figform = kwargs.pop('figform', 'pdf')
    if figname:
        print("Plotting {}".format(figname))

    def set_label(data):
        if data is ref:
            default = 'ref'
        else:
            default = 'sim'
        label = data.get('legend', default)
        return label

    if len(loc_varnames) < 3:
        arr = np.arange(2).reshape(2)  # array to locate subplots
        fig, axarr = plt.subplots(2, 1)
    elif len(loc_varnames) < 5:
        arr = np.arange(4).reshape(2, 2)  # array to locate subplots
        fig, axarr = plt.subplots(2, 2, sharex=True)
    else:
        arr = np.arange(6).reshape(3, 2)  # array to locate subplots
        fig, axarr = plt.subplots(3, 2, sharex=True)

    if 'xlabel' in list(ref.keys()):
        axarr[-1, 0].set_xlabel(ref['xlabel'], fontsize=12)
        axarr[-1, 1].set_xlabel(ref['xlabel'], fontsize=12)

    if 't' in list(sim_data[0].keys()):
        figtitle = title + "Time t = {}".format(sim_data[0]['t'])
    else:
        figtitle = title
    fig.suptitle(figtitle, fontsize=14)

    # skip symbols if too many points to display
    skip = max(len(ref['x'])//100, 1)
    if skip > 1:
        if figname:
            suffix = " in {}.{}".format(figname, figform)
        else:
            suffix = ""
        print("WARNING: skipping symbol for display" + suffix)
    flatiter = arr.flat  # flat iterator
    coor = flatiter.coords  # get coordinates of first element
    for i in flatiter:
        if i < len(loc_varnames):
            var = loc_varnames[i]
            axarr[coor].plot(ref['x'], ref[var], marker='o', mfc='None',
                             linestyle='', label=set_label(ref),
                             markevery=skip)
            for data in sim_data:
                axarr[coor].plot(data['x'], data[var], label=set_label(data))
            axarr[coor].set_title(var)

        coor = flatiter.coords  # get next element's coordinates

    coor = arr.flat.coords  # reset flat iterator to add legend to first plot
    axarr[coor].legend(loc='upper right', frameon=False)

    if figname:
        fig.set_size_inches(15, 10)
        fig.savefig('{}.{}'.format(figname, figform), format=figform, dpi=150)
        plt.close(fig)
    else:
        plt.show()


def check_sym(V, axis, signdiff=1, precision=1e-20):
    """Check whether solution is symmetric or not"""

    if axis == 'x':
        is_sym = ((abs(V[:, ::-1] - signdiff*V[:, :]).max()) < precision)
    elif axis == 'y':
        is_sym = ((abs(V[::-1, :] - signdiff*V[:, :]).max()) < precision)

    if not(is_sym):
        if axis == 'x':
            msg = (V[:, ::-1] - signdiff*V[:, :]).nonzero()
        elif axis == 'y':
            msg = (V[::-1, :] - signdiff*V[:, :]).nonzero()
        raise Exception("Asymmetry detected!\n{}".format(msg))

    return is_sym


def check(error, tolerance, tol_type, verbose=True):
    """Compute error and return a status and warning message"""

    status = "OK"
    msg = ""
    for k, v in error.items():
        tol = tolerance.get(k, 0.)
        if tol_type == 'exact':
            warn = (v != tol)
        elif tol_type == 'lower':
            warn = (v > tol)
        if warn:
            err_msg = "{}: Error = {} differs from expected tolerance " \
                      "Tol = {}".format(k, v, tol)
            if verbose:
                print(err_msg)
            if msg:  # Add carriage return if msg is not empty
                msg = msg + "\n" + err_msg
            else:
                msg = msg + err_msg
            if v < tol and status != "KO":  # only if error_type = 'exact'
                status = "Warning"
            else:
                status = "KO"
    if verbose:
        print(status)

    return status, msg
