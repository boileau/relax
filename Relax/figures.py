# -*- coding: utf-8 -*-
"""
Defines the figures to plot
"""

from math import pi, cos, sin
from matplotlib import rc, cm
import numpy as np
from importlib import import_module

# Figure layout dictionnary
FigDict = {
           'SISC-Fig4': 'Fig2D',
           'SISC-Fig5': 'Fig2D',
           'Fig2D_ICMF2016': 'Fig2D_ICMF2016'
           }


class Fig1D(object):
    """A figure object"""

    def __init__(self, section):

        self.section = section
        self.name = section.figname
        self.varname = section.varname
        self.batch = section.batch
        self.data = section.data
        self.extra = section.data.sol.extra

        self.fig = self.data.fig
        self.ax = self.data.ax
        self.x = self.data.x
        self.var = self.section.vardict[self.varname]

        self._set_param()
        self.fig.suptitle('')
        self._set_export()

    def _set_param(self):  # Not redefined for this class
        pass

    def _set_export(self):

        # predefined dpi for saved figures (default value is set to 100dpi)
        rc('savefig', dpi=300)
        self.form = "png"       # format of saved figures
        self.save = True
        self.file = self.name + '.' + self.form

    def plot(self):
        """Draw and save figure"""

        self._draw()
        self._set_axis()
        if self.save:
            self.fig.savefig(self.file, format=self.form)
        if not self.batch:
            self.fig.show()

    def _set_axis(self):
        """Axis labels and dimensions"""
        ax = self.ax
        ax.set_xlabel(r'$v$', labelpad=6)
        ylabel = r'$' + self.varname + '$'
        ax.set_ylabel(ylabel, labelpad=12)

    def _draw(self):
        """Draw a 1D plot"""
        self.ax.plot(self.x, self.var, '-', lw=1, mfc='k', markevery=1)


class Fig2D(Fig1D):
    """A 2D figure object"""

    def _set_param(self):  # Not defined for this class
        """Set parameters for a 2D figure"""

        self._set_rc()
        self._set_levels()
        self._set_colorbar()
        self._set_quiver()
        self.autoscale = False

    def _set_rc(self):
        # predefined figure size, (use "figure(figsize=(8,6))" otherwise)
        # define the figure workspace
        rc('figure.subplot', left=0.18, right=0.9, top=0.95, bottom=0.15)
        # predefined label font size (use "xlabels(fontsize=10)" otherwise)
        rc('axes',  labelsize=12)
        # predefined fontsize for x-axis and y-axis ticks marks
        rc('xtick', labelsize=10)
        rc('ytick', labelsize=10)

    def _set_axis(self):
        """Axis labels and dimensions"""
        ax = self.ax
        ax.axis([0, 1, 0, 1])
        ax.set_xlabel(r'$x$', labelpad=6)
        ax.set_ylabel(r'$y$', labelpad=12)

    def _set_levels(self):
        """iso-lines and iso-contours parameters"""
        self.nisol = 32
        vmax = 2.
        self.isolevels = np.arange(vmax/self.nisol, vmax+vmax/self.nisol,
                                   vmax/self.nisol)

        self.nisoc = 256
        self.colorlevels = np.arange(vmax/self.nisoc, vmax, vmax/self.nisoc)

    def _set_colorbar(self):
        """Colobar parameters"""
        self.cbformat = '%.2f'
        self.cbticks = np.linspace(0., 2.0, 6, endpoint=True)
        self.cborientation = 'vertical'

    def _set_quiver(self):
        pass

    def _draw(self):
        """Draw a 2D contour"""
        x = self.x
        var = self.var
        ax = self.ax
        fig = self.fig

        ax.set_aspect('equal')

        if not self.autoscale:
            ax.contour(x, x, var, self.nisol, colors='k',
                       linewidths=0.4, levels=self.isolevels)
            cf = ax.contourf(x, x, var, 256, cmap=cm.Greys,
                             levels=self.colorlevels, extend='max')

        else:
            ax.contour(x, x, var, 16, colors='k', linewidths=0.4)
            ax.contourf(x, x, var, 256, cmap=cm.Greys)

        if self.cborientation:
            fig.colorbar(cf, format=self.cbformat,
                         orientation=self.cborientation,
                         ticks=self.cbticks)


class Fig2D_ICMF2016(Fig2D):
    """A 2D figure object"""

    def _set_rc(self):
        # predefined figure size, (use "figure(figsize=(8,6))" otherwise)
        # define the figure workspace
        rc('figure.subplot', left=0.18, right=0.9, top=0.95, bottom=0.15)
        # predefined label font size (use "xlabels(fontsize=10)" otherwise)
        rc('axes',  labelsize=12)
        # predefined fontsize for x-axis and y-axis ticks marks
        rc('xtick', labelsize=10)
        rc('ytick', labelsize=10)

    def _set_export(self):

        # predefined dpi for saved figures (default value is set to 100dpi)
        rc('savefig', dpi=150)
        self.form = "png"       # format of saved figures
        self.save = True
        try:
            suffix = "_{:04d}".format(self.data.sol.index)
        except AttributeError:
            suffix = ''

        try:
            if self.data.sol.outdir:
                prefix = "{}/{}".format(self.data.sol.outdir, self.name)
            else:
                prefix = self.name
        except AttributeError:
            prefix = self.name
        self.file = "{}_{}{}.{}".format(prefix, self.varname, suffix,
                                        self.form)

    def _set_levels(self):
        """iso-lines and iso-contours parameters"""

        if self.extra and self.extra['nisolevel']:
            self.nisol = self.extra['nisolevel']
        else:
            self.nisol = 32

        try:
            vmax = self.data.sol.vmax
        except AttributeError:
            vmax = self.var.max()
        self.vmax = vmax
        self.isolevels = np.arange(vmax/self.nisol, vmax+vmax/self.nisol,
                                   vmax/self.nisol)

        self.nisoc = 256
        self.colorlevels = np.arange(vmax/self.nisoc, vmax, vmax/self.nisoc)

    def _set_colorbar(self):
        """Colobar parameters"""
        self.cbformat = '%.3g'
        self.cbticks = np.linspace(0., self.vmax, 7)
        self.cborientation = 'vertical'

    def _set_quiver(self):
        """Compute quiver field"""
        if self.extra and self.extra['quiverskip']:
            self.quiver = True
            quiverskip = self.extra['quiverskip']
            xq = self.x[::quiverskip]

            def taylorgreen(d, i, j):
                if d == 0:
                    # x-direction
                    utg = cos(2.*pi*xq[i]) * sin(2.*pi*xq[j])
                else:
                    # y-direction
                    utg = -sin(2.*pi*xq[i]) * cos(2.*pi*xq[j])

                return utg

            vtaylorgreen = np.vectorize(taylorgreen)
            self.quiveru = np.fromfunction(vtaylorgreen, shape=(2, len(xq),
                                           len(xq)), dtype=int)
            self.quiverx = xq
        else:
            self.quiver = False

    def _draw(self):
        """Draw a 2D contour"""
        x = self.x
        var = self.var
        ax = self.ax
        fig = self.fig

        title = "t = {:.2f}".format(self.data.time)
        fig.suptitle(title)

        ax.set_aspect('equal')
        ax.contour(x, x, var, self.nisol, colors='k',
                   linewidths=0.4, levels=self.isolevels)
        cf = ax.contourf(x, x, var, 256, cmap=cm.cubehelix_r,
                         levels=self.colorlevels, extend='max')

        if self.quiver:
            ax.quiver(self.quiverx, self.quiverx,
                      self.quiveru[0], self.quiveru[1])

        if self.cborientation:
            fig.colorbar(cf, format=self.cbformat,
                         orientation=self.cborientation,
                         ticks=self.cbticks)


def plot(section):
    """Plot a figure according to figname"""

    try:
        class_name = FigDict[section.figname]
    except KeyError:
        class_name = 'Fig' + section.dimtype
        print("WARNING: figure layout {} ".format(section.figname) + \
              "not found in FigDict (figures.py):")
        print("Using default layout " + class_name)

    module_ = import_module(__name__)
    # Get figure class reference according to name
    FigClass = getattr(module_, class_name)
    # Instanciate corresponding figure object
    fig = FigClass(section)
    fig.plot()
