module st_parameters

    implicit none
    integer, parameter :: nvar=7

end module st_parameters

module source_term

    use st_parameters
    implicit none

    contains
    
subroutine drag_2d(Nx, Ny, tau, dt, V, mu)

    implicit none
    integer, intent(in) :: Nx, Ny
    double precision, intent(in) :: tau, dt
    double precision, dimension(0:nvar-1, 0:Nx+1, 0:Ny+1), intent(inout) :: V
    double precision, dimension(0:4, 0:Nx+1, 0:Ny+1), intent(in) :: mu

    V(1,1:Nx,1:Ny) = (V(1,1:Nx,1:Ny) - mu(0,1:Nx,1:Ny))*exp(-dt/tau) + &
                     mu(0,1:Nx,1:Ny)
    V(2,1:Nx,1:Ny) = (V(2,1:Nx,1:Ny) - mu(1,1:Nx,1:Ny))*exp(-dt/tau) + &
                     mu(1,1:Nx,1:Ny)       
    V(3,1:Nx,1:Ny) = (V(3,1:Nx,1:Ny) - mu(2,1:Nx,1:Ny))*exp(-2.d0*dt/tau) + &
                     mu(2,1:Nx,1:Ny)
    V(4,1:Nx,1:Ny) = (V(4,1:Nx,1:Ny) - mu(3,1:Nx,1:Ny))*exp(-2.d0*dt/tau) + &
                     mu(3,1:Nx,1:Ny)
    V(5,1:Nx,1:Ny) = (V(5,1:Nx,1:Ny) - mu(4,1:Nx,1:Ny))*exp(-2.d0*dt/tau) + &
                     mu(4,1:Nx,1:Ny)       
   
end subroutine drag_2d


subroutine drag_1d(N, tau, dt, V, mu)

    implicit none
    integer, intent(in) :: N
    double precision, intent(in) :: tau, dt
    double precision, dimension(0:nvar-1, 0:N), intent(inout) :: V
    double precision, dimension(0:2, 0:N), intent(in) :: mu

    V(1,1:N) = (V(1,1:N) - mu(0,1:N))*exp(-dt/tau) + mu(0,1:N)
    V(2,1:N) = (V(2,1:N) - mu(1,1:N))*exp(-dt/tau) + mu(1,1:N)       
    V(3,1:N) = (V(3,1:N) - mu(2,1:N))*exp(-2*dt/tau) + mu(2,1:N)

end subroutine drag_1d

end module source_term
