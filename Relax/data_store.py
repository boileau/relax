# -*- coding: utf-8 -*-
"""
This module defines the Store and Probe classes used to store parameters
and results in a .h5 file.
"""

import h5py
import numpy as np
import os
from Relax import data_process as dproc
from Relax import nvar, small
from sys import exit


def store_string(name, value, group):
    """Store string 'name' of 'value' in 'group'"""
    dtype = 'S{0}'.format(len(value))
    dset = group.create_dataset(name, shape=(1,), dtype=dtype)
    dset[...] = value.encode('utf8')


class Store(object):
    "This class contains the methods to snapshot and probe solution"""

    def __init__(self, sim):
        self.sim = sim
        self.sections = sim.model.sections
        if sim.compute_sim:
            self.sol = Solution(sim)  # Initialize storage in .h5 file
            # Initialize probe objects
            self.probes = [Probe(sim, probe) for probe in sim.probe_list]
        if sim.compute_ref:
            self.sol_ref = Solution(sim, store_type='ref')
            self.probes_ref = [Probe(sim, probe, probe_type='ref')
                               for probe in sim.probe_list]

    def process(self):
        sim = self.sim
        if sim.compute_sim:
            self._probe_it(self.probes)
            self.sol.snapshot()  # store initial solution in solution.h5
        if sim.compute_ref:
            if self.probes_ref:
                sim.model.compute_ref()  # Compute ref if needed
            self._probe_it(self.probes_ref)
            # store reference solution in solution_ref.h5
            self.sol_ref.snapshot()

    def _probe_it(self, probes):
        """Probe sim or ref"""
        sim = self.sim
        for probe in probes:
            if sim.nsect == 0:  # Monodisperse case
                probe.probe_data(self.sections)
            else:  # Polydisperse case
                [probe.probe_data(section) for section in self.sections]
                probe.sum_over_sections()
            probe.time.append(sim.t)

    def _store_probes(self):
        """Store all probes value"""
        sim = self.sim
        if sim.compute_sim:
            [p.store(self.sol) for p in self.probes]
        if sim.compute_ref:
            [p.store(self.sol_ref) for p in self.probes_ref]

    def terminate(self):
        """Store elapsed time and close the solution file"""
        sim = self.sim
        if sim.compute_sim:
            self._store_probes()  # Store probes value in .h5 file
            self.sol.terminate()
        if sim.compute_ref:
            self.sol_ref.terminate()
        print("Elapsed time: {}s".format(sim.elapsed))


class Solution(object):
    """
    This class contains the attributes and methods related to .h5 storage.
    The output file is open by the __init__() method.
    """

    def __init__(self, sim, store_type='sim'):

        # Reference section for solution
        self.sim = sim
        self.sections = sim.model.sections
        self.type = store_type

        if self.type == 'ref':
            self.V_target = 'V_ref'
            Vk_target = 'Vk_ref'
            self.filename = sim.rundir + '/' + sim.solfile[:-3] + '_ref.h5'
        else:
            self.V_target = 'V'
            Vk_target = 'Vk'
            self.filename = sim.rundir + '/' + sim.solfile

        if sim.nsect:
            # Set the target solution array (Vk or Vk_ref) as output
            self.Vk_out = getattr(sim, Vk_target)

        try:
            self.f = h5py.File(self.filename, 'w')
        except:
            print("""WARNING: {} was open by a previous process
            => remove old file""".format(self.filename))
            os.remove(self.filename)
            self.f = h5py.File(self.filename, 'w')

        self.itelist = []
        self.timelist = []
        self.iteset = None
        self.timeset = None

        # Define groups
        self.gparam = self.f.create_group("param")
        self.ggrid = self.f.create_group("grid")
        if sim.nprobe:
            self.gprobes = self.f.create_group("probes")
        self.gsnap = self.f.create_group("snapshot")
        self.gsnap.create_dataset("nsnap", shape=(), data=0)
        self.gsnap.create_dataset("lastite", shape=(), data=0)

        dset = self.gparam.create_dataset("ndim", shape=(), data=0)
        if (sim.is2D):
            dset[()] = 2
        else:
            dset[()] = 1

        self._store_parameters()
        self._store_grid()

    def terminate(self):
        """Store elapsed time and close the solution file"""
        sim = self.sim
        self.gparam.create_dataset('elapsed_time',
                                   shape=np.shape(sim.elapsed),
                                   data=sim.elapsed)
        self.f.close()
        print("Solution stored in: {}".format(self.filename))

    def _store_parameters(self):
        """Store all parameters by iterating on __store_one_param()"""
        for name, par in self.sim.paramdict.items():
            value = par['value']

            # Convert boolean to string
            if isinstance(value, bool):
                value = str(value)

            # Special data set creation for strings
            if isinstance(value, str):
                store_string(name, value, self.gparam)
            else:
                self.gparam.create_dataset(name, shape=np.shape(value),
                                           data=value)

    def _store_grid(self):
        """Store grid data in .h5 file"""
        sim = self.sim
        group = self.ggrid
        group.create_dataset("Xgrid", shape=np.shape(sim.Xgrid),
                             data=sim.Xgrid)

    def snapshot(self):
        """Store solution if: \n
                - time is a multiple of dt_store \n
                - time is final time
                - ite is a multiple of ite_store \n"""
        sim = self.sim
        istore = (sim.t > (sim.tf - sim.dt/2.)) or (sim.ite == 0)
        if sim.maxite > 0:
            istore = istore or (sim.ite >= sim.maxite)
        if istore:
                self._store_snapshot()
        elif (sim.dt_store > 0.) and (sim.t % sim.dt_store < sim.dt) or \
             (sim.dt_store == 'all'):
                self._store_snapshot()
        elif (sim.ite_store > 0) and (sim.ite % sim.ite_store == 0):
                self._store_snapshot()

    def _store_snapshot(self):
        """Store the solution state in .h5 file"""

        sim = self.sim
        ite = sim.ite
        time = sim.t
        term = sim.term

        # Store snapshot iteration number and time as a list
        self.itelist.append(ite)
        itelist = np.asarray(self.itelist)

        if self.iteset:
            del self.f['snapshot/itelist']
        self.iteset = self.gsnap.create_dataset("itelist",
                                                shape=np.shape(itelist),
                                                data=itelist)
        self.timelist.append(time)
        timelist = np.asarray(self.timelist)
        if self.timeset:
            del self.f['snapshot/timelist']
        self.timeset = self.gsnap.create_dataset("timelist",
                                                 shape=np.shape(timelist),
                                                 data=timelist)

        groupname = "ite_"+str(ite)

        gsnapite = self.gsnap.create_group(groupname)
        if self.type == 'sim':
            print(term.move_down + "Storing {} >>> ite {}".format(self.type,
                                                                  ite))
        else:
            # Compute reference solution before storing
            sim.model.compute_ref()
            print("Storing {} >>> ite {}".format(self.type, ite))
        # Print time
        gsnapite.create_dataset("time", shape=(), data=time)

        # Increment number of snapshots
        dset = self.gsnap["nsnap"]
        dset[()] += 1

        dset = self.gsnap["lastite"]
        dset[()] = ite

        if sim.nsect == 0:
            # set the target solution array (V or V_ref)
            self.sections.V_out = getattr(self.sections, self.V_target)
            # Print monodisperse solution
            gsnapite.create_dataset("solution",
                                    shape=np.shape(self.sections.V_out),
                                    data=self.sections.V_out)
            gsnapite.create_dataset("color",
                                    shape=np.shape(self.sections.is_energy),
                                    data=self.sections.is_energy)
        else:
            # Print polydisperse solution
            for section in self.sections:
                # set the target solution array (V or V_ref)
                section.V_out = getattr(section, self.V_target)
                # Create group from section index
                groupname = "sect_"+str(section.k)
                gsect = gsnapite.create_group(groupname)
                gsect.create_dataset("solution",
                                     shape=np.shape(section.V_out),
                                     data=section.V_out)
            # Sum over all sections
            V_sum = dproc.sum_sections(self.Vk_out, output='prim')
            gsnapite.create_dataset('sum', shape=np.shape(V_sum), data=V_sum)
            # Only store first section of is_energy
            gsnapite.create_dataset("color",
                                    shape=np.shape(self.sections[0].is_energy),
                                    data=self.sections[0].is_energy)


class Probe(object):
    """This class define a probe object and its storage method"""

    def __init__(self, sim, probe, probe_type='sim'):
        """Define probe object"""
        # Unpack probe dictionary
        self.sim = sim
        self.name = probe['name']
        self.position = probe['position']
        self.comment = probe['comment']
        self.type = probe_type
        self.sections = sim.model.sections
        if self.type == 'ref':
            self.V_target = 'V_ref'
        else:
            self.V_target = 'V'
        self.coormask = self._find_coor()
        # Initialize an empty data lists
        self.time = []
        self.data = []
        if sim.nsect == 0:  # monodisperse case
            self.data.append([])
        else:  # polydisperse case
            # initialize list of nsect + 1 size,
            # section nsect+1 being for the sum over all sections
            [self.data.append([]) for i in range(sim.nsect)]
        self.sum_data = []
        self.sum_varnames = "rho", "rhou", "rhov", \
                            "E_11", "E_12", "E_22", "n", \
                            "u", "v", "e_11", "e_12", "e_22"

    def _find_coor(self):
        """Return a coordinate mask corresponding to probe location"""
        sim = self.sim
        x_probe = self.position
        in_domain_mask = (sim.Xgrid[:] > sim.xmin) & (sim.Xgrid[:] < sim.Lx)
        coormask = ((abs(sim.Xgrid[:] - x_probe) - 0.5*sim.dx < small) &
                    in_domain_mask)
        if len(sim.Xgrid[coormask]) == 2:
            filter = coormask[1:]*coormask[:-1]
            coormask[1:] = filter
        if len(sim.Xgrid[coormask]) == 0:
            msg = "Did not find point for probe: " + self.name + \
                  "\nSee Probe._find_coor() in data_store.py"
            exit(msg)
        elif len(sim.Xgrid[coormask]) > 1:
            msg = "Found several points for probe: " + self.name + \
                  "\nat location: {}\n".format(self.position) + \
                  "See Probe._find_coor() in data_store.py"
            exit(msg)
        return coormask

    def probe_data(self, section):
        """Probe solution for a given section"""
        section.V_out = getattr(section, self.V_target)
        val = section.V_out[:, self.coormask]  # apply mask to solution
        self.data[section.k].append(val)

    def sum_over_sections(self):
        """Sum over all sections"""
        sum_val = np.zeros(nvar + 3)
        for section in self.sections:
            val = section.V_out[:, self.coormask]  # apply mask to solution
            prim_val = val.transpose()[0]  # get primitive vars
            # convert to conservative vars
            cons_val = dproc.prim_to_cons(*prim_val)
            sum_val += np.array(cons_val)  # sum over sections
        prim_val = dproc.cons_to_prim(*sum_val)
        # concatenate conservative and primitive arrays:
        # (rho rhou rhov E_11 E_12 E22 nk) + (u v e_11 e_12 e_22 mu)
        all_val = np.concatenate((sum_val[:-3], prim_val[1:-1]))
        self.sum_data.append(all_val)  # Add to temporal list

    def store(self, sol):
        """Store a single probe in .h5 file"""

        gprobe = sol.gprobes.create_group(self.name)
        print("Storing {} >>> {}".format(self.type, self.name))
        gposition = gprobe.create_group("position")
        gposition.create_dataset("coor",
                                 shape=np.shape(self.position),
                                 data=self.position)
        gposition.create_dataset("mask",
                                 shape=np.shape(self.coormask),
                                 data=self.coormask)

        store_string("comment", self.comment, gprobe)

        gprobe.create_dataset("time", shape=np.shape(self.time),
                              data=self.time)

        gprobe.create_dataset("data",
                              shape=np.shape(self.data),
                              data=self.data)

        dset = gprobe.create_dataset("sum_data",
                                     shape=np.shape(self.sum_data),
                                     data=self.sum_data)
        header = "# t           | " + " | ".join("{0: <12}".format(var) for var
                                                 in self.sum_varnames)
        dset.attrs["text"] = header
