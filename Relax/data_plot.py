# -*- coding: utf-8 -*-
"""
This module defines the methods to plot results during simulation.
"""
from numpy import arange, zeros, zeros_like, linspace
from matplotlib import rc
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from Relax.data_process import build_vardict, sum_sections, varnames

# ----------------------------------
# Define Default Figure Parameters
# ----------------------------------
# 5 values: 'serif' (Times), 'sans-serif' (Helvetica),
# 'cursive' (Zapf-Chancery), 'fantasy' (Western), and 'monospace' (Courier)
rc('font', family='sans-serif', style='normal')
rc('legend', fancybox=True, numpoints=1, fontsize='medium')
# predefined figure size, (use "plt.figure(figsize=(8,6))" otherwise)
rc('figure', figsize=(14.0, 12.0))
# define the figure workspace
rc('figure.subplot', left=0.0714, right=0.9286, top=0.833, bottom=0.1666)
# 'ms' can be also used instead of 'markersize'
rc('lines', antialiased=True, markersize=6.5)
# predefined label font size (use "xlabels(fontsize=10)" otherwise)
rc('axes',  labelsize=13)
# predefined fontsize for x-axis ticks marks (use "xticks(size=10)" otherwise)
rc('xtick', labelsize=12)
rc('ytick', labelsize=12)
# predefined fontsize for y-axis ticks marks (use "yticks(size=10)" otherwise)
rc('xtick.major', pad=10)
rc('ytick.major', pad=6)
# predefined dpi for saved figures (default value is set to 100dpi)
rc('savefig', dpi=150)
cbfraction = 0.15  # default: 0.15
cbshrink = 0.8
cbformat = '%.2g'


def plot_var(fig, x, variables, xlabel='x'):
    """Plot all variables as 1D-subplots in the same fig"""
    index = 0
    for name, value in variables.items():  # Iter over variables dict
        index += 1
        ax = fig.add_subplot(3, 4, index)
        ax.plot(x, value, '-', lw=1, mfc='k', markevery=1)
        ax.set_xlabel(xlabel, fontsize=10)
        ax.autoscale_view(True, True, True)
        ax.set_title(name, fontsize=12)


class Plot1D(object):
    """This class is instanciated by Scheme class"""

    def __init__(self, sim):
        self.sim = sim

    def plot(self, V, is_energy, figtitle, show=True):
        """Plot a 1D solution"""

        sim = self.sim
        Xgrid = sim.Xgrid
        N = sim.N
        gamma = sim.gamma

        # Define Default Figure Parameters
        rc('figure.subplot',
           left=0.0714, right=0.9286,
           top=0.92, bottom=0.07,
           wspace=0.2, hspace=0.4)  # define the figure workspace

        fig = plt.figure()
        fig.suptitle(figtitle, fontsize=14)

        x = Xgrid[1:-1]
        variables = build_vardict(V[:, 1:-1], N, gamma, color=is_energy[1:-1])

        plot_var(fig, x, variables)

        if show:
            plt.show()


class Plot2D(Plot1D):
    """This class is instanciated by Scheme class"""

    def plot(self, V, is_energy, figtitle, show=True):
        """Plot a 2D solution"""

        sim = self.sim
        Xgrid = sim.Xgrid
        N = sim.N
        gamma = sim.gamma

        # Define Default Figure Parameters
        rc('legend', fancybox=True, numpoints=1, fontsize='medium')
        # predefined figure size, (use "plt.figure(figsize=(8,6))" otherwise)
        rc('figure', figsize=(14.0, 10.0))
        # define the figure workspace
        rc('figure.subplot', left=0.0714, right=0.9286, top=0.9, bottom=0.1)

        fig, axes = plt.subplots(nrows=4, ncols=3, sharex=True, sharey=True,
                                 figsize=(8, 8))
        fig.suptitle(figtitle, fontsize=14)

        x = Xgrid[0, 1:-1, 1]
        variables = build_vardict(V[:, 1:-1, 1:-1], N, gamma,
                                  color=is_energy[1:-1, 1:-1])

        for i, ax in enumerate(axes.flat, start=0):
            ax.set(aspect=1)
            if i < len(varnames):
                var = varnames[i]
                ctf = ax.contourf(x, x, variables[var], 64)
                ax.autoscale_view(True, True, True)
                ax.set_xticks(linspace(x.min(), x.max(), 5.), '')
                # create an axes on the right side of ax.
                # The width of cax will be 5% of ax and the padding between cax
                # and ax will be fixed at 0.05 inch.
                divider = make_axes_locatable(ax)
                cax = divider.append_axes("right", size="5%", pad=0.05)

                plt.colorbar(ctf, cax=cax, format=cbformat)
                ax.set_title(var, fontsize=12)
            else:
                fig.delaxes(ax)
        fig.subplots_adjust(left=0.1)

        if show:
            plt.show()


class PolyPlot(object):
    """This class handles polydisperse plots"""

    def __init__(self, sim):
        self.sim = sim
        if self.sim.is2D:
            self.poly_plot = Plot2D(sim)
        else:
            self.poly_plot = Plot1D(sim)

    def plot(self, figtitle, show):
        """
        Plot a polydisperse field by summing on all sections.
        """
        sim = self.sim
        V_sum = sum_sections(sim.Vk, output='prim')
        # WARNING: use only is_energy from section 0
        title = "sum of sections for " + figtitle
        self.poly_plot.plot(V_sum, sim.model.sections[0].is_energy, title,
                            show)


def plot_mu(Xgrid, mu, N):
    # predefined figure size, (use "plt.figure(figsize=(8,6))" otherwise)
    rc('figure', figsize=(14.0, 6.0))

    fig = plt.figure()

    # ----------------------------------
    # Loading data
    # ----------------------------------
    # remise des variables à 0
    x = zeros(N)
    u = zeros((N, N))
    v = zeros_like(u)
    e = zeros_like(u)
    # remplissage des variables
    x = Xgrid[0, 1:N+1, 1]
    u = mu[0, 1:N+1, 1:N+1].transpose()
    v = mu[1, 1:N+1, 1:N+1].transpose()
    e = mu[2, 1:N+1, 1:N+1].transpose()

    # ----------------------------------
    # Create subplot
    # ----------------------------------
    ax = fig.add_subplot(131, aspect='equal')
    ax.contourf(x, x, u, 256)
    ax.autoscale_view(True, True, True)
    ax.axis([0, 1, 0, 1])
    ax.xticks(arange(0, 1, 0.2), '')
    ax.yticks(arange(0, 1, 0.2), '')
    ax.title('Horizontal velocity', fontsize=12)
    ax.colorbar(fraction=cbfraction,
                shrink=cbshrink,
                format=cbformat)

    ax = fig.add_subplot(132, aspect='equal')
    ax.contourf(x, x, v, 256)
    ax.autoscale_view(True, True, True)
    ax.axis([0, 1, 0, 1])
    ax.xticks(arange(0, 1, 0.2), '')
    ax.yticks(arange(0, 1, 0.2), '')
    ax.title('Vertical velocity', fontsize=12)
    ax.colorbar(fraction=cbfraction,
                shrink=cbshrink,
                format=cbformat)

    ax = fig.add_subplot(133, aspect='equal')
    ax.contourf(x, x, e, 256)
    ax.autoscale_view(True, True, True)
    ax.axis([0, 1, 0, 1])
    ax.xlabel(r'$x$', labelpad=6)
    ax.ylabel(r'$y$', labelpad=12)
    ax.title('Internal energy', fontsize=12)
    ax.colorbar(fraction=cbfraction,
                shrink=cbshrink,
                format=cbformat)
