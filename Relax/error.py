#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Compute relative error between relax simulation and a reference solution.
Methods for error computing are either:
- inside this module (default)
- in the 'post' subdirectory of current case if existing
"""

import imp
import sys
from os import path
from Relax import data_process as dproc


class set_case_post(object):
    """Context manager: update sys.path and returns case_post module"""

    def __init__(self, case_path):
        if path.exists(case_path + '/post/case_post.py'):
            self.path = case_path + '/post'
            print("Using specific error postprocessing: {}/case_post.py"
                  .format(self.path))
            self.default = False
        else:
            print("Using default error postprocess.")
            self.default = True

    def __enter__(self):
        if self.default:
            case_post = sys.modules[__name__]
        else:
            sys.path.append(self.path)
            case_post = self.import_case_post()
        return case_post

    def import_case_post(self):
        mod_name = 'case_post'
        tup = imp.find_module(mod_name, [self.path])
        return imp.load_module(mod_name, *tup)

    def __exit__(self, type, value, traceback):
        if not self.default:
            sys.path.remove(self.path)


def compute_error(verbose=False):
    """Basic function to compute error"""

    error = {}
    ref = dproc.load_sim(sol_path='./Ref/solution_ref.h5')
    sim = dproc.load_sim()

    for var in dproc.varnames:
        error[var] = dproc.L2_err(sim[var], ref[var])

    if verbose:
        for k, v in error.items():
            print("Error({}) = {}".format(k, v))

    return error


def get_error_status(tol, tol_type, verbose=True):
    """Compute error and return status"""
    error = compute_error(verbose=verbose)
    return dproc.check(error, tol, tol_type, verbose=verbose)


def get_tol():
    """Load tol.py and return corresponding tol dict"""

    fhandle = None
    mod_name = 'run'
    try:
        tup = imp.find_module(mod_name, ['.'])
        run = imp.load_module(mod_name, *tup)
        if run.err_tol == 0.:
            tol = dproc.zero_tol
        else:
            tol = run.err_tol
        # Tolerance type check: lower or exact
        if hasattr(run, 'tol_type'):
            tol_type = run.tol_type
        else:
            # Default is exact
            tol_type = 'exact'
        fhandle = tup[0]
    finally:
        if fhandle:
            fhandle.close()

    return tol, tol_type


def get_status(case_post, verbose=False):
    """
    Get tolerance from run directory and return error calling case methods
    """
    tol, tol_type = get_tol()
    return case_post.get_error_status(tol, tol_type, verbose=verbose)

if __name__ == '__main__':
    with set_case_post('..') as case_post:
        status, msg = get_status(case_post, verbose=True)
        print(msg)
