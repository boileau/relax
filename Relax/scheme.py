"""
This module contains the classes and methods for numerical schemes.
"""

from Relax import test_case
from Relax import data_plot as dp
from Relax import data_process as dproc
from Relax import libfortran as libfort
from numpy import absolute
from Relax import great, nvar


class MonoDisperse(object):
    """
    Define a Model object as single System object to solve.
    """

    def __init__(self, sim):
        # Construct a monodisperse system instancianting a single section
        self.sim = sim
        self.sections = Section(sim, k=0)

    # Default methods are for monodisperse
    def transport_step(self):
        """Apply the transport step."""
        if self.sim.compute_sim:
            self.sections.transport.compute()

    def ST_step(self):
        """Apply the source term and filter small values."""
        if self.sim.compute_sim:
            self.sections.source_term.compute()
            self.sections.filter_small()

    def _compute_dt_CFL(self):
        return self.sections.transport.compute_dt()

    def _compute_dt_ST(self):
        if self.sim.isSecST:
            dt_ST = self.sections.source_term.compute_dt()
        else:
            dt_ST = 0.
        return dt_ST

    def _check_dt_fixed(self):
        """Check that dt_fixed is lower than any dt_*"""
        sim = self.sim
        sim.dt = sim.dt_fixed
        if sim.dt_fixed > sim.dt_CFL:
            print("WARNING: dt_fixed > sim.dt_CFL")
            print("         dt_fixed = {}".format(sim.dt_fixed))
            print("         dt_CFL   = {}".format(sim.dt_CFL))
        if sim.dt_fixed > sim.dt_ST and sim.dt_ST != 0.:
            print("WARNING: dt_fixed > sim.dt_ST")
            print("         dt_fixed = {}".format(sim.dt_fixed))
            print("         dt_ST    = {}".format(sim.dt_CFL))
        if sim.dt_fixed > sim.dt_evap and sim.dt_evap != 0.:
            print("WARNING: dt_fixed > sim.dt_evap")
            print("         dt_fixed = {}".format(sim.dt_fixed))
            print("         dt_evap  = {}".format(sim.dt_CFL))

    def compute_dt(self):
        """dt = min(dt_CFL, dt_ST)"""
        sim = self.sim
        sim.dt_CFL = self._compute_dt_CFL()
        sim.dt_ST = self._compute_dt_ST()

        if not sim.dt_fixed:
            sim.dt = sim.dt_CFL
            if sim.isSecST:
                sim.dt = min(sim.dt, sim.dt_ST)
        else:
            sim.dt = sim.dt_fixed
            self._check_dt_fixed()

    def plot(self, title, show):
        """Plot solution interactively."""
        self.sections.plot(title, show)

    def compute_ref(self):
        """Compute the reference solution"""
        sim = self.sim
        if sim.compute_ref:
            # Compute reference solution only if needed
            if sim.ref_time != sim.t:
                sim.reference(sim.t, sim.Vk_ref, sim.Xgrid)
                sim.ref_time = sim.t


class PolyDisperse(MonoDisperse):
    """
    Define one system to solve for each section (overload the MonoDisperse
    class).
    """

    def __init__(self, sim):
        self.sim = sim
        test_case.init_polydisp(sim)  # Initialize sections
        self.sections = [Section(sim, k) for k in range(sim.nsect)]
        if (sim.isAllST):
            self.source_term = SizeTransport(self)

        self.poly_plot = dp.PolyPlot(sim)

    # Overload methods for multi-section system
    def transport_step(self):
        """Loop on sections to compute transport step."""
        if self.sim.compute_sim:
            [section.transport.compute() for section in self.sections
             if section.dotransport]

    def ST_step(self):  # overload method for polydisperse
        """Apply source term as a single method for all sections."""
        sim = self.sim
        if sim.compute_sim:
            if sim.isAllST:
                self.source_term.compute()

            if sim.isSecST:  # If some section-ST has to be applied
                [section.source_term.compute() for section in self.sections]

    def _compute_dt_CFL(self):
        return min([section.transport.compute_dt()
                    for section in self.sections])

    def _compute_dt_ST(self):
        if self.sim.isSecST:
            dt_ST = min([section.source_term.compute_dt()
                         for section in self.sections])
        else:
            dt_ST = 0.
        return dt_ST

    def compute_dt(self):
        """dt = min_over_all_sections(dt_CFL, dt_ST)"""
        sim = self.sim
        sim.dt_CFL = self._compute_dt_CFL()
        sim.dt_ST = self._compute_dt_ST()

        if not sim.dt_fixed:
            if sim.dt_CFL < great:
                sim.dt = sim.dt_CFL
            else:
                print("Warning: cannot transport a void field.")
                sim.void = True
            if sim.isSecST:
                sim.dt = min(sim.dt, sim.dt_ST)
            if sim.dt_evap != 0.:
                sim.dt = min(sim.dt, sim.dt_evap)
            if sim.dt_drag != 0.:
                sim.dt = min(sim.dt, sim.dt_drag)
        else:
            sim.dt = sim.dt_fixed
            self._check_dt_fixed()

    def plot(self, title, show):
        """
        Plot polydisperse solution interactively:
            - for each section as a function of x, if nsect < 4
            - only one node as a function of section, if nsect >= 4

        (To be improved...)
        """
        sim = self.sim
        if (sim.nsect < 4):  # Plot each section if number of sections < 6
            for section in self.sections:
                section_title = "Section {} - ".format(section.k) + title
                section.plot(section_title, show)
        title = "t = {}".format(sim.t)
        self.poly_plot.plot(title, show)


class Section(object):
    """
    Define a set of variables and methods corresponding to a single section.
    """

    def __init__(self, sim, k):
        """Instanciate a Transport scheme and source-term"""
        self.sim = sim
        self.k = k  # initialize section number
        self.dotransport = True  # tell if section must be transported
        sim = self.sim

        test_case.init_solution(self)

        # Construct transport scheme
        if sim.schemetype == 'bouchut':
            self.transport = Bouchut(self)
        elif sim.schemetype in ('relax_pgd', 'relax_iso', 'relax_isoh',
                                'relax_ag', 'relax_agh'):
            if (sim.order == 'one'):
                self.transport = Relaxation(self)
            else:
                self.transport = Relaxation_o2(self)

        # Construct source term
        if sim.isSecST:
            self.source_term = WithDrag(self)
        else:
            self.source_term = No_ST(self)

        # Construct BC's
        if sim.is2D:
            self.BC = self.BC_2D
            self.my_plot = dp.Plot2D(sim)
        else:
            self.BC = self.BC_1D
            self.my_plot = dp.Plot1D(sim)

    def BC_1D(self, dummy):
        """Apply boundary conditions if periodic."""

        if self.sim.BCx == 'periodic':
            self.V[:, 0] = self.V[:, -2]
            self.V[:, -1] = self.V[:, 1]
#        elif sim.BCx == 'outflow':
#            self.V[:, 0] = self.V[:, 1]
#            self.V[:, -1] = self.V[:, -2]

    def BC_2D(self, direction):
        """Apply boundary conditions for 2D."""

        sim = self.sim

        if direction == 'x':
            BC = sim.BCx
        elif direction == 'y':
            BC = sim.BCy
        elif direction == 'xy':
            BCx = sim.BCx
            BCy = sim.BCy
        else:
            exit("Wrong direction in BC_2D")

        if direction == 'xy':
            if BCx == 'periodic':
                self.V[:, 0, :] = self.V[:, -2, :]
                self.V[:, -1, :] = self.V[:, 1, :]
            if BCy == 'periodic':
                self.V[:, :, 0] = self.V[:, :, -2]
                self.V[:, :, -1] = self.V[:, :, 1]
        else:
            if BC == 'periodic':
                self.V[:, 0, :] = self.V[:, -2, :]
                self.V[:, -1, :] = self.V[:, 1, :]

#        This should be reconsidered...
#        if (BC == 'periodic') and (sim.BCx != '2jets'):
#            self.V[:, :, 0] = self.V[:, :, -2]
#            self.V[:, :, -1] = self.V[:, :, 1]
#
#        if direction == 'x' and BC == '2jets':
#            self.V[:, 0:3, :] = self.V2[:, 0:3, :]
#            self.V[:, -1, :] = self.V2[:, -1, :]
#            self.V[:, :, 0:3] = self.V2[:, :, 0:3]

    def filter_small(self):
        """Set rho to 0 if under a precision value."""

        # Filter rho close to void
        mask = (absolute(self.V[0]) < self.sim.mac_prec)
        self.V[:, mask] = 0.0
        # Filter low energy
#        mask = (absolute(self.V[3:5,1:-1,1:-1]) < sim.mac_prec)
#        self.V[3:5,mask] = 0.0

    def plot(self, title, show):
        """Plot solution when asked by plot_sol."""
        self.my_plot.plot(self.V, self.is_energy, title, show)


class Transport(object):
    """
    This Master class defines the methods for the transport step:
        - Flux calculation
        - Update formula
        - Dimensional splitting with matrix transposition for 2D

    This a template class: it will be overloaded but never instanciated
    """

    def __init__(self, section):  # Overloaded later
        self.section = section
        self.sim = section.sim

    def compute_flux(self):
        """overloaded by child classes"""
        pass

    def compute(self):
        """Compute fluxes for either 1D or 2D problems using Fortran."""
        sim = self.sim
        if sim.is2D:
            if (sim.ite % 2 == 0):
                # Do x-direction first, y-direction second (may be optimized)
                self.one_step('x')
                self.transpose_fortran()
                self.one_step('y')
                self.transpose_fortran()
            else:
                # Do y-direction first, x-direction second
                self.transpose_fortran()
                self.one_step('y')
                self.transpose_fortran()
                self.one_step('x')
        else:
            self.one_step('x')

    def one_step(self, direction):
        """
        One transport step is composed of:
            1) flux calculation
            2) update formula
            3) boundary conditions
        """
        self.compute_flux()
        self.update()
        # dproc.check_sym(self.section.V[5], direction, signdiff=1)
        self.section.BC(direction)

    def update_1D_fortran(self):
        section = self.section
        V = section.V
        F = section.F
        is_energy = section.is_energy
        dt = self.sim.dt
        libfort.transport.update_1d(V, F, is_energy, dt)

    def update_2D_fortran(self):
        """Call Fortran update subroutine."""
        V = self.section.V
        F = self.section.F
        is_energy = self.section.is_energy
        dt = self.sim.dt
        libfort.transport.update_2d(V, F, is_energy, dt)

    def transpose_fortran(self):
        """Transpose solution to perform next dimensional splitting step."""
        libfort.transport.transpose_2d(self.section.V, self.section.is_energy)

    def transpose(self):
        for i in range(nvar):
            self.section.V[i] = self.section.V[i].transpose()

        # Change velocities
        self.section.V[1:2, :, :] = self.section.V[2:1:-1, :, :]
        # Change energies
        self.section.V[3:5, :, :] = self.section.V[5:3:-1, :, :]


class Bouchut(Transport):
    """Transport scheme using the Bouchut method."""

    def __init__(self, section):
        super(Bouchut, self).__init__(section)
        sim = self.sim

        # Define order and associate Fortran functions
        def flux_o1(*args):
            V, F = args[0:2]
            libfort.transport.flux_bouchut_o1(V, F)

        def flux_o2(*args):
            V, F, dx, dt = args
            libfort.transport.flux_bouchut_o2(V, F, dx, dt)

        if sim.order == "one":
            self.flux_1D = flux_o1
        elif sim.order == "two":
            self.flux_1D = flux_o2

        if sim.is2D:
            self.update = self.update_2D_fortran
        else:
            self.update = self.update_1D_fortran

    def compute_dt(self):
        """Compute time step based on the CFL condition."""
        sim = self.sim
        return sim.dx*sim.CFL/(absolute((self.section.V[1])).max() + 1.e-30)

    def compute_flux(self):
        """Flux are calculated by Fortran using Bouchut's scheme."""
        V = self.section.V
        F = self.section.F
        sim = self.sim
        dx = sim.dx
        dt = sim.dt
        N = sim.N

        # Compute fluxes
        if sim.is2D:
            for i in range(N+2):
                self.flux_1D(V[:, :, i], F[:, :, i], dx, dt)
                self.BC_flux_1D(F[:, :, i])
        else:
            self.flux_1D(V, F, dx, dt)
            self.BC_flux_1D(F)  # Correct fluxes for BC

    def BC_flux_1D(self, F):
        """Boundary conditions are specific to Bouchut's scheme."""

        BCx = self.sim.BCx

        if BCx == 'periodic':
            # Add F+ and F- at BC as done in Fortran for internal values
            F[:, 1] += F[:, -1]
            F[:, -1] = F[:, 1]
        elif BCx == 'closed' or BCx == 'outflow':
            F[:, 1] = F[:, 2]  # Flux gradient is 0 at left boundary
            F[:, -1] = F[:, -2]  # Flux gradient is 0 at right boundary


class Relaxation(Transport):
    """Transport scheme using the relaxation method"""

    def __init__(self, section):
        super(Relaxation, self).__init__(section)

        if self.sim.is2D:
            self.compute_is_energy = libfort.transport.is_energy_2d
            self.compute_vmax = libfort.transport.cflpression_2d
            self.update = self.update_2D_fortran
            self.flux = self.flux_2D
            self.BC_flux = self.BC_flux_2D
        else:
            self.compute_is_energy = libfort.transport.is_energy_1d
            self.compute_vmax = libfort.transport.cflpression_1d
            self.update = self.update_1D_fortran
            self.flux = self.flux_1D
            self.BC_flux = self.BC_flux_1D

        self.color()  # Compute initial value of is_energy

    def compute_dt(self):
        """Compute transport time step for relaxation scheme."""
        sim = self.sim
        section = self.section
        vmax = self.compute_vmax(section.V)
        if vmax != 0:
            section.dotransport = True
            return sim.CFL*sim.dx/vmax
        else:
            section.dotransport = False
            return great

    def flux_1D(self, V, V_inter, F, is_energy):
        """Compute flux on a 1D array"""
        # Left and right values are equal to cell value
        V_inter[:nvar, :] = V[:, :]
        V_inter[nvar:, :] = V[:, :]
        libfort.transport.flux_relax(V_inter, F, is_energy)

    def flux_2D(self, V, V_inter, F, is_energy):
        """Compute flux on a 2D array by calling flux_1D"""
        N = self.sim.N
        for i in range(N+2):
            self.flux_1D(V[:, :, i],
                         V_inter[:, :, i],
                         F[:, :, i],
                         is_energy[:, i])

    def BC_flux_1D(self, F):
        sim = self.sim
        if sim.BCx == 'constant_flux':
            F[:, 1] = F[:, 2]
            F[:, -2] = F[:, -3]
        elif sim.BCx == 'outflow':  # Flux gradient is constant
            F[:, 1] = 2*F[:, 2] - F[:, 3]
            F[:, -2] = 2*F[:, -3] - F[:, -4]

    def BC_flux_2D(self, F):
        sim = self.sim
        if sim.BCx == 'outflow' or sim.BCy == 'outflow':
            if not hasattr(sim, 'warned'):
                print("WARNING: BC_flux_2D not coded yet")
                sim.warned = True

    def compute_flux(self):
        """Compute 1D or 2D fluxes using relaxation scheme."""
        V = self.section.V
        F = self.section.F
        V_inter = self.section.V_inter
        is_energy = self.section.is_energy

        self.color()  # Compute energy sensor
        self.flux(V, V_inter, F, is_energy)  # Compute fluxes
        self.BC_flux(F)

    def color(self):
        """
        Handle color depending on the type of scheme (PGD, GD, or hybrid).
        """
        sim = self.sim
        section = self.section
        if sim.schemetype == 'relax_pgd':
            # All cells are without pressure
            section.is_energy[:] = 0
        elif sim.schemetype in ('relax_iso', 'relax_ag'):
            # All cells are with pressure
            section.is_energy[:] = 1
        elif sim.schemetype in ('relax_isoh', 'relax_agh'):
            # Hybrid scheme
            section.is_energy = self.compute_is_energy(self.section.V)


class Relaxation_o2(Relaxation):
    """Transport scheme using the 2nd order relaxation method."""

    def __init__(self, section):
        # Call parent classes constructors
        super(Relaxation_o2, self).__init__(section)

        if self.sim.is2D:
            self.BC_per = self.BC_per_2D
            self.compute_is_energy = libfort.transport.is_energy_2d
            self.update_1rst_step = libfort.transport.update_2d
        else:
            self.BC_per = self.BC_per_1D
            self.compute_is_energy = libfort.transport.is_energy_1d
            self.update_1rst_step = libfort.transport.update_1d

    def BC_per_1D(self, V):
        if self.sim.BCx == 'periodic':
            V[:, 0] = V[:, -2]  # Left ghost cell equals far-left real cell
            V[:, -1] = V[:, 1]  # Right ghost cell equals far-right real cell

    def BC_per_2D(self, V):
        if self.sim.BCx == 'periodic':
            V[:, :, 0] = V[:, :, -2]  # L-ghost cell equals far-L real cell
            V[:, :, -1] = V[:, :, 1]  # R-ghost cell equals far-R real cell

    def flux_1D(self, V, V_inter, F, is_energy_flux, is_energy_slope):
        """Compute flux on a 1D array"""
        # Reconstruct values at interfaces
        libfort.transport.compute_val_inter_o2(V, V_inter, is_energy_slope)
        self.BC_per_1D(V_inter)  # Apply periodic BC to V_inter
        libfort.transport.flux_relax(V_inter, F, is_energy_flux)

    def flux_2D(self, V, V_inter, F, is_energy_flux, is_energy_slope):
        """Compute flux on a 2D array by calling flux_1D"""
        N = self.sim.N
        for i in range(N+2):
            self.flux_1D(V[:, :, i],
                         V_inter[:, :, i],
                         F[:, :, i],
                         is_energy_flux[:, i],
                         is_energy_slope[:, i])

    def compute_flux(self):
        """Compute 1D or 2D fluxes using 2nd order relaxation scheme."""
        V = self.section.V
        V2 = self.section.V2
        F = self.section.F
        V_inter = self.section.V_inter
        is_energy = self.section.is_energy
        is_energy2 = self.section.is_energy2
        dt = self.sim.dt

        self.color()  # Compute energy sensor
        V2[:] = V[:]  # Initialize V2

        # Compute fluxes
        self.flux(V, V_inter, F, is_energy, is_energy)  # 1rst RK step
        if self.sim.order == 'two':
            # 2nd RK step for 2nd order in time and space
            self.update_1rst_step(V2, F, is_energy, 0.5*dt)  # Update V2
            self.BC_per(V2)  # Apply periodic BC to V2
            is_energy2 = self.compute_is_energy(V2)  # Update is_energy
            self.flux(V2, V_inter, F, is_energy, is_energy2)  # 2nd RK step


class SectionSourceTerm(object):
    """This master class applies a source term to each section."""
    def __init__(self, section):
        self.section = section
        self.sim = self.section.sim

    def compute_dt(self):
        """Compute time step associated to the source term."""
        sim = self.sim
        return sim.CFL*sim.dx/sim.maxmu


class WithDrag(SectionSourceTerm):
    """
    This class inherits from SectionSourceTerm to apply a source term to
    momentum.
    """

    def __init__(self, section):
        super(WithDrag, self).__init__(section)
        sim = self.sim

        if sim.nsect:
            # Polydisperse case: stokes depends on section
            self.stokes = self.section.stokes
        else:
            self.stokes = sim.stokes

        if sim.is2D:
            self.drag = libfort.source_term.drag_2d
        else:
            self.drag = libfort.source_term.drag_1d

    def compute(self):
        """Use Fortran to compute and apply the drag force."""
        sim = self.sim
        self.drag(self.stokes, sim.dt/2.0, self.section.V, sim.mu)
        self.section.BC('xy')


class No_ST(SectionSourceTerm):
    """Do not apply any source term when not asked."""

    def compute(self):
        pass

    def compute_dt(self):
        pass


class SizeTransport(object):
    """
    This class solves the transport due to evaporation coupled to drag in
    the size space.
    """

    def __init__(self, model):
        self.sim = model.sim
        self.sections = model.sections
        if self.sim.is2D:
            self.source = libfort.tsm_aff_evap_drag.compute_evap_drag_ag_2d
        else:
            self.source = libfort.tsm_aff_evap_drag.compute_evap_drag_ag_1d

    def compute(self):
        """Call Fortran to compute size-space transport."""
        sim = self.sim
        self.source(sim.Nene, sim.Vk, sim.S, sim.Sinta, sim.Sintb,
                    sim.ug, sim.dt/2.)
        for section in self.sections:
            section.BC('xy')
