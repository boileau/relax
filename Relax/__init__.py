# -*- coding: utf-8 -*-
"""
Parameters shared by all modules
"""

nvar = 7  # number of variables (same as in transport.f90)
great = 1.e16
small = 1.e-15
