#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Test program for AG_TSM
"""

from collections import OrderedDict
from Relax.libfortran import tsm_aff_evap_drag as tsm
from numpy import asfortranarray, linspace, zeros, zeros_like, arange, pi, sqrt
import matplotlib.pyplot as plt


# Global parameters
Smax = 11.310e3
rhol = 634.239             # liquid density (kg/m**3)
alpha_drag = 0.9680385e-6  # alpha (m^2/s)
Rs = 1.99e-7               # Evaporation coefficient (m**2/s)
ratio_mn = rhol/(6.0*sqrt(pi))
eps = 1e-6  # convergence parameter
tsm_order = 2
tmax = Smax/(Rs*1e12)
varnames = "nk", "mk", "uk", "sigmak"
output_prefix = "../Cases/AG_TSM/relax/"


def plot_var(fig, x, variables, xlabel='x'):
    """
    Plot all variables as subplots in the same fig
    """
    index = 0
    for name, value in variables.items():  # Iter over variables dict
        index += 1
        ax = fig.add_subplot(2, 2, index)
        ax.plot(x, value, '-', lw=1, mfc='k', markevery=1)
        ax.set_xlabel(xlabel, fontsize=10)
        ax.autoscale_view(True, True, True)
        ax.set_title(name, fontsize=12)


def init_fig(figtitle, *var):
    """Initialize a figure to plot"""
    fig = plt.figure()
    fig.suptitle(figtitle, fontsize=14)
    variables = OrderedDict(tup for tup in zip(varnames, var))
    return fig, variables


def init_param(Nsec, Nt):
    """initialize test case parameters"""

    dt = tmax/Nt
    dS = Smax/Nsec

    # Create Fortran-type arrays
    S = asfortranarray(linspace(0., Smax, Nsec+1))
    Sinta = zeros(Nsec, order='f')
    Sintb = zeros_like(Sinta)
    time = arange(0, tmax, dt)

    # Filling Sinta and Sintb
    for k in range(Nsec):
        Sm52 = (S[k+1]**2.5 - S[k]**2.5)/2.5
        Sm72 = (S[k+1]**3.5 - S[k]**3.5)/3.5
        Sinta[k] = S[k+1]*Sm52 - Sm72
        Sintb[k] = Sm72 - S[k]*Sm52

    return dt, dS, S, Sinta, Sintb, time


def init_file(filename, header="# time, mtot, qdmtot"):
    f = open(output_prefix + filename, mode='w')
    print(header)
    f.write(header + "\n")
    return f


def print_sumk(f, t, nk, mk, uk, sigmak):
    """Output time, mtot, qdmtot"""
    out = "{:.8e} {:.8e} {:.8e}".format(t, sum(mk), sum(mk*uk))
    print(out)
    f.write(out + "\n")


def evap_drag_0D():
    """OD test case"""

    Nsec = 25  # Number of sections
    Nt = 20    # Number of time steps
    ug = (4.0, 0.0)   # max velocity

    dt, dS, S, Sinta, Sintb, time = init_param(Nsec, Nt)

    # Set parameters in Fortran module
    tsm.alpha_drag = alpha_drag
    tsm.rs = Rs
    tsm.ratio_mn = ratio_mn
    tsm.s_lim = Smax
    tsm.method_order = tsm_order

    nk = zeros(Nsec, order='f')
    mk = zeros_like(nk)
    uk = zeros((2, Nsec), order='f')  # 2 space dimensions
    sigmak = zeros((3, Nsec), order='f')

    rpar = zeros(4, order='f')
    rpar[0] = 0.0              # time t
    rpar[1] = Rs*1e12          # converting unity: m**2/s -> micron**2/s
    rpar[2] = alpha_drag*1e12  # converting unity: m**2/s -> micron**2/s
    rpar[3] = ug[0]

    # Initialize arrays
    nk, mk, uk[0], sigmak[0] = tsm.init_0d(S, eps, rpar)

    fig, variables = init_fig("Evap and drag", nk, mk, uk[0], sigmak[0])

    f = init_file('evap_drag_0D.dat')

    for t in time:
        print("t = {} | [{:>3}%]".format(t, int(t/tmax*100)))
        print_sumk(f, t, nk, mk, uk[0], sigmak[0])
        nk, mk, uk, sigmak = tsm.evap_drag_aff(S, Sinta, Sintb, dt,
                                               ug, nk, mk, uk, sigmak)
        plot_var(fig, S[1:], variables, xlabel='S')

    print_sumk(f, time[-1]+dt, nk, mk, uk[0], sigmak[0])  # Print last ite
    f.close()


if __name__ == '__main__':
    evap_drag_0D()
    plt.show()
