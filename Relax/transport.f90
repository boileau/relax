module parameters

    implicit none
    integer, parameter :: nvar=7

end module parameters


module transport

    use parameters
    implicit none
    double precision :: gamma, alpha, epsi_e, mac_prec, dx, st
    integer :: iso

    contains


subroutine cflpression_1d(N, V, vmax)

    implicit none
    integer, intent(in) :: N
    double precision, dimension(0:nvar-1, 0:N+1), intent(in) :: V
    double precision, intent(out) :: vmax
    integer :: i

    vmax = 0.0D0

    do i = 0, N
        call cflpression_cell(V(:,i), V(:,i+1), vmax)
    enddo

end subroutine cflpression_1d


subroutine cflpression_2d(N, V, vmax)

    implicit none
    integer, intent(in) :: N
    double precision, dimension(0:nvar-1, 0:N+1, 0:N+1), intent(in) :: V
    double precision, intent(out) :: vmax
    double precision, dimension(0:nvar-1, 0:N+1, 0:N+1) :: V_tmp
    integer :: i, j

    ! Transpose in a temporary array
    do i = 0, nvar-1
        V_tmp(i,:,:) = transpose(V(i,:,:))
    end do
    V_tmp(1:2,:,:) = V_tmp(2:1:-1,:,:)  ! Permute velocity components
    if (iso == 0) then
        V_tmp(3:5,:,:) = V_tmp(5:3:-1,:,:)  ! Permute energy components
    endif

    vmax = 0.0D0
    do j = 0, N
        do i = 0, N
            call cflpression_cell(V(:,i,j), V(:,i+1,j), vmax)
            call cflpression_cell(V_tmp(:,i,j), V_tmp(:,i+1,j), vmax)
        enddo
    enddo

end subroutine cflpression_2d


subroutine cflpression_cell(Vl, Vr, vmax)
! Compute maximum eigen value to respect the CFL condition

    implicit none
    double precision, dimension(0:nvar-1), intent(in) :: Vl, Vr
    double precision, intent(inout) :: vmax
    double precision :: rho_l, v_l, c_l, vo_l, p11_l, e11_l
    double precision :: rho_r, v_r, c_r, vo_r, p11_r, e11_r

    ! Check that there is no void in left or right cells
    if ((Vl(0) > mac_prec) .or. (Vr(0) > mac_prec)) then

        v_l = Vl(1)
        e11_l = max(Vl(3), epsi_e)
        c_l = dsqrt(gamma*(gamma - 1.0)*e11_l)

        v_r = Vr(1)
        e11_r = max(Vr(3), epsi_e)
        c_r = dsqrt(gamma*(gamma - 1.0)*e11_r)

        if (Vl(0) <= mac_prec) then
            ! problematic case 1
            rho_l = 0.0D0
            rho_r = Vr(0)
            p11_l = (gamma - 1.0)*rho_l*e11_l
            p11_r = (gamma - 1.0)*rho_r*e11_r
            vo_l = c_l + alpha*Pos((p11_r - p11_l)/(rho_r*c_r) + v_l - v_r)
            vo_r = c_r

        else if (Vr(0) <= mac_prec) then
            ! problematic case 2
            rho_r = 0.0D0
            rho_l = Vl(0)
            p11_l = (gamma - 1.0)*rho_l*e11_l
            p11_r = (gamma - 1.0)*rho_r*e11_r
            vo_r = c_r + alpha*Pos((p11_l - p11_r)/(rho_l*c_l) + v_l - v_r)
            vo_l = c_l
        else
            ! Standard formula for gas dynamics
            rho_r = Vr(0)
            rho_l = Vl(0)
            p11_l = (gamma - 1.0)*rho_l*e11_l
            p11_r = (gamma - 1.0)*rho_r*e11_r

            ! computing vo = a/rho
            if (p11_r >= p11_l) then
                vo_l = c_l + alpha*Pos((p11_r - p11_l)/(rho_r*c_r) + v_l - v_r)  !a_l/rho_l
                vo_r = c_r + alpha*Pos((p11_l - p11_r)/(vo_l*rho_l) + v_l - v_r) !a_r/rho_r
            else
                vo_r = c_r + alpha*Pos((p11_l - p11_r)/(rho_l*c_l) + v_l - v_r)  !a_r/rho_r
                vo_l = c_l + alpha*Pos((p11_r - p11_l)/(vo_r*rho_r) + v_l - v_r) !a_l/rho_l
            end if

        end if

        ! 6 eigen values:
        !    - v_l - vo_l
        !    - v_l
        !    - v_l + vo_l
        !    - v_r - vo_r
        !    - v_r
        !    - v_r + vo_r
        vmax = max(vmax, abs(v_l - vo_l), abs(v_l + vo_l), abs(v_r - vo_r), &
                     abs(v_r + vo_r))
    else  ! Fluxes are zero
        vmax = vmax
    end if

end subroutine cflpression_cell


subroutine is_energy_1D(N, V, is_energy)

    implicit none
    integer, intent(in) :: N
    double precision, dimension(0:nvar-1, 0:N+1), intent(in) :: V
    integer, dimension(0:N+1), intent(out) :: is_energy
    integer :: i

    do i = 0, N+1
        if ((V(3, i) + V(5, i)) < epsi_e) then
             is_energy(i) = 0
        else
             is_energy(i) = 1
        endif
    enddo

end subroutine is_energy_1D


subroutine is_energy_2D(N, V, is_energy)

    implicit none
    integer, intent(in) :: N
    double precision, dimension(0:nvar-1, 0:N+1, 0:N+1), intent(inout) :: V
    integer, dimension(0:N+1, 0:N+1), intent(out) :: is_energy
    integer :: i

    do i = 0, N+1
        call is_energy_1D(N, V(:,:,i), is_energy(:,i))
    enddo

    call transpose_2D(N, V, is_energy)

    do i = 0, N+1
        call is_energy_1D(N, V(:, :, i), is_energy(:, i))
    enddo

    call transpose_2D(N, V, is_energy)

end subroutine is_energy_2D


!subroutine calcule_est_avec_energie_croissement_1D(N, V, is_energy)
!
!    implicit none
!    integer, intent(in   ) :: N
!    double precision :: energie_croisement ,rhog ,rhod ,ud ,ug ,u0, rho0
!    double precision, dimension(0:nvar-1,0:N+1)  , intent(inout ) :: V
!    integer, dimension(0:N+1), intent(inout ) :: is_energy
!    integer :: i
!
!    do i= 0,N+1
!        if (V(3,i)+V(5,i).lt.epsi_e) then ! strictement inferieur
!            is_energy(i) = 0
!            energie_croisement=0.d0
!            if((i>0).and.(i<N+1)) then
!                   rhog=V(0,i-1)
!                   rhod=V(0,i+1)
!                   ug=V(1,i-1)
!                   ud=V(1,i+1)
!                   rho0=V(0,i) ! indice 0 pour la cellule cible
!                   u0=V(1,i)
!
!                   if((u0>0).and.(ug>u0).and.rhog+rho0>mac_prec) then
!                        energie_croisement=((rhog*rho0)/(rhog+rho0)**2)*(ug-u0)**2
!
!
!                   elseif((u0<0).and.(ud<u0).and.rhod+rho0>mac_prec) then
!                        energie_croisement=((rhod*rho0)/(rhod+rho0)**2)*(ud-u0)**2
!                   endif
!
!                  if(energie_croisement>epsi_e) then
!                        is_energy(i) = 1
!                  endif
!                  if((ug>0).and.(ud<0)) then
!                   ! on calcule l'energie interne qui peuvent etre créer par un croisement de deux jet
!
!                        energie_croisement=(rhog*rhod*(ug-ud)**2)/(rhog+rhod)**2
!                        if(energie_croisement>epsi_e) then
!                            is_energy(i) = 1
!                        endif
!
!                  endif
!            endif
!       else
!            is_energy(i) = 1
!       endif
!
!
!    enddo
!
!end subroutine calcule_est_avec_energie_croissement_1D

!subroutine calcule_est_croissement_2d(N, V,is_energy)
!
!    implicit none
!    integer, intent(in   ) :: N
!    double precision, dimension(0:nvar-1,0:N+1,0:N+1)  , intent(inout ) :: V
!    integer, dimension(0:N+1,0:N+1)  , intent(inout ) :: is_energy
!    integer :: i
!
!    do i= 0,N+1
!      call  calcule_est_avec_energie_croissement_1D(N, V(:,:,i), is_energy(:,i))
!
!    enddo
!    call transpose_2D(N,V,is_energy)
!    do i= 0,N+1
!       call calcule_est_avec_energie_croissement_1D(N, V(:,:,i), is_energy(:,i))
!
!    enddo
!    call transpose_2D(N,V,is_energy)
!
!end subroutine calcule_est_croissement_2d


subroutine compute_val_inter_o2(N, V, V_inter, is_energy)

    implicit none
    integer, intent(in) :: N
    double precision, dimension(0:nvar-1,0:N+1), intent(in) :: V
    double precision, dimension(0:2*nvar-1,0:N+1), intent(inout) :: V_inter
    integer, dimension(0:N+1), intent(in) :: is_energy
    double precision:: Drho, De_11, De_12, De_22, Du, Dv, Dmu
    double precision:: u_mean , v_mean, rho_mean, mu_mean
    double precision:: e_11_mean, e_12_mean, e_22_mean
    integer :: i, j

    do i = 1, N

        rho_mean = V(0,i)

        if (rho_mean > 0.d0) then
            !*  Computing slopes
            Drho = minmod(V(0,i) - V(0,i-1), V(0,i+1) - V(0,i))
            Du = minmod(V(1,i) - V(1,i-1), V(1,i+1) - V(1,i))
            Dv = minmod(V(2,i) - V(2,i-1), V(2,i+1) - V(2,i))
            De_11 = minmod(V(3,i) - V(3,i-1), V(3,i+1) - V(3,i))
            De_12 = minmod(V(4,i) - V(4,i-1), V(4,i+1) - V(4,i))
            De_22 = minmod(V(5,i) - V(5,i-1), V(5,i+1) - V(5,i))
            Dmu  = minmod(V(6,i) - V(6,i-1), V(6,i+1) - V(6,i))


            u_mean = V(1,i) - Drho * Du / (12.0D0*rho_mean)
            v_mean = V(2,i) - Drho * Dv / (12.0D0*rho_mean)
            mu_mean = V(6,i)

            if (is_energy(i) > 0) then
                if (iso == 0) then
                    e_11_mean = V(3,i) - Du**2/24.d0 - Drho*De_11/(12.d0*rho_mean)
                    e_12_mean = V(4,i) - Du*Dv/24.d0 - Drho*De_12/(12.d0*rho_mean)
                    e_22_mean = V(5,i) - Dv**2/24.d0 - Drho*De_22/(12.d0*rho_mean)
                else
                    e_11_mean = V(3,i) - (Du**2 + Dv**2)/24.d0 - &
                                        Drho*De_11/(12.d0*rho_mean)
                    e_12_mean = 0.d0
                    e_22_mean = 0.d0

                endif
                ! Might be improved: slope could be only reduced (and not zero)
                if ((e_11_mean - dabs(0.5d0*De_11)) < 0.d0 .or. &
                   ((e_22_mean - dabs(0.5d0*De_22)) < 0.d0)) then
                    Drho = 0.d0
                    Du = 0.d0
                    Dv = 0.d0
                    De_11 = 0.d0
                    De_12 = 0.d0
                    De_22 = 0.d0
                    Dmu = 0.d0
                    u_mean = V(1,i)
                    v_mean = V(2,i)
                    e_11_mean = V(3,i)
                    e_12_mean = V(4,i)
                    e_22_mean = V(5,i)
                    mu_mean = V(6,i)
                end if
            else
                e_11_mean = epsi_e
                e_12_mean = epsi_e
                e_22_mean = epsi_e
                De_11 = 0.0D0
                De_12 = 0.0D0
                De_22 = 0.0D0
            endif
        else
            Drho = 0.d0
            Du = 0.d0
            Dv = 0.d0
            De_11 = 0.d0
            De_12 = 0.d0
            De_22 = 0.d0
            Dmu = 0.d0
            u_mean = 0.d0
            v_mean = 0.d0
            e_11_mean = epsi_e
            e_12_mean = epsi_e
            e_22_mean = epsi_e
            mu_mean = 0.d0
        end if
        ! Left state of cell
        V_inter(0,i) = rho_mean - 0.5d0*Drho
        V_inter(1,i) = u_mean - 0.5d0*Du
        V_inter(2,i) = v_mean - 0.5d0*Dv
        V_inter(3,i) = e_11_mean - 0.5d0*De_11
        V_inter(4,i) = e_12_mean - 0.5d0*De_12
        V_inter(5,i) = e_22_mean - 0.5d0*De_22
        V_inter(6,i) = mu_mean - 0.5d0*Dmu  ! Not validated yet

        ! Right state of cell
        V_inter(7,i) = rho_mean + 0.5D0*Drho
        V_inter(8,i) = u_mean + 0.5D0*Du
        V_inter(9,i) = v_mean + 0.5D0*Dv
        V_inter(10,i) = e_11_mean + 0.5D0*De_11
        V_inter(11,i) = e_12_mean + 0.5D0*De_12
        V_inter(12,i) = e_22_mean + 0.5D0*De_22
        V_inter(13,i) = mu_mean + 0.5d0*Dmu  ! Not validated yet

    enddo

    ! Boundary values cannot be reconstructed
    do j = 0, nvar-1
        ! Left boundary
        V_inter(j,0) = V(j,0)
        V_inter(nvar+j,0) = V(j,0)
        ! Right boundary
        V_inter(j,N+1) = V(j,N+1)
        V_inter(nvar+j,N+1) = V(j,N+1)
    enddo

end subroutine compute_val_inter_o2


subroutine flux_bouchut_o1(N, V, F)

    implicit none
    integer, intent(in) :: N
    double precision, dimension(0:nvar-1,0:N+1), intent(in) :: V
    double precision, dimension(0:nvar-1,0:N+1), intent(inout) :: F
    double precision :: rhoj, uj, vj, muj, u_p, u_m
    double precision, dimension(0:3) :: Fp_R, Fm_L
    integer :: i, j

    F(:,1) = 0.d0  ! Initialize flux at first (real) cell

    do j = 1, N
        rhoj = V(0,j)
        if (rhoj /= 0.d0) then
            uj = V(1,j)
            vj = V(2,j)
            muj = V(6,j)

            u_p = max(uj, 0.d0)
            u_m = min(uj, 0.d0)

            Fp_R(0) = rhoj*u_p
            Fp_R(1) = rhoj*uj*u_p
            Fp_R(2) = rhoj*vj*u_p
            Fp_R(3) = rhoj*muj*u_p
            Fm_L(0) = rhoj*u_m
            Fm_L(1) = rhoj*uj*u_m
            Fm_L(2) = rhoj*vj*u_m
            Fm_L(3) = rhoj*muj*u_m
        else
            Fp_R(:) = 0.d0
            Fm_L(:) = 0.d0
        endif
        F(0:2,j) = F(0:2,j) + Fm_L(0:2)  ! Compute F_rho, F_u, F_v
        F(6,j) = F(6,j) + Fm_L(3)  ! Compute F_mu
        F(0:2,j+1) = Fp_R(0:2)
        F(6,j+1) = Fp_R(3)
    enddo

end subroutine flux_bouchut_o1


double precision function Dphi(Drho, rhoj, phijm1, phij, phijp1, dx, dt)
    implicit none
    double precision, intent(in) :: Drho, rhoj, phijm1, phij, phijp1, dx, dt
    Dphi = 0.5D0*(dsign(1.0D0, phijp1 - phij) + dsign(1.0D0, phij - phijm1))* &
           min((dabs(phijp1 - phij))/(1.0D0 - dx*Drho/(6.d0*rhoj)), &
                 (dabs(phij - phijm1))/(1.0D0 + dx*Drho/(6.d0*rhoj)), &
                  dx/dt) / dx
end function Dphi


subroutine flux_bouchut_o2(N, V, F, dx, dt)

    implicit none
    integer, intent(in) :: N
    double precision, intent(in) :: dx, dt
    double precision, dimension(0:nvar-1,0:N+1), intent(in) :: V
    double precision, dimension(0:nvar-1,0:N+1), intent(inout) :: F
    double precision :: rhoj, rhojp1, rhojm1
    double precision :: uj, ujp1, ujm1, vj, vjp1, vjm1, muj, mujp1, mujm1
    double precision :: Drho, Du, Dv, Dmu, u_mean, v_mean, mu_mean
    double precision :: rho_L, u_L, rho_R, u_R, u_p_R, u_m_L
    double precision :: v_L, v_R, mu_L, mu_R
    double precision, dimension(0:3) :: Fp_R, Fm_L
    double precision :: a_L, a_m_L, a_R, a_p_R
    double precision :: b1_p, b2_p, b3_p, b4_p, b1_m, b2_m, b3_m, b4_m
    integer :: i, j

    F(:,1) = 0.d0  ! Initialize flux at first (real) cell

    do j = 1, N
        rhoj = V(0,j)
        if (rhoj /= 0.d0) then
            ! Computing slopes
            rhojp1 = V(0,j+1)
            rhojm1 = V(0,j-1)
            ujp1   = V(1,j+1)
            ujm1   = V(1,j-1)
            uj     = V(1,j)
            vjp1   = V(2,j+1)
            vjm1   = V(2,j-1)
            vj     = V(2,j)
            mujp1  = V(6,j+1)
            mujm1  = V(6,j-1)
            muj    = V(6,j)
            Drho = 0.5D0*(dsign(1.0D0, (rhojp1 - rhoj)) + &
                          dsign(1.0D0, (rhoj - rhojm1))) * &
                          min(dabs(rhojp1 - rhoj), dabs(rhoj - rhojm1), &
                                2.0D0*rhoj)/dx
            Du = Dphi(Drho, rhoj, ujm1, uj, ujp1, dx, dt)
            Dv = Dphi(Drho, rhoj, vjm1, vj, vjp1, dx, dt)
            Dmu = Dphi(Drho, rhoj, vjm1, vj, vjp1, dx, dt)

            if (Du <= -1.0d0/dt) then
                write(*,*) "WARNING: Du <= -1/dt "
            endif
            u_mean = uj - Drho*Du/(12.0D0*rhoj)*dx*dx
            v_mean = vj - Drho*Dv/(12.0D0*rhoj)*dx*dx
            mu_mean = muj - Drho*Dmu/(12.0D0*rhoj)*dx*dx

            ! L: cell left state
            rho_L = rhoj - 0.5D0*Drho*dx
            u_L   = u_mean - 0.5D0*Du*dx
            v_L   = v_mean - 0.5D0*Dv*dx
            mu_L  = mu_mean - 0.5D0*Dmu*dx

            ! R: cell right state
            rho_R = rhoj + 0.5D0*Drho*dx
            u_R   = u_mean + 0.5D0*Du*dx
            v_R   = v_mean + 0.5D0*Dv*dx
            mu_R  = mu_mean + 0.5D0*Dmu*dx

            u_p_R = max(u_R, 0.d0)
            u_m_L = min(u_L, 0.d0)

            a_R   = u_R/(1 + dt*Du)
            a_p_R = u_p_R/(1 + dt*Du)
            b1_p  = Drho
            b2_p  = -rho_R*Du + a_R*Drho + dt/3.*Drho*Du*a_R
            b3_p  = rho_R*Dv + v_R*Drho - 2*dt/3.*Drho*Dv*a_R
            b4_p  = rho_R*Dmu + mu_R*Drho - 2*dt/3.*Drho*Dmu*a_R

            a_L   = u_L/(1 + dt*Du)
            a_m_L = u_m_L/(1 + dt*Du)
            b1_m  = Drho
            b2_m  = -rho_L*Du + a_L*Drho + dt/3.*Drho*Du*a_L
            b3_m  = rho_L*Dv + v_L*Drho - 2*dt/3.*Drho*Dv*a_L
            b4_m  = rho_L*Dmu + mu_L*Drho - 2*dt/3.*Drho*Dmu*a_L

            Fp_R(0) = rho_R*a_p_R - 0.5d0*dt*a_p_R*a_p_R*b1_p
            Fp_R(1) = rho_R*a_R*a_p_R - 0.5d0*dt*a_p_R*a_p_R*b2_p
            Fp_R(2) = rho_R*v_R*a_p_R - 0.5d0*dt*a_p_R*a_p_R*b3_p
            Fp_R(3) = rho_R*mu_R*a_p_R - 0.5d0*dt*a_p_R*a_p_R*b4_p

            Fm_L(0) = rho_L*a_m_L - 0.5d0*dt*a_m_L*a_m_L*b1_m
            Fm_L(1) = rho_L*a_L*a_m_L - 0.5d0*dt*a_m_L*a_m_L*b2_m
            Fm_L(2) = rho_L*v_L*a_m_L - 0.5d0*dt*a_m_L*a_m_L*b3_m
            Fm_L(3) = rho_L*mu_L*a_m_L - 0.5d0*dt*a_m_L*a_m_L*b4_m

        else
            Fp_R(:) = 0.d0
            Fm_L(:) = 0.d0
        endif

        F(0:2,j) = F(0:2,j) + Fm_L(0:2)  ! Compute F_rho, F_u, F_v
        F(6,j) = F(6,j) + Fm_L(3)  ! Compute F_mu
        F(0:2,j+1) = Fp_R(0:2)
        F(6,j+1) = Fp_R(3)
    enddo

end subroutine flux_bouchut_o2


subroutine flux_relax(N, V_inter, F, is_energy)

    implicit none
    integer, intent(in) :: N
    integer, dimension(0:N+1), intent(in) :: is_energy
    double precision, dimension(0:2*nvar-1,0:N+1), intent(inout) :: V_inter
    double precision, dimension(0:nvar-1,0:N+1), intent(inout) :: F
    integer :: j
    integer :: flux_with_press

    do j = 1, N+1
        ! Compute parameters for with/without pressure interfacing
        flux_with_press = max(is_energy(j), is_energy(j-1))

        call flux_relax_cell(V_inter(nvar:2*nvar-1,j-1), V_inter(0:nvar-1,j), &
                             F(0:nvar-1,j), flux_with_press)
    enddo

end subroutine flux_relax


subroutine flux_relax_cell(Vl, Vr, F, is_energy)

    implicit none
    double precision, dimension(0:nvar-1), intent(in) :: Vl, Vr
    double precision, dimension(0:nvar-1), intent(inout) :: F

    integer :: is_energy
    double precision :: rho_l, u_l, v_l, muk_l
    double precision :: e11_l, e12_l, e22_l, c_l, p11_l, p12_l
    double precision :: rho_r, u_r, v_r, muk_r
    double precision :: e11_r, e12_r, e22_r, c_r, p11_r, p12_r
    double precision :: vo_l, vo_r, a_l, a_r, inverse_al, inverse_ar
    double precision :: u_star, v_star, p11_star, p12_star, muk_star
    double precision :: rho_temp, u_temp, v_temp, muk_temp
    double precision :: e11_temp, e12_temp, e22_temp, p11_temp, p12_temp

    F = 0.d0  ! Initialization of output

    if ((Vl(0) <= mac_prec) .and. (Vr(0) <= mac_prec)) then
        F(:) = 0.d0  ! If void at left and right, all fluxes are zero
    else
        ! Clip to zero for small values of rho
        if (Vl(0) <= mac_prec) then
            rho_l = 0.D0
            rho_r = Vr(0)
        else if (Vr(0) <= mac_prec) then
            rho_r = 0.D0
            rho_l = Vl(0)
        else
            rho_r = Vr(0)
            rho_l = Vl(0)
        end if

        u_l = Vl(1)
        v_l = Vl(2)
        u_r = Vr(1)
        v_r = Vr(2)
        muk_l = Vl(6)
        muk_r = Vr(6)

        ! Compute pressures and sound velocity
        if (is_energy == 0) then
            e11_l = 0.D0
            e12_l = 0.D0
            e22_l = 0.D0
            e11_r = 0.D0
            e12_r = 0.D0
            e22_r = 0.D0
        else if (is_energy == 1) then
            e11_l = Vl(3)
            e12_l = Vl(4)
            e22_l = Vl(5)
            e11_r = Vr(3)
            e12_r = Vr(4)
            e22_r = Vr(5)
        else
            write(*,*) 'Problem: is_energie should be equal to 0 or 1'
            e11_l = Vl(3)
            e12_l = Vl(4)
            e22_l = Vl(5)
            e11_r = Vr(3)
            e12_r = Vr(4)
            e22_r = Vr(5)
        end if
        ! c_l is taken >= epsi_e
        c_l = dsqrt(gamma*(gamma - 1.d0)*max(e11_l, epsi_e))
        ! c_r is taken >= epsi_e
        c_r = dsqrt(gamma*(gamma - 1.d0)*max(e11_r, epsi_e))
        p11_l = (gamma - 1.d0)*rho_l*e11_l
        p12_l = (gamma - 1.d0)*rho_l*e12_l
        p11_r = (gamma - 1.d0)*rho_r*e11_r
        p12_r = (gamma - 1.d0)*rho_r*e12_r

        if (rho_l <= mac_prec) then
            vo_l = c_l + alpha*Pos(+u_l - u_r)
            a_l = 0.D0  ! as a_l=rho_l*(...)
            vo_r = c_r + alpha*Pos(+u_l - u_r)  ! vo_r = a_r/rho_r
            a_r = vo_r*rho_r
            inverse_al = 0.D0
            inverse_ar = 1.D0/a_r
        else if (rho_r <= mac_prec) then
            vo_r = c_r + alpha*Pos(+u_l - u_r)
            a_r = 0.D0
            vo_l = c_l + alpha*Pos(+u_l - u_r)
            a_l = vo_l*rho_l
            inverse_al = 1.D0/a_l
            inverse_ar = 0.D0
        else  ! Case where rho_r and rho_l > mac_prec
            if (p11_r >= p11_l) then  ! Case where p_r > p_l
                vo_l = c_l + alpha*Pos((p11_r - p11_l)/(rho_r*c_r) + u_l - u_r)
                a_l = vo_l*rho_l
                vo_r = c_r + alpha*Pos((p11_l - p11_r)/a_l + u_l - u_r)
                a_r = vo_r*rho_r
            else  ! Case where p_l > p_r
                vo_r = c_r + alpha*Pos((p11_l - p11_r)/(rho_l*c_l) + u_l - u_r)
                a_r = vo_r*rho_r
                vo_l = c_l + alpha*Pos((p11_r - p11_l)/a_r + u_l - u_r)
                a_l = vo_l*rho_l
            end if
            inverse_al = 1.0D0/a_l
            inverse_ar = 1.0D0/a_r
        end if

        ! Compute other parameters using relaxation formula
        if ((a_l + a_r) == 0.d0) then
            write(*,*) 'Problem: a_l + a_r = 0'
        endif

        u_star = (a_l*u_l + a_r*u_r + p11_l - p11_r)/(a_l + a_r)
        v_star = (a_l*v_l + a_r*v_r + p12_l - p12_r)/(a_l + a_r)
        p11_star = (a_r*p11_l + a_l*p11_r - a_r*a_l*(u_r - u_l))/(a_l + a_r)
        p12_star = (a_r*p12_l + a_l*p12_r - a_r*a_l*(v_r - v_l))/(a_l + a_r)
        muk_star = (a_l*muk_l + a_r*muk_r)/(a_l + a_r)

        ! Choose the characteristic according to the computed velocities
        if ((u_l - vo_l) <= (u_r + vo_r)) then
            ! eigen value (u_l-a_r/rho_l is positive
            ! so u* and u_r+a_r/rho_r are positive
            if ((u_l - vo_l) >= 0.D0) then
                ! All eigen values are positive
                ! Interface value is left value
                rho_temp = rho_l
                u_temp = u_l
                v_temp = v_l
                p11_temp = p11_l
                p12_temp = p12_l
                e11_temp = e11_l
                e12_temp = e12_l
                e22_temp = e22_l
                muk_temp = muk_l

            ! eigen values u_r-a_r/rho_r is negative and u* > 0
            ! so u_r+a_r/rho_r is positive
            else if (u_star > 0.D0) then
                if ((u_star - (u_l - vo_l)) == 0.d0) then  ! Exception
                     write(*,*) 'Problem: v_e - u_l = 0'
                endif
                rho_temp = a_l/(u_star - (u_l - vo_l))
                u_temp = u_star
                v_temp = v_star
                p11_temp = p11_star
                p12_temp = p12_star
                e11_temp = e11_l + (p11_star**2 - p11_l**2)/2.D0*inverse_al**2
                e12_temp = e12_l + (p11_star*p12_star - &
                                    p11_l*p12_l)/2.D0*inverse_al**2
                e22_temp = e22_l + (p12_star**2 - p12_l**2)/2.D0*inverse_al**2
                muk_temp = muk_star

            ! eigen values u_r-a_r/rho_r and u* are negative
            ! and u_r+a_r/rho_r is positive
            else if ((u_r + vo_r) > 0.0d0) then
                if ((u_star - (u_l - vo_l)) == 0.D0) then  ! Exception
                     write(*,*) 'Problem: v_e - u_r = 0'
                endif
                rho_temp = a_r/((u_r + vo_r) - u_star)
                u_temp = u_star
                v_temp = v_star
                p11_temp = p11_star
                p12_temp = p12_star
                e11_temp = e11_r + (p11_star**2 - p11_r**2)/2.D0*inverse_ar**2
                e12_temp = e12_r + (p11_star*p12_star - &
                                    p11_r*p12_r)/2.D0*inverse_ar**2
                e22_temp = e22_r + (p12_star**2 - p12_r**2)/2.D0*inverse_ar**2
                muk_temp = muk_star

            ! eigen values u_r-a_r/rho_r, u*, u_r+a_r/rho_r are negative
            else
                rho_temp = rho_r
                u_temp = u_r
                v_temp = v_r
                p11_temp = p11_r
                p12_temp = p12_r
                e11_temp = e11_r
                e12_temp = e12_r
                e22_temp = e22_r
                muk_temp = muk_r
            endif

        else   ! (u_l-a_l/rho_l) > (u_r+a_r/rho_r)
            write (*,*) 'Error in flux_relax_cell: u_l - vo_l > u_r + vo_r'
            if ((rho_l == 0.d0) .or. (rho_r == 0.d0)) then
                write (*,*) 'Problem: rho_r or rho_l is zero'
            end if

            rho_temp = 0.d0
            u_temp = 0.d0
            v_temp = 0.d0
            p11_temp = 0.d0
            p12_temp = 0.d0
            e11_temp = 0.d0
            e12_temp = 0.d0
            e22_temp = 0.d0
            muk_temp = 0.d0

        endif

        ! Compute 2 first flux components
        F(0) = rho_temp*u_temp
        F(1) = rho_temp*u_temp**2 + p11_temp

        ! Compute 3rd component
        if (iso == 1) then  ! Isotropic case
            if ((rho_temp*u_temp) > 0.D0) then
                v_temp = v_l
            else
                v_temp = v_r
            end if
            F(2) = rho_temp*u_temp*v_temp
        else
            F(2) = rho_temp*u_temp*v_temp + p12_temp  ! Anisotropic case
        endif

        ! Compute energy components
        if (is_energy == 0) then
            F(3) = 0.d0
            F(4) = 0.d0
            F(5) = 0.d0
        else if (is_energy == 1) then
            if (iso == 0) then
                F(3) = rho_temp*u_temp*(e11_temp + u_temp**2*0.5D0) + &
                       p11_temp*u_temp
                F(4) = rho_temp*u_temp*(e12_temp + (u_temp*v_temp)*0.5D0) + &
                       0.5D0*(p11_temp*v_temp + p12_temp*u_temp)
                F(5) = rho_temp*u_temp*(e22_temp + (v_temp**2)*0.5D0) + &
                       p12_temp*v_temp
            else
                F(3) = rho_temp*u_temp*(e11_temp + &
                       (u_temp**2+v_temp**2)*0.5D0) + p11_temp*u_temp
            endif
        else
            write(*,*) 'Problem: is_energy should be equal to 0 or 1'
        end if

        F(6) = rho_temp*u_temp*muk_temp  ! Compute muk (passive scalar)

        if ((Vl(0) < 0.D0) .or. (Vr(0) < 0.D0)) then
            write(*,*) "Problem: rho is negative"
            write(*,*) 's1', u_l - vo_l, u_star, u_r + vo_r
            write(*,*) a_l, a_r
            write(*,*) rho_temp, u_temp
        end if
    end if

end subroutine flux_relax_cell


subroutine update_1D(N, V, F, is_energy, dt)

    implicit none
    integer, intent(in) :: N
    double precision, intent(in) :: dt
    double precision, dimension(0:nvar-1,0:N+1), intent(inout) :: V
    integer, dimension(0:N+1), intent(in) :: is_energy
    double precision, dimension(0:nvar-1,0:N+1), intent(in) :: F
    integer :: j
    integer :: is_energy_next
    double precision :: rhou_n, rhov_n, rhomuk_n
    double precision :: rho_e11, rho_e12, rho_e22, irho

    do j = 1, N
        ! Compute parameters for with/without pressure interfacing
        is_energy_next = max(is_energy(j+1), is_energy(j), is_energy(j-1))

        rhou_n = V(0,j)*V(1,j)
        rhov_n = V(0,j)*V(2,j)
        if (iso == 0) then
            rho_e11 = V(0,j)*(V(3,j) + 0.5D0*V(1,j)**2)
            rho_e12 = V(0,j)*(V(4,j) + 0.5D0*V(1,j)*V(2,j))
            rho_e22 = V(0,j)*(V(5,j) + 0.5D0*V(2,j)**2)
        else
            rho_e11 = V(0,j)*(V(3,j) + 0.5D0*(V(1,j)**2 + V(2,j)**2))
        endif
        rhomuk_n = V(0,j)*V(6,j)

        V(0,j) = V(0,j) - dt/dx*(F(0,j+1) - F(0,j))

        if (V(0,j) <= mac_prec) then
            if (V(0,j) < 0.d0) then
                write(*,*) "Error, negative rho in update_1D", j, V(0,j)
            endif
            V(:,j) = 0.D0
        else
            irho = 1.D0/V(0,j)
            V(1,j) = irho*(rhou_n - dt/dx*(F(1,j+1) - F(1,j)))
            V(2,j) = irho*(rhov_n - dt/dx*(F(2,j+1) - F(2,j)))
            V(6,j) = irho*(rhomuk_n - dt/dx*(F(6,j+1) - F(6,j)))

            if (iso == 0) then
                V(3,j) = irho*(rho_e11 - dt/dx*(F(3,j+1) - F(3,j)) - &
                               0.5D0*V(0,j)*V(1,j)**2)
                V(4,j) = irho*(rho_e12 - dt/dx*(F(4,j+1) - F(4,j)) - &
                               0.5D0*V(0,j)*V(1,j)*V(2,j))
                V(5,j) = irho*(rho_e22 - dt/dx*(F(5,j+1) - F(5,j)) - &
                               0.5D0*V(0,j)*V(2,j)**2)
            else
                V(3,j) = irho*(rho_e11 - dt/dx*(F(3,j+1) - F(3,j)) - &
                               0.5D0*V(0,j)*(V(1,j)**2 + V(2,j)**2))
                V(4:5,j) = 0.D0
            endif
            V(3:5,j) = is_energy_next*V(3:5,j)
        endif
    enddo

end subroutine update_1D


subroutine update_2D(N, V, F, is_energy, dt)

    implicit none
    integer, intent(in) :: N
    double precision, intent(in) :: dt
    double precision, dimension(0:nvar-1,0:N+1,0:N+1), intent(inout) :: V
    integer, dimension(0:N+1,0:N+1), intent(in) :: is_energy
    double precision, dimension(0:nvar-1,0:N+1,0:N+1), intent(in) :: F
    integer :: i

    do i = 0, N+1
        call update_1D(N, V(:,:,i), F(:,:,i), is_energy(:,i), dt)
    enddo

end subroutine update_2D


subroutine transpose_2d(N, V, is_energy)
! Modify velocity fields for alternate directions

    implicit none
    integer, intent(in) :: N
    double precision, dimension(0:nvar-1,0:N+1,0:N+1), intent(inout) :: V
    integer, dimension(0:N+1,0:N+1), intent(inout) :: is_energy
    integer :: i

    do i = 0, nvar-1
        V(i,:,:) = transpose(V(i,:,:))
    end do

    ! Permutting velocity components
    V(1:2,:,:) = V(2:1:-1,:,:)
    if (iso == 0) then
        V(3:5,:,:) = V(5:3:-1,:,:)
    endif
    ! Transposition also on is_energy sensor
    is_energy(:,:) = transpose(is_energy(:,:))

end subroutine transpose_2d

! Other functions

double precision function Pos(r)

    implicit none
    double precision, intent(in) :: r
    if (r < 0.D0) then
        Pos = 0.D0
    else
        Pos = r
    end if

end function Pos

double precision function Neg(r)

    implicit none
    double precision, intent(in) :: r
    Neg = -Pos(-r)

end function Neg

double precision function minmod(x, y)

    implicit none
    double precision, intent(in) :: x, y
    if ((x > 0.0E0) .and. (y > 0.0E0)) then
        minmod = min(x, y)
    else if ((x < 0.0E0) .and. (y < 0.0E0)) then
        minmod = max(x, y)
    else
        minmod = 0.0E0
    end if

end function minmod


double precision function superbee(x, y)

    implicit none
    double precision, intent(in) :: x, y
    if ((x > 0.0E0) .and. (y > 0.0E0)) then
        superbee = max(min(2*x, y), min(y, 2*x))
    else if ((x < 0.0E0) .and. (y < 0.0E0)) then
        superbee = min(max(2*x, y),max(y, 2*x))
    else
        superbee = 0.0E0
    end if

end function superbee

end module transport
