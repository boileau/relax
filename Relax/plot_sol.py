#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
This module is to be called outside the main program after the simulation is
done. \n
Usage: plot_sol.py [-h] --sol File --var Var \n
where:\n
- File is usually solution.h5 \n
- var can be rho, u, v, e_11, e_12, e_22, p_11, p_12 or p_22
"""

from matplotlib import rc, rcdefaults
import matplotlib.pyplot as plt
import argparse
from numpy import zeros, linspace
import os
from Relax.data_process import build_vardict, get_grid, get_ite_list, \
                               get_last_ite, get_param, get_probe_data, \
                               get_probe_info, get_probe_list, \
                               get_probe_mask, get_probe_description, \
                               get_time, load_color, load_snapshot, \
                               open_solution, print_ite_list, \
                               print_probe_list, varnames
from Relax import figures
from Relax import nvar

ascii_prefix = "./out/"


def set_rc():
    """Define Default Figure Parameters"""
    rcdefaults()  # Reset rc
    # 5 values: 'serif' (Times), 'sans-serif' (Helvetica),
    # 'cursive' (Zapf-Chancery), 'fantasy' (Western), and 'monospace' (Courier)
    rc('font', family='sans-serif', style='normal')
    rc('legend', fancybox=True, numpoints=1, fontsize='medium')
    # predefined figure size, (use "plt.figure(figsize=(8,6))" otherwise)
    rc('figure', figsize=(7.0, 6.0))
    # define the figure workspace
    rc('figure.subplot', left=0.125, right=0.95, top=0.9, bottom=0.1)
    # 'ms' can be also used instead of 'markersize'
    rc('lines', antialiased=True, markersize=6.5, color='black')
    # predefined label font size (use "xlabels(fontsize=10)" otherwise)
    rc('axes',  labelsize=14)
    # predefined fontsize for x and y-axis ticks marks
    # (use "xticks(size=10)" otherwise)
    rc('xtick', labelsize=12)
    rc('ytick', labelsize=12)
    rc('xtick.major', pad=10)
    rc('ytick.major', pad=6)


class Probe(object):
    """This class defines the data and methods associated to a probe."""

    def __init__(self, sol):
        self.name = sol.probename
        self.f = sol.f
        self.varname = sol.varname

        # Initialize figure for probe
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)
        self.ax.set_xlabel("time")
        self.ax.set_ylabel(self.varname)
        self.__read_info()  # Read position and comment
        title = 'Time evolution of {} for {}'.format(self.varname,
                                                     self.description)
        self.fig.suptitle(title, fontsize=14)
        self.x = self.time
        self.nite = len(self.time)

    def __read_info(self):
        self.coor, self.comment = get_probe_info(self.f, self.name)
        self.mask = get_probe_mask(self.f, self.name)
        self.description = get_probe_description(self.f, self.name)
        print('Probe: ' + self.description)
        self.time = get_probe_data(self.f, self.name, 'time')


class PolyProbe(Probe):
    """Methods associated to a probe in a polydisperse simulation"""

    def __init__(self, sol):
        # Call mother class constructor
        super(PolyProbe, self).__init__(sol)
        self.Smax = sol.Smax
        self.nsect = sol.nsect
        self.gamma = sol.gamma
        self.ite = sol.ite
        self.kfig = plt.figure()
        title = "{}: size distribution of {} for iteration {} (t = {})".format(
                self.varname, self.name, self.ite, sol.t)
        self.kfig.suptitle(title, fontsize=14)
        self.kax = self.kfig.add_subplot(111)
        self.kax.set_xlabel(r"Size $[\mu m^2]$")
        self.kax.set_ylabel(self.varname)
        self.S = linspace(0., self.Smax, self.nsect)
        self.__kload()
        self._sum_load()
        self.show = True

    def __kload(self):
        """Build a dictionnary for each k-section"""
        Vk = zeros((nvar, self.nsect))
        for k in range(self.nsect):
            path = "snapshot/ite_{}/sect_{}/solution".format(self.ite, k)
            Vk[:, k] = self.f[path][:, self.mask[:]][:, 0]

        self.kvardict = build_vardict(Vk[:], self.nsect, self.gamma)

    def kplot(self):
        """Plot size-distribution of given variable"""
        self.kax.plot(self.S, self.kvardict[self.varname], '-', lw=1, mfc='k',
                      markevery=1)
        if self.show:
            plt.show()

    def _sum_load(self):
        path = "probes/" + self.name + "/sum_data"
        self.sum_data = self.f[path][()]
        self.header = self.f[path].attrs.get("text")

    def print_sum_data(self):

        filename = ascii_prefix + self.name + "_sum_data.dat"
        with open(filename, 'w') as f:
            print("Output section-average data in {}".format(filename))
            f.write("{}\n".format(self.header))
            for time, sum_data in zip(self.time, self.sum_data):
                data_str = "".join("{:.8e} ".format(var) for var in sum_data)
                line = "{:.8e} {}\n".format(time, data_str)
                f.write(line)


class Data(object):
    """Master Class that defines the data to plot and the methods"""

    def __init__(self, sol):
        self.sol = sol
        self.f = self.sol.f
        varname = self.sol.varname
        self.nsect = self.sol.nsect
        self.ndim = self.sol.ndim
        self.x = zeros(self.sol.N)
        self.x = get_grid(self.f)
        self.ite = self.sol.ite

        # Read snapshot info
        self.time = get_time(self.f, self.ite)
        self.color = load_color(self.f, ite=self.ite)

        if not (self.sol.probename or self.sol.ascii):
            # Initialize figure for space cut
            if self.sol.batch:
                plt.ioff()

            self.fig = plt.figure()
            title = "Distribution of {} for iteration {} (t = {:.3e})".\
                    format(varname, self.ite, self.time)
            self.fig.suptitle(title, fontsize=14)
            self.ax = self.fig.add_subplot(111)
            self.ax.set_xlabel("x")
            self.ax.set_ylabel(varname)

    def _set_probe(self, probe_object):
        """Instanciate a probe object"""
        if self.sol.probename:
            self.probe = probe_object(self.sol)
        else:
            self.probe = None


class MonoDisperse(Data):
    """This class defines data and methods for monodisperse cases"""

    def __init__(self, sol):
        # Call mother class constructor
        super(MonoDisperse, self).__init__(sol)
        self._set_probe(Probe)
        self.section = Section(self, 0)  # Create a single section dataset

    def get_data(self):
        """Store solution in a dictionnary"""
        self.section.load()
        if self.probe:
            self.section.load_probe()

    def plot_data(self):
        """Plot data to screen"""
        self.section.plot()

    def print_data(self):
        """Print data to ascii file"""
        self.section.print_data()


class PolyDisperse(Data):
    """
    This class defines additional data for polydisperse case and
    corresponding methods.
    """

    def __init__(self, sol, isec):
        # Call mother class constructor
        super(PolyDisperse, self).__init__(sol)
        self.isec = isec
        self._set_probe(PolyProbe)
        if self.isec == 'all':
            self.sections = [Section(self, k) for k in range(self.nsect)]
            self.section_sum = Section(self, 'sum')
            self.sections_all = self.sections + [self.section_sum]
        else:
            self.sections_all = [Section(self, self.isec)]

    def get_data(self):
        """Store solution in a dictionnary for all sections"""
        for section in self.sections_all:
            section.load()
            if self.probe:
                if section.k == 'sum':
                    section.load_probe_sum()
                else:
                    section.load_probe()
        if self.probe:
            self.section_sum.load_probe_sum()

    def plot_data(self):
        """Plot data to screen for all sections"""
        [section.plot() for section in self.sections_all]
        # plot size distribution at probe position
        if self.probe:
            self.probe.kplot()

    def print_data(self):
        """Print data to ascii file"""
        [section.print_data() for section in self.sections]
        if self.probe:
            self.probe.print_sum_data()


class Section(object):
    """
    This class defines the data and methods associated to a single section.
    """

    def __init__(self, data, k):

        self.data = data
        sol = self.data.sol
        self.N = sol.N
        self.gamma = sol.gamma
        self.varname = sol.varname
        self.figname = sol.figname
        self.batch = sol.batch
        if sol.probename:
            self.probe = self.data.probe
            self.probe_vardict = []
        self.k = k  # section number
        self.f = self.data.f  # file name
        self.ite = self.data.ite

        # Define methods according to dimension and type of plot
        if (self.data.ndim == 1):
            self.dimtype = '1D'
            self.print_data = self.print_1D
            if sol.probename:
                self.plot = self.plot_probe
            else:
                self.plot = self.plot_1D
        else:  # 2D case
            self.dimtype = '2D'
            if sol.probename:
                self.plot = self.plot_probe
            else:
                self.plot = self.plot_2D

    def load(self):
        """Store solution data as a dictionary"""
        self.time, self.V = load_snapshot(self.f, ite=self.ite, k=self.k)
        self.vardict = build_vardict(self.V, self.N, self.gamma,
                                     color=self.data.color)

    def plot_1D(self):
        """Plot section in 1D"""

        if self.figname:
            figures.plot(self)
        else:
            # Default layout is used
            label = 'k = {}'.format(self.k)
            self.data.ax.plot(self.data.x, self.vardict[self.varname], '-',
                              lw=1, mfc='k', markevery=1, label=label)
            self.data.ax.legend(loc='upper right', frameon=False)

    def plot_2D(self):
        """Plot section in 2D"""

        if self.figname:
            figures.plot(self)
        else:
            # Default layout is used
            x = self.data.x
            ax = self.data.ax
            ctf = self.data.ax.contourf(x, x, self.vardict[self.varname], 256)
            ax.autoscale_view(True, True, True)
            ax.set_ylabel(r'$y$', labelpad=12)
            ax.set_xticks(linspace(x.min(), x.max(), 5.), '')
            cbfraction = 0.15
            cbshrink = 0.8
            cbformat = '%.2g'
            plt.colorbar(ctf, fraction=cbfraction,
                         shrink=cbshrink,
                         format=cbformat)

    def print_1D(self):
        """Write 1D array in a file"""
        if self.data.nsect == 0:
            filename = ascii_prefix + self.varname + ".dat"
        else:
            filename = ascii_prefix + "{}_{}.dat".format(self.varname, self.k)
        with open(filename, 'w') as f:
            print("Output {}(x) in {}".format(self.varname, filename))
            header = "# x | {} \n".format(self.varname)
            f.write(header)
            for i in range(0, self.N):
                f.write("{:.8e} {:.8e} \n".format(self.data.x[i],
                        self.vardict[self.varname][i]))

    def load_probe(self):
        """Load and store all probes as a dictionary"""
        path = "probes/{}/data".format(self.probe.name)
        data = self.f[path][self.k, :, :, 0].transpose()
        self.probe_vardict = build_vardict(data, self.probe.nite, self.gamma)

    def load_probe_sum(self):
        """Load and store all probes as a dictionary"""
        sum_data = get_probe_data(self.f, self.probe.name, 'sum_data')

        self.probe_vardict = {}
        self.probe_vardict['nk'] = sum_data[:, 6]
        self.probe_vardict['rho'] = sum_data[:, 0]
        self.probe_vardict['rhou'] = sum_data[:, 1]
        self.probe_vardict['rhov'] = sum_data[:, 2]
        self.probe_vardict['E_11'] = sum_data[:, 3]
        self.probe_vardict['E_12'] = sum_data[:, 4]
        self.probe_vardict['E_22'] = sum_data[:, 5]
        self.probe_vardict['u'] = sum_data[:, 7]
        self.probe_vardict['v'] = sum_data[:, 8]
        self.probe_vardict['e_11'] = sum_data[:, 9]
        self.probe_vardict['e_12'] = sum_data[:, 10]
        self.probe_vardict['e_22'] = sum_data[:, 11]

    def plot_probe(self):
        """Plot section for all probes"""
        if self.k == 'sum':
            linewidth = 2
            label = 'sum'
        else:
            linewidth = 1
            label = 'k = {}'.format(self.k)
        if self.varname in self.probe_vardict:
            self.probe.ax.plot(self.probe.x, self.probe_vardict[self.varname],
                               '-', lw=1, mfc='k', markevery=1,
                               linewidth=linewidth, label=label)
            self.probe.ax.legend(loc='upper right', frameon=False)


class Solution(object):
    """Main class for plotting a hdf5 solution file"""

    def __init__(self, solfile='solution.h5', varname='list', figname=None,
                 batch=False, probename=None, ascii=False, ite_str='last',
                 isec='all', extra=None):
        set_rc()  # set default rc

        self.varname = varname
        self.figname = figname
        self.batch = batch
        self.probename = probename
        self.ascii = ascii
        self.extra = extra

        if self.varname == 'list':  # Give the list of variables to user
            print("List of variables to plot:")
            for name in varnames:
                print("- " + name)
            self.listonly = True
        else:
            self.listonly = False

        self.f = open_solution(solfile)

        if ite_str == 'last':  # Default is last ite
            self.ite = get_last_ite(self.f)
            self.t = get_time(self.f, self.ite)
            print('Plotting last snapshot {} (t = {})'.format(self.ite,
                                                              self.t))
        else:
            if ite_str == 'list':
                print_ite_list(self.f)
                self.listonly = True
            else:
                ite_list = get_ite_list(self.f)
                ite = int(ite_str)  # convert to integer
                if ite not in ite_list:
                    print_ite_list(self.f)
                    msg = "ite {} not found in {}".format(ite, solfile)
                    exit(msg)
                else:
                    self.ite = ite
                    self.t = get_time(self.f, self.ite)
                    print('Plotting iteration {} (t = {})'.format(self.ite,
                                                                  self.t))

        if self.probename == 'list':
            print_probe_list(self.f)
            self.listonly = True
        elif self.probename:
            nprobe = get_param(self.f, 'nprobe')  # number of probes
            if nprobe == 0:
                exit("No probe found in " + solfile)
            else:
                probe_list = get_probe_list(self.f)
                if self.probename not in probe_list:
                    print_probe_list(self.f)
                    msg = "probe {} not found in {}".format(self.probename,
                                                            solfile)
                    exit(msg)

        if not self.listonly:  # Read parameters
            self.ndim = get_param(self.f, 'ndim')  # Space dimension
            self.N = get_param(self.f, 'N')  # number of grid points
            self.nsect = get_param(self.f, 'nsect')
            self.Smax = get_param(self.f, 'Smax')
            self.gamma = get_param(self.f, 'gamma')

            if self.nsect == 0:
                if isec != 'all':
                    msg = "Cannot specify a section number in a " + \
                           "monodisperse solution"
                    raise Exception(msg)
                self.data = MonoDisperse(self)
            else:
                self.data = PolyDisperse(self, isec)

    def process(self):
        """Process solution file to plot variable or output an ascii file"""
        if not self.listonly:
            self.data.get_data()  # Load data from file
            # Plot data according to type of simulation (Mono/Polydisperse)
            if self.ascii:  # Output ascii file only
                if not os.path.exists(ascii_prefix):
                    os.makedirs(ascii_prefix)
                self.data.print_data()
            else:
                self.data.plot_data()
                if not self.batch:
                    plt.show()
        self._terminate()

    def _terminate(self):
        """Only close solution file"""
        self.f.close()  # Close and stop processing


def get_input():
    """Get user input from command line"""
    parser = argparse.ArgumentParser(description="Plot solution file in HDF5 \
                                                  format")
    parser.add_argument('--sol', nargs=1, required=False, metavar="File",
                        help="name of solution files")
    parser.add_argument('--ite', nargs=1, required=False, metavar="Ite",
                        help="Iteration number")
    parser.add_argument('--var', nargs=1, required=True, metavar="Var",
                        help="Variable to plot")
    parser.add_argument('--probe', nargs=1, required=False, metavar="probe_0",
                        help="a probe to plot")
    parser.add_argument('--ascii', nargs='?', const=True, required=False,
                        help="Only output an ascii file")
    parser.add_argument('--fig', nargs=1, required=False, metavar="FigName",
                        help="select layout according to figure name")
    parser.add_argument('--sec', nargs='?', required=False, metavar="Section",
                        help="section number or 'sum'")
    parser.add_argument('--batch', action="store_true", required=False,
                        help="Write output file without printing to screen")
    args = parser.parse_args()

    if args.sol:
        solfile = args.sol[0]
    else:
        solfile = 'solution.h5'
        print('Using default solution name: {}'.format(solfile))

    if args.ite:
        ite = args.ite[0]
    else:
        ite = 'last'  # Default ite number is last

    if args.probe:
        probename = args.probe[0]
    else:
        probename = None

    varname = args.var[0]
    batch = args.batch
    ascii = args.ascii

    if args.fig:
        figname = args.fig[0]  # Get fig type if specified
    else:
        figname = None

    if args.sec:
        isec = args.sec
    else:
        isec = 'all'

    return solfile, varname, figname, batch, probename, ascii, ite, isec


if __name__ == '__main__':

    sol = Solution(*get_input())
    sol.process()
