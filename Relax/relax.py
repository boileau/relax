#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Main program of relax code
Usage: relax.py [-h] path/to/run_directory
"""

from Relax import utils
from Relax.simulation import Simulation
from timeit import default_timer


class _Run(object):
    """
    Handle a single relax simulation. This class is instanciated once or
    several times for a parametric study.
    """

    def __init__(self, *args, **kwargs):
        """Initialize simulation according to input file"""
        self.sim = Simulation(*args, **kwargs)

    def run(self):
        """Run simulation"""

        sim = self.sim

        sim.plot_sol(miniplot=True)  # plot initial solution
        sim.store.process()  # probe and store initial solution in solution.h5

        start_time = default_timer()  # Start timer
        while (sim.iterate()):
            sim.model.compute_dt()      # compute time step
            sim.model.ST_step()         # 1/2 step for source terms
            sim.model.transport_step()  # Full step for transport
            sim.model.ST_step()         # 1/2 step for source terms
            sim.update_t()              # t = t + dt
            sim.store.process()         # probe and store solution
            sim.plot_sol()         # plot every dt_plot
            sim.progress()              # Print simulation progress

        sim.elapsed = default_timer() - start_time

        sim.plot_sol(miniplot=True, show=True)  # plot last solution
        sim.store.terminate()  # Store elapsed time and close the .h5 file

        return sim.elapsed


class Relax(object):
    """Main class that instanciates a single or multiple simulation runs"""

    def __init__(self, **kwargs):
        # Get arguments or set to defaults
        rundir = kwargs.get('rundir', '.')
        verbose = kwargs.get('verbose', 0)
        # Parse run.py file to get base parameters dictionary
        basedict = utils.parsefile(rundir)
        # Build a list of run dictionaries
        rundictlist = utils.build_suite(basedict)
        # Instanciate a list of runs corresponding to the list of run dicts
        self.suite = [_Run(rundict, rundir=rundir, verbose=verbose)
                      for rundict in rundictlist]

    def run(self):
        """Execute the run suite and return elapsed time"""
        elapsed_times = [run.run() for run in self.suite]
        if len(elapsed_times) == 1:
            return elapsed_times[0]
        else:
            return elapsed_times


def main():
    """This function is useful for the packaging tool"""
    # Read run directory path and verbosity level from the command line
    # and instanciate a run suite
    relax = Relax(**utils.parsearg())
    relax.run()  # Run the simulation suite

if __name__ == '__main__':
    main()
