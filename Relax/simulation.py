# -*- coding: utf-8 -*-
"""
This module contains the global simulation properties to be shared by
other modules.
"""

from blessings import Terminal
from collections import OrderedDict
from Relax import data_store as ds
from Relax import libfortran as libfort
import matplotlib.pyplot as plt
from progressbar import Percentage, ProgressBar, Bar, FormatLabel
import re
from Relax.scheme import MonoDisperse, PolyDisperse
import sys
from tabulate import tabulate

schemedict = {
    'relax-pgd-o1':       {'type': 'relax_pgd',  'order': 'one'},
    'relax-iso-o1':       {'type': 'relax_iso',  'order': 'one'},
    'relax-pgd-o2':       {'type': 'relax_pgd',  'order': 'two'},
    'relax-iso-o2':       {'type': 'relax_iso',  'order': 'two'},
    'relax-isoh-o1':      {'type': 'relax_isoh', 'order': 'one'},
    'relax-isoh-o2':      {'type': 'relax_isoh', 'order': 'two'},
    'relax-pgd-o2space':  {'type': 'relax_pgd',  'order': 'two-space'},
    'relax-iso-o2space':  {'type': 'relax_iso',  'order': 'two-space'},
    'relax-isoh-o2space': {'type': 'relax_isoh', 'order': 'two-space'},
    'relax-ag-o1':        {'type': 'relax_ag',   'order': 'one'},
    'relax-agh-o1':       {'type': 'relax_agh',  'order': 'one'},
    'Bouchut-o1':         {'type': 'bouchut',    'order': 'one'},
    'Bouchut-o2':         {'type': 'bouchut',    'order': 'two'}
}


class Simulation(object):

    def __init__(self, rundict, **kwargs):
        """Set parameters according to input file"""

        self.rundir = kwargs.get('rundir', '.')
        self.verbose = kwargs.get('verbose', 0)

        self.term = Terminal()  # Initialize a terminal for output

        self._init_param()  # Set global parameters to default values

        for name, value in rundict.items():
            if re.match(r'probe', name):
                # if probe number is not zero, store probe param
                if value:
                    self._set_probes(value)
            else:
                # Update paramdict with values from rundict
                self.update(name, value)

        # Get scheme name from input parameters
        scheme_name = self.paramdict['scheme']['value']
        try:
            # Get dico scheme from list of schemes
            this_scheme = schemedict[scheme_name]
        except KeyError:
            message = "Unknown scheme: " + scheme_name +\
                      "\nAvailable schemes:"
            print(message)
            scheme_names = sorted(schemedict.keys())
            for name in scheme_names:
                print(("  - {}".format(name)))
            sys.exit(1)

        self.schemetype = this_scheme['type']
        self.update('order', this_scheme['order'])
        self.update('rundir', self.rundir)

        # Compute space step from grid parameters
        Lx = self.paramdict['Lx']['value']
        N = self.paramdict['N']['value']
        self.update('dx', Lx/N)

        if self.paramdict['sol_type']['value'] == 'ref':
            compute_sim = False
            compute_ref = True
        elif self.paramdict['sol_type']['value'] == 'both':
            compute_sim = True
            compute_ref = True
        else:
            compute_sim = True
            compute_ref = False

        self.update('compute_sim', compute_sim)
        self.update('compute_ref', compute_ref)

        # Fill simulation namespace with parameters values
        for name, par in self.paramdict.items():
            setattr(self, name, par['value'])

        # Initialize Fortran module parameters with sim values
        libfort.transport.gamma = self.gamma
        libfort.transport.alpha = (self.gamma + 1.0)/2.0
        libfort.transport.epsi_e = self.epsi_e
        libfort.transport.mac_prec = self.mac_prec
        libfort.transport.dx = self.dx
        libfort.transport.st = self.stokes
        if self.schemetype[:8] == 'relax_ag':
            libfort.transport.iso = 0
        else:
            libfort.transport.iso = 1

        # Instanciate a model according to mono or polydisperse configuration
        if self.nsect == 0:
            self.model = MonoDisperse(self)
        else:
            self.model = PolyDisperse(self)

        self.init_t()  # Initialize time variables
        self.store = ds.Store(self)  # Initialize storage in .h5 file
        plt.close('all')  # close all plots

        # Initialize a progress bar for time stepping
        self.init_progressbar()
        self.print_table()  # Print parameters as a table

    def _init_param(self):
        """Reset all parameters to default value"""

        self.probe_list = []
        self.dt_ST = 0.  # Initialize source-term time step
        self.void = False  # Initialize void trigger

        # Set list of parameters
        # (Default values will be overwritten by input file values)
        self.paramdict = OrderedDict([
         ('rundir',   {'value': '.',
                       'comment': 'Run directory'}),
         ('casetype', {'value': 'SOD',
                       'comment': 'Test case type'}),
         ('is2D',     {'value': False,
                       'comment': 'Is the simulation 2D?'}),
         ('scheme',   {'value': None,
                       'comment': 'Scheme type'}),
         ('order',    {'value': 'one',
                       'comment': 'Scheme precision order'}),
         ('CFL',      {'value': 1.0,
                       'comment': 'CFL number'}),
         ('CFL_evap', {'value': 0.4,
                       'comment': 'CFL number for evaporation'}),
         ('epsi_e',   {'value': 1.0e-10,
                       'comment': 'Energy threshold for PGD/GD'}),
         ('mac_prec', {'value': 1.0e-16,
                       'comment': 'Filtering small numbers'}),
         ('xmin',     {'value': 0.0,
                       'comment': 'position of 1rst grid point'}),
         ('Lx',       {'value': 1.0,
                       'comment': 'Length of the domain'}),
         ('N',        {'value': 10,
                       'comment': 'Discretization point number'}),
         ('dx',       {'value': 0.0,
                       'comment': 'Space discretization step'}),
         ('BCx',      {'value': 'closed',
                       'comment': 'X Boundary condition'}),
         ('BCy',      {'value': 'closed',
                       'comment': 'Y Boundary condition'}),
         ('gamma',    {'value': 1.4,
                       'comment': 'Polytropic coefficient'}),
         ('stokes',   {'value': 0.5,
                       'comment': 'Stokes number'}),
         ('isSecST',  {'value': False,
                       'comment': 'Independant ST for each section?'}),
         ('isAllST',  {'value': False,
                       'comment': 'ST coupling all sections?'}),
         ('nsect',    {'value': 0,
                       'comment': 'Number of sections'}),
         ('Smax',     {'value': 0,
                       'comment': 'Maximum particle size'}),
         ('rhol',     {'value': 634.239,
                       'comment': 'Liquid density [kg/m^3]'}),
         ('alpha_drag', {'value': 0.9680385e-6,
                         'comment': 'Drag acceleration [m^2/s]'}),
         ('sol_type', {'value': 'sim',
                       'comment': 'Compute simulation or reference?'}),
         ('compute_ref', {'value': False,
                          'comment': 'Compute reference solution?'}),
         ('compute_sim', {'value': True,
                          'comment': 'Compute simulated solution?'}),
         ('Rs',       {'value': 1.99e-7,
                       'comment': 'Evaporation coeff [m^2/s]'}),
         ('tsm_order', {'value': 2,
                        'comment': 'TSM method order'}),
         ('t0',       {'value': 0.0,
                       'comment': 'Initial simulation time'}),
         ('tf',       {'value': 10.0,
                       'comment': 'Final simulation time'}),
         ('maxite',   {'value': 0,
                       'comment': 'Maximum time iterations'}),
         ('dt_fixed', {'value': 0.,
                       'comment': 'Constant time step'}),
         ('dt_store', {'value': 0.,
                       'comment': 'Storage time step'}),
         ('ite_store', {'value': 0,
                        'comment': 'Storage iteration step'}),
         ('dt_plot',  {'value': 0.,
                       'comment': 'Live solution plotting'}),
         ('miniplot', {'value': True,
                       'comment': 'Plot 1rst and last solution?'}),
         ('nprobe',   {'value': 0,
                       'comment': 'Number of probes'}),
         ('solfile',  {'value': 'solution.h5',
                       'comment': 'solution file name'}),
        ])

        # Add a 'default' field to parameters
        for v in self.paramdict.values():
            v['default'] = True  # Current values are default

    def update(self, name, value):
        """Update parameter value"""
        if name in list(self.paramdict.keys()):
            self.paramdict[name]['value'] = value
            # value is no longer default
            self.paramdict[name]['default'] = False

    def _set_probes(self, probes_input):
        """Set temporal probes in simulation"""
        self.update('nprobe', len(probes_input))
        for index, probe in enumerate(probes_input):
            self.probe_list.append({"name": "probe_" + str(index),
                                    "position": probe['position'],
                                    "comment": probe['comment'],
                                    "data": 0.})

    def print_table(self):
        """Print parameters as a table to output"""
        if self.verbose > 0:
            param = []
            for name, par in self.paramdict.items():
                if par['default']:
                    default = '(default)'
                else:
                    default = ''
                param.append([name, par['value'], default, par['comment']])
            print(tabulate(param))

    def init_t(self):
        """Initialize attributes with zeros"""
        self.ite = 0
        self.t = self.t0
        self.dt = 0.0

    def update_t(self):
        """Update time and iteration number"""
        self.ite += 1
        self.t += self.dt

    def print_t(self):
        """Print current time and iteration to screen"""
        print("ite: {:} t = {:}".format(self.ite, self.t))

    def init_progressbar(self):
        """Initialiaze a progress bar for time stepping"""
        widgets = ["Stepping    >>> ", FormatLabel("t = %(value).3f "),
                   Bar('=', '[', ']'), ' ', Percentage()]
        self.pbar = ProgressBar(maxval=self.tf - self.t0, widgets=widgets,
                                fd=sys.stdout)
        self.pbar.start()

    def progress(self):
        """
        Print simulation progress in terms of current time and percentage of
        final time using a progress bar
        """
        debug = False
        if debug:
            percent = int(100*(self.t - self.t0)/(self.tf - self.t0))
            print("ite = {:>8} | {:>3}% | t = {:.10e} | dt = {:.16e}" \
                  .format(self.ite, percent, self.t, self.dt))
        else:
            try:
                self.pbar.update(self.t - self.t0)
            except ValueError:
                pass

    def iterate(self):
        """Returns a boolean to continue or stop the time iteration"""
        if self.maxite:
            do_iter = (self.t < (self.tf - self.dt/2.)) and \
                   (self.ite < self.maxite) and (not self.void)
        else:
            do_iter = (self.t < (self.tf - self.dt/2.)) and (not self.void)

        if not do_iter:  # Ending sequence
            if self.pbar:
                # shift down if progress bar is still active
                prefix = self.term.move_down
            else:
                prefix = ""
            print(prefix + "Finally     >>> ite {:} - t = {:.3e}".format(
                self.ite, self.t))
        return do_iter

    def plot_sol(self, really_do=False, show=False, miniplot=False):
        """Plot solution if required"""
        if self.verbose > 1:
            if really_do:
                # Store solution if explicitely asked
                do_plot = True
            elif (miniplot and self.miniplot):
                do_plot = True
            else:
                # Store solution if time is a multiple of dt_plot
                do_plot = (self.dt_plot > 0.) \
                           and (self.t % self.dt_plot < self.dt)

            if do_plot:
                title = "Case: {} - ite {} - t = {:.3e}".format(self.casetype,
                                                                self.ite,
                                                                self.t)
                self.model.plot(title, show)
